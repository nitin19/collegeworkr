<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{   
    protected $table = 'chathistory';

    protected $fillable = [
        'sender_id', 'receiver_id', 'message', 'hired_id', 'job_id', 'created_at', 'updated_at', 'status', 'deleted', 'is_read'
    ];

    protected $hidden = [
        
    ]; 

    

}
