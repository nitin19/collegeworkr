<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Clientjobpost extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
        protected $fillable = ['user_id','job_heading', 'job_description', 'job_location' , 'worker_required','workertype','worker_desc','equipment_needed' , 'status' , 'deleted','created_by','latitude','longitude'];


}
