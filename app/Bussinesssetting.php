<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bussinesssetting extends Model
{
     public $fillable = ['phone','emails','facebook_url','twitter_url','google_url','linkedin_url'];
}