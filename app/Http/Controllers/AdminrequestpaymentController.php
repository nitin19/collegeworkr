<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Home;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;

class AdminrequestpaymentController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        /*$workerpayment_info = DB::table('workerpayment')
                    ->get();*/
      try{   
        /*$workerpayment_info = DB::table('hire')
                                  ->where('job_status','completed')
                                  ->where('status','3')
                                  ->paginate(2); */

         $workerpayment_info = DB::table('payments_succeed')  
                              ->join('clientjobpost', 'clientjobpost.id','payments_succeed.job_id')
                              ->join('categories', 'categories.id', 'clientjobpost.job_heading')
                              ->join('users', 'users.id', 'payments_succeed.worker_id') 
                              ->join('hire', 'hire.id', 'payments_succeed.hire_id')
                              ->where('payments_succeed.worker_payment_status','=','0')
                              ->get();    

                              //echo'<pre>';
                              //print_r($workerpayment_info);                  
                              //echo'</pre>';                  
          return view('admin.requestpayment.index', compact('workerpayment_info'));
          //  ->with('i', ($request->input('page', 1) - 1) * 4);
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
      }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
      }
    }
    public function show($id) {
    }
    public function edit($id) {             
    }
  
    public function update(Request $request) {
      try{
       $id = $request->id;
       $transactionid = $request->transactionid;
       $job_amount = $request->job_amount;
       $job_client_id = $request->job_client_id;
       $job_id = $request->job_id;
       
       $update = DB::table('workerpayment')
            ->where('id', $id)
            ->update(['payment_status' => 'approved', 'transaction_id' => $transactionid, 'amount' => $job_amount]);

       $update = DB::table('client_payment')
            ->where('user_id', $job_client_id)
            ->where('client_jobid', $job_id)
            ->update(['payment_status' => 'approved']);

       return redirect()->action('AdminrequestpaymentController@index')->with('success','Updated Successfully');  
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
      }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
      }
}

public function reject_payment_update(Request $request) {
  try{
       $worker_payment_id = $request->worker_payment_id;
       $client_user_id = $request->client_user_id;
       $worker_id = $request->worker_id;
       $job_id = $request->job_id;
       $jobtitle = $request->jobtitle;
       
       /*$update = DB::table('workerpayment')
            ->where('id', $worker_payment_id)
            ->update(['payment_status' => 'decline']);

       $update = DB::table('client_payment')
            ->where('user_id', $client_user_id)
            ->where('client_jobid', $job_id)
            ->update(['payment_status' => 'decline']); */
        $update = DB::table('payments_succeed')
            ->where('payment_id', $worker_payment_id)
            ->update(['worker_payment_status' => 2 ]);

            if( $update ){
              echo'true';
              //========================// 
                $created_at  = date('Y-m-d H:m:i'); 
                $message = 'Admin Rejected Your payment for <strong>'. $jobtitle .'</strong>.';
                $hireworker_notification = DB::table('new_notifications')
                      ->insertGetId([ 'worker_id' =>$worker_id, 'message' =>$message, 'message_worker' =>$message, 'created_at' => $created_at, 'notification_redirect_url' => '/workerpayment' ]); 
              //========================//

            }else{
              echo'false';
            }

       //return redirect()->action('AdminrequestpaymentController@index')->with('success','Updated Successfully');
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
      }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
      }  
}

//=================================//
public function updateWorkerPayment(Request $request){
  $transactionid = $request->transactionid;
  $payment_id = $request->payment_id;
  $workerid = $request->workerid;
  $jobtitle = $request->jobtitle;
  $update = DB::table('payments_succeed')
            ->where('payment_id', $payment_id)
            ->update(['worker_payment_status' => 1, 'worker_payment_transaction_id' =>  $transactionid, 'worker_payment_date' => date('Y-m-d h:i:s') ]);

            if( $update ){
              echo'true';
              //========================// 
                $created_at  = date('Y-m-d H:m:i'); 
                $message = 'Admin Approved Your payment for <strong>'. $jobtitle .'</strong>.';
                $hireworker_notification = DB::table('new_notifications')
                      ->insertGetId([ 'worker_id' =>$workerid, 'message' =>$message, 'message_worker' =>$message, 'created_at' => $created_at, 'notification_redirect_url' => '/workerpayment' ]); 
              //========================//
            }else{
              echo'false';
            } 

}
//=================================//
}
?>