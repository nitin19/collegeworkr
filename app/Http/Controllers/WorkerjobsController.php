<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use Illuminate\Pagination\Paginator;

class WorkerjobsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
         $show =  $request->show; 
                    if($show!=''){
                    $pagination = 1*$show;                                       
                    }
                    else {
                      $pagination = 5;   
                    }
         $user_id = Auth::user()->id;

         $user_lat = Auth::user()->latitude;
         $user_long = Auth::user()->longitude;

         $results_per_page = 2;




         //$clientjobpost  = DB::table('clientjobpost')->get();

          //foreach ($clientjobpost as $clientjobposts) {
              //$circle_radius = '3959';
              //$max_distance = '50';
              //$job_latitude = $clientjobposts->latitude;
              //$job_longitude = $clientjobposts->longitude;



/*              $haversine = "(3959 * acos(cos(radians(" . $user_lat . ")) 
                    * cos(radians(`latitude`)) 
                    * cos(radians(`longitude`) 
                    - radians(" . $user_long . ")) 
                    + sin(radians(" . $user_lat . ")) 
                    * sin(radians(`latitude`))))";

              $reserved_data = "(Select client_jobid from reservedjobs where worker_userid =".$user_id.")";
                //print_r($reserved_data);
            //exit();
              $clientjobpost = DB::table('clientjobpost')
                    ->select('id, user_id, created_at, start_date, time_estimate, job_location, job_description, job_heading, latitude, longitude')
                    ->selectRaw("{$haversine} AS distance")
                    ->selectRaw("{$haversine} AS distances")
                    ->whereRaw('distance < 50')
                    ->whereNOTIn('ID',function($query){$query->select('client_jobid')->from('reservedjobs');})
                    ->orderBy('distance','DESC')
                    ->get();

              print_r($clientjobpost);
              exit();
*/


            $clientjobpost = DB::select('SELECT * FROM( SELECT id, user_id, created_at, start_date, time_estimate, job_location, job_description, job_heading, latitude, longitude, ("3959" * acos(cos(radians(' . $user_lat . ')) * cos(radians(latitude)) * cos(radians(longitude) - radians(' . $user_long . ')) + sin(radians(' . $user_lat . ')) * sin(radians(latitude))))
                    AS distance
                    FROM clientjobpost WHERE status !=0
                    ) AS distances
                    WHERE distance < "50" and ID NOT IN (Select client_jobid from reservedjobs where worker_userid ='.$user_id.')
                    ORDER BY distance'
            );
            
            $client_pagination = new Paginator($clientjobpost, $results_per_page);
           // $pagination = Paginator::make($clientjobpost, count($clientjobpost), $results_per_page);

        
            $appliedjobpost = DB::select('SELECT * FROM(SELECT id, user_id, created_at, start_date, time_estimate, job_location, job_description, job_heading, latitude, longitude, ("3959" * acos(cos(radians(' . $user_lat . ')) * cos(radians(latitude)) * cos(radians(longitude) - radians(' . $user_long . ')) + sin(radians(' . $user_lat . ')) * sin(radians(latitude))))
                    AS distance
                    FROM clientjobpost
                    ) AS distances
                    WHERE distance < "50" and ID IN (Select client_jobid from reservedjobs where worker_userid ='.$user_id.')
                    ORDER BY distance'
            );
             $admin_id = Auth::user()->id;
             $hired_info = DB::table('hire')
                        ->join('clientjobpost', 'clientjobpost.id', '=', 'hire.client_job_id')
                        ->join('categories', 'categories.id', '=', 'clientjobpost.job_heading')
                        ->join('users', 'users.id', '=', 'hire.worker_id')
                        //->select('hire.id as hire_id','categories.*', 'users.*') 
                        ->select('hire.id as hireid', 'hire.proposal_id as proposal_id', 'hire.client_job_id as client_job_id', 'hire.worker_id as worker_id', 'hire.job_status as job_status', 'hire.status as status', 'hire.deleted as deleted', 'hire.created_at as created_at', 'hire.updated_at as updated_at', 'hire.hours_to_complete as hours_to_complete', 'hire.mints_to_complete as mints_to_complete', 'hire.updated_hours as updated_hours', 'hire.updated_mints as updated_mints', 'hire.time_approved as time_approved', 'hire.job_summary as job_summary', 'hire.future_appointments as future_appointments', 'hire.private_notes as private_notes', 'clientjobpost.*', 'categories.*', 'users.*' )
                        ->where(['hire.worker_id'=>$admin_id,'hire.status'=> '2' , 'hire.deleted'=> '0']) 
                        ->orderBy('hire.id', 'desc')
                        ->get();
            //$applied_pagination = new Paginator($clientjobpost, $results_per_page);
            //echo'<pre>';
            //print_r( $hired_info );             
            //echo'</pre>';             

            $acceptjobpost = DB::select('SELECT * FROM(SELECT id, user_id, created_at, start_date, time_estimate, job_location, job_description, job_heading, latitude, longitude, ("3959" * acos(cos(radians(' . $user_lat . ')) * cos(radians(latitude)) * cos(radians(longitude) - radians(' . $user_long . ')) + sin(radians(' . $user_lat . ')) * sin(radians(latitude))))
                    AS distance
                    FROM clientjobpost 
                    ) AS distances
                    WHERE distance < "50" and ID IN (Select client_job_id from hire where worker_id = '.$user_id.' and status = "2" and job_status = "hired")
                    ORDER BY distance'
            );

            $acceptjobpost = DB::table('hire')
                            ->where('worker_id',$user_id)
                            ->where('status',1)
                            ->where('deleted',0)
                            ->get();

            //$accept_pagination = new Paginator($clientjobpost, $results_per_page);

            $inprogressjobpost = DB::select('SELECT * FROM(SELECT id, user_id, created_at, start_date, time_estimate, job_location, job_description, job_heading, latitude, longitude, ("3959" * acos(cos(radians(' . $user_lat . ')) * cos(radians(latitude)) * cos(radians(longitude) - radians(' . $user_long . ')) + sin(radians(' . $user_lat . ')) * sin(radians(latitude))))
                    AS distance
                    FROM clientjobpost 
                    ) AS distances
                    WHERE distance < "50" and ID IN (Select client_job_id from hire where worker_id = '.$user_id.' and status = "2" and job_status = "hired")
                    ORDER BY distance'
            );
            //$inprogress_pagination = new Paginator($clientjobpost, $results_per_page);

            /* $completejobpost = DB::select('SELECT * FROM(SELECT id, user_id, created_at, start_date, time_estimate, job_location, job_description, job_heading, latitude, longitude, ("3959" * acos(cos(radians(' . $user_lat . ')) * cos(radians(latitude)) * cos(radians(longitude) - radians(' . $user_long . ')) + sin(radians(' . $user_lat . ')) * sin(radians(latitude))))
                AS distance
                FROM clientjobpost 
                ) AS distances
                WHERE distance < "50" and ID IN (Select client_job_id from hire where worker_id = '.$user_id.' and status = "3" and job_status = "completed")
                ORDER BY distance'
            ); */
            //$complete_pagination = new Paginator($clientjobpost, $results_per_page);
            $completejobpost = DB::table('hire')
                        ->join('clientjobpost', 'clientjobpost.id', '=', 'hire.client_job_id')
                        ->join('categories', 'categories.id', '=', 'clientjobpost.job_heading')
                        ->join('users', 'users.id', '=', 'clientjobpost.user_id')
                        //->select('hire.id as hire_id','categories.*', 'users.*') 
                        ->select('hire.id as hireid', 'hire.proposal_id as proposal_id', 'hire.client_job_id as client_job_id', 'hire.worker_id as worker_id', 'hire.job_status as job_status', 'hire.status as status', 'hire.deleted as deleted', 'hire.created_at as created_at', 'hire.updated_at as updated_at', 'hire.hours_to_complete as hours_to_complete', 'hire.mints_to_complete as mints_to_complete', 'hire.updated_hours as updated_hours', 'hire.updated_mints as updated_mints', 'hire.time_approved as time_approved', 'hire.job_summary as job_summary', 'hire.future_appointments as future_appointments', 'hire.private_notes as private_notes', 'clientjobpost.*', 'categories.*', 'users.*' )
                        ->where(['hire.worker_id'=>$admin_id,'hire.job_status'=> 'completed' , 'hire.deleted'=> '0']) 
                        ->orderBy('hire.id', 'desc')
                        ->get();
              //echo'<pre>';
              //print_r($completejobpost);  
              //echo'</pre>';
            

            $cancelledjobpost = DB::select('SELECT * FROM(SELECT id, user_id, created_at, start_date, time_estimate, job_location, job_description, job_heading, latitude, longitude, ("3959" * acos(cos(radians(' . $user_lat . ')) * cos(radians(latitude)) * cos(radians(longitude) - radians(' . $user_long . ')) + sin(radians(' . $user_lat . ')) * sin(radians(latitude))))
                AS distance
                FROM clientjobpost 
                ) AS distances
                WHERE distance < "50" and ID IN (Select client_job_id from hire where worker_id = '.$user_id.' and status = "4" and job_status = "cancelled")
                ORDER BY distance'
            );
            //$cancelled_pagination = new Paginator($clientjobpost, $results_per_page);

            
      /*  $clientjobpost  = DB::table('clientjobpost')
                          ->where('user_id', '!=', $id)
                          ->where(['status' => '1','deleted' => '0'])
                          ->orderBy('id', 'DESC')
                          ->paginate($pagination);*/
                         //$get_open_Jobs =  DB::select('EXEC get_Open_Jobs');



/*          $clientjobpost  = DB::table('clientjobpost')->get();

          foreach ($clientjobpost as $clientjobposts) {
              //$circle_radius = '3959';
              $max_distance = '50';
              $job_latitude = $clientjobposts->latitude;
              $job_longitude = $clientjobposts->longitude;
              $haversine = "(3959 * acos(cos(radians(" . $job_latitude['latitude'] . ")) 
                    * cos(radians(`latitude`)) 
                    * cos(radians(`longitude`) 
                    - radians(" . $job_longitude['longitude'] . ")) 
                    + sin(radians(" . $job_latitude['latitude'] . ")) 
                    * sin(radians(`latitude`))))";
              $clientjobposts = DB::table('clientjobpost')
                    ->select('id', 'users_id', 'cities_id')
                    ->selectRaw("{$haversine} AS distance")
                    ->whereRaw("{$haversine} < ", $max_distance);               
           }*/
          
                        /*$clientjobpost = DB::select(
                            'call get_Open_Jobs('.$user_id.')'
                        );



                        $appliedjobpost = DB::select(
                            'call get_applied_jobs('.$user_id.')'
                        );
                        $acceptjobpost  = DB::select(
                            'call get_accepted_jobs('.$user_id.')'
                        );
                        $inprogressjobpost  = DB::select(
                            'call get_inprogress_jobs('.$user_id.')'
                        );
                        $completejobpost = DB::select(
                            'call get_completed_jobs('.$user_id.')'
                        );
                        $cancelledjobpost = DB::select(
                            'call get_cancelled_jobs('.$user_id.')'
                        );
*/

          return view('workerjobs.index',compact('clientjobpost','client_pagination','appliedjobpost','acceptjobpost','inprogressjobpost','completejobpost','cancelledjobpost','show','pagination','hired_info'))
                  ->with('i', ($request->input('page', 1) - 1) * 10);
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
        catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }

     public function show($id)
    {
        try{
            $job_id              =  $id; 
            $worker_id           = Auth::user()->id;
            $worker_chat_listing = DB::table('hire')
                                   ->where(['client_job_id'=>$job_id,'status'=>'2','deleted' =>'0' ,'worker_id'=>$worker_id])
                                   ->first();  
            $job_listing         = DB::table('clientjobpost')
                                   ->where(['id'=>$job_id,'status'=>'1','deleted' =>'0'])
                                   ->first();

            return view('workerjobs.show',compact('job_listing','worker_chat_listing'));
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
        catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }

     public function update(Request $request,$id)
    {    
        try{
         $worker_userid = Auth::user()->id;
         $user_type = Auth::user()->user_role;
         $worker_first_name  = Auth::user()->firstname;
         $worker_last_name  = Auth::user()->lastname;
         $client_userid         = $request->client_userid;
         $client_jobid          = $request->client_jobid;
         $created_at            = date('Y-m-d H:m:i');
         $updated_at            = date('Y-m-d H:m:i');
         $created_by            = $worker_userid;
         $updated_by            = $worker_userid;
         $job_status            = $request->job_status;

          $job_data =DB::table('clientjobpost')
                         ->where('id','=',$client_jobid)
                         ->first();
          $cat_id = $job_data->job_heading;
          $cat_data =DB::table('categories')
                         ->where('id','=',$cat_id)
                         ->first();

          $message ='';
          $message = $worker_first_name . 'has Applied for this job' . $cat_data->cat_name;

         $bnk= DB::table('reservedjobs')
             ->insertGetId(['job_status' =>$job_status,'worker_userid' =>$worker_userid ,'client_userid' => $client_userid,'client_jobid' => $client_jobid,'created_at' => $created_at,'updated_at' => $updated_at,'created_by' => $created_by,'updated_by' => $updated_by]);
              
         $bnk= DB::table('notification')
             ->insertGetId(['job_status' =>$job_status,'worker_userid' =>$worker_userid ,'client_userid' => $client_userid,'client_jobid' => $client_jobid,'message' => $message,'created_at' => $created_at,'updated_at' => $updated_at,'created_by' => $created_by,'updated_by' => $updated_by]);
         return redirect()->route('workerjobs.index');
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
        catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function complete_btn_status_update(Request $request)
    {    
        $complete_status = $request->completed_btn_status;
        $worker_id = $request->user_id;
        $client_job_id = $request->client_job_id;
        $update_status = DB::table('hire')
                            ->where('worker_id','=',$worker_id)
                            ->where('client_job_id','=',$client_job_id)
                            ->update(['status' => $complete_status,'job_status' => 'completed']);
         if($update_status){
            echo 'true';
         } else {
            echo 'false';
         }
    }
    public function cancelled_btn_status_update(Request $request)
    {    
        $cancelled_status = $request->cancelled_btn_status;
        $worker_id = $request->user_id;
        $client_job_id = $request->client_job_id;
        $update_status = DB::table('hire')
                            ->where('worker_id','=',$worker_id)
                            ->where('client_job_id','=',$client_job_id)
                            ->update(['status' => $cancelled_status,'job_status' => 'cancelled']);
         if($update_status){
            echo 'true';
         } else {
            echo 'false';
         }
    }
    public function worker_feedback(Request $request)
    {   
        try{ 
        $rating         = $request->rating;
        $feedback_text  = $request->feedback_text;
        $client_user_id = $request->client_user_id;
        $worker_user_id = $request->worker_user_id;
        $rev_id         = $request->rev_id;
        $cat_id         = $request->cat_id;
        $created_at     = date('Y-m-d H:m:i');
        $created_by     = $request->worker_user_id;
        $updated_at     = date('Y-m-d H:m:i');
        $updated_by     = $request->worker_user_id;

        $bnk            = DB::table('feedback')
                          ->insertGetId(['cat_id' =>$cat_id,'res_jobid' =>$rev_id,'user_id' =>$worker_user_id,'feedback_rating'=>$rating,'feedback_description' =>$feedback_text,'status' =>'1', 'deleted' =>'0','created_by' => $created_by,'created_at' => $created_at,'updated_by' => $updated_by,'updated_at' => $updated_at]);
        return redirect()->route('workerjobs.index'); 
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
        catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    //======================================//
    public function updateWorkedHour(Request $request){
        //echo'working';
         $idToUpdate = $request->idToUpdate;
         $mints = $request->mints;
         $hours = $request->hours;
         $clientjobid = $request->clientjobid;
         $jobauthor = $request->jobauthor;
         $jobheading = $request->jobheading;
         $job_summary = $request->job_summary;
         $future_appointement = $request->future_appointement;
         $private_notes = $request->private_notes;
		
		 $update_status = DB::table('hire')
                            ->where('id','=',$idToUpdate)
                            ->update(['hours_to_complete' => $hours,'mints_to_complete' => $mints, 'job_summary' => $job_summary ,'future_appointments' => $future_appointement, 'private_notes' => $private_notes ]);
         if($update_status){
            echo 'true';
            //============== USER CREATE NOTIFICATION TO ADMIN 07-02-2019 =================//
              $created_at = date('Y-m-d H:m:i');
              $jobCategoryData = DB::table('categories')->where('id',$jobheading)->first();
              $jobTitle = $jobCategoryData->cat_name;
              
              $usernametodisplay = Auth::user()->name;

              $__message = '"<strong>'. $usernametodisplay .'</strong>" updated the time for "<strong>'. $jobTitle .'</strong>"';
              //$redirectURL = 'hourdetails?hireid='. urlencode(base64_encode($idToUpdate)); 
              $redirectURL = 'hourdetails?hireid='. $idToUpdate; 
              $usercreate_notification = DB::table('new_notifications')
                      ->insertGetId([ 'message' => $__message, 'created_at' =>$created_at, 'is_for_admin' =>0, 'read_by_admin' => 0, 'client_id' => $jobauthor, 'notification_redirect_url' => $redirectURL ]);
              //============================================================================//
         } else {
            echo 'false';
         } 
    }

    

}
