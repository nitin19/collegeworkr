<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Image;
use Validator;
use DB;
use Hash;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use App\Http\Controllers\Redirect;

class WorkerdashboardController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }

     public function index(Request $request)

    {  
        try{
        $id                   = Auth::user()->id;
        $userworker           = DB::table('users')
                                ->where('id','=', $id)
                                ->where('status', '=', 1)
                                ->where('deleted', '=', 0)
                                ->first();                    
        $worker_qualification = DB::table('worker_qualification')
                                ->where('status', '=', 1)
                                ->where('deleted', '=', 0)
                                ->orderBy('id')
                                ->get();
        $worker_attend        = DB::table('worker_attend')
                                ->where('status', '=', 1)
                                ->where('deleted', '=', 0)
                                ->orderBy('id')
                                ->get();
        $state                = DB::table('state')
                                ->where('status', '=', 1)
                                ->where('deleted', '=', 0)
                                ->orderBy('id')
                                ->get();
        $worker_college       = DB::table('worker_college')
                                ->where('status', '=', 1)
                                ->where('deleted', '=', 0)
                                ->orderBy('id')
                                ->get();
        $categories           = DB::table('categories')
                                ->where('status', '=', 1)
                                ->where('deleted', '=', 0)
                                ->get();
        $equipment            = DB::table('equipment')
                                ->where('status', '=', 1)
                                ->where('deleted', '=', 0)
                                ->get();
        $feedback             = DB::table('feedback')
                                ->where('user_id',$id)
                                ->where('status', '=', 1)
                                ->where('deleted', '=', 0)
                                ->sum('feedback_rating');
        $feedback_count       = DB::table('feedback')
                                ->where('user_id',$id)
                                ->where('status', '=', 1)
                                ->where('deleted', '=', 0)
                                ->count('feedback_rating');
        $customer_feedback_count = DB::table('feedback')
                              ->where('user_id',$id)
                              ->where('status', '=', 1)
                              ->where('deleted', '=', 0)
                              ->count('user_id');
        $revjob_feedback_count = DB::table('feedback')
                            ->where('user_id',$id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->count('res_jobid');
        $workerjobpreference = DB::table('workerjobpreference')
                            ->where('user_id',$id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->first();

 
        return view('workerprofile.index',compact('userworker','worker_qualification','worker_attend','state','worker_college','categories','equipment','feedback','feedback_count','customer_feedback_count','revjob_feedback_count', 'workerjobpreference'));
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
        catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    
     public function create()

    {


    }
    
    public function store(Request $request)

    {
        try{
        DB::enableQueryLog();
        $id = Auth::user()->id;
        $worker_slogan                 = $request->worker_slogan;
        $worker_about                  = $request->worker_about;
        $worker_attend                 = $request->worker_attend;
        $worker_school                 = $request->worker_school;
        $worker_qualification          = $request->worker_qualification;
        $dob                           = $request->dob;
        $phone                         = $request->phone;
        $gender                        = $request->gender;
        $worker_crime                  = $request->worker_crime;
        $address                       = $request->address;
        $address1                      = $request->address1;
        $city                          = $request->city;
        $state                         = $request->state;
        $worker_college                = $request->worker_college;
        $zip_code                      = $request->zip_code;
        
        $full_address = $address.','.$address1.','.$city.','.$state;
        $prepAddr = str_replace(' ','+',$full_address);
        $geocode=file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address='".$prepAddr."'&key=AIzaSyAmECi5THi1W3CHDdmC2bI6q6dDZNRAY-I");
            $output= json_decode($geocode);

        $lat = $output->results[0]->geometry->location->lat;
        $long = $output->results[0]->geometry->location->lng;

        $upQry = DB::table('users')
                ->where('id', $id)
                ->update(['worker_slogan' => $worker_slogan, 'worker_about' => $worker_about, 'worker_attend' => $worker_attend, 'worker_school' => $worker_school, 'worker_qualification' => $worker_qualification,'dob' => $dob,'phone' => $phone, 'gender' => $gender,'worker_crime'=> $worker_crime,'address' => $address,'address1' => $address1,'city' => $city,'state' => $state,'worker_college'=> $worker_college, 'latitude'=> $lat, 'longitude'=> $long, 'zip_code' => $zip_code]);

         if($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalName();
            $image->move(public_path('uploads/profile/'), $name);
            $upImgQry = DB::table('users')
            ->where('id', $id)
            ->update(['image' => $name]);
        }
          return redirect()->route('workerprofile.index')

                         ->with('success','profile updated successfully');
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
        catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }

    }
    
    public function show($id)

    {

       

    }
    
   public function edit($id)

    {


    }
    
   public function jobpreference(Request $request){

         $location               = $request->location;
         $prferloc               = implode(',', $location);
         $user_id                = Auth::user()->id;
         $status                 = '1';
         $deleted                = '0';
         $created_by             = $user_id;
         $created_at             = date('Y-m-d h:i:s');


         $workerjobpreference = DB::table('workerjobpreference')
                            ->where('user_id',$user_id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->first();
        if( !empty($workerjobpreference) ):
            $update_status = DB::table('workerjobpreference')
                            ->where('user_id','=',$user_id)
                            ->update(['location' => $prferloc ,'updated_at' => $created_at  ]);

        else:
            $location = DB::table('workerjobpreference')->insertGetId(['user_id' =>$user_id,'location' => $prferloc, 'status' => $status, 'deleted' => $deleted ,'created_by' => $created_by ,'created_at' => $created_at]);
        endif;    

          
            if($location){
                echo 'true';

            }else{
                echo "false";
            } 
    }

    public function worker_profile_update(Request $request, $id){
        try{
         $firstname              = $request->firstname;
         $lastname               = $request->lastname;
         $phone                  = $request->phone;
         $address                = $request->address;
         $user_id                = Auth::user()->id;
         $updated_by             = $user_id;
         $updated_at             = date('Y-m-d H:m:i');

          $full_address = $address; // Google HQ
            //$address = 'NJ 07094,USA'; // Google HQ
            $prepAddr = str_replace(' ','+',$full_address);
            $geocode=file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address='".$prepAddr."'&key=AIzaSyAmECi5THi1W3CHDdmC2bI6q6dDZNRAY-I");
            $output= json_decode($geocode);

            $lat = $output->results[0]->geometry->location->lat;
            $long = $output->results[0]->geometry->location->lng;

         $exp_id = DB::table('users')
                        ->where('id', $id)
                        ->update(['firstname' => $firstname, 'lastname' => $lastname, 'phone' => $phone, 'updated_by' => $updated_by, 'address' => $address, 'latitude' => $lat, 'longitude' => $long, 'updated_at' => $updated_at]);

         if($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalName();
            $image->move(public_path('uploads/profile/'), $name);
            $upImgQry = DB::table('users')
            ->where('id', $id)
            ->update(['image' => $name]);
         }

        return redirect()->route('workerprofile.index')
            ->with('success','profile updated successfully');
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
        catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }

   public function workerexperience(Request $request){
    try{     
         $user_id                = $request->user_id;
         $cat_id                 = $request->catid;
         $experience             = $request->experience;
         $additional_info        = $request->additionalinfo;
         $status                 = '1';
         $deleted                = '0';
         $created_by             = $user_id;
         $created_at             = date('Y-m-d H:m:i');
         $updated_by			 = $user_id;
         $updated_at			 = date('Y-m-d H:m:i');	

         $worker_experience = DB::table('worker_categories_exprience')
                            ->where('user_id', '=', $user_id)
                            ->where('cat_id', '=', $cat_id)
                            ->first();
          if($worker_experience !=''){

          	$exp_id = DB::table('worker_categories_exprience')
                		->where('id', $worker_experience->id)
                		->update(['experience' => $experience,'additional_info' => $additional_info,'updated_by' => $updated_by ,'updated_at' => $updated_at]);


          }else{

         $exp_id = DB::table('worker_categories_exprience')
         		  ->insertGetId(['user_id' =>$user_id,'cat_id' => $cat_id,'experience' => $experience,'additional_info' => $additional_info, 'status' => $status, 'deleted' => $deleted ,'created_by' => $created_by ,'created_at' => $created_at]);
     	}
        if($exp_id){
            echo $exp_id;

        }else{
            echo "false";
        }
    }catch(\Illuminate\Database\QueryException $e){
        $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);

        }catch(\Exception $e){
            $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));

            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }
    }

    public function updtworkexp(Request $request){
      
      try{
         $user_id                = $request->user_id;
         $cat_id                 = $request->catid;
         $status                 = $request->status;
         $deleted                = $request->deleted;
         $updated_by			 = $user_id;
         $updated_at			 = date('Y-m-d H:m:i');	

         $exp_id = DB::table('worker_categories_exprience')
                		->where(['user_id' => $user_id,'cat_id' => $cat_id])
                		->update(['status' => $status,'deleted' => $deleted,'updated_by' => $updated_by ,'updated_at' => $updated_at]);
       
        if($exp_id){
            echo $exp_id;

        }else{
            echo "false";
        }
    }catch(\Illuminate\Database\QueryException $e){
        $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);

        }catch(\Exception $e){
            $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));

            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }
  }

   public function workerequipment(Request $request)
   {

         $user_id                = $request->user_id;
         $equ_id                 = $request->equ_id;
         $equlinfo               = $request->equlinfo;
         $status                 = '1';
         $deleted                = '0';
         $created_by             = $user_id;
         $created_at             = date('Y-m-d H:m:i');
         $updated_by             = $user_id;
         $updated_at             = date('Y-m-d H:m:i'); 

         $worker_equipment = DB::table('worker_equipment')
                            ->where('user_id', '=', $user_id)
                            ->where('equ_id', '=', $equ_id)
                            ->first();
            if(count($worker_equipment)> 0){

            $equid = DB::table('worker_equipment')
                        ->where('id', $worker_equipment->id)
                        ->update(['description' => $equlinfo,'updated_by' => $updated_by ,'updated_at' => $updated_at]);
          }else{

         $equid = DB::table('worker_equipment')
                  ->insertGetId(['user_id' =>$user_id,'equ_id' => $equ_id,'description' => $equlinfo,'status' => $status, 'deleted' => $deleted ,'created_by' => $created_by ,'created_at' => $created_at]);
        }

        if($equid){
            echo $equid;

        }else{
            echo "false";
        }
    
    }

    public function updtworkequ(Request $request)
    {
         $user_id                = $request->user_id;
         $equ_id                 = $request->equ_id;
         $status                 = $request->status;
         $deleted                = $request->deleted;
         $updated_by             = $user_id;
         $updated_at             = date('Y-m-d H:m:i'); 

         $equid = DB::table('worker_equipment')
                        ->where(['user_id' => $user_id,'equ_id' => $equ_id])
                        ->update(['status' => $status,'deleted' => $deleted,'updated_by' => $updated_by ,'updated_at' => $updated_at]);
       
        if($equid){
            echo $equid;

        }else{
            echo "false";
        }
   
  }

}
