<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Home;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;

class AdminupdateController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

    }   
     public function customer(Request $request)
     {
      
      try{
       $firstname = $request->firstname;
       $lastname = $request->lastname;
       $password = $request->password;
       $email = $request->email;
       $password = $request->password;
       $address = $request->address;
       $phone = $request->phone;
       $address1 = $request->address1;
       $city = $request->city; 
       $state = $request->state;  
       $id = $request->id;   
       if($password=='')  {
       $update = DB::table('users')
            ->where('id', $id)
            ->update(['firstname' => $firstname, 'lastname' => $lastname, 'email' => $email, 'phone' => $phone, 'address' => $address, 'address1' => $address1, 'city' => $city, 'state' => $state]);
       }
       else {
       $password_md5 = md5($password);
       $update = DB::table('users')
            ->where('id', $id)
            ->update(['firstname' => $firstname, 'lastname' => $lastname, 'email' => $email, 'password' => $password, 'hdpswd' => $password_md5, 'phone' => $phone, 'address' => $address, 'address1' => $address1, 'city' => $city, 'state' => $state]);
       }
        if ($request->hasFile('input_img')) {
        if($request->file('input_img')->isValid()) {
            $file = $request->file('input_img');
            $image_name = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = '/uploads/profile/';
            $file->move(public_path(). $destinationPath, $image_name);            
            $update_image = DB::table('users')
            ->where('id', $id)
            ->update([ 'image' => $image_name]); 
        } 
        }
       return redirect()->action('AdmincustomerController@edit',$id)->with('success','Updated Successfully');

      }catch(\Illuminate\Database\QueryException $e){
         $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
        
        // $errorClass = new ErrorClass();
       
        // $errors = $errorClass->saveErrors($e);

        }catch(\Exception $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));

            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }
    }
  public function worker(Request $request)
     {
      
      try{
       $firstname = $request->firstname;
       $lastname = $request->lastname;
       $password = $request->password;
       $email = $request->email;
       $address = $request->address;
       $address1 = $request->address1;
       $phone = $request->phone;
       $city = $request->city; 
       $state = $request->state;  
       $id = $request->id; 
       if($password=='')  {
       $update = DB::table('users')
            ->where('id', $id)
            ->update(['firstname' => $firstname, 'lastname' => $lastname, 'email' => $email, 'phone' => $phone, 'address' => $address, 'address1' => $address1, 'city' => $city, 'state' => $state]);
       }
       else {
       $password_md5 = md5($password);
       $update = DB::table('users')
            ->where('id', $id)
            ->update(['firstname' => $firstname, 'lastname' => $lastname, 'email' => $email, 'phone' => $phone, 'password' => $password, 'hdpswd' => $password_md5, 'address' => $address, 'address1' => $address1, 'city' => $city, 'state' => $state]);
       }  
        if ($request->hasFile('input_img')) {
        if($request->file('input_img')->isValid()) {
            $file = $request->file('input_img');
            $image_name = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = '/uploads/profile/';
            $file->move(public_path(). $destinationPath, $image_name);            
            $update_image = DB::table('users')
            ->where('id', $id)
            ->update([ 'image' => $image_name]); 
        } 
        }         
       return redirect()->action('AdminworkerController@edit',$id)->with('success','Updated Successfully');
    }catch(\Illuminate\Database\QueryException $e){
      $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
        
        // $errorClass = new ErrorClass();
       
        // $errors = $errorClass->saveErrors($e);

        }catch(\Exception $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));

            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }
    }
}

?>