<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Home;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;

class AdminjobsController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        try{
            $search = $request->search; 
            $pagination = 10;
            if($search=='') {
            $jobs_count = DB::table('categories')
                      ->join('clientjobpost', 'clientjobpost.job_heading', '=', 'categories.id')
                      ->where('clientjobpost.status', '1')
                      ->where('clientjobpost.deleted', '0')
                      ->count();
            $jobs_info = DB::table('categories')
                     ->join('clientjobpost', 'clientjobpost.job_heading', '=', 'categories.id')
                     ->where('clientjobpost.status', '1')
                     ->where('clientjobpost.deleted', '0')
                     ->orderBy('clientjobpost.id', 'desc')  
                     ->paginate($pagination);
            }
            else {
            $jobs_count = DB::table('clientjobpost')
                    ->join('categories', 'clientjobpost.job_heading', '=', 'categories.id')
                    ->orWhere('clientjobpost.job_description', 'like', '%'.$search.'%')
                    ->orWhere('categories.cat_name', 'like', '%'.$search.'%')
                    ->where('clientjobpost.status', '1')
                    ->where('clientjobpost.deleted', '0')
                    ->count();
            $jobs_info = DB::table('clientjobpost')
                        ->orWhere('clientjobpost.job_description', 'like', '%'.$search.'%')
                        ->orWhere('categories.cat_name', 'like', '%'.$search.'%')
                        ->where('status', '1')
                        ->where('deleted', '0')
                        ->orderBy('id', 'desc')  
                        ->paginate($pagination);   
            }
            return view('admin.jobs.index', compact('jobs_info', 'jobs_count'))
                 ->with('i', ($request->input('page', 1) - 1) * 4);
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }

    }
    public function show($id) {
        try{
            $pagination = 10;
            $jobs_count = DB::table('clientjobpost')
                      ->join('categories', 'clientjobpost.job_heading', '=', 'categories.id')
                      ->where('clientjobpost.status', '1')
                      ->where('clientjobpost.deleted', '0')
                      ->where('clientjobpost.id', $id)
                      ->count();
            $jobs_info = DB::table('clientjobpost')
                     ->join('categories', 'clientjobpost.job_heading', '=', 'categories.id')
                     ->where('clientjobpost.status', '1')
                     ->where('clientjobpost.deleted', '0')
                     ->where('clientjobpost.id', $id)
                     ->orderBy('clientjobpost.id', 'desc')  
                     ->paginate($pagination);

            $hireData = DB::table('hire')
                        ->join('users','users.id', 'hire.worker_id')
                        ->where('client_job_id', '=',$id )
                        ->get();         
                     return view('admin.jobs.show', compact('jobs_info', 'jobs_count', 'id', 'hireData'));
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function edit($id) {
    
    }
   
    public function update(Request $request) {

    }
}

?>