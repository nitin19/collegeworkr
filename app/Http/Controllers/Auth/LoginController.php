<?php

namespace App\Http\Controllers\Auth;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Auth;
use Exception;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected function authenticated($request, $user) { 
        
    if($user->user_role == 'admin') { 

            return redirect()->intended('/admindashboard'); 

        }  
        else if($user->user_role == 'worker') {
            return redirect()->intended('/workerjobs'); 
        }
        else if($user->user_role == 'customer'){
            return redirect()->intended('/customerdashboard'); 
        } 
        else {
            //Auth::logout();
            return redirect()->intended('/home')->with('error','Your account is not active.');  
            //return redirect()->intended('/home'); 
        }
   // protected $redirectTo = '/workerjobs';
}
  /*  if(Auth::user()->user_role=='customer'){
      protected $redirectTo = '/customerdashboard';
    } else {
        protected $redirectTo = '/workerjobs';
    }*/

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
     public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();
        } catch (Exception $e) {
            return redirect('auth/facebook');
        }
        $authUser = $this->findOrCreateUser($user);

        Auth::login($authUser, true);

        return redirect()->route('home');
    }

    private function findOrCreateUser($facebookUser)
    {
        $authUser = User::where('facebook_id', $facebookUser->id)->first();

        if ($authUser){
            return $authUser;
        }

        return User::create([
            'firstname' => $facebookUser->name,
            'email' => $facebookUser->email,
            'facebook_id' => $facebookUser->id,
            'image' => $facebookUser->avatar
        ]);
    }
/*    public function logout(Request $request) {
  Auth::logout();
  return redirect('/home');
}*/
}
