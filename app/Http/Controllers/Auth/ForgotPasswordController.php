<?php

namespace App\Http\Controllers\Auth;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use DB;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendemail(){
        $email      = $_GET['forgetemail'];
        $userdetail = DB::table('users')
                      ->where('email',$email)
                      ->first();

        $last_id = $userdetail->id;
        $user_first_name = $userdetail->firstname;
        $user_email = $userdetail->email;
        $user_pass = $userdetail->hdpswd;
        $user_invitecode = $userdetail->invitecode;
        $site_url = url('/');
        $logo_url = url('/public').'/admin/images/newlogo.png';
        $mail_bg_img = url('/public').'/images/bg.png';
        $email_icon = url('/public').'/images/email-icon.png';
        $phone_img = url('/public').'/images/phone1.png';
        $mail_icon = url('/public').'/images/mail.png';
        $thankyouimg = url('/public').'/images/thank-you-img.png';
   
        $name       = $userdetail->name;
        $password   = $userdetail->hdpswd;
        $to         = $email;
        $subject    = "Request for password";
        $message    = '';

        $message .= '<html>
                            <head>
                              <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
                              <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
                            </head>
                            <body>
                            <div class="mail_background" style="background-color: #d2effd;padding: 40px 0px 0px 0;">
                                <div class="container">
                                    <div class="mail_back_img" style="background-image: url('.$mail_bg_img.') ;background-size: contain;background-position: top;background-repeat: no-repeat;padding: 0px 0px 0px 0px;">
                                        <div class="" style="width: 500px;margin: 0 auto;">
                                          <div class="row">
                                            <div class="" style="text-align: center;padding-bottom: 40px;padding-top: 45px;">
                                              <img style="width:150px" src="'.$logo_url.'" alt="Logo">
                                            </div>
                                            <div class="" style="text-align: center;padding-bottom: 0px;">
                                              <img style="width:60px" src='.$email_icon.'>
                                            </div>
                                            <div class="" style="text-align: center;">
                                              <h6 style="font-size:20px;font-weight:500;margin-top: 8px;margin-bottom: 0px;letter-spacing: 1px;"><span style="color:#1ba4e0">Hey </span>'.$user_first_name.', </h6>  
                                            </div>
                                            <div class="" style="text-align: center;padding: 15px 0px;">
                                              <img style="width: 230px;" src='.$thankyouimg.'>
                                            </div>
                                            <div class="" style="text-align: center;padding-bottom: 20px;">
                                              <h5 style="font-size:20px;font-weight:400;margin-bottom: 0px;margin-top: 0px;">Please note your email and password.</h5>
                                              <p style="font-size:14px;font-weight:400;margin:0px;padding: 0 60px 0px 60px;">You need this every time you want to log on to the website to keep it safe and do not share it with anyone.</p>
                                               <ul class="user_details" style="padding: 0 135px 0 135px;text-align: left;list-style: none;">
                                                   <li><strong>Name: ' . $user_first_name  .'</li>
                                                   <li><strong>Email: ' . $user_email  .'</li>
                                                   <li><strong>Password: ' . $user_pass  .'</li>
                                               </ul>
                                            </div>
                                            <div class="" style="text-align: center;padding-bottom: 10px">
                                              <a href="'.url('/').'" style="background-color: #f3721f;padding: 7px 90px;color: #fff;font-size: 16px;font-weight: 400;text-transform: uppercase;text-decoration: none;display: inline-block;border-radius: 6px;">Start journey</a>
                                            </div>
                                            
                                            <div class="" style="text-align: center;padding-top: 5px;padding-bottom: 10px;">
                                              <p style="font-size:12px;font-weight:400;margin: 0px;">In case of any questions, please feel free to reach us on</p>
                                            </div>
                                            <div class="row" style="overflow:hidden;margin:0 auto;transform: translate(9%);padding-bottom: 20px;padding-left: 60px;">
                                              <div class="col-sm-6">
                                                  <div style="margin-right: 20px;background-color: #fcddc9;padding: 10px;border-left: 1px solid #f3721f;    border-bottom: 1px solid #f3721f;border-radius: 5px;float: left;width: 36%;">
                                                    <div style="float: left;width: 15%;margin-right: 20px;">
                                                      <img style="width: 100%;" src='.$phone_img.'>
                                                    </div>
                                                    <div style="float: left;width: 70%;">
                                                      <a href="tell:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:10px;">Call us at</a> 
                                                      <a href="tell:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:12px;">1800–419–7713</a>
                                                    </div>
                                                  </div>
                                              </div>
                                              <div class="col-sm-6">
                                                  <div style="padding: 10px;background-color: #c8e9f8;border-left: 1px solid #1ba4e0;border-bottom: 1px solid #1ba4e0;border-radius: 5px;float: left;width: 36%;">
                                                      <div style="float: left;width:15%;margin-right:20px;">
                                                          <img style="width:100%" src='.$mail_icon.'>
                                                      </div>
                                                      <div style="float: left;width: 70%;">
                                                          <a style="display: block;font-size:10px;" href="mailto:info@collegewrk.com" target="_top">Email us at</a>
                                                          <a  style="display: block;font-size:12px;" href="mailto:info@collegewrk.com" target="_top">info@collegewrk.com</a>
                                                      </div>
                                                  </div>
                                              </div>
                                        </div>
                                 </div>
                            </div>
                      </div>
                      <div class="" style="text-align: center;background-color:#e5e5e5;border-top:1px solid #ccc;border-bottom:1px solid #ccc;padding: 5px 0px;width:485px;margin: -6px auto 0px;">
                          <p style="font-size:11px;font-weight:bold;margin:0px;">Team College Wrk</p>
                          <p style="text-align:right;margin: 0px 10px 0px 0px;font-size: 8px;font-weight: bold;">Designed  by : <a href="http://www.binarydata.in/" target="_blank">Binary Data</a></p>
                      </div>
              </div>
      </div>
  </body>
</html>';


        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        // $headers .= 'From: <Collegeworker@gmail.com>' . "\r\n";

        // $mail_status = mail($to,$subject,$message,$headers);


        // $subject = "Technical Support Request";
        // $message = "We received a support ticket from mydomain.com:\n\n";
        //  $from = "info@collegewrk.com";
        // $headers .= "From: $from";
        $headers .= 'From: CollegeWrk <info@collegewrk.com>' . "\r\n";
       $mail_status = mail($to,$subject,$message,$headers);
    
        /*Auth::login($user);
          return redirect('/userhome');*/
        
        if($mail_status){
            echo 'true';
        }
        else{
            echo 'false';
        }


        /*if($mail_status){
            return redirect('password/reset')->with('success','Details are send to your email.  ');
        }else{
            return redirect('password/reset')->with('error','Some problem exists. Please try again.');
        }*/
    }
    public function checkforgotemail(){
        $email = $_GET['email'];
        $existingemail = DB::table('users')
                      ->where('email', '=', $email)
                      ->get();
        if(count($existingemail) > '0'){
            echo "true";
        }else{
            echo "false";
        }        
    }
}
