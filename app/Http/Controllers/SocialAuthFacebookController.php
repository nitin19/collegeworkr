<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Socialite;
use Redirect;
use Auth;
use Exception;
use App\Errorlogs;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ErrorsClass;

class SocialAuthFacebookController extends Controller
{
    public function facebookredirect($service)
    {
        return Socialite::driver($service)->stateless()->redirect();
    }

     public function facebookcallback($service)
    {
        try {
            
        $facebookUser = Socialite::with ( $service )->stateless()->user ();
         $user = User::where('email',$facebookUser->email)->first();
            if($user) {
               Auth::loginUsingId($user->id);
            }
            else {
                $user = new User;
                $user->firstname = $facebookUser->name;
                $user->email = $facebookUser->email;
                $user->google_id = $facebookUser->id;
                $user->password = md5(rand(1,10000));
                $user->user_role = 'worker';
                $user->invitecode  = rand();
                $user->save();
                Auth::loginUsingId($user->id);
            }
           /* return redirect()->to('/home');*/
            return redirect('/workerprofile');
        //   return view ( 'pages.home' )->withDetails ( $user )->withService ( $service );
        
        }  
        catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
}
