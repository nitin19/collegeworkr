<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Home;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use Illuminate\Pagination\Paginator;

class WorkerpayController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        try{
        $id        = Auth::user()->id;
        $bnkworker = DB::table('workerpayment')
                            ->where('user_id', '=', $id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->first();  
        $past_payments = DB::table('payments_succeed')
                         ->join('clientjobpost', 'clientjobpost.id', 'payments_succeed.job_id' )
                         ->join('categories', 'categories.id', 'clientjobpost.job_heading')
                         ->where( 'worker_id', '=', $id )
                         ->where('worker_payment_status', '=', 1)
                         ->get();                    
         
         return view('workerpay.index',compact('bnkworker', 'past_payments'));
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
        catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }

    public function store(Request $request)
    {    
        try{
        $user_id = Auth::user()->id;
        $bank_routing_number        = $request->bank_routing_number;
        $bank_account_number        = $request->bank_account_number;
        $created_at                 = date('Y-m-d H:m:i');
        $updated_at                 = date('Y-m-d H:m:i');
        $status                     = '1';
        $deleted                    = '0';
        $created_by                 = $user_id;
        $updated_by                 = $user_id;

        $checkDetails = DB::table('workerpayment')
                            //->where('bank_routing_number', '=', $bank_routing_number)
                            ->where('bank_account_number', '=', $bank_account_number)
                            ->first();
        if(!empty($checkDetails)){
            return redirect()->route('workerpay.index')
                   ->with('error','Account number is already registered.');
        }else{
            $bnk = DB::table('workerpayment')
             ->insertGetId(['user_id' => $user_id,'bank_routing_number' => $bank_routing_number,'bank_account_number' => $bank_account_number, 'status' => $status, 'deleted' => $deleted ,'created_at' => $created_at,'updated_at' => $updated_at,'created_by' => $created_by,'updated_by' => $updated_by, 'payment_status' => 'pending']);

             return redirect()->route('workerpay.index')
                   ->with('success','Add a new bank account successfully');
        }


        
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
        catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }

     public function show($id)

    {

       

    }
    
   public function edit($id)

    {


    }
     public function update(Request $request,$id)
    {  
        try{
         $user_id = Auth::user()->id;
         $bank_routing_number        = $request->bank_routing_number;
         $bank_account_number        = $request->bank_account_number;
         //============================//
         	$checkDetails = DB::table('workerpayment')
                            //->where('bank_routing_number', '=', $bank_routing_number)
                            ->where('bank_account_number', '=', $bank_account_number)
                            ->where('user_id', '!=', $user_id)
                            ->first();
         //============================//
            if( !empty($checkDetails) ){
            	return redirect()->route('workerpay.index')
                   ->with('error','Account number is already registered.');
            }else{
            		$bankupd = DB::table('workerpayment')
                    ->where(['id' => $id])
                    ->update(['bank_routing_number' => $bank_routing_number,'bank_account_number' => $bank_account_number]);

                    return redirect()->route('workerpay.index')
                   ->with('success','Account information is updated successfully');
            }
        		
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
        catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }

    }
     
}
