<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Home;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;

class AdminhireController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
      try{
        $job_id  =  $request->job_id;
        $search = $request->search; 
        $pagination = 10;
        $jobs_count = DB::table('categories')
                      ->join('clientjobpost', 'clientjobpost.job_heading', '=', 'categories.id')
                      ->where('clientjobpost.status', '1')
                      ->where('clientjobpost.deleted', '0')
                      ->count();

                  $jobs_info = DB::table('categories')
                     ->join('clientjobpost', 'clientjobpost.job_heading', '=', 'categories.id')
                     ->where('clientjobpost.status', '1')
                     ->where('clientjobpost.deleted', '0')
                     ->orderBy('clientjobpost.id', 'desc')  
                     ->paginate($pagination);   
                      
     
        $cusomter_count = DB::table('users')->where('user_role', 'worker')->where('status', '1')->where('deleted', '0')->count();
        $cusomter_count = DB::table('users')
                            ->orWhere('name', 'like', '%'.$search.'%')
                            ->orWhere('email', 'like', '%'.$search.'%')
                            ->where('status', '1')
                            ->where('deleted', '0')->count();
        $customer_info = DB::table('users')
                            ->where('user_role', 'worker')
                            ->where('status', '1')
                            ->where('deleted', '0')
                            ->orderBy('id', 'desc')  
                            ->paginate($pagination);
        
        $hire_info = DB::table('hire')
                        ->join('clientjobpost', 'clientjobpost.id', '=', 'hire.client_job_id')
                        ->join('categories', 'categories.id', '=', 'clientjobpost.job_heading')
                        ->join('users', 'users.id', '=', 'hire.worker_id')
                        ->where(['hire.deleted'=> '0']) 
                        //->where(['hire.status'=> '2' , 'hire.deleted'=> '0']) 
                        ->orderBy('hire.id', 'desc') 
                        ->paginate($pagination); 


        return view('admin.hire.index',compact('jobs_info', 'jobs_count', 'cusomter_count', 'customer_info', 'hire_info'));  
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
      }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
      }          
    }

    public function adminhirecreate(Request $request)
    {
      try{
        $id = Auth::user()->id; 
        $job_id  =     $request->id;
        $job_posts  = $request->job_posts;
        $job_userid = DB::table('clientjobpost')
                      ->where('id', '=', $job_posts)
                      ->where('status', '=', 1)
                      ->where('deleted', '=', 0)
                      ->first();
        $cat_id = $job_userid->job_heading;
        $job_name = DB::table('categories')
                      ->where('id', '=', $cat_id)
                      ->where('status', '=', 1)
                      ->where('deleted', '=', 0)
                      ->first(); 
        $job_cat_name = $job_name->cat_name;
        $client_jobid = $job_userid->user_id;           
        $workers    = $request->workers;
        $workers_info = DB::table('users')
                      ->where('id', '=', $workers)
                      ->where('status', '=', 1)
                      ->where('deleted', '=', 0)
                      ->first();
        $workers_name = $workers_info->name;
        $workers_lastname = $workers_info->lastname;              
        $created_at             = date('Y-m-d H:m:i');
        $updated_at             = date('Y-m-d H:m:i');
        $hireworker = DB::table('hire')
                      ->insertGetId(['proposal_id' =>0, 'client_job_id' =>$job_posts, 'worker_id' =>$workers, 'job_status' =>'hired', 'status' =>2]);
        $message = 'You are hired for this job ' .$job_cat_name;
        $customermessage = 'A Suitable WRKer <strong>'. $workers_name .'</strong> has been hired as per the requirement of your job post.';              
        //===================================================//
        /* code commented 07-02-2019
         $hireworker_notification = DB::table('notification')
                      ->insertGetId(['client_userid' =>$client_jobid, 'worker_userid' =>$workers, 'client_jobid' =>$job_posts, 'message' =>$message, 'customer_message' =>$customermessage, 'job_status' =>'hired', 'created_by' =>$id, 'created_at' =>$created_at, 'updated_by' =>$id, 'updated_at' =>$updated_at, 'status_read' =>0]); */

         $hireworker_notification = DB::table('new_notifications')
                      ->insertGetId(['client_id' =>$client_jobid, 'worker_id' =>$workers, 'message' =>$customermessage, 'message_worker' =>$message, 'created_at' => $created_at ]);               

        //==================================================//              
        


        $hireworker_reservedjobs = DB::table('reservedjobs')
                      ->insertGetId(['client_userid' =>$client_jobid, 'worker_userid' =>$workers, 'client_jobid' =>$job_posts, 'job_status' =>'Applied', 'created_by' =>$id, 'created_at' =>$created_at, 'updated_by' =>$id, 'updated_at' =>$updated_at]);                              
          //return view('admin.hire.index');
       /* $cusomter_count = DB::table('clientjobpost')->where('user_role', 'worker')->where('status', '1')->where('deleted', '                0')->count();*/
        $hired_info = DB::table('hire')
                        ->join('clientjobpost', 'clientjobpost.id', '=', 'hire.client_job_id')
                        ->join('categories', 'categories.id', '=', 'clientjobpost.job_heading')
                        ->join('users', 'users.id', '=', 'hire.worker_id')
                        ->where(['hire.id'=>$hireworker,'hire.status'=> '2' , 'hire.deleted'=> '0']) 
                        ->first();
 
  

       $worker_name =  $hired_info->name; 
       $worker_lastname =  $hired_info->lastname; 
       $worker_email =  $hired_info->email;
       $worker_phone =  $hired_info->phone; 
       $job_post_user_id = $hired_info->user_id;
       $job_post_user_description = $hired_info->job_description;
       $job_post_job_location = $hired_info->job_location;
       $job_post_name = $hired_info->cat_name;
       $worker_qualification = $hired_info->worker_qualification;

       $job_post_info = DB::table('users')
                     ->where('id',$job_post_user_id)
                     ->where('status', '1')
                     ->where('deleted', '0')
                     ->first();
       $job_post_user_name = $job_post_info->name;
       $job_post_user_lastname = $job_post_info->lastname;
       $job_post_user_email = $job_post_info->email;
       $job_post_user_phone = $job_post_info->phone;
       $site_url = url('/');
       $admin_id = Auth::user()->id;
       $admin_info    = DB::table('users')
                   ->where('id',$admin_id)
                   ->where('status', '1')
                   ->where('deleted', '0')
                   ->first();
      if($admin_info!=''){
        $admin_user_email = $admin_info->email;

        } else {
          $admin_info = '';
        }


  /* ========================= worker email ======================== */
        $to = $worker_email;

        $subject = "Hired";
        $message ='';
        $message .= '<html>
                     <head></head>
                     <body>
                    <div class="container" style="width: 500px;padding: 10px;margin-left: 10px;margin-bottom: 10px;border: 1px solid #ccc;">
                       <div class="refralcode_sub_head_sec" style="text-align: center;background-color: #1aa3df;padding: 20px;">
                         <h2 class="refferoffer_head" style="margin-top:0px;margin-bottom:0px;font-size:25px;color:#fff;">Congratulations!</h2>
                         <p class="refralcode_sub_head" style="color:#fff;font-size: 16px;margin-top: 0px;margin-bottom: 0px;">Dear ' .$worker_name.' , You are hired for this job <strong>'.$job_post_name.'Below are the details of the job:</strong></p> 
                       </div>
                      <div class="refralcode_head_sec" style="text-align: center;padding: 20px;">
                       <ul>
                         <li style="list-style:none;"><p class="refralcode_head" style="color:#333;font-size: 16px;margin-top: 0px;margin-bottom: 0px;">JOB description: <strong>'.$job_post_user_description.'</strong></p></li>
                         <li style="list-style:none;"><p class="refralcode_head" style="color:#333;font-size: 16px;margin-top: 0px;margin-bottom: 0px;">Job Location: <strong>'.$job_post_job_location.'</strong></p></li>
                         <ul>
                       </div>
                       <div class="refralcode_head_sec" style="text-align: center;padding: 20px;">
                        <p class="refralcode_head" style="color:#333;font-size: 16px;margin-top: 0px;margin-bottom: 0px;">Please acknowledge the email and confirm us your availability. Please feel free to ask any question and we are always here to help you.<br>
                        <a href="tel:9192953290">9192953290</a><br>
                        <a href="mailto:admin@collegewrk.com">admin@collegewrk.com</a>
                        </p> 
                       </div>
                       <div class="refrallink_sec" style="text-align: center;padding: 20px;">
                         <p class="refrallink_head" style="color:#333;font-size: 22px;margin-top: 0px;margin-bottom: 0px;font-weight: bold;"></p>
                         <div class="refral_link_btn_sec" style="margin-top: 15px;"><a class="refral_btn" href="'.$site_url.'" style="background-color: #1aa3df;padding: 10px 25px 12px 25px;color: #fff;text-decoration: none;font-size: 18px;font-weight: bold;border-radius: 5px;">Go to site.. </a></div>
                       </div>
                    </div>
                    </body>
                    </html>';
        $headers = "From: info@collegewrk.com" . "\r\n" ."CC: dev2.bdpl@gmail.com";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        $send_mail = mail($to,$subject,$message,$headers);

  /*====== jobpost email=======*/
        $to = $job_post_user_email;
        $subject = "Hired";
        $message ='';
        $message .= 
                    '<html>
                     <head></head>
                     <body>
                    <div class="container" style="width: 500px;padding: 10px;margin-left: 10px;margin-bottom: 10px;border: 1px solid #ccc;">
                       <div class="refralcode_sub_head_sec" style="text-align: center;background-color: #1aa3df;padding: 20px;">
                         <p class="refralcode_sub_head" style="color:#fff;font-size: 16px;margin-top: 0px;margin-bottom: 0px;">Dear <strong>' .$job_post_user_name. ' </strong>A Suitable WRKer has been hired as per the requirement of your job post. Below are the details of the WRKer:</p> 
                       </div>
                       <div class="refralcode_head_sec" style="text-align: center;padding: 20px;">
                       <ul>
                         <li style="list-style:none;"><p class="refralcode_head" style="color:#333;font-size: 16px;margin-top: 0px;margin-bottom: 0px;">WRKer Name: <strong>' .$worker_name. '</strong></p></li>
                         <li style="list-style:none;"><p class="refralcode_head" style="color:#333;font-size: 16px;margin-top: 0px;margin-bottom: 0px;">WRKer Qualification: <strong>' .$worker_qualification. '</strong></p></li>
                         <li style="list-style:none;"><p class="refralcode_head" style="color:#333;font-size: 16px;margin-top: 0px;margin-bottom: 0px;">WRKer Email: <strong>' .$worker_email. '</strong></p></li>
                         <li style="list-style:none;"><p class="refralcode_head" style="color:#333;font-size: 16px;margin-top: 0px;margin-bottom: 0px;">WRKer Number: <strong>' .$worker_phone. '</strong></p></li>
                         <ul>
                       </div>
                       <div class="refrallink_sec" style="text-align: center;padding: 20px;">
                         <div class="refral_link_btn_sec" style="margin-top: 15px;"><a class="refral_btn" href="'.$site_url.'" style="background-color: #1aa3df;padding: 10px 25px 12px 25px;color: #fff;text-decoration: none;font-size: 18px;font-weight: bold;border-radius: 5px;">Go to site</a></div>
                       </div>
                    </div>
                    </body>
                    </html>';
        $headers = "From: info@collegewrk.com" . "\r\n" ."CC: dev2.bdpl@gmail.com";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        $send_mail_2 = mail($to,$subject,$message,$headers);

      /*====== Admin email=======*/
        $to = $admin_user_email;

        $subject = "Hired";
        $message = '';
        $message .= '<html>
                     <head></head>
                     <body>
                    <div class="container" style="width: 500px;padding: 10px;margin-left: 10px;margin-bottom: 10px;border: 1px solid #ccc;">
                       <div class="refralcode_sub_head_sec" style="text-align: center;background-color: #1aa3df;padding: 20px;">
                         <h2 class="refferoffer_head" style="margin-top:0px;margin-bottom:0px;font-size:25px;color:#fff;">Hello Admin</h2>
                         <p class="refralcode_sub_head" style="color:#fff;font-size: 16px;margin-top: 0px;margin-bottom: 0px;">Dear Admin You assign this job <strong>'.$worker_name.'</strong> to this WRKer<strong>' .$worker_name. '</strong></p> 
                       </div>
                         <div class="refral_link_btn_sec" style="margin-top: 15px;"><a class="refral_btn" href="'.$site_url.'" style="background-color: #1aa3df;padding: 10px 25px 12px 25px;color: #fff;text-decoration: none;font-size: 18px;font-weight: bold;border-radius: 5px;">Click Here.. </a></div>
                       </div>
                    </div>
                    </body>
                    </html>';
        $headers = "From: info@collegewrk.com" . "\r\n" ."CC: dev2.bdpl@gmail.com";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        $send_mail3 = mail($to,$subject,$message,$headers);
        if($send_mail){
          echo 'true';
        } else {
          echo 'false';
        }              
        return redirect()->route('adminhire.index'); 
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
      }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
      }         
    }  
     public function show($id) {
       
    }

}
?>
