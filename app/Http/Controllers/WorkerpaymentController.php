<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use Illuminate\Pagination\Paginator;

class WorkerpaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
        
        $user_id = Auth::user()->id;
        //=======================================//
            $worker_upcoming_payment = DB::table('payments_succeed')  
                              ->join('clientjobpost', 'clientjobpost.id','payments_succeed.job_id')
                              ->join('categories', 'categories.id', 'clientjobpost.job_heading')
                              ->join('users', 'users.id', 'payments_succeed.client_id') 
                              ->join('hire', 'hire.id', 'payments_succeed.hire_id')
                              ->where('payments_succeed.worker_payment_status','=','0')
                              ->where('payments_succeed.worker_id','=', $user_id )
                              ->get();

            $worker_received_payment = DB::table('payments_succeed')  
                              ->join('clientjobpost', 'clientjobpost.id','payments_succeed.job_id')
                              ->join('categories', 'categories.id', 'clientjobpost.job_heading')
                              ->join('users', 'users.id', 'payments_succeed.client_id') 
                              ->join('hire', 'hire.id', 'payments_succeed.hire_id')
                              ->where('payments_succeed.worker_payment_status','=','1')
                              ->where('payments_succeed.worker_id','=', $user_id )
                              ->get();
            $worker_reject_payment = DB::table('payments_succeed')  
                              ->join('clientjobpost', 'clientjobpost.id','payments_succeed.job_id')
                              ->join('categories', 'categories.id', 'clientjobpost.job_heading')
                              ->join('users', 'users.id', 'payments_succeed.client_id') 
                              ->join('hire', 'hire.id', 'payments_succeed.hire_id')
                              ->where('payments_succeed.worker_payment_status','=','2')
                              ->where('payments_succeed.worker_id','=', $user_id )
                              ->get();                  
            //echo'<pre>';
            //print_r($worker_upcoming_payment);                  
            //echo'</pre>';                  
        //=======================================//



        /*=====$worker_upcoming_payment = DB::table('client_payment')
            ->join('reservedjobs', 'reservedjobs.client_jobid', '=', 'client_payment.client_jobid')
            ->join('users', 'users.id', '=', 'reservedjobs.worker_userid')
            ->where('users.id',$user_id)
            ->where('client_payment.payment_status','succeeded')
            ->select('client_payment.client_jobid', 'reservedjobs.client_jobid', 'reservedjobs.client_userid','reservedjobs.worker_userid','client_payment.paid_amount')
            ->orderBy('client_payment.id','DESC')
            ->paginate(10); */
/*        $worker_upcomingpayment = $worker_upcoming_payments->toArray();
        $worker_upcoming_payment = $worker_upcomingpayment['data'];*/
       /* =====$worker_received_payment = DB::table('client_payment')
            ->join('reservedjobs', 'reservedjobs.client_jobid', '=', 'client_payment.client_jobid')
            ->join('users', 'users.id', '=', 'reservedjobs.worker_userid')
            ->where('users.id',$user_id)
            ->where('client_payment.payment_status','approved')
            ->select('client_payment.client_jobid', 'reservedjobs.client_jobid', 'reservedjobs.client_userid','reservedjobs.worker_userid','client_payment.paid_amount')
            ->orderBy('client_payment.id','DESC')
            ->paginate(10); */
/*        $last_query = end($worker_received_payment);
        print_r($last_query );
        exit();*/
        /*=====$worker_reject_payment = DB::table('client_payment')
            ->join('reservedjobs', 'reservedjobs.client_jobid', '=', 'client_payment.client_jobid')
            ->join('users', 'users.id', '=', 'reservedjobs.worker_userid')
            ->where('users.id',$user_id)
            ->where('client_payment.payment_status','decline')
            ->select('client_payment.client_jobid', 'reservedjobs.client_jobid', 'reservedjobs.client_userid','reservedjobs.worker_userid','client_payment.paid_amount')
            ->orderBy('client_payment.id','DESC')
            ->paginate(10); */



            /*select client_payment.client_jobid, reservedjobs.client_jobid, reservedjobs.client_userid, reservedjobs.worker_userid from client_payment JOIN reservedjobs ON reservedjobs.client_jobid = client_payment.client_jobid JOIN users ON users.id = reservedjobs.worker_userid where users.id = '12'*/ 
        /*$worker_upcoming_payment = DB::select(
            'call  get_worker_upcoming_payment ('.$user_id.')'
        );*/
        /*$worker_received_payment = DB::select(
            'call get_worker_received_payment('.$user_id.')'
        );*/
        /*$worker_reject_payment = DB::select(
            'call  get_worker_reject_payment ('.$user_id.')'
        );*/
        //print_r($workerpayment);
        return view('workerpayment.index', compact('worker_upcoming_payment','worker_received_payment','worker_reject_payment'));
                 //  ->with('i', ($request->input('page', 1) - 1) * 5);
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
        catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
     public function show($id)
    {

    }

     public function update(Request $request,$id)
    {    
        try{
            
            return redirect()->route('workerpayment.index');
        } 
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
        catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
}
