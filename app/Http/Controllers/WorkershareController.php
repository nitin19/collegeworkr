<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Home;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use Illuminate\Pagination\Paginator;

class WorkershareController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{

        $id           = Auth::user()->id;
        $usercustomer = DB::table('users')
                        ->where(['id' => $id,'status' => '1','deleted' => '0'])
                        ->first(); 
         return view('workershare.index',compact('usercustomer'));
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']."  Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
        catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
public function worker_send_invitation_code(Request $request)
    {
        $sendemail = $request->recipient_email;
        $refferal_code = $request->refferal_code;
        $worker_name = $request->worker_name;
        $worker_id = $request->worker_id;
        $created_date = date('Y-m-d H:i:s');
        $site_url = url('/');
        $send_email_data = DB::table('worker_send_reffral_code')->insert(['worker_id' => $worker_id,'send_email' => $sendemail,'send_reffral_code' => $refferal_code,'status' => '1', 'deleted' => '0','created_at' => $created_date]);

       
        $to = $sendemail;
        $subject = "Worker Send Invitation Refferal Code";
         $message ='';
        $message .= 
                    '<html>
                     <head></head>
                     <body>
                    <div class="container" style="width: 500px;padding: 10px;margin-left: 10px;margin-bottom: 10px;border: 1px solid #ccc;">
                       <div class="refralcode_sub_head_sec" style="text-align: center;background-color: #1aa3df;padding: 20px;">
                         <h2 class="refferoffer_head" style="margin-top:0px;margin-bottom:0px;font-size:25px;color:#fff;">Give $10 and Get $10</h2>
                         <p class="refralcode_sub_head" style="color:#fff;font-size: 16px;margin-top: 0px;margin-bottom: 0px;">Your Friend <strong>' .$worker_name. '</strong> has invited you to complete purchase via.</p> 
                       </div>
                       <div class="refralcode_head_sec" style="text-align: center;padding: 20px;">
                         <p class="refralcode_head" style="color:#333;font-size: 16px;margin-top: 0px;margin-bottom: 0px;">Your Invitation Code: <strong>' .$refferal_code. '</strong></p> 
                       </div>
                       <div class="refrallink_sec" style="text-align: center;padding: 20px;">
                         <p class="refrallink_head" style="color:#333;font-size: 22px;margin-top: 0px;margin-bottom: 0px;font-weight: bold;"> -: Please Click Here for Apply work :- </p>
                         <div class="refral_link_btn_sec" style="margin-top: 15px;"><a class="refral_btn" href="'.$site_url.'" style="background-color: #1aa3df;padding: 10px 25px 12px 25px;color: #fff;text-decoration: none;font-size: 18px;font-weight: bold;border-radius: 5px;">Click Here.. </a></div>
                       </div>
                    </div>
                    </body>
                    </html>';
        $headers = "From: info@collegewrk.com" . "\r\n" ."CC: dev2.bdpl@gmail.com";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        $send_mail = mail($to,$subject,$message,$headers);
        if($send_mail){
          echo 'true';
        } else {
          echo 'false';
        }
        //return view('workershare.index');
    }
}
