<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Home;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;

class AdmindashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      try{
        $cusomter_count = DB::table('users')->where('user_role', 'customer')->where('status', '1')->where('deleted', '0')->count();
        $username = Auth::user()->name;
        $worker_count = DB::table('users')->where('user_role', 'worker')->where('status', '1')->where('deleted', '0')->count();
        $jobpost_count = DB::table('clientjobpost')->where('status', '1')->where('deleted', '0')->count();
        // $customer_info = DB:: table('users')
        //                    -> join('clientjobpost','users.id','=','clientjobpost.user_id')
        //                  ->orderBy('users.id', 'desc')
        //                  ->where('users.status', '1')
        //                  ->where('users.deleted', '0')
        //                  ->where('clientjobpost.status', '1')
        //                  ->where('clientjobpost.deleted', '0')
        //                  ->limit(5)
        //                  ->get();
        $customer_info = DB::table('users')
                     ->where('user_role', 'customer')
                     ->where('status', '1')
                     ->where('deleted', '0')
                     ->orderBy('id', 'desc')  
                     ->limit(5)
                     ->get(); 
        $job_info = DB::table('clientjobpost')
                  ->where('status', '1')
                  ->where('deleted', '0')
                  ->limit(4)
                  ->get();
        $bronze_count = DB::table('users')
                  ->where('workertype', 'bronze')
                  ->where('status', '1')
                  ->where('deleted', '0')
                  ->count();
        $gold_count = DB::table('users')
                  ->where('workertype', 'gold')
                  ->where('status', '1')
                  ->where('deleted', '0')
                  ->count();
        $silver_count = DB::table('users')
                  ->where('workertype', 'silver')
                  ->where('status', '1')
                  ->where('deleted', '0')
                  ->count();  
        $package_count = DB::table('users')
                  ->where('workertype','!=', '')
                  ->where('status', '1')
                  ->where('deleted', '0')
                  ->count();
        $worker_info = DB::table('users')
                     ->where('user_role', 'worker')
                     ->where('status', '1')
                     ->where('deleted', '0')
                     ->orderBy('id', 'desc')  
                     ->limit(5)
                     ->get(); 
        $reservedjobs = DB::table('reservedjobs')
                      ->orderBy('id', 'desc')
                      ->limit(4)                                      
                      ->get();
        $hire_count = DB::table('hire')                                 
                    ->count();
        return view('admin.dashboard', compact('cusomter_count', 'worker_count', 'jobpost_count', 'username', 'customer_info', 'job_info', 'bronze_count', 'gold_count', 'silver_count', 'package_count', 'worker_info', 'reservedjobs', 'hire_count'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
      }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
      }
    }
    public static function humanTiming ($time) {
          $time = time() - $time; 
          $time = ($time<1)? 1 : $time;
          $tokens = array (
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
            );
          foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
          }
        }
}

?>