<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Home;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;

class AdminclientpaymentController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        try{
            $client_payment_info = DB::table('client_payment')
                    ->join('clientjobpost', 'client_payment.client_jobid', '=', 'clientjobpost.id')
                    ->get();
            return view('admin.clientpayment.index', compact('client_payment_info'));
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function show($id) {
        try{
            $client_payment = DB::table('client_payment')
                    ->join('clientjobpost', 'client_payment.client_jobid', '=', 'clientjobpost.id')
                    ->where('client_payment.client_jobid',$id)
                    ->first();
            return view('admin.clientpayment.show',compact('client_payment'));
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function edit($id) {            
    }
   
    public function update(Request $request) {
    }
    //============== 13-02-2019 =================//
    public function pendingPayments( Request $request ){
        try{
            $pending_paymentData = DB::table('hire')
                        ->join('clientjobpost', 'clientjobpost.id', '=', 'hire.client_job_id')
                        ->join('categories', 'categories.id', '=', 'clientjobpost.job_heading')
                        ->join('users', 'users.id', '=', 'hire.worker_id')
                        //->select('hire.id as hire_id','categories.*', 'users.*') 
                        ->select('hire.id as hireid', 'hire.proposal_id as proposal_id', 'hire.client_job_id as client_job_id', 'hire.worker_id as worker_id', 'hire.job_status as job_status', 'hire.status as status', 'hire.deleted as deleted', 'hire.created_at as created_at', 'hire.updated_at as updated_at', 'hire.hours_to_complete as hours_to_complete', 'hire.mints_to_complete as mints_to_complete', 'hire.updated_hours as updated_hours', 'hire.updated_mints as updated_mints', 'hire.time_approved as time_approved', 'hire.job_summary as job_summary', 'hire.future_appointments as future_appointments', 'hire.private_notes as private_notes', 'clientjobpost.*', 'categories.*', 'users.*' )
                        ->where(['hire.time_approved'=>1,'hire.payment_status'=> '0' , 'hire.deleted'=> '0']) 
                        ->orderBy('hire.id', 'desc')
                        ->get();
            //echo'<pre>';
            //print_r($pending_paymentData);        
            //echo'</pre>'; 
            return view('admin.clientpayment.pendingpayments',compact('pending_paymentData'));
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }       
    }
    //===========================================//
     public function processClientPayments( Request $request ){
        $payableamount = $request->payableamount;
        $amount = round($payableamount) * 100;
        $clientid = $request->clientid;
        $worker_id = $request->worker_id;
        $jobid = $request->jobid;
        $hireid = $request->hireid;
        $jobtitle = $request->jobtitle;
        $stripeDetails = DB::table('user_payment_credentials')
                        ->where('user_id', '=', $clientid )
                        ->first();
        if(!empty($stripeDetails)){
            $stripe_id = $stripeDetails->stripe_id;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://api.stripe.com/v1/charges'); 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "amount=". $amount ."&currency=usd&customer=". $stripe_id);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_USERPWD, env('STRIPE_SEC_KEY') . ':' . '');

            $headers = array();
            $headers[] = 'Content-Type: application/x-www-form-urlencoded';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                //echo 'Error:' . curl_error($ch);
                echo json_encode(array('status' => 'error', 'msg' => curl_error($ch) ));
            }
            //curl_close ($ch);
            //echo'<pre>';
            //print_r( $result);
            //echo'</pre>';
            $result =  json_decode( $result );
                if( array_key_exists('error', $result) ){
                   echo json_encode(array('status' => 'error', 'msg' => 'Something went wrong'));
                }else{ 
                    $transaction_id = $result->id;
                    $last_insert_id = DB::table('payments_succeed')->insertGetId(['client_id'=> $clientid, 'job_id' => $jobid, 'worker_id' => $worker_id, 'transaction_id' => $transaction_id, 'created_at' => date('Y-m-d h:i:s'), 'hire_id' => $hireid, 'amount_paid' =>  $amount / 100 ]);
                   $updateJobStatus = DB::table('hire')->where('id', $hireid)->update(['payment_status' => 1 ]);
                   //========================// 
                $created_at  = date('Y-m-d H:m:i'); 
                $message = 'Admin Process a payment from your account for <strong>'. $jobtitle .'</strong>.';
                $hireworker_notification = DB::table('new_notifications')
                      ->insertGetId([ 'client_id' =>$clientid, 'message' =>$message, 'message_worker' =>$message, 'created_at' => $created_at, 'notification_redirect_url' => '/customerpayment' ]); 
              //========================// 

                    echo json_encode(array('status' => 'success', 'msg' => 'Payment successfully'));
                } 
        }else{
            echo json_encode(array('status' => 'error', 'msg' => 'Payment Detail missing')); 
        }               
            

       } 
    //===========================================//
        public function adminAllPayments(Request $request ){
            $allpayments = DB::table('payments_succeed')  
                              ->join('clientjobpost', 'clientjobpost.id','payments_succeed.job_id')
                              ->join('categories', 'categories.id', 'clientjobpost.job_heading')
                              ->join('users', 'users.id', 'payments_succeed.worker_id') 
                              ->join('hire', 'hire.id', 'payments_succeed.hire_id')
                              ->where('payments_succeed.worker_payment_status','!=','0')
                              ->get(); 
            //echo'<pre>';
            //print_r($allpayments);        
            //echo'</pre>'; 
            return view('admin.clientpayment.allpayments',compact('allpayments'));
        }

   //============================================//    
}
?>