<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use Illuminate\Pagination\Paginator;

class CustomerpaymentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
        
        $user_id = Auth::user()->id;
        $customer_upcoming_payment = DB::table('hire')
                        ->join('clientjobpost', 'clientjobpost.id', '=', 'hire.client_job_id')
                        ->join('categories', 'categories.id', '=', 'clientjobpost.job_heading')
                        ->join('users', 'users.id', '=', 'hire.worker_id')
                        //->select('hire.id as hire_id','categories.*', 'users.*') 
                        ->select('hire.id as hireid', 'hire.proposal_id as proposal_id', 'hire.client_job_id as client_job_id', 'hire.worker_id as worker_id', 'hire.job_status as job_status', 'hire.status as status', 'hire.deleted as deleted', 'hire.created_at as created_at', 'hire.updated_at as updated_at', 'hire.hours_to_complete as hours_to_complete', 'hire.mints_to_complete as mints_to_complete', 'hire.updated_hours as updated_hours', 'hire.updated_mints as updated_mints', 'hire.time_approved as time_approved', 'hire.job_summary as job_summary', 'hire.future_appointments as future_appointments', 'hire.private_notes as private_notes', 'users.*', 'categories.*' )
                        ->where(['hire.time_approved'=>1,'hire.payment_status'=> '0' , 'hire.deleted'=> '0', 'clientjobpost.user_id'=>$user_id]) 
                         
                        ->orderBy('hire.id', 'desc')
                        ->get();

        $customer_received_payment = DB::table('payments_succeed')  
                              ->join('clientjobpost', 'clientjobpost.id','payments_succeed.job_id')
                              ->join('categories', 'categories.id', 'clientjobpost.job_heading')
                              ->join('users', 'users.id', 'payments_succeed.client_id') 
                              //->join('hire', 'hire.id', 'payments_succeed.hire_id')
                              //->where('payments_succeed.worker_payment_status','=','1')
                              ->where('payments_succeed.client_id','=',$user_id )
                              ->get();                       

        /*echo'<pre>';
        print_r($customer_upcoming_payment);                      
        echo'</pre>'; 
        echo'<pre>';
        print_r($customer_received_payment);                      
        echo'</pre>';  */                    

        /*$customer_upcoming_payment = DB::table('client_payment')
            ->join('reservedjobs', 'reservedjobs.client_jobid', '=', 'client_payment.client_jobid')
            ->join('users', 'users.id', '=', 'reservedjobs.client_userid')
            ->where('users.id',$user_id)
            ->where('client_payment.payment_status','succeeded')
            ->select('client_payment.client_jobid', 'reservedjobs.client_jobid', 'reservedjobs.client_userid','reservedjobs.worker_userid','client_payment.paid_amount')
            ->orderBy('client_payment.id','DESC')
            ->paginate(10);

        $customer_received_payment = DB::table('client_payment')
            ->join('reservedjobs', 'reservedjobs.client_jobid', '=', 'client_payment.client_jobid')
            ->join('users', 'users.id', '=', 'reservedjobs.client_userid')
            ->where('users.id',$user_id)
            ->where('client_payment.payment_status','approved')
            ->select('client_payment.client_jobid', 'reservedjobs.client_jobid', 'reservedjobs.client_userid','reservedjobs.worker_userid','client_payment.paid_amount')
            ->orderBy('client_payment.id','DESC')
            ->paginate(10);

        $customer_reject_payment = DB::table('client_payment')
            ->join('reservedjobs', 'reservedjobs.client_jobid', '=', 'client_payment.client_jobid')
            ->join('users', 'users.id', '=', 'reservedjobs.client_userid')
            ->where('users.id',$user_id)
            ->where('client_payment.payment_status','decline')
            ->select('client_payment.client_jobid', 'reservedjobs.client_jobid', 'reservedjobs.client_userid','reservedjobs.worker_userid','client_payment.paid_amount')
            ->orderBy('client_payment.id','DESC')
            ->paginate(10);
        */    
            
        /*$customer_upcoming_payment = DB::select(
            'call get_customer_upcoming_payment('.$user_id.')'
        );
        $customer_received_payment = DB::select(
            'call get_customer_received_payment('.$user_id.')'
        );
        $customer_reject_payment = DB::select(
            'call get_customer_reject_payment ('.$user_id.')'
        );*/
        $customer_reject_payment =[];
        return view('customerpayment.index', compact('customer_upcoming_payment','customer_received_payment','customer_reject_payment'));
                 //  ->with('i', ($request->input('page', 1) - 1) * 5);
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
     public function show($id)
    {

    }

     public function update(Request $request,$id)
    {    
         
       return redirect()->route('workerpayment.index');
    }
}
