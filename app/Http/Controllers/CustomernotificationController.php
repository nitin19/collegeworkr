<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Home;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;

class CustomernotificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         
       try{
        $id             = Auth::user()->id;
/*        $notification   = DB::table('notification')
                        ->where('status_read', '=', 1)
                        ->where('client_userid','=', $id)
                        ->paginate(10); */
          //DB::enableQueryLog();
          $applied_worker = DB::table('clientjobpost') 
            ->join('users', 'clientjobpost.user_id', '=', 'users.id')
            //->join('hire', 'clientjobpost.id', '=', 'hire.client_job_id')
            ->where('clientjobpost.user_id',$id)
            //->where('hire.status','!=','2')
            ->select('clientjobpost.id AS JobID','clientjobpost.created_at AS created_at','clientjobpost.worker_required AS worker_required','clientjobpost.job_location AS job_location','clientjobpost.job_heading AS JobHeading', 'clientjobpost.worker_type AS WorkerType','clientjobpost.job_description','clientjobpost.created_at AS JobPostDate','users.firstname AS customerFirstName','users.lastname AS customerLastName','users.id AS customer_id','users.address1','users.address')
            ->orderBy('clientjobpost.id','DESC')
            ->paginate(10);
          //dd(DB::getQueryLog());

          /*$applied_worker = DB::table('clientjobpost')
            ->join('reservedjobs', 'clientjobpost.id', '=', 'reservedjobs.client_jobid')
            ->join('users', 'reservedjobs.worker_userid', '=', 'users.id')
            ->where('reservedjobs.job_status','Applied')
            ->select('clientjobpost.id AS JobID', 'clientjobpost.job_heading AS JobHeading', 'clientjobpost.worker_type AS WorkerType','reservedjobs.client_userid','reservedjobs.id as ProposalId','reservedjobs.worker_userid','clientjobpost.job_description','clientjobpost.created_at AS JobPostDate','users.firstname AS WorkerFirstName','users.lastname AS WorkerLastName','users.id AS Workerid','users.address1','users.address')
            ->orderBy('clientjobpost.id','DESC')
            ->paginate(10);*/

          $hired_worker = DB::table('clientjobpost')
            ->join('hire', 'clientjobpost.id', '=', 'hire.client_job_id')
            ->join('users', 'hire.worker_id', '=', 'users.id')
            ->where('hire.job_status','hired')
            ->where('hire.status','2')
            ->where('clientjobpost.user_id',$id)
            ->select('clientjobpost.id AS JobID', 'clientjobpost.job_heading AS JobHeading','clientjobpost.user_id AS client_id','clientjobpost.worker_type AS WorkerType','hire.worker_id','hire.id','clientjobpost.job_description','clientjobpost.created_at AS JobPostDate','users.firstname AS WorkerFirstName','users.lastname AS WorkerLastName','users.id AS Workerid','users.address1','users.address')
            ->orderBy('clientjobpost.id','DESC')
            ->paginate(10);

          /*$hired_worker = DB::table('clientjobpost')
            ->join('hire', 'clientjobpost.id', '=', 'hire.client_job_id')
            ->join('users', 'hire.worker_id', '=', 'users.id')
            ->join('reservedjobs', 'clientjobpost.id', '=', 'reservedjobs.client_jobid')
            ->where('hire.job_status','hired')
            ->where('hire.status','2')
            ->select('clientjobpost.id AS JobID', 'clientjobpost.job_heading AS JobHeading','clientjobpost.user_id AS client_id','clientjobpost.worker_type AS WorkerType','hire.worker_id','hire.id','clientjobpost.job_description','reservedjobs.id as ProposalId','clientjobpost.created_at AS JobPostDate','users.firstname AS WorkerFirstName','users.lastname AS WorkerLastName','users.id AS Workerid','users.address1','users.address')
            ->orderBy('clientjobpost.id','DESC')
            ->paginate(10);*/
            

            $complte_job_worker = DB::table('clientjobpost')
              ->join('hire', 'clientjobpost.id', '=', 'hire.client_job_id')
              ->join('users', 'hire.worker_id', '=', 'users.id')
              ->where('hire.job_status','completed')
              ->where('hire.status','3')
              ->where('clientjobpost.user_id',$id)
              ->select('clientjobpost.id AS JobID', 'clientjobpost.job_heading AS JobHeading', 'clientjobpost.worker_type AS WorkerType','hire.worker_id','clientjobpost.job_description','clientjobpost.created_at AS JobPostDate','users.firstname AS WorkerFirstName','users.lastname AS WorkerLastName','users.id AS Workerid','users.address1','users.address')
              ->orderBy('clientjobpost.id','DESC')
              ->paginate(10);


          /*$complte_job_worker = DB::table('clientjobpost')
            ->join('hire', 'clientjobpost.id', '=', 'hire.client_job_id')
            ->join('users', 'hire.worker_id', '=', 'users.id')
            ->where('hire.job_status','completed')
            ->where('hire.status','3')
            ->select('clientjobpost.id AS JobID', 'clientjobpost.job_heading AS JobHeading', 'clientjobpost.worker_type AS WorkerType','hire.worker_id','clientjobpost.job_description','clientjobpost.created_at AS JobPostDate','users.firstname AS WorkerFirstName','users.lastname AS WorkerLastName','users.id AS Workerid','users.address1','users.address')
            ->orderBy('clientjobpost.id','DESC')
            ->paginate(10);
*/

          /*$complte_job_worker = DB::table('clientjobpost')
            ->join('hire', 'clientjobpost.id', '=', 'hire.client_job_id')
            ->join('users', 'hire.worker_id', '=', 'users.id')
            ->join('reservedjobs', 'clientjobpost.id', '=', 'reservedjobs.client_jobid')
            ->where('hire.job_status','completed')
            ->where('hire.status','3')
            ->select('clientjobpost.id AS JobID', 'clientjobpost.job_heading AS JobHeading', 'clientjobpost.worker_type AS WorkerType','hire.worker_id','clientjobpost.job_description','reservedjobs.id as ProposalId','clientjobpost.created_at AS JobPostDate','users.firstname AS WorkerFirstName','users.lastname AS WorkerLastName','users.id AS Workerid','users.address1','users.address')
            ->orderBy('clientjobpost.id','DESC')
            ->paginate(10);*/

          $cancelled_job_worker = DB::table('clientjobpost')
            ->join('hire', 'clientjobpost.id', '=', 'hire.client_job_id')
            ->join('users', 'hire.worker_id', '=', 'users.id')
            ->where('hire.job_status','cancelled')
            ->where('hire.status','4')
            ->where('clientjobpost.user_id',$id)
            ->select('clientjobpost.id AS JobID', 'clientjobpost.job_heading AS JobHeading', 'clientjobpost.worker_type AS WorkerType','hire.worker_id','clientjobpost.job_description','clientjobpost.created_at AS JobPostDate','users.firstname AS WorkerFirstName','users.lastname AS WorkerLastName','users.id AS Workerid','users.address1','users.address')
            ->orderBy('clientjobpost.id','DESC')
            ->paginate(10);


          /*$cancelled_job_worker = DB::table('clientjobpost')
            ->join('hire', 'clientjobpost.id', '=', 'hire.client_job_id')
            ->join('users', 'hire.worker_id', '=', 'users.id')
            ->join('reservedjobs', 'clientjobpost.id', '=', 'reservedjobs.client_jobid')
            ->where('hire.job_status','cancelled')
            ->where('hire.status','4')
            ->select('clientjobpost.id AS JobID', 'clientjobpost.job_heading AS JobHeading', 'clientjobpost.worker_type AS WorkerType','hire.worker_id','clientjobpost.job_description','reservedjobs.id as ProposalId','clientjobpost.created_at AS JobPostDate','users.firstname AS WorkerFirstName','users.lastname AS WorkerLastName','users.id AS Workerid','users.address1','users.address')
            ->orderBy('clientjobpost.id','DESC')
            ->paginate(10);*/

        return view('customernotification.index',compact('applied_worker','hired_worker','complte_job_worker','cancelled_job_worker')) 
                ->with('i', ($request->input('page', 1) - 1) * 10);

        }catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }

    }

      public function show($id)
    {   
      try{
             $client_jobid =  $id;   
             $job_listing=DB::table('notification')
                         ->where(['id'=>$client_jobid,'status_read' => '1'])
                         ->first();
                         
             return view('customernotification.show',compact('job_listing'));
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }

      public function update(Request $request,$id)
    {    
      try{
          $client_id          = Auth::user()->id;
          $client_first_name  = Auth::user()->firstname;
          $client_last_name  = Auth::user()->lastname;
          $client_job_id      = $request->client_job_id;
          $worker_id          = $request->worker_id;
          $proposal_id        = $request->proposal_id;
          $job_status         = $request->job_status;
          $created_at         = date('Y-m-d H:m:i');
          $updated_at         = date('Y-m-d H:m:i');
          $created_by         = $client_id;
          $updated_by         = $client_id;

          $worker_data =DB::table('users')
                         ->where('id','=',$worker_id)
                         ->first(); 
          $job_data =DB::table('clientjobpost')
                         ->where('id','=',$client_job_id)
                         ->first();
          $cat_id = $job_data->job_heading;
          $cat_data =DB::table('categories')
                         ->where('id','=',$cat_id)
                         ->first();
 
          $message ='';
          $message = 'Congulations,'. $client_first_name .'&nbsp has Hired for this job &nbsp'. $cat_data->cat_name;

         $bnk= DB::table('hire')
             ->insertGetId(['client_job_id' =>$client_job_id,'worker_id' =>$worker_id,'proposal_id' =>$proposal_id,'job_status' =>$job_status,'status' =>'2', 'deleted' =>'0','created_at' => $created_at,'updated_at' => $updated_at]);
             
         $bnk= DB::table('notification')
             ->insertGetId(['job_status' =>$job_status,'worker_userid' =>$worker_id ,'client_userid' => $client_id,'client_jobid' => $client_job_id, 'message' => $message, 'created_at' => $created_at,'updated_at' => $updated_at,'created_by' => $created_by,'updated_by' => $updated_by,'status_read' => '0']);

         return redirect()->route('customernotification.index');
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function complete_btn_status_update(Request $request)
    {    
       $complete_status = $request->completed_btn_status;
       $worker_id = $request->user_id;
       $client_job_id = $request->client_job_id;
       //$proposal_id = $request->proposal_id;
       $created_at         = date('Y-m-d H:m:i');
       $updated_at         = date('Y-m-d H:m:i');

       $update_status = DB::table('hire')
                            ->where('worker_id','=',$worker_id)
                            ->where('client_job_id','=',$client_job_id)
                            ->update(['status' => $complete_status,'job_status' => 'completed']);
          /*$bnk= DB::table('hire')
             ->insertGetId(['client_job_id' =>$client_job_id,'worker_id' =>$worker_id,'proposal_id'=>$proposal_id, 'job_status' =>'completed','status' =>$complete_status, 'deleted' =>'0','created_at' => $created_at,'updated_at' => $updated_at]);*/
        
         if($update_status){
          //=============== Notification code 06-02-2019 ==================//
              $created_at = date('Y-m-d H:m:i');
              $clientjobData = DB::table('clientjobpost')->where('id', $client_job_id)->first();
              $job_heading =  $clientjobData->job_heading;
              $jobCategoryData = DB::table('categories')->where('id',$job_heading)->first();
              $jobTitle = $jobCategoryData->cat_name;
              
              $customer_name = Auth::user()->firstname;
              $message = 'Job <strong>'. $jobTitle .'</strong>  completed by <strong>'. $customer_name .'</strong>.';
              $usercreate_notification = DB::table('new_notifications')
                      ->insertGetId([ 'message' => $message, 'created_at' =>$created_at, 'is_for_admin' =>1, 'read_by_admin' => 0, 'message_worker' => $message, 'worker_id' => $worker_id ]); 
            //=============================================================// 


            echo 'true';
         } else {
            echo 'false';
         }
    }
    public function cancelled_btn_status_update(Request $request)
    {    
        $cancelled_status = $request->cancelled_btn_status;
        $worker_id = $request->user_id;
        $client_job_id = $request->client_job_id;
        //$proposal_id = $request->proposal_id;
        $created_at         = date('Y-m-d H:m:i');
        $updated_at         = date('Y-m-d H:m:i');

        $update_status = DB::table('hire')
                            ->where('worker_id','=',$worker_id)
                            ->where('client_job_id','=',$client_job_id)
                            ->update(['status' => $cancelled_status,'job_status' => 'cancelled']);
        /*$bnk= DB::table('hire')
                 ->insertGetId(['client_job_id' =>$client_job_id,'worker_id' =>$worker_id,'proposal_id'=>$proposal_id,'job_status' =>'cancelled','status' =>$cancelled_status, 'deleted' =>'0','created_at' => $created_at,'updated_at' => $updated_at]);*/

         if($update_status){
           //=============== Notification code 06-02-2019 ==================//
              $created_at = date('Y-m-d H:m:i');
              $clientjobData = DB::table('clientjobpost')->where('id', $client_job_id)->first();
              $job_heading =  $clientjobData->job_heading;
              $jobCategoryData = DB::table('categories')->where('id',$job_heading)->first();
              $jobTitle = $jobCategoryData->cat_name;
              
              $customer_name = Auth::user()->firstname;
              $message = 'Job <strong>'. $jobTitle .'</strong> cancelled by <strong>'. $customer_name .'</strong>.';
              $usercreate_notification = DB::table('new_notifications')
                      ->insertGetId([ 'message' => $message, 'created_at' =>$created_at, 'is_for_admin' =>1, 'read_by_admin' => 0, 'message_worker' => $message, 'worker_id' => $worker_id ]); 
            //=============================================================//          
            echo 'true';
         } else {
            echo 'false';
         }
    }
    public function customer_feedback(Request $request)
    {   
      try{ 
        $rating = $request->rating;
        $feedback_text = $request->feedback_text;
        $client_user_id = $request->client_user_id;
        $worker_user_id = $request->worker_user_id;
        $rev_id = $request->rev_id;
        $cat_id = $request->cat_id;
        $created_at = date('Y-m-d H:m:i');
        $created_by = $request->client_user_id;
        $updated_at = date('Y-m-d H:m:i');
        $updated_by = $request->client_user_id;
        $complete_status = $request->completed_btn_status;

        $bnk= DB::table('feedback')
             ->insertGetId(['cat_id' =>$cat_id,'res_jobid' =>$rev_id,'user_id' =>$client_user_id,'feedback_rating'=>$rating,'feedback_description' =>$feedback_text,'status' =>'1', 'deleted' =>'0','created_by' => $created_by,'created_at' => $created_at,'updated_by' => $updated_by,'updated_at' => $updated_at, 'worker_id' => $worker_user_id ]);

        $update_status = DB::table('hire')
                            ->where('worker_id','=',$worker_user_id)
                            ->where('client_job_id','=',$rev_id)
                            ->update(['status' => $complete_status,'job_status' => 'completed']);

        if($update_status){
          //=============== Notification code 06-02-2019 ==================//
              $created_at = date('Y-m-d H:m:i');
              $clientjobData = DB::table('clientjobpost')->where('id', $rev_id)->first();
              $job_heading =  $clientjobData->job_heading;
              $jobCategoryData = DB::table('categories')->where('id',$job_heading)->first();
              $jobTitle = $jobCategoryData->cat_name;
              
              $customer_name = Auth::user()->firstname;
              $message = 'Job <strong>'. $jobTitle .'</strong> completed by <strong>'. $customer_name .'</strong>.';
              $usercreate_notification = DB::table('new_notifications')
                      ->insertGetId([ 'message' => $message, 'created_at' =>$created_at, 'is_for_admin' =>1, 'read_by_admin' => 0, 'message_worker' => $message, 'worker_id' => $worker_user_id ]); 
            //=============================================================// 
         } 

        return redirect()->route('customernotification.index');
        }
        catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        } 
    }
    //=================== 13-02-2019 ===================//
    public function hourDetails(Request $request){
      try{
      $hire_id = $request->hireid;
      if( isset( $hire_id ) ){
        $hireData =DB::table('hire')
                         ->where('hire.id','=',$hire_id)
                         ->join('clientjobpost', 'clientjobpost.id' ,'=', 'hire.client_job_id')
                         ->join('categories', 'categories.id' ,'=', 'clientjobpost.job_heading')
                         ->first(); 
            //echo'<pre>';
            //print_r($hireData);
            //echo'</pre>';
           return view('customernotification.hourdetails',compact('hireData'));               
          }
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
    public function updateHourStatus(Request $request){
        //echo'working';
         $hireid = $request->hireid;
         $clientjobid = $request->clientjobid;
         $worker_id = $request->worker_id;
         $updatedHour = $request->updatedHour;
         $updatedMints = $request->updatedMints;
         $job_cat = $request->job_cat;

         $update_status = DB::table('hire')
                            ->where('id','=',$hireid)
                            ->update([ 'updated_hours' => $updatedHour, 'updated_mints' => $updatedMints, 'time_approved' => 1 ]);
          //=====================================//                  
          if($update_status){
            echo 'true';
            //============== USER CREATE NOTIFICATION TO ADMIN 07-02-2019 =================//
              $created_at = date('Y-m-d H:m:i');
              $usernametodisplay = Auth::user()->name;
              if($updatedHour !=''){
                $msg = ' updated and approved the time for';
              }else{
                 $msg = ' approved the time for';
              } 
              $__message = '<strong>'. $usernametodisplay .'</strong> '. $msg .' <strong>'. $job_cat .'</strong>';
              //$redirectURL = 'hourdetails?hireid='. urlencode(base64_encode($idToUpdate)); 
              $usercreate_notification = DB::table('new_notifications')
                      ->insertGetId([ 'message' => $__message, 'created_at' =>$created_at, 'is_for_admin' =>1, 'read_by_admin' => 0, 'worker_id' => $worker_id, 'message_worker' => $__message  ]);
              //============================================================================//
         } else {
            echo 'false';
         } 
         //=========================================//
      }
    //======================================//



}
