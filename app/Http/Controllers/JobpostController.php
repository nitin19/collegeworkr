<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;
use Auth;
use Image;
use Validator;
use DB;
use Hash;
use App\Classes\ErrorsClass;

class JobpostController extends Controller
{    

    public function __construct()
    {
       //$this->middleware('auth');
    }


     public function index(Request $request)

    {
      return view('jobpost.index');

     }
    
     public function create()

    {

    }
    public function store()

    {

    }
    public function show($id)

    {

    }
    
   public function edit($id)

    {


    }
    public function update(Request $request,$id)

    {


    }
    public function destroy($id)

    {


    }
    public function jobstore(Request $request)

    {
             $user_id                = $request->userid;
             $job_heading            = $request->job_type;
             $job_description        = $request->job_desc;
             //$address                = $request->address;
             $address1               = $request->address1;
             $job_location           = $request->job_location;
             $worker_type            = $request->worker_type;
             $worker_required        = $request->worker_required;
             $worker_desc            = $request->worker_desc;
             $equipment_needed       = $request->equipment_needed;
             $status                 = '0';
             //$status                 = '1'; //=== commented 11-02-2019 
             $deleted                = '0';
             $created_by             = $request->userid;
             $created_at             = date('Y-m-d H:m:i');
             $updated_by             = $request->userid;
            /* $updated_at             = date('Y-m-d H:m:i');
*/
            //$full_address = $address.','.$address1; // Google HQ
            $full_address = $job_location;
            //$address = 'NJ 07094,USA'; // Google HQ
            $prepAddr = str_replace(' ','+',$full_address);
            $geocode=file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address='".$prepAddr."'&key=AIzaSyAmECi5THi1W3CHDdmC2bI6q6dDZNRAY-I");
            $output= json_decode($geocode);

            $lat = $output->results[0]->geometry->location->lat;
            $long = $output->results[0]->geometry->location->lng;
       
/*
            echo $address.'
            Lat: '.$lat.'
            Long: '.$long;

            exit();*/
   
        $jobpost = DB::table('clientjobpost')->insertGetId(['user_id'=>$user_id, 'job_heading'=>$job_heading, 'job_description'=>$job_description, 'job_location'=>$job_location, 'worker_type'=>$worker_type, 'worker_required'=>$worker_required, 'worker_desc' =>$worker_desc, 'equipment_needed'=>$equipment_needed, 'status'=>$status, 'deleted'=> $deleted, 'created_by'=>$created_by, 'latitude'=>$lat, 'longitude'=>$long]);
  
             if($jobpost){
              //============== USER CREATE NOTIFICATION TO ADMIN 07-02-2019 =================//
              $created_at = date('Y-m-d H:m:i');
              $jobCategoryData = DB::table('categories')->where('id',$job_heading)->first();
              $jobTitle = $jobCategoryData->cat_name;
              $UserData = DB::table('users')->where('id',$user_id)->first();
              $usernametodisplay = $UserData->name;

              $__message = 'New Job <strong>'. $jobTitle .'</strong> posted by <strong>'. $usernametodisplay .'</strong>';
              $usercreate_notification = DB::table('new_notifications')
                      ->insertGetId([ 'message' => $__message, 'created_at' =>$created_at, 'is_for_admin' =>1, 'read_by_admin' => 0]); 
              //============================================================================//

              $to = 'dev2.bdpl@gmail.com';
              $__site_url = url('/');
              $subject = "New Job Posted";
              $message ='';
              $message .= '<html>
                     <head></head>
                     <body>
                    <div class="container" style="width: 500px;padding: 10px;margin-left: 10px;margin-bottom: 10px;border: 1px solid #ccc;">
                       <div class="refralcode_sub_head_sec" style="text-align: center;background-color: #1aa3df;padding: 20px;">
                         <h2 class="refferoffer_head" style="margin-top:0px;margin-bottom:0px;font-size:25px;color:#fff;">Hi Admin</h2>
                         <p class="refralcode_sub_head" style="color:#fff;font-size: 16px;margin-top: 0px;margin-bottom: 0px;">New Job is Posted</strong></p> 
                       </div>
                      <div class="refralcode_head_sec" style="text-align: center;padding: 20px;">
                       <ul>
                         <li style="list-style:none;"><p class="refralcode_head" style="color:#333;font-size: 16px;margin-top: 0px;margin-bottom: 0px;">JOB description: <strong>'.$job_description.'</strong></p></li>
                         <li style="list-style:none;"><p class="refralcode_head" style="color:#333;font-size: 16px;margin-top: 0px;margin-bottom: 0px;">Job Location: <strong>'.$job_location.'</strong></p></li>
                         <ul>
                       </div>
                      
                       <div class="refrallink_sec" style="text-align: center;padding: 20px;">
                         <p class="refrallink_head" style="color:#333;font-size: 22px;margin-top: 0px;margin-bottom: 0px;font-weight: bold;"></p>
                         <div class="refral_link_btn_sec" style="margin-top: 15px;"><a class="refral_btn" href="'. $__site_url .'" style="background-color: #1aa3df;padding: 10px 25px 12px 25px;color: #fff;text-decoration: none;font-size: 18px;font-weight: bold;border-radius: 5px;">Go to site.. </a></div>
                       </div>
                    </div>
                    </body>
                    </html>';
        $headers = "From: info@collegewrk.com" . "\r\n" ."CC: dev2.bdpl@gmail.com";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        $send_mail = mail($to,$subject,$message,$headers);


              //==============================================================//
              $upQry = DB::table('users')
                       ->where('id', $user_id)
                       ->update(['address' => $job_location, 'address1' => $address1,'workertype' => $worker_type,'updated_by' => $updated_by,  'latitude' => $lat, 'longitude' => $long]);

                echo $jobpost;

             }else{
                echo "false";
             }


     }
    


    
    public function updatejob(Request $request)

    {
        $user_id                = $request->userid;
        $id                     = $request->jobid;
        $start_date             = date("Y-m-d", strtotime($request->start_date));
        $start_time             = $request->start_time;
        $time_estimate          = $request->time_estimate;
        $scheduling_details     = $request->scheduling_details;
        $additional_info        = $request->additional_info;
        $updated_by             = $request->userid;
        $updated_at             = date('Y-m-d H:m:i');



        $upQry = DB::table('clientjobpost')
                ->where('id', $id)
                ->update(['start_date' => $start_date, 'start_time' => $start_time, 'time_estimate' => $time_estimate, 'scheduling_details' => $scheduling_details, 'additional_info' => $additional_info, 'updated_by' => $updated_by, 'updated_at' => $updated_at]);

        if($upQry){
        /* $data          = DB::table('clientjobpost')
                          ->where(['id'=>$id,'user_id'=>$user_id,'status'=>'1','deleted'=>'0'])
                          ->first(); */

        $data          = DB::table('clientjobpost')
                          ->where(['id'=>$id,'user_id'=>$user_id, 'deleted'=>'0'])
                          ->first(); 

         $equ_ids       = $data->equipment_needed;
       
         $equipmentids  = explode(',', $equ_ids);
         $equipmentamount = 0;
         if($equ_ids != ''){ 
         foreach ($equipmentids as $equipmentid) {
            $equipments = DB::table('equipment')
                          ->where('id', $equipmentid)
                          ->first();
 
            $equipmentamount = $equipmentamount + $equipments->equ_price;
          }
        } 
        $data->equipmentamount = $equipmentamount;
        echo json_encode($data);
        }else{
        echo "false";
        }

    }

    public function getequipment(Request $request)

    {
        $cat_id = $request->cat_id;
        $equipments = DB::table('equipment')
                        ->where(['cat_id'=>$cat_id,'status'=>'1','deleted'=>'0'])
                        ->get();
        $html = '';
        if(count($equipments)>0){
        $html .= '<div class="form-group">
                  <label>Will any equipment be needed? (optional)</label>
                  <p> We do not provide moving trucks. We recommend renting a
                        moving truck and we can supply the labor for loading and
                        unloading. CollegeWRK can provide personal pick-up trucks 
                        forsmaller moves.
                  </p>
                  <ul class="list-signup">';

        foreach ($equipments as $equipment) {
                    
        $html.='<li>
                    <div class="sgn-ls">
                    <h5> <input type="checkbox" name="check_list[]" value="'.$equipment->id.'" class="che_sec">'.$equipment->equ_name.'<span>($'.$equipment->equ_price.')</span></h5>
                    <h6>'.$equipment->equ_description.'</h6>
                    </div>
                </li>';
        }

        $html.='</ul>
                </div>'; 
     
     }                  
        echo $html;

    }

    public function checkrefferalcode(Request $request)

    {
        $referralcode = $request->referralcode;
        $clientpay = $request->clientpay;
        $refferal_total_amount = $clientpay - 20;

         $referralcodedata = DB::table('users')
                      ->where('invitecode', '=', $referralcode)
                      ->where('refferal_code_status', '=', 0)
                      ->where('status', '=', 1)
                      ->where('deleted', '=', 0)
                      ->first();

         
        if($referralcodedata){
            echo json_encode(array('check_status'=>"2",'refferal_total_amount'=>$refferal_total_amount));
        } else {
            echo json_encode(array('check_status'=>"3"));
        }
    }

  public function reciept(Request $request){
    
   if(!empty($_GET['stripeToken'])){

    //get token, card and user info from the form
    $token  = $_GET['stripeToken'];
    $user_id = $_GET['userid'];
    $client_jobid = $_GET['lastjobid'];
    $name = $_GET['paymentname'];
    $email = $_GET['paymentemail'];
    $card_num = $_GET['card_num'];
    $card_cvc = $_GET['cvc'];
    $card_exp_month = $_GET['exp_month'];
    $card_exp_year = $_GET['exp_year'];
    
    //include Stripe PHP library
    require_once('stripe-php/init.php');
    
    //set api key
    $stripe = array(
      "secret_key"      => env('STRIPE_SEC_KEY'),
      "publishable_key" => env('STRIPE_PUB_KEY')
    );
    
    \Stripe\Stripe::setApiKey($stripe['secret_key']);
    
    //add customer to stripe
    $customer = \Stripe\Customer::create(array(
        'email' => $email,
        'source'  => $token
    ));
    
    //item information
    $itemName = "CollegeWRK Client Payment";
    $itemNumber = "PS123456";
    $itemPrice = $_GET['clientpay'];
    $currency = "usd";
    $orderID = "SKA92712382139";
    
    //charge a credit or a debit card
    $charge = \Stripe\Charge::create(array(
        'customer' => $customer->id,
        'amount'   => $itemPrice,
        'currency' => $currency,
        'description' => $itemName,
        'metadata' => array(
        'order_id' => $orderID
        )
    ));
    
    //retrieve charge details
    $chargeJson = $charge->jsonSerialize();

    //check whether the charge is successful
    if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1){
        //order details 
        $amount = $chargeJson['amount'];
        $balance_transaction = $chargeJson['balance_transaction'];
        $currency = $chargeJson['currency'];
        $status = $chargeJson['status'];
        $date = date("Y-m-d H:i:s");

        $last_insert_id = DB::table('client_payment')->insertGetId(['user_id'=> $user_id, 'client_jobid'=> $client_jobid,'name' =>$name,'email' => $email, 'card_num' => $card_num, 'card_cvc' => $card_cvc, 'card_exp_month' => $card_exp_month,'card_exp_year' => $card_exp_year,'item_name' => $itemName, 'item_number' => $itemNumber, 'item_price' => $itemPrice ,'item_price_currency' => $currency ,'paid_amount' => $amount ,'paid_amount_currency' => $currency ,'txn_id' =>$balance_transaction ,'payment_status' => $status ,'created' => $date ,'modified' => $date]);
        if($last_insert_id){
          $refferal_code_status = DB::table('users')
                                  ->where('id', '=',$user_id)
                                  ->update(['refferal_code_status' => 1]);
        }
        //if order inserted successfully
       if($last_insert_id && $status == 'succeeded'){

                 $clientpayment= DB::table('client_payment')->where('id', $last_insert_id)->first();

                 $client_jobid = $clientpayment->client_jobid;
                //==============================================//
                 $updateJobStatus = DB::table('clientjobpost')->where('id', $client_jobid)->update(['status' => 1 ]); 
                //==============================================//


                 $clientJobAddr = DB::table('clientjobpost')->where('id', $client_jobid)->first();

                 $circle_radius = '3959';
                 $max_distance = '50';
                 $job_lat = $clientJobAddr->latitude;
                 $job_long = $clientJobAddr->longitude;
/*                 $cat_id = $clientJobAddr->job_heading;
                 $cat_data = DB::table('categories')->where('id', $cat_id)->first();
                 $cat_name = $cat_data->cat_name;*/

                 
                $worker_users = DB::select(
                   'SELECT * FROM
                        (SELECT id, name, firstname, lastname, email, hdpswd, invitecode, latitude, longitude, (' . $circle_radius . ' * acos(cos(radians(' . $job_lat . ')) * cos(radians(latitude)) *
                        cos(radians(longitude) - radians(' . $job_long . ')) +
                        sin(radians(' . $job_lat . ')) * sin(radians(latitude))))
                        AS distance
                        FROM users WHERE user_role = "worker") AS distances
                    WHERE distance < ' . $max_distance . '
                    ORDER BY distance'
                );

                foreach ($worker_users as $worker_users_data) {
                   $worker_name = $worker_users_data->name;
                   $worker_email = $worker_users_data->email;
                   $job_last_insert_id = $last_insert_id;
                   $clientpayment= DB::table('client_payment')->where('id', $job_last_insert_id)->first();
                   $client_jobid = $clientpayment->client_jobid;
                   $job_amount = $clientpayment->paid_amount;
                   $jobdata= DB::table('clientjobpost')->where('id', $client_jobid)->first();
                   $cat_id = $jobdata->job_heading;
                   $job_location = $jobdata->job_location;
                   $catdata= DB::table('categories')->where('id', $cat_id)->first();
                   $cat_name = $catdata->cat_name;
                   //$last_id = $user->id;
                   $user_first_name = $worker_users_data->firstname;
                   $user_email = $worker_users_data->email;
                   $user_pass = $worker_users_data->hdpswd;
                   $user_invitecode = $worker_users_data->invitecode;
                   $site_url = url('/');
                   $logo_url = url('/public').'/admin/images/newlogo.png';
                   $mail_bg_img = url('/public').'/images/bg.png';
                   $email_icon = url('/public').'/images/email-icon.png';
                   $phone_img = url('/public').'/images/phone1.png';
                   $mail_icon = url('/public').'/images/mail.png';
                   $thankyouimg = url('/public').'/images/thank-you-img.png';

                   $message = '';
                   $to = $user_email;
                   $subject = "Today's Jobs for you";
                   $message = '<html>
                               <head>
                                <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
                                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
                              </head>
                              <body>
                              <div class="mail_background" style="background-color: #d2effd;padding: 40px 0px 0px 0;">
                                <div class="container">
                                    <div class="mail_back_img" style="background-image: url('.$mail_bg_img.') ;background-size: contain;background-position: top;background-repeat: no-repeat;padding: 0px 0px 0px 0px;">
                                        <div class="" style="width: 500px;margin: 0 auto;">
                                          <div class="row">
                                            <div class="" style="text-align: center;padding-bottom: 40px;padding-top: 45px;">
                                              <img style="width:150px" src="'.$logo_url.'" alt="Logo">
                                            </div>
                                            <div class="" style="text-align: center;padding-bottom: 0px;">
                                              <img style="width:60px" src='.$email_icon.'>
                                            </div>
                                            <div class="" style="text-align: center;">
                                              <h6 style="font-size:20px;font-weight:500;margin-top: 8px;margin-bottom: 0px;letter-spacing: 1px;"><span style="color:#1ba4e0">Hey </span>'.$user_first_name.', </h6>  
                                            </div>
                                            <div class="" style="text-align: center;padding: 15px 0px;">
                                              <img style="width: 230px;" src='.$thankyouimg.'>
                                            </div>
                                            <div class="" style="text-align: center;padding-bottom: 20px;">
                                              <h5 style="font-size:20px;font-weight:400;margin-bottom: 0px;margin-top: 0px;">For Choosing College Wrk</h5>
                                              <p style="font-size:14px;font-weight:400;margin:0px;">It is always our endeavor to provide you best-in-class service</p>
                                              <div class="job_details" style="padding: 10px 35px 0px 35px;text-align: left;">
                                                   <div style="width: 50%;float: left;padding: 5px 45px 5px 15px;font-size: 18px;border-right: 2px solid #12b6d9;"><strong>' . $cat_name .', ' . $job_location .'</div>
                                                   <div style="text-align: center;padding: 0px 0 0 0px;font-size: 25px;color: #12b6d9;"><strong> $' . $job_amount . '</div>
                                               </div>
                                               <p style="font-size:14px;font-weight:400;margin:0px;">Get more jobs click on this button and login into your account.</p>
                                            </div>
                                            <div class="" style="text-align: center;padding-bottom: 10px">
                                              <a href="#" style="background-color: #f3721f;padding: 7px 90px;color: #fff;font-size: 16px;font-weight: 400;text-transform: uppercase;text-decoration: none;display: inline-block;border-radius: 6px;">Start journey</a>
                                            </div>
                                            
                                            <div class="" style="text-align: center;padding-top: 5px;padding-bottom: 10px;">
                                              <p style="font-size:12px;font-weight:400;margin: 0px;">In case of any questions, please feel free to reach us on</p>
                                            </div>
                                            <div class="row" style="overflow:hidden;margin:0 auto;transform: translate(9%);padding-bottom: 20px;padding-left: 60px;">
                                              <div class="col-sm-6">
                                                  <div style="margin-right: 20px;background-color: #fcddc9;padding: 10px;border-left: 1px solid #f3721f;    border-bottom: 1px solid #f3721f;border-radius: 5px;float: left;width: 36%;">
                                                    <div style="float: left;width: 15%;margin-right: 20px;">
                                                      <img style="width: 100%;" src='.$phone_img.'>
                                                    </div>
                                                    <div style="float: left;width: 70%;">
                                                      <a href="tell:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:10px;">Call us at</a> 
                                                      <a href="tell:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:12px;">1800–419–7713</a>
                                                    </div>
                                                  </div>
                                              </div>
                                              <div class="col-sm-6">
                                                  <div style="padding: 10px;background-color: #c8e9f8;border-left: 1px solid #1ba4e0;border-bottom: 1px solid #1ba4e0;border-radius: 5px;float: left;width: 36%;">
                                                      <div style="float: left;width:15%;margin-right:20px;">
                                                          <img style="width:100%" src='.$mail_icon.'>
                                                      </div>
                                                      <div style="float: left;width: 70%;">
                                                          <a style="display: block;font-size:10px;" href="mailto:info@collegewrk.com" target="_top">Email us at</a>
                                                          <a  style="display: block;font-size:12px;" href="mailto:info@collegewrk.com" target="_top">info@collegewrk.com</a>
                                                      </div>
                                                  </div>
                                              </div>
                                        </div>
                                 </div>
                            </div>
                      </div>
                      <div class="" style="margin: -10px auto 0px auto;text-align: center;background-color:#e5e5e5;border-top:1px solid #ccc;border-bottom:1px solid #ccc;padding: 5px 0px;width:485px;margin: -6px auto 0px;">
                          <p style="font-size:11px;font-weight:bold;margin:0px;">Team College Wrk</p>
                          <p style="text-align:right;margin: 0px 10px 0px 0px;font-size: 8px;font-weight: bold;">Designed  by : <a href="http://www.binarydata.in/" target="_blank">Binary Data</a></p>
                      </div>
                    </div>
                </div>
            </body>
        </html>';

                $headers = "From: Collegeworker@gmail.com" . "\r\n" .
                "CC: dev1.bdpl@gmail.com";
                $headers .= "MIME-Version: 1.0\r\n";
                $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
                mail($to,$subject,$message,$headers);

                   /*$txt = '';
                   $to = $worker_email;
                   $subject = "Today's Jobs For You";
                   $txt = "Hello Worker";
                   $headers = "From: binarydata2017@gmail.com" . "\r\n" .
                              "CC: dev1.bdpl@gmail.com";

                   mail($to,$subject,$txt,$headers);*/

                }

              echo "true";
            //$statusMsg = "<h2>The transaction was successful.</h2><h4>Order ID: {$last_insert_id}</h4>";
        }else{
            echo "false";
          //  $statusMsg = "Transaction has been failed";
        }
    }else{
       echo "Transaction has been failed";
    }
}else{
    echo "Form submission error.......";
}

}
 public function notitysec(Request $request)
    {
         $notify_id = $request->noti;
         $field_name = $request->field_name;

         $count_total = DB::table('new_notifications')
                      ->where('notification_id', '=',$notify_id)
                      ->update([$field_name => 1]);   
                           
        if($count_total){
            echo 'true';
        } else {
            echo 'false';
        }

    }
 /********************************/
 public function readAllNotifications(Request $request)
    {
         $user_id = $request->user_id;
         $field_name = $request->field_name;
         $user_field = $request->user_field;
         if( $user_field =='' ){
          $count_total = DB::table('new_notifications')
                      ->where('is_for_admin', '=',1)
                      ->update([$field_name => 1]);   
         }else{
            $count_total = DB::table('new_notifications')
                      ->where( $user_field , '=', $user_id )
                      ->update([$field_name => 1]);
         }
            
                           
        if($count_total){
            echo 'true';
        } else {
            echo 'false';
        }

    }   
 /********************************/   
/*public function workernotitysec(Request $request)
    {
         $notify_id = $request->noti;

         $count_total = DB::table('notification')
                      ->where('id', '=',$notify_id)
                      ->update(['status_read' => 1]);   
                           
        if($count_total){
            echo 'true';
        } else {
            echo 'false';
        }

    }*/

public function createUserStripe(Request $request){

  //===============================//
$stripe_token = $request->stripe_token;
//$stripe_data = $request->stripe_data;
$userid = $request->userid;
$lastjobid = $request->lastjobid;
$paymentname = $request->paymentname;
$paymentemail = $request->paymentemail;
$hear_from = $request->hear_from; 
$promo_code = $request->promo_code;
$totalamt = $request->totalamt; 
//================== CREATE USER ON STRIPE =====================//
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'https://api.stripe.com/v1/customers');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, "source=". $stripe_token ."&email=". $paymentemail );
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_USERPWD, env('STRIPE_SEC_KEY') . ':' . '');

$headers = array();
$headers[] = 'Content-Type: application/x-www-form-urlencoded';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
if (curl_errno($ch)) {
    //echo 'Error:' . curl_error($ch);
  echo'error';
}
//===========================================================//
  curl_close ($ch);
  //echo'<pre>';
  //print_r(json_decode( $result ));  
  //echo'</pre>'; 
  $result =  json_decode( $result );
  if( array_key_exists('error', $result) ){
    echo'error';
  }else{
    $customer_id =  $result->id;
    $last_digit_cc =  $result->sources->data[0]->last4;
    $last_insert_id = DB::table('user_payment_credentials')->insertGetId(['user_id'=> $userid, 'last_digit_cc' => $last_digit_cc, 'stripe_id' => $customer_id, 'created_at' => date('Y-m-d h:i:s') ]);
    //======================//
    if( $last_insert_id ){
        $refferal_code_status = DB::table('users')
                                  ->where('id', '=',$userid)
                                  ->update(['refferal_code_status' => 1]);
        //===================================//
        $updateJobStatus = DB::table('clientjobpost')->where('id', $lastjobid)->update(['status' => 1 , 'budget'=> $totalamt]);
        $client_jobid = $lastjobid;

        $clientJobAddr = DB::table('clientjobpost')->where('id', $client_jobid)->first();

                 $circle_radius = '3959';
                 $max_distance = '50';
                 $job_lat = $clientJobAddr->latitude;
                 $job_long = $clientJobAddr->longitude;  
                 $job_amount = $clientJobAddr->budget;
                 $cat_id = $clientJobAddr->job_heading;  
                 $catdata= DB::table('categories')->where('id', $cat_id)->first();
                 $cat_name = $catdata->cat_name;
                 $job_location = $clientJobAddr->job_location;
                 $worker_users = DB::select(
                   'SELECT * FROM
                        (SELECT id, name, firstname, lastname, email, hdpswd, invitecode, latitude, longitude, (' . $circle_radius . ' * acos(cos(radians(' . $job_lat . ')) * cos(radians(latitude)) *
                        cos(radians(longitude) - radians(' . $job_long . ')) +
                        sin(radians(' . $job_lat . ')) * sin(radians(latitude))))
                        AS distance
                        FROM users WHERE user_role = "worker") AS distances
                    WHERE distance < ' . $max_distance . '
                    ORDER BY distance'
                );

            //==========================//
               //=================================//
                 if(!empty($worker_users)):
                 foreach ($worker_users as $worker_users_data) {
                   $worker_name = $worker_users_data->name;
                   $worker_email = $worker_users_data->email;
                   //$job_last_insert_id = $lastjobid;
                   //$clientpayment= DB::table('client_payment')->where('id', $job_last_insert_id)->first();
                   //$client_jobid = $clientpayment->client_jobid;
                   //$job_amount = $clientpayment->paid_amount;
                   //$jobdata= DB::table('clientjobpost')->where('id', $client_jobid)->first();
                   //$cat_id = $jobdata->job_heading;
                   //$job_location = $jobdata->job_location;
                   //$catdata= DB::table('categories')->where('id', $cat_id)->first();
                   //$cat_name = $catdata->cat_name;
                   //$last_id = $user->id;
                   $user_first_name = $worker_users_data->firstname;
                   $user_email = $worker_users_data->email;
                   $user_pass = $worker_users_data->hdpswd;
                   $user_invitecode = $worker_users_data->invitecode;
                   $site_url = url('/');
                   $logo_url = url('/public').'/admin/images/newlogo.png';
                   $mail_bg_img = url('/public').'/images/bg.png';
                   $email_icon = url('/public').'/images/email-icon.png';
                   $phone_img = url('/public').'/images/phone1.png';
                   $mail_icon = url('/public').'/images/mail.png';
                   $thankyouimg = url('/public').'/images/thank-you-img.png';

                   $message = '';
                   $to = $user_email;
                   $subject = "Today's Jobs for you";
                   $message = '<html>
                               <head>
                                <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
                                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
                              </head>
                              <body>
                              <div class="mail_background" style="background-color: #d2effd;padding: 40px 0px 0px 0;">
                                <div class="container">
                                    <div class="mail_back_img" style="background-image: url('.$mail_bg_img.') ;background-size: contain;background-position: top;background-repeat: no-repeat;padding: 0px 0px 0px 0px;">
                                        <div class="" style="width: 500px;margin: 0 auto;">
                                          <div class="row">
                                            <div class="" style="text-align: center;padding-bottom: 40px;padding-top: 45px;">
                                              <img style="width:150px" src="'.$logo_url.'" alt="Logo">
                                            </div>
                                            <div class="" style="text-align: center;padding-bottom: 0px;">
                                              <img style="width:60px" src='.$email_icon.'>
                                            </div>
                                            <div class="" style="text-align: center;">
                                              <h6 style="font-size:20px;font-weight:500;margin-top: 8px;margin-bottom: 0px;letter-spacing: 1px;"><span style="color:#1ba4e0">Hey </span>'.$user_first_name.', </h6>  
                                            </div>
                                            <div class="" style="text-align: center;padding: 15px 0px;">
                                              <img style="width: 230px;" src='.$thankyouimg.'>
                                            </div>
                                            <div class="" style="text-align: center;padding-bottom: 20px;">
                                              <h5 style="font-size:20px;font-weight:400;margin-bottom: 0px;margin-top: 0px;">For Choosing College Wrk</h5>
                                              <p style="font-size:14px;font-weight:400;margin:0px;">It is always our endeavor to provide you best-in-class service</p>
                                              <div class="job_details" style="padding: 10px 35px 0px 35px;text-align: left;">
                                                   <div style="width: 50%;float: left;padding: 5px 45px 5px 15px;font-size: 18px;border-right: 2px solid #12b6d9;"><strong>' . $cat_name .', ' . $job_location .'</div>
                                                   <div style="text-align: center;padding: 0px 0 0 0px;font-size: 25px;color: #12b6d9;"><strong> $' . $job_amount . '</div>
                                               </div>
                                               <p style="font-size:14px;font-weight:400;margin:0px;">Get more jobs click on this button and login into your account.</p>
                                            </div>
                                            <div class="" style="text-align: center;padding-bottom: 10px">
                                              <a href="#" style="background-color: #f3721f;padding: 7px 90px;color: #fff;font-size: 16px;font-weight: 400;text-transform: uppercase;text-decoration: none;display: inline-block;border-radius: 6px;">Start journey</a>
                                            </div>
                                            
                                            <div class="" style="text-align: center;padding-top: 5px;padding-bottom: 10px;">
                                              <p style="font-size:12px;font-weight:400;margin: 0px;">In case of any questions, please feel free to reach us on</p>
                                            </div>
                                            <div class="row" style="overflow:hidden;margin:0 auto;transform: translate(9%);padding-bottom: 20px;padding-left: 60px;">
                                              <div class="col-sm-6">
                                                  <div style="margin-right: 20px;background-color: #fcddc9;padding: 10px;border-left: 1px solid #f3721f;    border-bottom: 1px solid #f3721f;border-radius: 5px;float: left;width: 36%;">
                                                    <div style="float: left;width: 15%;margin-right: 20px;">
                                                      <img style="width: 100%;" src='.$phone_img.'>
                                                    </div>
                                                    <div style="float: left;width: 70%;">
                                                      <a href="tell:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:10px;">Call us at</a> 
                                                      <a href="tell:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:12px;">1800–419–7713</a>
                                                    </div>
                                                  </div>
                                              </div>
                                              <div class="col-sm-6">
                                                  <div style="padding: 10px;background-color: #c8e9f8;border-left: 1px solid #1ba4e0;border-bottom: 1px solid #1ba4e0;border-radius: 5px;float: left;width: 36%;">
                                                      <div style="float: left;width:15%;margin-right:20px;">
                                                          <img style="width:100%" src='.$mail_icon.'>
                                                      </div>
                                                      <div style="float: left;width: 70%;">
                                                          <a style="display: block;font-size:10px;" href="mailto:info@collegewrk.com" target="_top">Email us at</a>
                                                          <a  style="display: block;font-size:12px;" href="mailto:info@collegewrk.com" target="_top">info@collegewrk.com</a>
                                                      </div>
                                                  </div>
                                              </div>
                                        </div>
                                 </div>
                            </div>
                      </div>
                      <div class="" style="margin: -10px auto 0px auto;text-align: center;background-color:#e5e5e5;border-top:1px solid #ccc;border-bottom:1px solid #ccc;padding: 5px 0px;width:485px;margin: -6px auto 0px;">
                          <p style="font-size:11px;font-weight:bold;margin:0px;">Team College Wrk</p>
                          <p style="text-align:right;margin: 0px 10px 0px 0px;font-size: 8px;font-weight: bold;">Designed  by : <a href="http://www.binarydata.in/" target="_blank">Binary Data</a></p>
                      </div>
                    </div>
                </div>
            </body>
        </html>';

                $headers = "From: Collegeworker@gmail.com" . "\r\n" .
                "CC: dev1.bdpl@gmail.com";
                $headers .= "MIME-Version: 1.0\r\n";
                $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
                mail($to,$subject,$message,$headers);
            }
          endif;
        //=================================//
            //==========================//
     echo'success';                         
    }else{
      echo'error';
    } 
  //=================//
  }
 //===================// 
  }    
  //===============================//
public function updatePostStatus(Request $request){

//===============================//

$userid = $request->userid;
$lastjobid = $request->lastjobid;
$paymentname = $request->paymentname;
$paymentemail = $request->paymentemail;
$hear_from = $request->hear_from; 
$promo_code = $request->promo_code;
$totalamt = $request->totalamt; 

 $updateJobStatus = DB::table('clientjobpost')->where('id', $lastjobid)->update(['status' => 1, 'budget'=> $totalamt]);
 if( $updateJobStatus ){
  echo'success';
 }else{
  echo'error';
 }
}


  //===============================//
}