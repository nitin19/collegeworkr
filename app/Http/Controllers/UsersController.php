<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Image;
use Validator;
use DB;
use Hash;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use App\Http\Controllers\Redirect;

class UsersController extends Controller
{    
    public function __construct()
    {
       //$this->middleware('auth');
    }

     protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
        ]);
    }

     public function index(Request $request)

    {
      

     }
    
     public function create()

    {

 /*       return view('users.create');
*/
    }
    public function store(Request $request)

    {
      try{

             $firstname                 = $request->firstname;
             $lastname                  = $request->lastname;
             $email                     = $request->email;
             $password                  = bcrypt($request->password);
             $hdpswd                    = $request->password;
             $invitecode                = $firstname.rand();
             $user_role                 = $request->user_role;
             $address                   = $request->address;
             $address2                  = $request->address2;

            $full_address = $address.','.$address2; // Google HQ
            //$address = 'NJ 07094,USA'; // Google HQ
            $prepAddr = str_replace(' ','+',$full_address);
            $geocode=file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address='".$prepAddr."'&key=AIzaSyAmECi5THi1W3CHDdmC2bI6q6dDZNRAY-I");
            $output= json_decode($geocode);

            $lat = $output->results[0]->geometry->location->lat;
            $long = $output->results[0]->geometry->location->lng;


            $user = User::create(['name' =>$firstname, 'firstname' =>$firstname,'lastname' => $lastname, 'email' => $email, 'password' => $password, 'hdpswd' => $hdpswd, 'invitecode' => $invitecode,'user_role' => $user_role, 'address' => $address, 'address1' => $address2, 'latitude' => $lat, 'longitude' => $long]);
            Auth::login($user);
            $last_id = $user->id;
            $user_first_name = $user->firstname;
            $user_email = $user->email;
            $user_pass = $user->hdpswd;
            $user_invitecode = $user->invitecode;
            $site_url = url('/');
            $logo_url = url('/public').'/admin/images/newlogo.png';
            $mail_bg_img = url('/public').'/images/bg.png';
            $email_icon = url('/public').'/images/email-icon.png';
            $phone_img = url('/public').'/images/phone1.png';
            $mail_icon = url('/public').'/images/mail.png';
            $thankyouimg = url('/public').'/images/thank-you-img.png';
            if($last_id)
            {
              //============== USER CREATE NOTIFICATION TO ADMIN 07-02-2019 =================//
              $created_at             = date('Y-m-d H:m:i');
              $__message = 'New worker <strong>'. $user_first_name .'</strong> registered';
              $usercreate_notification = DB::table('new_notifications')
                      ->insertGetId([ 'message' => $__message, 'created_at' =>$created_at, 'is_for_admin' =>1, 'read_by_admin' => 0]); 
              //============================================================================//
                $message = '';
                $to = $user_email;
                $subject = "Worker Registration";
                $message = '<html>
                            <head>
                              <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
                              <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
                            </head>
                            <body>
                            <div class="mail_background" style="background-color: #d2effd;padding: 40px 0px 0px 0;">
                                <div class="container">
                                    <div class="mail_back_img" style="background-image: url('.$mail_bg_img.') ;background-size: contain;background-position: top;background-repeat: no-repeat;padding: 0px 0px 0px 0px;">
                                        <div class="" style="width: 500px;margin: 0 auto;">
                                          <div class="row">
                                            <div class="" style="text-align: center;padding-bottom: 40px;padding-top: 45px;">
                                              <img style="width:150px" src="'.$logo_url.'" alt="Logo">
                                            </div>
                                            <div class="" style="text-align: center;padding-bottom: 0px;">
                                              <img style="width:60px" src='.$email_icon.'>
                                            </div>
                                            <div class="" style="text-align: center;">
                                              <h6 style="font-size:20px;font-weight:500;margin-top: 8px;margin-bottom: 0px;letter-spacing: 1px;"><span style="color:#1ba4e0">Hey</span>'.$user_first_name.', </h6>  
                                            </div>
                                            <div class="" style="text-align: center;padding: 15px 0px;">
                                              <img style="width: 230px;" src='.$thankyouimg.'>
                                            </div>
                                            <div class="" style="text-align: center;padding-bottom: 20px;">
                                              <h5 style="font-size:20px;font-weight:400;margin-bottom: 0px;margin-top: 0px;">For Choosing College Wrk</h5>
                                              <p style="font-size:14px;font-weight:400;margin:0px;">It is always our endeavor to provide you best-in-class service</p>
                                            </div>
                                            <div class="" style="text-align: center;padding-bottom: 10px">
                                              <a href="#" style="background-color: #f3721f;padding: 7px 90px;color: #fff;font-size: 16px;font-weight: 400;text-transform: uppercase;text-decoration: none;display: inline-block;border-radius: 6px;">Start journey</a>
                                            </div>
                                            
                                            <div class="" style="text-align: center;padding: 0px 0px 0px 0px;">
                                              <p style="font-size:14px;font-weight:400;margin-top: 10px;margin-bottom: 0px;">Find referral code below to refer your Friend</p>
                                            </div>
                                            <div class="" style="text-align: center;padding-bottom: 0px;">
                                              <h2 style="font-size:24px;font-weight:600;margin-top: 0px;margin-bottom: 0px"><span style="color:#f3721f;">College</span> <span style="color:#1ba4e0;">Wrk</span></h2>
                                            </div>
                                            <div class="" style="text-align: center;max-width: 500px;margin: 0 auto;padding-bottom: 0px;">
                                              <p style="font-size:12px;font-weight:500;color:#1ba4e0;margin: 5px;">Your Referral Code:</p>
                                              <p style="border-top: 1px solid #ccc;border-bottom: 1px solid #ccc;padding: 4px 0px;    border-radius: 10px;font-size: 13px;color: #5d5d5d;width: 250px;margin: 0 auto;">'.$user_invitecode.'</p>
                                            </div>
                                            
                                            <div class="" style="text-align: center;padding-top: 5px;padding-bottom: 10px;">
                                              <p style="font-size:12px;font-weight:400;margin: 0px;">In case of any questions, please feel free to reach us on</p>
                                            </div>
                                            <div class="row" style="overflow:hidden;margin:0 auto;transform: translate(9%);padding-bottom: 20px;padding-left: 60px;">
                                              <div class="col-sm-6">
                                                  <div style="margin-right: 20px;background-color: #fcddc9;padding: 10px;border-left: 1px solid #f3721f;    border-bottom: 1px solid #f3721f;border-radius: 5px;float: left;width: 36%;">
                                                    <div style="float: left;width: 15%;margin-right: 20px;">
                                                      <img style="width: 100%;" src='.$phone_img.'>
                                                    </div>
                                                    <div style="float: left;width: 70%;">
                                                      <a href="tell:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:10px;">Call us at</a> 
                                                      <a href="tell:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:12px;">1800–419–7713</a>
                                                    </div>
                                                  </div>
                                              </div>
                                              <div class="col-sm-6">
                                                  <div style="padding: 10px;background-color: #c8e9f8;border-left: 1px solid #1ba4e0;border-bottom: 1px solid #1ba4e0;border-radius: 5px;float: left;width: 36%;">
                                                      <div style="float: left;width:15%;margin-right:20px;">
                                                          <img style="width:100%" src='.$mail_icon.'>
                                                      </div>
                                                      <div style="float: left;width: 70%;">
                                                          <a style="display: block;font-size:10px;" href="mailto:info@collegewrk.com" target="_top">Email us at</a>
                                                          <a  style="display: block;font-size:12px;" href="mailto:info@collegewrk.com" target="_top">info@collegewrk.com</a>
                                                      </div>
                                                  </div>
                                              </div>
                                        </div>
                                 </div>
                            </div>
                      </div>
                      <div class="" style="text-align: center;background-color:#e5e5e5;border-top:1px solid #ccc;border-bottom:1px solid #ccc;padding: 5px 0px;width:485px;margin: -6px auto 0px;">
                          <p style="font-size:11px;font-weight:bold;margin:0px;">Team College Wrk</p>
                          <p style="text-align:right;margin: 0px 10px 0px 0px;font-size: 8px;font-weight: bold;">Designed  by : <a href="http://www.binarydata.in/" target="_blank">Binary Data</a></p>
                      </div>
              </div>
      </div>
  </body>
</html>';
                $headers = "From: info@collegewrk.com" . "\r\n" .
                "CC: info@collegewrk.com";
                $headers .= "MIME-Version: 1.0\r\n";
                $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
                mail($to,$subject,$message,$headers);

            } 
            
            return redirect()-> route('workerjobs.index');
              /*return redirect()-> route('workerdashboard.index')
                               -> with('success','Thank you for your registration! Your account is now ready to use');
*/
              }catch(\Illuminate\Database\QueryException $e){
                $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
        
        // $errorClass = new ErrorClass();
       
        // $errors = $errorClass->saveErrors($e);

        }catch(\Exception $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));

            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }

     }
    

    public function show($id)

    {

    }
    
   public function edit($id)

    {


    }
    
    public function update(Request $request, $id)

    {

   
    }


   public function storeclient(Request $request)

    {
     try{
       
        $firstname                 = $request->firstname;
        $lastname                  = $request->lastname;
        $email                     = $request->email;
        $password                  = bcrypt($request->password);
        $hdpswd                    = $request->password;
        $invitecode                = $firstname.rand();
        $user_role                 = $request->user_role;
        
        $user = User::create(['name'=>$firstname, 'firstname' =>$firstname,'lastname' => $lastname, 'email' => $email, 'password' => $password, 'hdpswd' => $hdpswd,'invitecode' => $invitecode,'user_role' => $user_role]);
        $last_id = $user->id;
        $user_first_name = $user->firstname;
        $user_email = $user->email;
        $user_pass = $user->hdpswd;
        $user_invitecode = $user->invitecode;
        $site_url = url('/');
        $logo_url = url('/public').'/admin/images/newlogo.png';
        $mail_bg_img = url('/public').'/images/bg.png';
        $email_icon = url('/public').'/images/email-icon.png';
        $phone_img = url('/public').'/images/phone1.png';
        $mail_icon = url('/public').'/images/mail.png';
        $thankyouimg = url('/public').'/images/thank-you-img.png';

        if($last_id){
             //============== USER CREATE NOTIFICATION TO ADMIN 07-02-2019 =================//
              $created_at             = date('Y-m-d H:m:i');
              $__message = 'New customer <strong>'. $user_first_name .'</strong> registered';
              $usercreate_notification = DB::table('new_notifications')
                      ->insertGetId([ 'message' => $__message, 'created_at' =>$created_at, 'is_for_admin' =>1, 'read_by_admin' => 0]);   

               //============================================================================//

            $message = '';
            $to = $user_email;
            $subject = "Customer Registration";
            $message = '<html>
                            <head>
                              <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
                              <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
                            </head>
                            <body>
                            <div class="mail_background" style="background-color: #d2effd;padding: 40px 0px 0px 0;">
                                <div class="container">
                                    <div class="mail_back_img" style="background-image: url('.$mail_bg_img.') ;background-size: contain;background-position: top;background-repeat: no-repeat;padding: 0px 0px 0px 0px;">
                                        <div class="" style="width: 500px;margin: 0 auto;">
                                          <div class="row">
                                            <div class="" style="text-align: center;padding-bottom: 40px;padding-top: 45px;">
                                              <img style="width:150px" src="'.$logo_url.'" alt="Logo">
                                            </div>
                                            <div class="" style="text-align: center;padding-bottom: 0px;">
                                              <img style="width:60px" src='.$email_icon.'>
                                            </div>
                                            <div class="" style="text-align: center;">
                                              <h6 style="font-size:20px;font-weight:500;margin-top: 8px;margin-bottom: 0px;letter-spacing: 1px;"><span style="color:#1ba4e0">Hey</span>'.$user_first_name.', </h6>  
                                            </div>
                                            <div class="" style="text-align: center;padding: 15px 0px;">
                                              <img style="width: 230px;" src='.$thankyouimg.'>
                                            </div>
                                            <div class="" style="text-align: center;padding-bottom: 20px;">
                                              <h5 style="font-size:20px;font-weight:400;margin-bottom: 0px;margin-top: 0px;">For Choosing College Wrk</h5>
                                              <p style="font-size:14px;font-weight:400;margin:0px;">It is always our endeavor to provide you best-in-class service</p>
                                            </div>
                                            <div class="" style="text-align: center;padding-bottom: 10px">
                                              <a href="#" style="background-color: #f3721f;padding: 7px 90px;color: #fff;font-size: 16px;font-weight: 400;text-transform: uppercase;text-decoration: none;display: inline-block;border-radius: 6px;">Start journey</a>
                                            </div>
                                            
                                            <div class="" style="text-align: center;padding: 0px 0px 0px 0px;">
                                              <p style="font-size:14px;font-weight:400;margin-top: 10px;margin-bottom: 0px;">Find referral code below to refer your Friend</p>
                                            </div>
                                            <div class="" style="text-align: center;padding-bottom: 0px;">
                                              <h2 style="font-size:24px;font-weight:600;margin-top: 0px;margin-bottom: 0px"><span style="color:#f3721f;">College</span> <span style="color:#1ba4e0;">Wrk</span></h2>
                                            </div>
                                            <div class="" style="text-align: center;max-width: 500px;margin: 0 auto;padding-bottom: 0px;">
                                              <p style="font-size:12px;font-weight:500;color:#1ba4e0;margin: 5px;">Your Referral Code:</p>
                                              <p style="border-top: 1px solid #ccc;border-bottom: 1px solid #ccc;padding: 4px 0px;    border-radius: 10px;font-size: 13px;color: #5d5d5d;width: 250px;margin: 0 auto;">'.$user_invitecode.'</p>
                                            </div>
                                            
                                            <div class="" style="text-align: center;padding-top: 5px;padding-bottom: 10px;">
                                              <p style="font-size:12px;font-weight:400;margin: 0px;">In case of any questions, please feel free to reach us on</p>
                                            </div>
                                            <div class="row" style="overflow:hidden;margin:0 auto;transform: translate(9%);padding-bottom: 20px;padding-left: 60px;">
                                              <div class="col-sm-6">
                                                  <div style="margin-right: 20px;background-color: #fcddc9;padding: 10px;border-left: 1px solid #f3721f;    border-bottom: 1px solid #f3721f;border-radius: 5px;float: left;width: 36%;">
                                                    <div style="float: left;width: 15%;margin-right: 20px;">
                                                      <img style="width: 100%;" src='.$phone_img.'>
                                                    </div>
                                                    <div style="float: left;width: 70%;">
                                                      <a href="tell:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:10px;">Call us at</a> 
                                                      <a href="tell:1800–419–7713" target="_top" style="display: block;color: #f3721f;font-size:12px;">1800–419–7713</a>
                                                    </div>
                                                  </div>
                                              </div>
                                              <div class="col-sm-6">
                                                  <div style="padding: 10px;background-color: #c8e9f8;border-left: 1px solid #1ba4e0;border-bottom: 1px solid #1ba4e0;border-radius: 5px;float: left;width: 36%;">
                                                      <div style="float: left;width:15%;margin-right:20px;">
                                                          <img style="width:100%" src='.$mail_icon.'>
                                                      </div>
                                                      <div style="float: left;width: 70%;">
                                                          <a style="display: block;font-size:10px;" href="mailto:info@collegewrk.com" target="_top">Email us at</a>
                                                          <a  style="display: block;font-size:12px;" href="mailto:info@collegewrk.com" target="_top">info@collegewrk.com</a>
                                                      </div>
                                                  </div>
                                              </div>
                                        </div>
                                 </div>
                            </div>
                      </div>
                      <div class="" style="text-align: center;background-color:#e5e5e5;border-top:1px solid #ccc;border-bottom:1px solid #ccc;padding: 5px 0px;width:485px;margin: -6px auto 0px;">
                          <p style="font-size:11px;font-weight:bold;margin:0px;">Team College Wrk</p>
                          <p style="text-align:right;margin: 0px 10px 0px 0px;font-size: 8px;font-weight: bold;">Designed  by : <a href="http://www.binarydata.in/" target="_blank">Binary Data</a></p>
                      </div>
              </div>
      </div>
  </body>
</html>';
            $headers = "From: Collegeworker@gmail.com" . "\r\n" .
                "CC: dev1.bdpl@gmail.com";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
            mail($to,$subject,$message,$headers);
 
            echo $last_id;
            }else{
                echo "false";
            }
        }catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
        
        // $errorClass = new ErrorClass();
       
        // $errors = $errorClass->saveErrors($e);

        }catch(\Exception $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));

            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }
    }

  public function checkuseremail(Request $request){

   try{
   $email = $request->email;

    $existingemail = DB::table('users')
                      ->where('email', '=', $email)
                      ->get();

    if(count($existingemail) > '0'){

      echo "false";

    }else{

      echo "true";
      }
    }catch(\Illuminate\Database\QueryException $e){
      $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
        
        // $errorClass = new ErrorClass();
       
        // $errors = $errorClass->saveErrors($e);

        }catch(\Exception $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));

            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }

  } 

  public function checkloginemail(Request $request){

   try{
   $email = $request->email;

    $existingemail = DB::table('users')
                      ->where('email', '=', $email)
                      ->get();

    if(count($existingemail) > '0'){
      echo "true";
    }else{
      echo "false";
      }
    }catch(\Illuminate\Database\QueryException $e){
      $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
        
        // $errorClass = new ErrorClass();
       
        // $errors = $errorClass->saveErrors($e);

        }catch(\Exception $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));

            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }

  } 

  /*public function checkloginpassword(Request $request){

   try{
   $email = $request->email;
   $password = $request->password;

   $existingemail = DB::table('users')
                      ->where('email', '=', $email)
                      ->where('hdpswd', '=', $password)
                      ->get();

   if ($existingemail){
      echo "true";
   } else {
      echo "false";
   }
    }catch(\Illuminate\Database\QueryException $e){
        
        $errorClass = new ErrorClass();
       
        $errors = $errorClass->saveErrors($e);

        }catch(\Exception $e){
            $errorClass = new ErrorClass();
            $errors = $errorClass->saveErrors($e);
        }

  } */

}
