<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Image;
use Validator;
use DB;
use Hash;
use App\Errorlogs;
use App\Classes\ErrorsClass;
use App\Http\Controllers\Redirect;

class CustomerdashboardController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }

     public function index(Request $request)

    { 
      try{
        $id = Auth::user()->id;
        $usercustomer = DB::table('users')
                            ->where('id',$id)
                            ->where('status',1)
                            ->where('deleted',0)
                            ->first();           
                     
        $categories   = DB::table('categories')
                            ->where(['status' => '1','deleted' => '0'])
                            ->get();
        $clientjobpost= DB::table('clientjobpost')
                            ->where(['user_id' => $id,'status' => '1','deleted' => '0'])
                            ->get(); 
        $recentjobs = DB::table('clientjobpost')
                            ->where(['user_id' => $id,'status' => '1','deleted' => '0'])
                            ->orderBy('id', 'desc')
                            ->take(3)
                            ->get();

        $reserved_job_data = DB::table('reservedjobs')
                            ->where(['client_userid'=>$id])
                            ->take(3)
                            ->get();

                            
        /*$feedback = DB::table('feedback')
                            ->where('worker_userid',$id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->sum('feedback_rating');
        $feedback_count = DB::table('feedback')
                            ->where('worker_userid',$id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->count('feedback_rating');
        $customer_feedback_count = DB::table('feedback')
                            ->where('worker_userid',$id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->count('client_userid');*/


        return view('customerdashboard.index',compact('usercustomer','categories','clientjobpost','recentjobs','reserved_job_data'));
        
        }catch(\Illuminate\Database\QueryException $e){
            $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);

      }catch(\Exception $e){
        $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));

            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
      }
    }
    
     public function create()

    {

    }
    
    public function store(Request $request)

    {

    }
    
    public function show($id)

    {

    }
    
   public function edit($id)

    {
        try{
        $id = Auth::user()->id;
        $usercustomer = DB::table('users')
                          ->where(['id' => $id,'status' => '1','deleted' => '0'])
                          ->first();
        $clientjobpost= DB::table('clientjobpost')
                          ->where(['user_id' => $id,'status' => '1','deleted' => '0'])
                          ->get();  

        $feedback = DB::table('feedback')
                            ->where('user_id',$id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->sum('feedback_rating');
        $feedback_count = DB::table('feedback')
                            ->where('user_id',$id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->count('feedback_rating');
        $customer_feedback_count = DB::table('feedback')
                            ->where('user_id',$id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->count('user_id');

        return view('customerdashboard.edit',compact('usercustomer','clientjobpost','feedback','feedback_count','customer_feedback_count'));

        }catch(\Illuminate\Database\QueryException $e){
            $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);

        }catch(\Exception $e){
            $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));

            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }


    }

    public function update(Request $request, $id){

      try{
        $user_id    = $request->user_id;
        $firstname  = trim($request->firstname);
        $lastname   = trim($request->lastname);
        $phone      = trim($request->phone);
        $updated_by = $user_id;
        $updated_at = date('Y-m-d H:i:s');

         $upQry = DB::table('users')
                  ->where('id', $user_id)
                  ->update(['name' => $firstname,'firstname' => $firstname, 'lastname' => $lastname, 'phone' => $phone, 'updated_by' => $updated_by, 'updated_at' => $updated_at]);

        if($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalName();
            $image->move(public_path('uploads/profile/'), $name); 

            $upImgQry = DB::table('users')
                        ->where('id', $id)
                        ->update(['image' => $name]);
        }
        if($upQry){
            return redirect()->route('customerdashboard.edit', $user_id)
                   ->with('success','User information is updated successfully');
        }else{
            return redirect()->route('customerdashboard.edit', $user_id)
                   ->with('error','Some problem occured. Please try again.');
        }
     }catch(\Illuminate\Database\QueryException $e){
        $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);

     }catch(\Exception $e){
        $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));

            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
     }
   }

    public function destroy($id) {

       }
    public function get_customer_dashboard_equipment(Request $request)

    {
        $cat_id = $request->cat_id;
        $equipments = DB::table('equipment')
                        ->where(['cat_id'=>$cat_id,'status'=>'1','deleted'=>'0'])
                        ->get();
        $html = '';
        if(count($equipments)>0){
        $html .= '<div class="form-group">
                  <label>Will any equipment be needed? (optional)</label>
                  <p> We do not provide moving trucks. We recommend renting a
                        moving truck and we can supply the labor for loading and
                        unloading. CollegeWRK can provide personal pick-up trucks 
                        forsmaller moves.
                  </p>
                  <ul class="list-signup">';

        foreach ($equipments as $equipment) {
                    
        $html.='<li>
                    <div class="sgn-ls">
                    <h5> <input type="checkbox" name="check_list[]" value="'.$equipment->id.'" class="che_sec">'.$equipment->equ_name.'<span>($'.$equipment->equ_price.')</span></h5>
                    <h6>'.$equipment->equ_description.'</h6>
                    </div>
                </li>';
        }

        $html.='</ul>
                </div>'; 
     
     }                  
        echo $html;

    }
    /*public function update_status(Request $request) {
            $worker_id = $request->worker_id;
            $job_status = $request->status;
            $updated_at = date('Y-m-d H:i:s');

            $upQry = DB::table('hire')
                  ->where('worker_id', $worker_id)
                  ->update(['job_status' => $job_status, 'updated_at' => $updated_at]);
            if($upQry){
              echo 'true';
            } else {
              echo 'false';
            }

    }*/

}
