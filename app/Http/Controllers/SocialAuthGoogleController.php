<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Socialite;
use Redirect;
use Auth;
use Exception;
use App\Errorlogs;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ErrorsClass;

class SocialAuthGoogleController extends Controller
{
    public function redirect($service)
    {
        return Socialite::driver($service)->stateless()->redirect();
    }

     public function callback($service)
    {
        try {
            
        $googleUser = Socialite::with ( $service )->stateless()->user ();

        
            //$googleUser = Socialite::with($service)->user();
/*            if($service == 'google'){ ?>
                <div class="title m-b-md">
                    welcome {{ $details->name }} ! <br>
                    your email is : {{ $details->email }} <br>
                    your are {{ $details->user() }}
                </div>
           <?php } */
         $user = User::where('email',$googleUser->email)->first();
            if($user) {
                 // echo'hello user';
                 Auth::loginUsingId($user->id);
            }
            else {
                  //echo'new user';
                $user = new User;
                $user->firstname = $googleUser->name;
                $user->email = $googleUser->email;
                $user->google_id = $googleUser->id;
                $user->password = md5(rand(1,10000));
                $user->user_role = 'worker';
                $user->invitecode  = $googleUser->name.rand();
                $user->save();
                Auth::loginUsingId($user->id);
            }
         //return redirect()->to('/home');
           //return view('/home')->withDetails($user)->withService($service);
           return redirect('/workerprofile');
        //   return view ( 'pages.home' )->withDetails ( $user )->withService ( $service );
        
        } 
        catch(\Exception $e) {
            $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
          //  $errorClass = new ErrorsClass();
          // $errors = $e->getMessage();
       } 
    }
}
