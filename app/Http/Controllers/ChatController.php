<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Chat;
use App\Errorlogs;
use Image;
use Validator;
use DB;
use Hash;
//use App\Errorlogs;
use App\Classes\ErrorsClass;
use App\Http\Controllers\Redirect;

class ChatController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
        date_default_timezone_set('America/New_York');
    }

    public function index(Request $request)
    { 
        try{
            $id = Auth::user()->id;
            $hired_id = trim($request->hired_id);
            $job_id = trim($request->job_id);
            $client_id = trim($request->client_id);
            $worker_id = trim($request->worker_id);
            $chatData = Chat::where('hired_id','=',$hired_id)->where('job_id','=',$job_id)->where('status','=','1')->where('deleted','=','0')->get()->toArray();
    return view('chat.index',compact('id', 'hired_id', 'job_id', 'client_id', 'worker_id', 'chatData'));
           // DB::enableQueryLog();
          //dd(DB::getQueryLog());
         }catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }catch(\Exception $e){
            $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }
    }

    public function saveChat(Request $request)
    { 
        try{
            $chat = new Chat;
            $chat->sender_id = Auth::id();
            Auth::id();
            $chat->receiver_id = trim($request->worker_id);
            
            $chat->message = trim($request->message);
            $chat->hired_id = trim($request->hired_id);
            $chat->job_id = trim($request->job_id);
            $chat->save();
            $insertedId = $chat->id;
            if($insertedId > 0) {
                echo json_encode(array('statusCode'=>"1",'msg'=>$request->message));
                //=============== NOTIFICATION CODE 07-02-2019 ========================//
                    $customer_name = Auth::user()->firstname;
                    $senderid = Auth::id();
                    $client_id = trim($request->client_id);
                    $worker_id =  trim($request->worker_id);

                    if( $client_id  == $senderid){
                    	$receiverid = $worker_id;
                        $fiels_to_modify = 'worker_id';
                    }elseif( $worker_id ==  $senderid ){
                    	 $receiverid = $client_id;
                         $fiels_to_modify = 'client_id';
                    } 

                    
                    $notification_message = 'New message form <strong>'. $customer_name .'</strong>';
                    $created_at = date('Y-m-d H:m:i');
                    $notification_redirect_url = 'chat?hired_id='. trim($request->hired_id) .'&&job_id='. trim($request->job_id) .'&&client_id='. $client_id .'&&worker_id='.$worker_id;
                    $messagesendnotification = DB::table('new_notifications')
                      ->insertGetId([ 'message' => $notification_message, 'created_at' =>$created_at, 'is_for_admin' =>0, 'read_by_admin' => 0, 'message_worker' => $notification_message, $fiels_to_modify => $receiverid, 'notification_redirect_url' => $notification_redirect_url ]); 


                //=====================================================================//
               } else {
                echo json_encode(array('statusCode'=>"0",'msg'=>'Error occured!'));
            }
        }catch(\Illuminate\Database\QueryException $e){
         $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }catch(\Exception $e){
            $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }    
    }

    public function getChat($hireid,$jobid)
    {
       try{
         
            $current_userId = Auth::id();
            $chatData = Chat::where('hired_id','=',$hireid)->where('job_id','=',$jobid)->where('status','=','1')->where('deleted','=','0')->get()->toArray();

            $Html  = '';
        if (count($chatData) > 0) {
            foreach ($chatData as $chat) {
                $time = strtotime($chat['created_at']);
              if ($current_userId == $chat['sender_id']) {
                $senderInfo = DB::table('users')->where('id', '=', $chat['sender_id'])->first();

                $Html.= '<li class="right clearfix">';
                $Html.= '<span class="chat-img pull-right">';
                if($senderInfo->image!='') {
                 $Html.= '<img src="'.url('/public').'/uploads/profile/'.$senderInfo->image.'" alt="User Avatar" class="profilpic img-circle" />';   
                } else {
                 $Html.= '<img src="'.url('/public').'/uploads/profile/1548139021.download.png" alt="User Avatar" class="profilpic img-circle" />';
                }
                $Html.= '</span>';
                $Html.= '<div class="chat-body clearfix">';
                $Html.=  '<div class="header">';
                $Html.=  '<small class=" text-muted">';
                $Html.= '<i class="fa fa-clock-o" aria-hidden="true"></i>'.$this->humanTiming($time).' ago';
                $Html.= '</small>';
                $Html.= '<strong class="pull-right primary-font">'.ucfirst($senderInfo->firstname).' '.ucfirst($senderInfo->lastname).'</strong>';
                $Html.= '</div>';
                $Html.= '<p>'.$chat['message'].'</p>';
                $Html.= '</div>';
                $Html.= '</li>';

                } else {

                $receiverInfo = DB::table('users')->where('id', '=', $chat['sender_id'])->first();

                $Html.= '<li class="left clearfix">';
                $Html.= '<span class="chat-img pull-left">';
                if($receiverInfo->image!='') {
                    $Html.= '<img src="'.url('/public').'/uploads/profile/'.$receiverInfo->image.'" alt="User Avatar" class="profilpic img-circle" />';
                } else {
                    $Html.= '<img src="'.url('/public').'/uploads/profile/1548139021.download.png" alt="User Avatar" class="profilpic img-circle" />';
                }
                $Html.= '</span>';
                $Html.= '<div class="chat-body clearfix">';
                $Html.= '<div class="header">';
                $Html.= '<strong class="primary-font">'.ucfirst($receiverInfo->firstname).' '.ucfirst($receiverInfo->lastname).'</strong>';
                $Html.= '<small class="pull-right text-muted">';
                $Html.= '<i class="fa fa-clock-o" aria-hidden="true"></i>'.$this->humanTiming($time).' ago';
                $Html.= '</small>';
                $Html.= '</div>';
                $Html.= '<p>'.$chat['message'].'</p>';
                $Html.= '</div>';
                $Html.= '</li>';

                }
            }

            echo $Html;

        }  else {
            echo '<li class="left clearfix">
                            <div class="chat-body clearfix">
                                <p>
                                    You have no message. Please send message to start chat.
                                </p>
                            </div>
                        </li>';
        }

        }catch(\Illuminate\Database\QueryException $e){
         $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }catch(\Exception $e){
            $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        } 

    }

    function humanTiming ($time) {
        $time = time() - $time; 
        $time = ($time<1)? 1 : $time;
        $tokens = array (
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
        );
    foreach ($tokens as $unit => $text) {
    if ($time < $unit) continue;
    $numberOfUnits = floor($time / $unit);
    return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
      }
    }

    public function getChathistory($jobid)
    {
       try{
        $pagination = 20;
        $chatCount = Chat::where('job_id','=',$jobid)->where('status','=','1')->where('deleted','=','0')->count();
        /*$chatData = DB::table('chathistory')
                     ->where('job_id', '=', $jobid)
                     ->where('status', '=', '1')
                     ->where('deleted', '=', '0')
                     ->paginate($pagination);*/
         $chatData = Chat::where('job_id','=',$jobid)->where('status','=','1')->where('deleted','=','0')->get()->toArray();             
                return view('chat.chathistory', compact('chatData', 'jobid', 'chatCount'));
            
        }catch(\Illuminate\Database\QueryException $e){
         $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }catch(\Exception $e){
            $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        } 

    }

    public function show_adminchat(Request $request)
    { 
        try{

            $id = Auth::user()->id;

            $usersCount = User::where('id','!=',$id)->where('user_role','!=','admin')->where('status','=','1')->where('deleted','=','0')->count();
            if( isset($_GET['id']) ){
                $user_id = $_GET['id'];
                $firstUser = User::where('id','!=',$id)->where('id','=',$user_id)->where('user_role','!=','admin')->where('status','=','1')->where('deleted','=','0')->orderBy('name','ASC')->first();
            }else{
                $firstUser = User::where('id','!=',$id)->where('user_role','!=','admin')->where('status','=','1')->where('deleted','=','0')->orderBy('name','ASC')->first();
            }

           

            $users = User::where('id','!=',$id)->where('user_role','!=','admin')->where('status','=','1')->where('deleted','=','0')->orderBy('name','ASC')->get()->toArray();
            
            return view('chat.adminchat', compact('users', 'usersCount', 'firstUser'));

         }catch(\Illuminate\Database\QueryException $e){
         $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }catch(\Exception $e){
            $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }
    }

    public function getadminChat($senderid,$receiverid)
    {
       try{
        
        $current_userId = Auth::id();

        DB::table('chathistory')
            ->where('sender_id', '=', $receiverid)
            ->where('receiver_id', '=', $senderid)
            ->update(['is_read' => 1]);

        $chatData = Chat::where('sender_id','=',$senderid)->where('receiver_id','=',$receiverid)->orWhere('sender_id','=',$receiverid)->where('receiver_id','=',$senderid)->where('status','=','1')->where('deleted','=','0')->get()->toArray();
       
        $Html  = '';
        if (count($chatData) > 0) {
            foreach ($chatData as $chat) {
                $time = strtotime($chat['created_at']);
              if ($current_userId == $chat['sender_id']) {
                $senderInfo = DB::table('users')->where('id', '=', $chat['sender_id'])->first();

                $Html.= '<li class="right clearfix">';
                $Html.= '<span class="chat-img pull-right">';
                if($senderInfo->image!='') {
                 $Html.= '<img src="'.url('/public').'/uploads/profile/'.$senderInfo->image.'" alt="User Avatar" class="profilpic img-circle" />';   
                } else {
                 $Html.= '<img src="'.url('/public').'/uploads/profile/1548139021.download.png" alt="User Avatar" class="profilpic img-circle" />';
                }
                $Html.= '</span>';
                $Html.= '<div class="chat-body clearfix">';
                $Html.=  '<div class="header">';
                $Html.=  '<small class=" text-muted">';
                $Html.= '<i class="fa fa-clock-o" aria-hidden="true"></i>'.$this->humanTiming($time).' ago';
                $Html.= '</small>';
                $Html.= '<strong class="pull-right primary-font">'.ucfirst($senderInfo->firstname).' '.ucfirst($senderInfo->lastname).'</strong>';
                $Html.= '</div>';
                $Html.= '<p>'.$chat['message'].'</p>';
                $Html.= '</div>';
                $Html.= '</li>';

                } else {

                $receiverInfo = DB::table('users')->where('id', '=', $chat['sender_id'])->first();

                $Html.= '<li class="left clearfix">';
                $Html.= '<span class="chat-img pull-left">';
                if($receiverInfo->image!='') {
                    $Html.= '<img src="'.url('/public').'/uploads/profile/'.$receiverInfo->image.'" alt="User Avatar" class="profilpic img-circle" />';
                } else {
                    $Html.= '<img src="'.url('/public').'/uploads/profile/1548139021.download.png" alt="User Avatar" class="profilpic img-circle" />';
                }
                $Html.= '</span>';
                $Html.= '<div class="chat-body clearfix">';
                $Html.= '<div class="header">';
                $Html.= '<strong class="primary-font">'.ucfirst($receiverInfo->firstname).' '.ucfirst($receiverInfo->lastname).'</strong>';
                $Html.= '<small class="pull-right text-muted">';
                $Html.= '<i class="fa fa-clock-o" aria-hidden="true"></i>'.$this->humanTiming($time).' ago';
                $Html.= '</small>';
                $Html.= '</div>';
                $Html.= '<p>'.$chat['message'].'</p>';
                $Html.= '</div>';
                $Html.= '</li>';

                }
            }

            echo $Html;

        }  else {
            echo '<li class="left clearfix">
                            <div class="chat-body clearfix">
                                <p>
                                    You have no message. Please send message to start chat.
                                </p>
                            </div>
                        </li>';
        }

        }catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }catch(\Exception $e){
            $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        } 

    }

    public function saveadminChat(Request $request)
    { 
        try{
            $chat = new Chat;
           // $chat->sender_id = Auth::id();
            $customer_name = Auth::user()->firstname;
            $chat->sender_id = trim($request->sender_id);
            $chat->receiver_id = trim($request->receiver_id);
            $chat_type = trim($request->chat_type);
            $receiverid = trim($request->receiver_id);
            $chat->message = trim($request->message);
            $chat->save();
            $insertedId = $chat->id;
            if($insertedId > 0) {
                echo json_encode(array('statusCode'=>"1",'msg'=>$request->message));
                     //$currentUser = Auth::id();
                    if( $receiverid  == 422 ){
                        $_is_for_admin = 1;
                        $notification_redirect_url = 'support?id='.trim($request->sender_id); 
                    }else{
                        $_is_for_admin = 0;
                        if( $chat_type !='' ){
                            $notification_redirect_url = $chat_type;
                            if( $chat_type =='workeradminchat' ){
                                $fiels_to_modify = 'worker_id';
                                //$msg = 'check condition 1';
                            }else{
                                $fiels_to_modify = 'client_id';
                                //$msg = 'check condition 2';
                            }
                            
                        }else{
                            $userData = DB::table('users')->where('id', $receiverid)->first();
                            $role =  $userData->user_role;
                            if( $role == 'worker' ){
                                $notification_redirect_url = 'workeradminchat';
                                $fiels_to_modify = 'worker_id';
                                //$msg = 'check condition 3';
                            }else{
                                $notification_redirect_url = 'customeradminchat';  
                                $fiels_to_modify = 'client_id';
                                //$msg = 'check condition 4';
                            }
                        } 
                    }

                    $notification_message = 'New message form <strong>'. $customer_name .'</strong>';
                    $created_at = date('Y-m-d H:m:i');
                    $messagesendnotification = DB::table('new_notifications')
                                                ->insertGetId([ 'message' => $notification_message, 'created_at' =>$created_at, 'is_for_admin' =>$_is_for_admin, 'read_by_admin' => 0, 'message_worker' => $notification_message , $fiels_to_modify => $receiverid , 'notification_redirect_url' => $notification_redirect_url]);  
                } else {
                echo json_encode(array('statusCode'=>"0",'msg'=>'Error occured!'));
            }



        }catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            
/*            $errorClass = new ErrorClass();
            $errors = $errorClass->saveErrors($e);*/
        }catch(\Exception $e){
            $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }    
    }

/**************Worker Admin Chat Start***************************************************************/
    public function workeradminchat(Request $request)
    { 
        try{
            $current_userid = Auth::user()->id;
            $adminInfo = User::where('user_role','=','admin')->where('status','=','1')->where('deleted','=','0')->first();
            $admin_id = $adminInfo->id;
            return view('chat.workeradminchat',compact('current_userid', 'admin_id'));
          
         }catch(\Illuminate\Database\QueryException $e){
     
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
           /* $errorClass = new ErrorClass();
            $errors = $errorClass->saveErrors($e);*/
        }catch(\Exception $e){
           
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
           /* $errorClass = new ErrorClass();
            $errors = $errorClass->saveErrors($e);*/
        }
    }

    public function saveworkeradminChat(Request $request)
    { 
        try{
            $chat = new Chat;
            $chat->sender_id = trim($request->sender_id);
            $chat->receiver_id = trim($request->receiver_id);
            $receiverid = trim($request->receiver_id);
            $chat->message = trim($request->message);
            $chat->save();
            $insertedId = $chat->id;
            if($insertedId > 0) {
                echo json_encode(array('statusCode'=>"1",'msg'=>$request->message));
                 if( $receiverid  == 422 ){
                        $_is_for_admin = 1;
                        $notification_redirect_url = 'support?type=workeradminchat&&id='.trim($request->sender_id);
                    }else{
                        $_is_for_admin = 0;
                        $notification_redirect_url = 'workeradminchat';
                    }
                    $customer_name = Auth::user()->firstname;
                    $notification_message = 'New message form <strong>'. $customer_name .'</strong>';
                    $created_at = date('Y-m-d H:m:i');
                    $messagesendnotification = DB::table('new_notifications')
                                                ->insertGetId([ 'message' => $notification_message, 'created_at' =>$created_at, 'is_for_admin' =>$_is_for_admin, 'read_by_admin' => 0, 'message_worker' => $notification_message, 'worker_id' => $receiverid , 'notification_redirect_url' => $notification_redirect_url ]); 


               } else {
                echo json_encode(array('statusCode'=>"0",'msg'=>'Error occured!'));
            }
        }catch(\Illuminate\Database\QueryException $e){
        $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }catch(\Exception $e){
            $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }    
    }

    public function getworkeradminChat($senderid,$receiverid)
    {
       try{
        
        $current_userId = Auth::id();

        $chatData = Chat::where('sender_id','=',$senderid)->where('receiver_id','=',$receiverid)->orWhere('sender_id','=',$receiverid)->where('receiver_id','=',$senderid)->where('status','=','1')->where('deleted','=','0')->get()->toArray();
       
        $Html  = '';
        if (count($chatData) > 0) {
            foreach ($chatData as $chat) {
                $time = strtotime($chat['created_at']);
              if ($current_userId == $chat['sender_id']) {
                $senderInfo = DB::table('users')->where('id', '=', $chat['sender_id'])->first();

                $Html.= '<li class="right clearfix">';
                $Html.= '<span class="chat-img pull-right">';
                if($senderInfo->image!='') {
                 $Html.= '<img src="'.url('/public').'/uploads/profile/'.$senderInfo->image.'" alt="User Avatar" class="profilpic img-circle" />';   
                } else {
                 $Html.= '<img src="'.url('/public').'/uploads/profile/1548139021.download.png" alt="User Avatar" class="profilpic img-circle" />';
                }
                $Html.= '</span>';
                $Html.= '<div class="chat-body clearfix">';
                $Html.=  '<div class="header">';
                $Html.=  '<small class=" text-muted">';
                $Html.= '<i class="fa fa-clock-o" aria-hidden="true"></i>'.$this->humanTiming($time).' ago';
                $Html.= '</small>';
                $Html.= '<strong class="pull-right primary-font">'.ucfirst($senderInfo->firstname).' '.ucfirst($senderInfo->lastname).'</strong>';
                $Html.= '</div>';
                $Html.= '<p>'.$chat['message'].'</p>';
                $Html.= '</div>';
                $Html.= '</li>';

                } else {

                $receiverInfo = DB::table('users')->where('id', '=', $chat['sender_id'])->first();

                $Html.= '<li class="left clearfix">';
                $Html.= '<span class="chat-img pull-left">';
                if($receiverInfo->image!='') {
                    $Html.= '<img src="'.url('/public').'/uploads/profile/'.$receiverInfo->image.'" alt="User Avatar" class="profilpic img-circle" />';
                } else {
                    $Html.= '<img src="'.url('/public').'/uploads/profile/1548139021.download.png" alt="User Avatar" class="profilpic img-circle" />';
                }
                $Html.= '</span>';
                $Html.= '<div class="chat-body clearfix">';
                $Html.= '<div class="header">';
                $Html.= '<strong class="primary-font">'.ucfirst($receiverInfo->firstname).' '.ucfirst($receiverInfo->lastname).'</strong>';
                $Html.= '<small class="pull-right text-muted">';
                $Html.= '<i class="fa fa-clock-o" aria-hidden="true"></i>'.$this->humanTiming($time).' ago';
                $Html.= '</small>';
                $Html.= '</div>';
                $Html.= '<p>'.$chat['message'].'</p>';
                $Html.= '</div>';
                $Html.= '</li>';

                }
            }

            echo $Html;

        }  else {
            echo '<li class="left clearfix">
                            <div class="chat-body clearfix">
                                <p>
                                    You have no message. Please send message to start chat.
                                </p>
                            </div>
                        </li>';
        }

        }catch(\Illuminate\Database\QueryException $e){
     $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }catch(\Exception $e){
            $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        } 

    }

/**************Worker Admin Chat End***************************************************************/

/**************Customer Admin Chat Start***************************************************************/
    public function customeradminchat(Request $request)
    { 
        try{
            $current_userid = Auth::user()->id;
            $adminInfo = User::where('user_role','=','admin')->where('status','=','1')->where('deleted','=','0')->first();
            $admin_id = $adminInfo->id;
            return view('chat.customeradminchat',compact('current_userid', 'admin_id'));
          
         }catch(\Illuminate\Database\QueryException $e){
         $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }catch(\Exception $e){
            $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }
    }

    public function savecustomeradminChat(Request $request)
    { 
        try{
            $chat = new Chat;
            $chat->sender_id = trim($request->sender_id);
            $chat->receiver_id = trim($request->receiver_id);
            $receiverid = trim($request->receiver_id);
            $chat->message = trim($request->message);
            $chat->save();
            $insertedId = $chat->id;
            if($insertedId > 0) {
                echo json_encode(array('statusCode'=>"1",'msg'=>$request->message));
                 if( $receiverid  == 422 ){
                        $_is_for_admin = 1;
                         $notification_redirect_url = 'support?type=customeradminchat&&id='.trim($request->sender_id);
                    }else{
                        $_is_for_admin = 0;
                        $notification_redirect_url = 'customeradminchat';
                    }
                    $customer_name = Auth::user()->firstname;
                    $notification_message = 'New message form <strong>'. $customer_name .'</strong>';
                    $created_at = date('Y-m-d H:m:i');
                    $messagesendnotification = DB::table('new_notifications')
                                                ->insertGetId([ 'message' => $notification_message, 'created_at' =>$created_at, 'is_for_admin' =>$_is_for_admin, 'read_by_admin' => 0, 'message_worker' => $notification_message , 'client_id' => $receiverid, 'notification_redirect_url' => $notification_redirect_url ]); 
               } else {
                echo json_encode(array('statusCode'=>"0",'msg'=>'Error occured!'));
            }
        }catch(\Illuminate\Database\QueryException $e){
        $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }catch(\Exception $e){
            $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }    
    }

    public function getcustomeradminChat($senderid,$receiverid)
    {
       try{
        
        $current_userId = Auth::id();

        $chatData = Chat::where('sender_id','=',$senderid)->where('receiver_id','=',$receiverid)->orWhere('sender_id','=',$receiverid)->where('receiver_id','=',$senderid)->where('status','=','1')->where('deleted','=','0')->get()->toArray();
       
        $Html  = '';
        if (count($chatData) > 0) {
            foreach ($chatData as $chat) {
                $time = strtotime($chat['created_at']);
              if ($current_userId == $chat['sender_id']) {
                $senderInfo = DB::table('users')->where('id', '=', $chat['sender_id'])->first();

                $Html.= '<li class="right clearfix">';
                $Html.= '<span class="chat-img pull-right">';
                if($senderInfo->image!='') {
                 $Html.= '<img src="'.url('/public').'/uploads/profile/'.$senderInfo->image.'" alt="User Avatar" class="profilpic img-circle" />';   
                } else {
                 $Html.= '<img src="'.url('/public').'/uploads/profile/1548139021.download.png" alt="User Avatar" class="profilpic img-circle" />';
                }
                $Html.= '</span>';
                $Html.= '<div class="chat-body clearfix">';
                $Html.=  '<div class="header">';
                $Html.=  '<small class=" text-muted">';
                $Html.= '<i class="fa fa-clock-o" aria-hidden="true"></i>'.$this->humanTiming($time).' ago';
                $Html.= '</small>';
                $Html.= '<strong class="pull-right primary-font">'.ucfirst($senderInfo->firstname).' '.ucfirst($senderInfo->lastname).'</strong>';
                $Html.= '</div>';
                $Html.= '<p>'.$chat['message'].'</p>';
                $Html.= '</div>';
                $Html.= '</li>';

                } else {

                $receiverInfo = DB::table('users')->where('id', '=', $chat['sender_id'])->first();

                $Html.= '<li class="left clearfix">';
                $Html.= '<span class="chat-img pull-left">';
                if($receiverInfo->image!='') {
                    $Html.= '<img src="'.url('/public').'/uploads/profile/'.$receiverInfo->image.'" alt="User Avatar" class="profilpic img-circle" />';
                } else {
                    $Html.= '<img src="'.url('/public').'/uploads/profile/1548139021.download.png" alt="User Avatar" class="profilpic img-circle" />';
                }
                $Html.= '</span>';
                $Html.= '<div class="chat-body clearfix">';
                $Html.= '<div class="header">';
                $Html.= '<strong class="primary-font">'.ucfirst($receiverInfo->firstname).' '.ucfirst($receiverInfo->lastname).'</strong>';
                $Html.= '<small class="pull-right text-muted">';
                $Html.= '<i class="fa fa-clock-o" aria-hidden="true"></i>'.$this->humanTiming($time).' ago';
                $Html.= '</small>';
                $Html.= '</div>';
                $Html.= '<p>'.$chat['message'].'</p>';
                $Html.= '</div>';
                $Html.= '</li>';

                }
            }

            echo $Html;

        }  else {
            echo '<li class="left clearfix">
                            <div class="chat-body clearfix">
                                <p>
                                    You have no message. Please send message to start chat.
                                </p>
                            </div>
                        </li>';
        }

        }catch(\Illuminate\Database\QueryException $e){
      $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }catch(\Exception $e){
            $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        } 

    }

/**************Customer Admin Chat End***************************************************************/

    public function getadminChatmsgcnt($sndid,$rcvid)
    {
       try{
        
        $current_userId = Auth::id();

        $chatDatacnt = Chat::where('sender_id','=',$sndid)->where('receiver_id','=',$rcvid)->where('status','=','1')->where('deleted','=','0')->where('is_read','=','0')->count();
       
       // echo $sndid.'>>'.$rcvid.'==='.$chatDatacnt;
            echo $chatDatacnt;

        }catch(\Illuminate\Database\QueryException $e){
        $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        }catch(\Exception $e){
            $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorClass();
            // $errors = $errorClass->saveErrors($e);
        } 

    }

}
