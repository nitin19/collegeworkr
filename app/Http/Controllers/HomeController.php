<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Home;
use Session;
use DB;
use Hash;
use Image;
use App\Errorlogs;
use App\Classes\ErrorsClass;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      try{
        return view('pages.home');
      }
      catch(\Illuminate\Database\QueryException $e){
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }catch(\Exception $e){ 
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror')); 
        }
    }
     public function store(Request $request)
     { 
      try { 
              $home = new Home;
              $home->name = $request->name;
              $home->email = $request->email;
              $home->message = $request->message;
              $home->save();
        if($home!=''){

         $to= "info@collegewrk.com";
         $subject = "College Workers";
         $message  = "";
         $message ="<div class='wrapper' id='mail_template' style='background-repeat: no-repeat; background-size: cover; margin: auto; min-height: 234px; background-position: center center; width: 650px;'>
           <div id='maindiv1' style='text-align: center; box-shadow: 0px 0px 10px rgb(230, 92, 51); float: left; min-height: 175px;'>
           <div class='mailcontent' style='float:left;text-align:left;color:#fff;'><br>
           <p style='font-family: trebuchet ms; font-size: 15px; line-height: 22px; text-align: left; color: rgb(0, 0, 0); margin:0px;padding-left:30px;'><b>Dear</b> <span style='color: #000; font-weight: bold;'>Admin</span><br></p>
             <p style='font-family: trebuchet ms; font-size: 17px; line-height: 22px; text-align: left; color: rgb(0, 0, 0); margin:0px;padding-left:30px;'><span style='color: #000; font-weight: bold;'> You have received new query from the below user</span></p>
           <p style='font-family: trebuchet ms; font-size: 15px; line-height: 22px; text-align: left; color: rgb(0, 0, 0); margin:0px;padding-left:30px;'><span style='color: #000; font-weight: bold;'> Name: $home->name</span></p>
            <p style='font-family: trebuchet ms; font-size: 15px; line-height: 22px; text-align: left; color: rgb(0, 0, 0); margin:0px;padding-left:30px;'><span style='color: #000; font-weight: bold;'> Email: $home->email</span></p>
           <p style='font-family: trebuchet ms; font-size: 15px; line-height: 22px; text-align: left; color: rgb(0, 0, 0); margin:0px;padding-left:30px;'><span style='color: #000; font-weight: bold;'> Message: $home->message</span></p><br>
            <p style='font-family: trebuchet ms; font-size: 15px; line-height: 22px; text-align: left; color: rgb(0, 0, 0); margin:0px;padding-left:30px;'><span style='color: #000; font-weight: bold;'>Thanks &regards <br>College Worker </span></p>
           </div><br>
        <div style='background: #ff8638 none repeat scroll 0% 0%; float: left; width: 100%; margin: 7px 0px 0px;'>
        <p style='color: #fff; font-family: trebuchet ms; font-size: 16px;'> 2018  © College Worker Designed by:Binary data Pvt.Ltd. </p>
        </div> 
           </div>  
           </div>
           ";
            $headers= "From: " . $to . " <" . $to . ">" . "\r\n" ;
            $headers.= "MIME-Version: 1.0" . "\r\n";
            $headers.= "Content-type:text/html;charset=UTF-8" . "\r\n";

            mail($to,$subject,$message,$headers);
            }  

            if($home!=''){
                 $to_client= $home->email;
                 $subject = "College Worker";
                 $message  = "";
                 $message ="<div class='wrapper' id='mail_template' style='background-repeat: no-repeat; background-size: cover; margin: auto; min-height: 234px; background-position: center center; width: 650px;'>
           <div id='maindiv1' style='text-align: center; box-shadow: 0px 0px 10px rgb(230, 92, 51); float: left; min-height: 175px;'>
           <div class='mailcontent' style='float:left;text-align:left;color:#fff;'><br>
           <p style='font-family: trebuchet ms; font-size: 15px; line-height: 22px; text-align: left; color: rgb(0, 0, 0); margin:0px;padding-left:30px;'><b>Dear</b> <span style='color: #000; font-weight: bold;'> $home->name</span><br></p>
              <p style='font-family: trebuchet ms; font-size: 17px; line-height: 22px; text-align: left; color: rgb(0, 0, 0); margin:0px;padding-left:30px;'><span style='color: #000; font-weight: bold;'> Your query has been submitted to College Worker We will get back to you soon!</span></p>
            <p style='font-family: trebuchet ms; font-size: 15px; line-height: 22px; text-align: left; color: rgb(0, 0, 0); margin:0px;padding-left:30px;'><span style='color: #000; font-weight: bold;'> Email: $home->email</span></p>
           <p style='font-family: trebuchet ms; font-size: 15px; line-height: 22px; text-align: left; color: rgb(0, 0, 0); margin:0px;padding-left:30px;'><span style='color: #000; font-weight: bold;'> Message: $home->message</span></p><br>
            <p style='font-family: trebuchet ms; font-size: 15px; line-height: 22px; text-align: left; color: rgb(0, 0, 0); margin:0px;padding-left:30px;'><span style='color: #000; font-weight: bold;'>Thanks &regards <br>College Worker </span></p>
           </div><br>
        <div style='background: #f8981d none repeat scroll 0% 0%; float: left; width: 100%; margin: 7px 0px 0px;'>
        <p style='color: #fff; font-family: trebuchet ms; font-size: 16px;'> 2018  © College Worker Designed by:Binary data Pvt.Ltd. </p>
        </div> 
           </div>  
           </div>
           ";
            $headers= "From: " . $to_client . " <" . $to_client . ">" . "\r\n" ;
            $headers.= "MIME-Version: 1.0" . "\r\n";
            $headers.= "Content-type:text/html;charset=UTF-8" . "\r\n";

            mail($to_client,$subject,$message,$headers);
         
             echo '<div style="display: flex;align-items: center;justify-content: center;flex-wrap: wrap;text-align: center;"> 
<div class="row">
<div class="col-sm-12 errer-page-505"> 
<div class=""> 
<h1 style="color: #07a8de;font-size: 150px;font-weight: bold;margin-bottom: -15px; ">Thank you!!</h1> 
<p style="font-weight: 300;font-size: 17px;color: #555555;margin-bottom: 5px;">Your message has been successfully sent. We will contact you very soon!</p> 
<a href='.url('/').' style="color: #07a8de;font-size: 15px;font-weight: 400;">Back To Home</a> 
</div> 
</div>
</div>
</div>';

            }  
          }

       catch (\Illuminate\Database\QueryException $e) {
        $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));

            // $errorClass = new ErrorsClass();
            // $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
          $customerror = "Operating System : " .$_SERVER['SERVER_SOFTWARE']."Browser : ".$_SERVER['HTTP_USER_AGENT']." Message : ". $e->getMessage() . " Line number : " . $e->getLine() . "\n File :". $e->getFile();

            $errorlog                   = new Errorlogs;
            $errorlog->error_message    = $e->getMessage();
            $errorlog->line_number      = $e->getLine();
            $errorlog->file_name        = $e->getFile();
            $errorlog->browser          = $_SERVER['HTTP_USER_AGENT'];
            $errorlog->operating_system = $_SERVER['SERVER_SOFTWARE'];
            $errorlog->loggedin_id      = Auth::user()->id;
            $errorlog->ip_address       = "";
            $errorlog->save();
            return view('errors.custom',compact('customerror'));
            // $errorClass = new ErrorsClass();
            // $errors = $errorClass->saveErrors($e);
       } 

        }
}

?>
