@extends('layouts.default')

@section('title', 'Home')

@section('content')

<script type="text/javascript">
  document.addEventListener('DOMContentLoaded',function(event){
  // array with texts to type in typewriter
  var dataText = [ "Delightful College Student Movers & More",];
  
  // type one text in the typwriter
  // keeps calling itself until the text is finished
  function typeWriter(text, i, fnCallback) {
    // chekc if text isn't finished yet
    if (i < (text.length)) {
      // add next character to h1
     document.querySelector(".header-title").innerHTML = text.substring(0, i+1) +'<two aria-hidden="true"></two>';

      // wait for a while and call this function again for next character
      setTimeout(function() {
        typeWriter(text, i + 1, fnCallback)
      }, 100);
    }
    // text finished, call callback if there is a callback function
    else if (typeof fnCallback == 'function') {
      // call callback after timeout
      setTimeout(fnCallback, 700);
    }
  }
  // start a typewriter animation for a text in the dataText array
   function StartTextAnimation(i) {
     if (typeof dataText[i] == 'undefined'){
        setTimeout(function() {
          StartTextAnimation(0);
        }, 20000);
     }
     // check if dataText[i] exists
    if (i < dataText[i].length) {
      // text exists! start typewriter animation
     typeWriter(dataText[i], 0, function(){
       // after callback (and whole text has been animated), start next text
       StartTextAnimation(i + 1);
     });
    }
  }
  // start the text animation
  StartTextAnimation(0);
});
</script>


<div class="iuk">
 <div class="container">
  <div class="headerWRK-text">
   <h1 class="header-title">Delightful College
Student Movers & More</h1>
   <p>CollegeWRK provides simple, reliable local moving and odd jobs help.</p>
   <ul class="list-inline">
          @if(Auth::check())
          <!-- <li><button class="post-btn" data-toggle="modal" data-target="#demo-modal-3">Post A Job</button> </li>
          <li><button class="apply-btn" data-toggle="modal" data-target="#myModal-2">Apply For Work</button></li> -->
          @else
          <li><button class="post-btn wow fadeIn" data-toggle="modal" data-target="#demo-modal-3">Post A Job</button></li>
          <li><button class="apply-btn wow fadeOut" data-toggle="modal" data-target="#myModal-2">Apply For Work</button></li>
          @endif
        </ul>
   </div>     
 </div>
</div>
<style>
.google_plus_btn {
    padding: 12px 169px 12px 169px!important;}
.facebook_btn {
    background-color: #2d4373 !important;
    padding: 14px 175px 14px 175px;}
button.post-btn {
    transition: .9s ease;
    -webkit-animation-name: hvr-pulse;
    animation-name: hvr-pulse;
}
 button.post-btn:hover {
    webkit-animation-name: hvr-pulse;
    animation-name: hvr-pulse;
    -webkit-animation-duration: 1s;
    animation-duration: 1s;
    -webkit-animation-timing-function: linear;
    animation-timing-function: linear;
    -webkit-animation-iteration-count: infinite;
    animation-iteration-count: infinite;
}
button.apply-btn {
    transition: .9s ease;
    -webkit-animation-name: hvr-pulse;
    animation-name: hvr-pulse;
}
 button.apply-btn:hover {
    webkit-animation-name: hvr-pulse;
    animation-name: hvr-pulse;
    -webkit-animation-duration: 1s;
    animation-duration: 1s;
    -webkit-animation-timing-function: linear;
    animation-timing-function: linear;
    -webkit-animation-iteration-count: infinite;
    animation-iteration-count: infinite;
}
.get-started button.post-btn {
    transition: .9s ease;
    -webkit-animation-name: hvr-pulse;
    animation-name: hvr-pulse;
}
.iuk {
    background-image: url("./public/images/slide.jpg");
    animation-name: hhh;
    animation-duration: 9s;
    animation-iteration-count: infinite;
    padding:170px 0px;
}
@keyframes  hhh {
     20%  {background-image: url("./public/images/slide.jpg");}
    60%  {background-image: url("./public/images/banner2.jpg");}
    100% {background-image: url("./public/images/college_wrk-Banner.jpg");}
}
.iuk h1{
font-size: 70px;
    font-family: 'ProximaNovaA-Bold';
    line-height: 64px;
    text-align: center;
   color:#ffffff;
   max-width:780px;
   margin:0 auto;
  }
  .other-color {
    color: #ff8638;
    display: block;
}
.iuk p{
  font-size: 28px;
    color: #ffffff;
    padding-top: 15px;
    padding-bottom: 40px;
    text-align: center;
  }
  .iuk ul {
    list-style: none;
    position: relative;
    text-align: center;
}
@media screen and (max-width: 980px){
.iuk h1 {font-size: 55px;max-width: 100%;line-height: 55px;}
.iuk p {padding-top: 10px;line-height: 32px;}
}

@media screen and (max-width: 600px){
.iuk {padding: 100px 0px;}
.iuk h1 {font-size: 40px;max-width: 100%;line-height: 45px;}
.iuk p {font-size: 22px;width: 500px;margin: 0 auto;}
}
@media screen and (max-width: 500px){
.iuk p {width: 100%;}
}
@media screen and (max-width: 425px){
.iuk h1 {font-size: 33px;line-height: 40px;}
.iuk p {font-size: 20px;line-height: 27px;}
.iuk p {font-size: 20px;line-height: 27px;padding-bottom: 30px;}
.list-inline>li {padding-bottom: 16px;}
}
@media screen and (max-width: 375px){
.iuk h1 {font-size: 30px;line-height: 40px;}
.iuk p {font-size: 18px;}
.apply-btn {height: 45px;font-size: 19px;}
.post-btn {height: 45px;font-size: 19px;}
}
@media screen and (max-width: 375px){
.iuk h1 {font-size: 27px;line-height: 40px;}
}
</style>

<section class="about">
  <div class="container">
    <div class="row">
      <div class="col-md-3 wow fadeInLeft">
        <h2>Who We<br>
Are</h2>
<h3>Why Choose us</h3>
      </div>
      <div class="col-md-9 wow fadeInRight">
        <h4>Smells racy free announcing than durable zesty smart exotic far feel. Screamin' affordable secret way absolutely.</h4>
        <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue.</p>
      </div>
    </div>
    <div class="row about-btm">
      <div class="col-md-4 col-sm-4 wow fadeInDown">
        <div class="Guarantee-cont">
          <div class="gaur-img">
            <img src="{{ url('/public') }}/images/right.png" alt="">
          </div>
          <div class="guar-text">
              <h6>Guarantee</h6>
              <p>Simple and intuitive online process 
      from posting to payment.</p>
      </div>
</div>
      </div>
      <div class="col-md-4 col-sm-4 wow fadeInDown">
          <div class="Guarantee-cont">
          <div class="gaur-img">
            <img src="{{ url('/public') }}/images/gaur.png" alt="">
          </div>
          <div class="guar-text">
              <h6>Dedicated Support</h6>
              <p>Simple and intuitive online process 
      from posting to payment.</p>
      </div>
</div>
      </div>
      <div class="col-md-4 col-sm-4 wow fadeInDown">
          <div class="Guarantee-cont">
          <div class="gaur-img">
            <img src="{{ url('/public') }}/images/education.png" alt="">
          </div>
          <div class="guar-text">
              <h6>Local College Students</h6>
              <p>Simple and intuitive online process 
      from posting to payment.</p>
      </div>
</div>
      </div>
    </div>
  </div>
</section>
<section class="services">
  <div class="container-fluid">
    <h2 class="wow fadeInRight">Our <span>Services</span></h2>
    <p class="tp-service wow fadeInRight">Donec rutrum congue leo eget malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae. </p>
    <div class="row">
      <div class="col-md-3 col-sm-4 wow fadeInLeft tab-left nopadding">
    <nav class="nav-sidebar">
    <ul class="nav tabs">
          <li class="active"><a href="#tab1" data-toggle="tab"><img src="{{ url('/public') }}/images/moving_color.png" alt="" class="blue"><img src="{{ url('/public') }}/images/moving_white.png" alt="" class="white"> Moving</a></li>
          <li class=""><a href="#tab2" data-toggle="tab"><img src="{{ url('/public') }}/images/yard_work_color.png" alt="" class="blue"><img src="{{ url('/public') }}/images/yard_work_white.png" class="white"> Yard Work</a></li>
          <li class="active"><a href="#tab8" data-toggle="tab"><img src="{{ url('/public') }}/images/calendar_color.png" alt="" class="blue"><img src="{{ url('/public') }}/images/calendar_white.png" alt="" class="white"> Events</a></li>
          <li class=""><a href="#tab3" data-toggle="tab"><img src="{{ url('/public') }}/images/cleaning_color.png" alt="" class="blue"><img src="{{ url('/public') }}/images/cleaning_white.png" alt="" class="white">Cleaning</a></li>
          <li class=""><a href="#tab4" data-toggle="tab"><img src="{{ url('/public') }}/images/painting_color.png" alt="" class="blue"><img src="{{ url('/public') }}/images/painting_white.png" alt="" class="white">Painting</a></li>
          <li class=""><a href="#tab5" data-toggle="tab"><img src="{{ url('/public') }}/images/tutoring_color.png" alt="" class="blue"><img src="{{ url('/public') }}/images/tutoring_white.png" alt="" class="white">Tutoring</a></li>
          <li class=""><a href="#tab6" data-toggle="tab"><img src="{{ url('/public') }}/images/technology_color.png" alt="" class="blue"><img src="{{ url('/public') }}/images/technology_whiter.png" alt="" class="white">Technology</a></li>
          <li class=""><a href="#tab7" data-toggle="tab"><img src="{{ url('/public') }}/images/odd_jobs.png" alt="" class="blue"><img src="{{ url('/public') }}/images/odd_jobs-white.png" alt="" class="white">Odd Jobs</a></li>
       <!--    <li class=""><a href="#tab8" data-toggle="tab"><img src="{{ url('/public') }}/images/8.png" alt="" class="blue"><img src="{{ url('/public') }}/images/8-white.png" alt="" class="white">Odd Jobs</a></li>   -->                             
    </ul>
  </nav>
    
</div>
<!-- tab content -->
<div class="col-md-9 col-sm-8 wow fadeInRight tab-right nopadding">
<div class="tab-content ">
<div class="tab-pane active text-style" id="tab1">
  <img src="{{ url('/public') }}/images/moving.jpg" alt="" class="servicesimage">
 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusm od tempor incididunt ut labore et dolore magna aliqua. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
 
       
      
</div>
<div class="tab-pane text-style" id="tab2">
  <img src="{{ url('/public') }}/images/yard-work.jpg" alt="" class="servicesimage">
 <p>tempor incididunt ut labore et dolore magna aliqua. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
 
 
</div>

<div class="tab-pane text-style" id="tab8">
  <img src="{{ url('/public') }}/images/yard-work.jpg" alt="" class="servicesimage">
   <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusm od tempor incididunt ut labore et dolore magna aliqua. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
 
  
</div> 

<div class="tab-pane text-style" id="tab3">
  <img src="{{ url('/public') }}/images/cleaning.jpg" alt="" class="servicesimage">
  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusm inting and typesetting industry. Lore m Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galpe specimen book.</p>
 
  
</div>
<div class="tab-pane text-style" id="tab4">
  <img src="{{ url('/public') }}/images/painting.jpg" alt="" class="servicesimage">
   <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusm od tempor incididunt ut labore et dolore magna aliqua. Lorem </p>
 
</div>
<div class="tab-pane text-style" id="tab5">
  <img src="{{ url('/public') }}/images/tutoring.jpg" alt="" class="servicesimage">
   <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusm od tempor incididunt ut labore et dolore magna aliqua. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
 
  
</div>
<div class="tab-pane text-style" id="tab6">
  <img src="{{ url('/public') }}/images/technology.jpg" alt="" class="servicesimage">
   <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusm od tempor incididunt ut labore et dolore magna aliqua. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
 
  
</div>
<div class="tab-pane text-style" id="tab7">
  <img src="{{ url('/public') }}/images/odd-jobs.jpg" alt="" class="servicesimage">
  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusm od tempor incididunt ut labore et dolore magna aliqua. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
 
  
</div>

</div>
    </div>
    </div>
  </div>
  
</section>
<section class="howitwork">
  <div class="container">
    <h2 class="wow fadeInLeft">How We Works</h2>
    <p class="hiw-tp wow fadeInLeft">Donec rutrum congue leo eget malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices <br>
posuere cubilia Curae. </p>
    <div class="row">
      <div class="col-sm-4 wow fadeInUp">
      <div class="work-inner">
        <img src="{{ url('/public') }}/images/post.png" alt="">
        <h3>Post your Job</h3>
        <p>Get an estimate and post your job in minutes. CollegeWrk in your area are notified and matched to your job. </p>
      </div>
      </div>
      <div class="col-sm-4 wow fadeInUp">
        <div class="work-inner"> 
        <img src="{{ url('/public') }}/images/confirm.png" alt="">
        <h3>Confirm your job</h3>
        <p>Get an estimate and post your job in minutes. CollegeWrk in your area are notified and matched to your job. </p>
      </div>
      </div>
      <div class="col-sm-4 wow fadeInUp">
        <div class="work-inner">
        <img src="{{ url('/public') }}/images/work.png" alt="">
        <h3>Get to Work</h3>
        <p>Get an estimate and post your job in minutes. CollegeWrk in your area are notified and matched to your job. </p>
      </div>
      </div>
    </div>
  </div>
  
</section>
<section class="get-started">
  <div class="container">
    <div class="row">
      <div class="col-md-12 wow fadeInLeft">
        <h2>Ready to get started?</h2>
        <p>We take the pain out of moving or home & office projects. CollegeWRK' platform makes the entire process simple, safe, and<br> dare we say...delightful. While we're not professionals (and not the best fit for moving pianos or priceless antiques),<br>  
CollegeWRK is Smart, strong, and eager to help make your moving day a success.</p>
<ul class="list-inline">
        @if(Auth::check())
           <!-- <li><button class="post-btn" data-toggle="modal" data-target="#demo-modal-3">Post A Job</button> </li>
          <li><button class="apply-btn" data-toggle="modal" data-target="#myModal-2">Apply For Work</button></li> -->
          @else
          <li><button class="post-btn" data-toggle="modal" data-target="#demo-modal-3">Post A Job</button> </li>
          <li><button class="apply-btn" data-toggle="modal" data-target="#myModal-2">Apply For Work</button></li>
          @endif
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="testimonial">
 <div class="container theme_columns_carousel">
    
    <!--*-*-*-*-*-*-*-*-*-*- BOOTSTRAP CAROUSEL *-*-*-*-*-*-*-*-*-*-->

    <div id="testimonial_2_columns_text_carousel" class="carousel slide testimonial_columns_text_carousel_wrapper" data-ride="carousel" data-interval="3000" data-pause="hover">
      <h3>Client Feedback</h3>
      <h1>Our Client us because <br>of our quality.</h1>
      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">

        <!--========= First Slide =========-->
        <div class="item active">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 hidden-xs">
              <div class="testimonial_columns_text_carousel_caption">
                 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <div class="author-info">
                  <img src="{{ url('/public') }}/images/client-1.jpg" alt="">
                <div class="author-cont">
               <h4>Rashed ca.</h4>
                <a href="#">Marketing Manager</a>
              </div>
               </div>
                
              </div>
            </div>
            
            <div class="col-xs-12 col-sm-6 col-md-6 ">
              <div class="testimonial_columns_text_carousel_caption">
                 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <div class="author-info">
                  <img src="{{ url('/public') }}/images/client-2.jpg" alt="">
                <div class="author-cont">
                <h4>Maria Rose</h4>
                <a href="#">Certified Engineer</a>
              </div>
               </div>
                
              </div>
            </div>
          </div>
        </div>

        <!--========= Second Slide =========-->
        <div class="item">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 ">
              <div class="testimonial_columns_text_carousel_caption">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <div class="author-info">
                  <img src="{{ url('/public') }}/images/client-1.jpg" alt="">
                <div class="author-cont">
              <h4>Rashed ca.</h4>
                <a href="#">Marketing Ma nager</a>
              </div>
               </div>
              </div>
            </div>
            
            <div class="col-xs-12 col-sm-6 col-md-6 hidden-xs">
              <div class="testimonial_columns_text_carousel_caption">
                 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <div class="author-info">
                  <img src="{{ url('/public') }}/images/client-2.jpg" alt="">
                <div class="author-cont">
                 <h4>Maria Rose</h4>
                <a href="#">Certified Engineer</a>
              </div>
               </div>
                
              </div>
            </div>
          </div>
        </div>

        <!--========= Third Slide =========-->
        <div class="item">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="testimonial_columns_text_carousel_caption">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
               <div class="author-info">
                  <img src="{{ url('/public') }}/images/client-2.jpg" alt="">
                <div class="author-cont">
                <h4>Maria Rose</h4>
                <a href="#">Certified Engineer</a>
              </div>
               </div>
                
              </div>
            </div>
            
            <div class="col-xs-12 col-sm-6 col-md-6 hidden-xs">
              <div class="testimonial_columns_text_carousel_caption">
                 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <div class="author-info">
                  <img src="{{ url('/public') }}/images/client-1.jpg" alt="">
                <div class="author-cont">
                <h4>Rashed ca.</h4>
                <a href="#">Marketing Manager</a>
              </div>
               </div>
                
              </div>
            </div>
          </div>
        </div>

      </div>

      <!--========= Indicators =========-->
      <ol class="carousel-indicators testimonial_columns_text_carousel_indicators">
        <li data-target="#testimonial_2_columns_text_carousel" data-slide-to="0" class="active"></li>
        <li data-target="#testimonial_2_columns_text_carousel" data-slide-to="1"></li>
        <li data-target="#testimonial_2_columns_text_carousel" data-slide-to="2"></li>
      </ol>

    </div>
  </div>
</section>

<section class="contact-us">
  <div class="container">
    <div class="row">
      <div class="col-md-12 wow fadeInDown">
        <div class="contact-inner">
          <h2>Write to Us</h2>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the<br>
industry's standard dummy text ever since the 1500s</p>
     <form class="settingform" id="contactFrm" action="{{ url('home/store') }}" method="POST" enctype="multipart/form-data">
                             {{ csrf_field() }}

  <div class="form-group col-md-6">
    <input type="text" name="name" class="form-control" placeholder="Full Name" id="name" value="" required minlength="2" maxlength="81">
  </div>
  <div class="form-group col-md-6">
    <input type="email" name="email" class="form-control" placeholder="Your Email Address" id="email" value="" required maxlength="181">
  </div>
  <div class="form-group col-md-12">
    <textarea placeholder="Write Message" rows="3" class="form-control" name="message" id="message"  required maxlength="5001"></textarea>
  </div>
  <div class="col-md-12" style="text-align: center;">
  <button type="submit">Send Message</button>
</div>
</form>

        </div>
      </div>
    </div>
  </div>
</section>
<section class="other-info">
  <div class="container">
    <div class="row">
                <?php
            $bussiness_info = DB::table('bussinesssetting')
                                ->get();

          ?>
      @foreach($bussiness_info as $bussiness_infos)
      <?php
           $bussiness_phone_number = $bussiness_infos->phone;
           $bussiness_email = $bussiness_infos->emails;
           $bussiness_address = $bussiness_infos->address;
      ?>
      <div class="col-md-4 col-sm-6 wow fadeInLeft">
        <div class="oth-inner">
          <i class="fa fa-phone" aria-hidden="true"></i>
          <div class="oth-text">
            <h5>Make an appoinment</h5>
          <h4>{{$bussiness_phone_number}}</h4>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 wow fadeInDown ">
         <div class="oth-inner">
          <i class="fa fa-envelope" aria-hidden="true"></i>
          <div class="oth-text">
            <h5>Send Us Message</h5>
            <h4>{{$bussiness_email}}</h4>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-4 wow fadeInRight">
        <div class="oth-inner">
          <!-- <i class="fa fa-home" aria-hidden="true"></i> -->
          <div class="oth-text">
            <!-- Begin Mailchimp Signup Form -->
            <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
            <style type="text/css">
                #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
            </style>
            <div id="mc_embed_signup">
              <form action="https://CollegeWRK.us19.list-manage.com/subscribe/post?u=0f7aa9196520bd891f7bcaa87&amp;id=c3258ec84b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
                    <h2>Subscribe to our mailing list</h2>
                    <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
                    <div class="mc-field-group">
                      <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
                    </label>
                      <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                    </div>
                    <div id="mce-responses" class="clear">
                      <div class="response" id="mce-error-response" style="display:none"></div>
                      <div class="response" id="mce-success-response" style="display:none"></div>
                    </div>    
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_0f7aa9196520bd891f7bcaa87_c3258ec84b" tabindex="-1" value=""></div>
                    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                </div>
              </form>
            </div>
            <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
            <script type='text/javascript'>
              (function($) {
                  window.fnames = new Array(); 
                  window.ftypes = new Array();
                    fnames[0]='EMAIL';
                    ftypes[0]='email';
                    /*fnames[1]='FNAME';
                    ftypes[1]='text';*/
                    /*fnames[2]='LNAME';
                    ftypes[2]='text';
                    fnames[3]='ADDRESS';
                    ftypes[3]='address';
                    fnames[4]='PHONE';
                    ftypes[4]='phone';
                    fnames[5]='BIRTHDAY';
                    ftypes[5]='birthday';*/
                    fnames[6]='MMERGE6';
                    ftypes[6]='number';
              }(jQuery));
              var $mcj = jQuery.noConflict(true);
            </script>
<!--End mc_embed_signup-->
<!--             <link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
            <style type="text/css">
                #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
            </style>
            <div id="mc_embed_signup">
              <form action="https://CollegeWRK.us19.list-manage.com/subscribe/post?u=0f7aa9196520bd891f7bcaa87&amp;id=c3258ec84b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                  <div id="mc_embed_signup_scroll">
                        <label for="mce-EMAIL">Subscribe to our mailing list</label>
                        <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
                        <div style="position: absolute; left: -5000px;" aria-hidden="true">
                          <input type="text" name="b_0f7aa9196520bd891f7bcaa87_c3258ec84b" tabindex="-1" value="">
                        </div>
                        <div class="clear">
                          <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                        </div>
              </form>
            </div> -->
            <!-- <h5>Visit us at address</h5>
            <h4>{{$bussiness_address}}</h4> -->

          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>

<div id="google-reviews"></div>
</section>
 <script>
 $(document).ready( function() {

    $("#contactFrm").validate({
        rules: {
                name: {
                    required: true,
                    minlength: 2,
                    maxlength: 80
                },
                email: {
                    required: true,
                    email: true,
                    maxlength: 180
                },
                messages: {
                  required: true,
                  maxlength: 5000
                }
            },
        messages: {
                name: {
                  required: "This is required field.", 
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 80 characters allowed."
                },
                email: {
                  required: "This is required field.",   
                  email: "Enter valid email",
                  maxlength: "Maximum 180 characters allowed."
                },
                message: {
                  required: "This is required field.",  
                  maxlength: "Maximum 5000 characters allowed."
                }  
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

 });
</script>
    <script>
      wow = new WOW(
        {
          animateClass: 'animated',
          offset:       100,
          callback:     function(box) {
            console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
          }
        }
      );
      wow.init();
      document.getElementById('moar').onclick = function() {
        var section = document.createElement('section');
        section.className = 'section--purple wow fadeInDown';
        this.parentNode.insertBefore(section, this);
      };
   </script>
   <style type="text/css">
     img.servicesimage {
    width: 100%;
    min-height: 593px;

}
   </style>
@stop