<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    
    /*'facebook' => [
    'client_id' => env('FACEBOOK_CLIENT_ID'),
    'client_secret' => env('FACEBOOK_CLIENT_SECRET'),
    'redirect' => env('CALLBACK_URL'),
     ],*/
    'facebook' => [
        'client_id' => env('FB_CLIENT_ID'),
        'client_secret' => env('FB_CLIENT_SECRET'),
        'redirect' => env('FB_REDIRECT')
    ],


/*    'google' => [
        'client_id' => '313550302890-tvmaud53f74qh9shq44ir4s3fvcubj1m.apps.googleusercontent.com',
        'client_secret' => 'x785NxfHxWsmkOCEyGlXYSLY',
        'redirect' => 'http://www.iebasketball.com/collegeworker/callback'],
];*/

'google' => [
        'client_id' => env('G+_CLIENT_ID'),
        'client_secret' => env('G+_CLIENT_SECRET'),
        'redirect' => env('G+_REDIRECT')]
];