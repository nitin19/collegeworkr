<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('comingsoon');
});*/

Route::get('/', function () {
    return view('pages.home');
});

Route::get('/error', function () {
    return view('error');
});

/*Route::get('/chat', function () {
    return view('customerdashboard.chat');
});*/

Auth::routes();

Route::get('/chat', 'ChatController@index');
Route::post('ajaxRequest', 'ChatController@saveChat');
Route::get('getChat/{hireid}/{jobid}','ChatController@getChat');
Route::get('/chathistory/{jobid}','ChatController@getChathistory');
Route::get('/support', 'ChatController@show_adminchat');
Route::post('adminajaxRequest', 'ChatController@saveadminChat');
Route::get('getadminChat/{senderid}/{receiverid}','ChatController@getadminChat');
Route::get('getadminChatmsgcnt/{sndid}/{rcvid}','ChatController@getadminChatmsgcnt');

Route::get('/workeradminchat','ChatController@workeradminchat');
Route::post('workeradminajaxRequest', 'ChatController@saveworkeradminChat');
Route::get('getworkeradminChat/{senderid}/{receiverid}','ChatController@getworkeradminChat');

Route::get('/customeradminchat','ChatController@customeradminchat');
Route::post('customeradminajaxRequest', 'ChatController@savecustomeradminChat');
Route::get('getcustomeradminChat/{senderid}/{receiverid}','ChatController@getcustomeradminChat');

Route::get('/redirect/{service}', 'SocialAuthGoogleController@redirect');
Route::get('/callback/{service}', 'SocialAuthGoogleController@callback');

Route::get('/facebookredirect/{service}', 'SocialAuthFacebookController@facebookredirect');
Route::get('/facebookcallback/{service}', 'SocialAuthFacebookController@facebookcallback');


Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home/store', 'HomeController@store');

Route::get('/pricing', 'PricingController@index')->name('pricing');
Route::get('/aboutus', 'AboutusController@index')->name('aboutus');

Route::get('auth/facebook', 'Auth/LoginController@redirectToProvider');
Route::get('auth/facebook/callback', 'Auth/LoginController@handleProviderCallback');
Route::get('userregister', 'Auth\LoginController@userregister');
Route::POST('/store','UsersController@store');
Route::GET('/storeclient','UsersController@storeclient');
Route::get('/checkuseremail', 'UsersController@checkuseremail');
Route::get('/checkloginemail', 'UsersController@checkloginemail');
/*Route::get('/checkloginpassword', 'UsersController@checkloginpassword');*/

Route::resource('jobpost','JobpostController');
Route::GET('/getequipment','JobpostController@getequipment');
Route::GET('/jobstore','JobpostController@jobstore');
Route::GET('/updatejob','JobpostController@updatejob');
Route::GET('/getalldata','JobpostController@getalldata');
Route::get('/reciept', 'JobpostController@reciept');
Route::get('/checkrefferalcode', 'JobpostController@checkrefferalcode');
Route::GET('/notitysec', 'JobpostController@notitysec');
Route::GET('/workernotitysec', 'JobpostController@workernotitysec');
Route::GET('/completeupdate', 'WorkerjobsController@complete_btn_status_update');
Route::GET('/cancelledupdate', 'WorkerjobsController@cancelled_btn_status_update');
Route::GET('/customercompleteupdate', 'CustomernotificationController@complete_btn_status_update');
Route::GET('/customercancelledupdate', 'CustomernotificationController@cancelled_btn_status_update');
Route::POST('/customerfeedback', 'CustomernotificationController@customer_feedback');
Route::POST('/workerfeedback', 'WorkerjobsController@worker_feedback');

Route::resource('workerdashboard','WorkerdashboardController');
Route::POST('/workerdashboard/store','WorkerdashboardController@store');
Route::get('/jobpreference','WorkerdashboardController@jobpreference');
Route::GET('/workerexperience','WorkerdashboardController@workerexperience');
Route::GET('/updtworkexp','WorkerdashboardController@updtworkexp');
Route::GET('/workerequipment','WorkerdashboardController@workerequipment');
Route::GET('/updtworkequ','WorkerdashboardController@updtworkequ');

Route::resource('customerdashboard','CustomerdashboardController');

Route::resource('customerpay', 'CustomerpaymentController');
Route::resource('customershare', 'CustomershareController');
Route::resource('customerjobs', 'CustomerjobsController');
Route::resource('customerjobhistory', 'CustomerjobhistoryController');
Route::resource('customergift', 'CustomergiftController');
Route::resource('customerhelp', 'CustomerhelpController');

Route::resource('workerpayment', 'WorkerpaymentController');
Route::resource('workerjobs', 'WorkerjobsController');
//================ 12-02-2019 ================//
Route::GET('/updateworkedhour', 'WorkerjobsController@updateWorkedHour'); 
Route::GET('/hourdetails', 'CustomernotificationController@hourDetails'); 
Route::GET('/updatehourstatus', 'CustomernotificationController@updateHourStatus'); 
Route::GET('/pendingpayments', 'AdminclientpaymentController@pendingPayments'); 
Route::GET('/adminallpayments', 'AdminclientpaymentController@adminAllPayments'); 
Route::GET('/createuserstripe', 'JobpostController@createUserStripe'); 
Route::GET('/processclientpayments', 'AdminclientpaymentController@processClientPayments'); 
Route::GET('/updatepoststatus', 'JobpostController@updatePostStatus'); 
Route::GET('/readallnotifications', 'JobpostController@readAllNotifications'); 
Route::GET('/updateworkerpayment', 'AdminrequestpaymentController@updateWorkerPayment'); 
//============================================//
Route::resource('workerprofile','WorkerdashboardController');
Route::resource('socialworkerprofile','WorkerdashboardController');
Route::resource('workershare','WorkershareController');
Route::resource('workerpay','WorkerpayController');
Route::resource('workerhelp','WorkerhelpController');
Route::GET('adminworker/update','AdminworkerController@update');
Route::GET('admincustomer/update','AdmincustomerController@update');

Route::GET('admindashboard','AdmindashboardController@index');

Route::POST('/adminupdate/worker','AdminupdateController@worker');
Route::POST('/adminupdate/customer','AdminupdateController@customer');
Route::resource('adminjobs','AdminjobsController');
Route::resource('adminhire','AdminhireController');
Route::POST('adminhirecreate','AdminhireController@adminhirecreate');
Route::GET('viewall','AdminhireController@viewall');
Route::resource('adminmessages','AdminmessagesController');
Route::POST('adminmessages/send_mail','AdminmessagesController@send_mail');
Route::resource('clientpayment','AdminclientpaymentController');
Route::resource('requestpayment','AdminrequestpaymentController');
Route::resource('adminprofile','AdminprofileController');
Route::resource('adminnotification','AdminnotificationController');
Route::resource('admincustomer','AdmincustomerController');
Route::resource('adminworker','AdminworkerController');
Route::resource('adminbussiness','AdminbussinessController');
Route::POST('/workerpay/store','WorkerpayController@store');
Route::POST('/update','WorkerpayController@update');
Route::POST('/update', 'WorkerjobsController@update');
Route::resource('customernotification', 'CustomernotificationController');
Route::POST('/update', 'CustomernotificationController@update');
Route::resource('workernotification', 'WorkernotificationController');
Route::POST('/update', 'WorkernotificationController@update');
Route::POST('/update', 'CustomerjobhistoryController@update');
Route::GET('/updatestatus', 'CustomerdashboardController@update_status');
Route::GET('/workersendinvitation', 'WorkershareController@worker_send_invitation_code');
Route::GET('/customersendinvitation', 'CustomershareController@customer_send_invitation_code');
Route::resource('customerpayment', 'CustomerpaymentsController');
Route::GET('/rejectpayment', 'AdminrequestpaymentController@reject_payment_update');

Route::GET('/forgetpassword', 'Auth\ForgotPasswordController@sendemail');
Route::GET('/checkforgetemail', 'Auth\ForgotPasswordController@checkforgotemail');

Route::POST('/workerprofileupdate/{id}','WorkerdashboardController@worker_profile_update');


