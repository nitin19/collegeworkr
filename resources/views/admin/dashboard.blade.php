@extends('layouts.defautladmin')

@section('title', 'Admindashbaord')

@section('content')
<?php
use \App\Http\Controllers\AdmindashboardController; ?>
<div class="breadcrumbs">
            <div class="col-sm-12">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Welcome, <span class="color_text">{{ ucfirst($username) }}</span></h1>
                    </div>
                </div>
            </div>
            
        </div>

        <div class="content mt-3">

           <div class="col-sm-6 col-lg-3">
           	<a href="{{ url('/admincustomer') }}">
                <div class="card text-white bg-flat-color-1">
                    <div class="card-body">
                        <div class="dropdown float-right">
                          
                            <img src="{{ url('/public') }}/admin/images/slicing/customers.png" alt="Customers_icon" class="dash-img-col">
                        </div>
                         <p class="text-light">Total number Customers</p>
                        <h4 class="mb-0">
                            <span class="count">{{ $cusomter_count }}</span>
                        </h4>
                      </div>

                </div></a>
            </div>
            <!--/.col-->

            <div class="col-sm-6 col-lg-3">
            	<a href="{{ url('/adminworker') }}">
                <div class="card text-white bg-flat-color-2">
                    <div class="card-body">
                        <div class="dropdown float-right">
                            
                            <img src="{{ url('/public') }}/admin/images/slicing/workers.png" alt="Customers_icon">
                        </div>
                        <p class="text-light">Total Number of workers</p>
                        <h4 class="mb-0">
                            <span class="count">{{ $worker_count }}</span>
                        </h4>
                        

                    </div>
                </div></a>
            </div>
            <!--/.col-->

            <div class="col-sm-6 col-lg-3">
            	<a href="{{ url('/adminjobs') }}">
                <div class="card text-white bg-flat-color-3">
                    <div class="card-body">
                        <div class="dropdown float-right">
                            
                            <img src="{{ url('/public') }}/admin/images/slicing/jobs.png" alt="Customers_icon">
                        </div>
                        <p class="text-light">Total Posted Jobs</p>
                        <h4 class="mb-0">
                            <span class="count">{{ $jobpost_count }}</span>
                        </h4>

                    </div>

                       
                </div></a>
            </div>
            <!--/.col-->

            <div class="col-sm-6 col-lg-3"><a href="#">
                <div class="card text-white bg-flat-color-4">
                    <div class="card-body">
                        <div class="dropdown float-right">
                            
                            <img src="{{ url('/public') }}/admin/images/slicing/applied_jobs.png" alt="Customers_icon">
                        </div>
                         <p class="text-light">Total Hire Jobs</p>
                        <h4 class="mb-0">
                            <span class="count">{{ $hire_count }}</span>
                        </h4>
                       
                      

                    </div>
                </div></a>
            </div>
            <!--/.col-->
           
            </div>

<div class="content Contextua_sec">
	<div class="col-sm-9">
		<h2>Customers Details<span class="badge bad_border pull-right"><a href="{{ url('/admincustomer')}}">View all</a></span></h2>
	</div>
	<div class="col-sm-3">
		<h2><span class="badge pull-left">Job Notifications</span><span class="badge bad_border pull-right"><a href="{{ url('/adminjobs')}}">View all</a></span></h2>
	</div>
	<div class="col-sm-9">
		  <table class="table-width col_or wow fadeIn animated delay-3s">
		    <thead>
		      <tr class="table-active">
		        <th class="col8">Name</th>
		        <th class="col6">Email</th>
		        <!-- <th>Phone</th> -->
<!--		        <th>Worker Type</th>
-->		        <th class="col8">Address</th>
               <!--  <th>City</th>
                <th>State</th> -->
		      </tr>
		    </thead>
		    <tbody>
		    	<?php $count_customer = count($customer_info); ?>
		    	@if($count_customer>0)
		    	@foreach($customer_info as $customer)
		      <?php
		    	$customeruserid = $customer->id;
		    	  $imagename = DB::table('users')
                     ->where('id','=',$customeruserid)
                     ->first();
                   $image_users = $imagename->image;
                   ?>
		      <tr class="wow fadeIn animated delay-5s">
		        <td class="col8">@if($image_users=='')
				<img src="{{ url('/public') }}/images/avatar.jpg" alt="John Doe" class="mr-2 rounded-circle" style="width:40px;height:40px">
				@else 
				<img src="{{ url('/public') }}/uploads/profile/{{ $image_users }}" alt="John Doe" class="mr-2 rounded-circle" style="width:40px;height:40px">
				@endif
				{{ $customer->name }} {{ $customer->lastname }}</td>
		        <td class="col6">{{ $customer->email }}</td>
		        <!-- <td>{{ $customer->phone }}</td> -->
<!--		        <td>{{ $customer->workertype }}</td>
-->		        <td class="col6">{{ $customer->address }}</td>
                <!-- <td>{{ $customer->city }}</td>
                <td>{{ $customer->state }}</td> -->
		      </tr>      
@endforeach
@else 
<tr><h3 class="norecordmsg">No Record Found</h3></tr>@endif
		    </tbody>
		  </table>
	</div>
	<div class="col-sm-3  wow fadeInRight animated delay-5s">

	<div class="clean_sec">	
		<ul>
<?php $count_jobs = count($job_info); ?>
@if($count_jobs>0)
@foreach($job_info as $job) 
<?php
$created_date = $job->created_at;
$categories = $job->job_heading;
$categories_info = DB::table('categories')
				   ->where('id','=',$categories)
				   ->where('status','=','1')
				   ->where('deleted','=','0')
				   ->get();
		    ?>
		    @foreach($categories_info as $categories_infos)
		    <?php
				   $cat_name = $categories_infos->cat_name;			   
				   $time = strtotime($created_date);
			
			?>
			<li>
				<h6> {{ substr($cat_name, 0, 30) }}<span class="pull-right"> <?php echo AdmindashboardController::humanTiming($time).' ago'; ?></span></h6>
				<p> {{ substr($job->job_description, 0, 70) }} </p>
			</li>
			@endforeach
			@endforeach
			@else 
			<h3 class="norecordmsg">No Record Found</h3>
			@endif
			</ul>
	</div>
	</div>	
</div>

<!-- <div class="content border mt-4 mb-4" >
	<div class="col-sm-12 packag_sec p-4" style="background-color: #ffffff;">
	<h2>Packages<span class="packag_head">( {{ $package_count }} )</span></h2>
	<div class="Packag_sec">
		<div class="col-sm-4">
			<div class="media Packag_sec" style="background-color: #383c45;color:#fff;">
			    <img src="{{ url('/public') }}/admin/images/slicing/Bronze.png" alt="John Doe" class="mr-3 rounded-circle" style="width:60px;">
			    <div class="media-body">
			      <h4>Bronze Package</h4>
			      <p>{{ $bronze_count }} Members</p>      
			    </div>
		  	</div>
	  	</div>
	  	<div class="col-sm-4">
		  	<div class="media Packag_sec" style="background-color: #25a6dc;color:#fff;">
			    <img src="{{ url('/public') }}/admin/images/slicing/Silver.png" alt="John Doe" class="mr-3 rounded-circle" style="width:60px;">
			    <div class="media-body">
			      <h4>Silver Package</h4>
			      <p>{{ $silver_count }} Members</p>      
			    </div>
		  	</div>
	  	</div>
	  	<div class="col-sm-4">
		  	<div class="media Packag_sec" style="background-color: #ff6666;color:#fff;">
			    <img src="{{ url('/public') }}/admin/images/slicing/Gold.png" alt="John Doe" class="mr-3 rounded-circle" style="width:60px;">
			    <div class="media-body">
			      <h4>Gold Package</h4>
			      <p>{{ $gold_count }} Members</p>      
			    </div>
		  	</div>
	  	</div>
	</div>
	</div>
</div>	 -->
    
<div class="content Contextua_sec">
	<div class="col-sm-9">
		<h2>WRKers Details<span class="badge bad_border pull-right"><a href="{{ url('/adminworker')}}">View all</a></span></h2>
	</div>
	<!--<div class="col-sm-3">
		<h2><span class="badge pull-left">Recenly Applied Jobs</span></h2>
	</div> -->
	
	  <div class="col-sm-9">
		  <table class="table-width wow fadeIn aniobject-fitmated delay-3s">
		    <thead>
		      <tr class="table-active">
		        <th class="col4">Name</th>
		        <th class="col4">Tier</th>
		        <th class="col4">Email</th>
		        <!-- <th>Phone</th> -->
		        <th class="col4">Rating</th>
		        <th class="col4">Address</th>
                <!-- <th>City</th>
                <th>State</th> -->
		      </tr>
		    </thead>
		    <tbody>
		    	<?php $count_worker = count($worker_info); ?>
		    	@if($count_worker>0)
		    	@foreach($worker_info as $worker)
		    	<?php
		    	  $workeruserid = $worker->id;
		    	  $imagename = DB::table('users')
                     ->where('id','=',$workeruserid)
                     ->first();
                   $image_users = $imagename->image;
                  
                   ?>
		      <tr class="wow fadeIn animated delay-5s">
		        <td class="col4">@if($image_users=='')
				<img src="{{ url('/public') }}/images/avatar.jpg" alt="John Doe" class="mr-3 rounded-circle" style="width:40px;height:40px">
				@else 
				<img src="{{ url('/public') }}/uploads/profile/{{ $image_users }}" alt="John Doe" class="mr-3 rounded-circle" style="width:40px;height:40px">
				@endif
				{{ $worker->name }} {{ $worker->lastname }} </td>
				
				<!--modified code 30nov2018-->
				
			<?php 

                    $feedback = DB::table('feedback')
                            ->where('worker_id',$workeruserid)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->sum('feedback_rating');
                    //echo $feedback ;

                     $feedback_count = DB::table('feedback')
                            ->where('worker_id',$workeruserid)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->count('feedback_rating');
                     $customer_feedback_count = DB::table('feedback')
                            ->where('worker_id',$workeruserid)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->count('user_id');

                    $feedback;
                          $feedback_count;
                          $customer_feedback_count;
                          if($feedback!='0' && $feedback_count!='0' && $customer_feedback_count!='0'){
                              $feedback_average =  $feedback / $feedback_count;
                          } else {
                              $feedback_average = '';
                          }
                    ?>
				<td class="col4">
					<h6><span class="rating_sec">
                        @if($feedback_average <= 2)
                        Brownze
                        @elseif($feedback_average > 3 && $feedback_average <= 4)
                        Silver
                        @elseif($feedback_average > 4 && $feedback_average <= 5)
                        Gold
                        @else 
                        @endif
                      </span>
                    </h6>
				</td>
				<!--end-->
				
		        <td class="col4">{{ $worker->email }}</td>
		        <!-- <td>{{ $worker->phone }}</td> -->
		        <td class="col4">
                 <?php 

                   /* $feedback = DB::table('feedback')
                            ->where('user_id',$workeruserid)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->sum('feedback_rating');
                    $feedback_count = DB::table('feedback')
                            ->where('user_id',$workeruserid)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->count('feedback_rating');
                    $customer_feedback_count = DB::table('feedback')
                            ->where('user_id',$workeruserid)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->count('user_id');

                    $feedback;
                          $feedback_count;
                          $customer_feedback_count;
                          if($feedback!='0' && $feedback_count!='0' && $customer_feedback_count!='0'){
                              $feedback_average =  $feedback / $feedback_count;
                          } else {
                              $feedback_average = '';
                          } */
                    ?>

                    <h6><span>Rating:</span> <span class="rating_sec">
            
                        @if($feedback_average <= 2)
                        {{$feedback_average}}
                        <img src="{{url('/public')}}/images/brownz.png">
                        ({{$customer_feedback_count}}) 
                        @elseif($feedback_average > 3 && $feedback_average <= 4)
                        {{$feedback_average}}
                        <img src="{{url('/public')}}/images/silver.png">
                        ({{$customer_feedback_count}}) 
                        @elseif($feedback_average > 4 && $feedback_average <= 5)
                        {{$feedback_average}}
                        <img src="{{url('/public')}}/images/gold.png">
                        ({{$customer_feedback_count}})
                        @else 
                        @endif

                      </span>
                    </h6> </td>
		        <!-- <td>{{ $worker->workertype }}</td> -->
		        <td class="col4">{{ $worker->address }}</td>
                <!-- <td>{{ $worker->city }}</td>
                <td>{{ $worker->state }}</td> -->
		      </tr>      
@endforeach
		   @else
		   <h3 class="norecordmsg">No Record Found</h3>
		   @endif
		    </tbody>
		  </table>
	</div>
	<div class="col-sm-3 wow fadeInRight animated delay-5s">

	</div>  
    
    	
@include('layouts.footer_admin') 
  
</div> 

    </div>
    @stop
    
    <style>
 
    </style>