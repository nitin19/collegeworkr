@extends('layouts.defautladmin')

@section('title', 'Adminpayments')

@section('content')

<div class="content customers_detail">
  <div class="col-sm-12">
      <div class="adminjob">
    <h2>All Payments<span class="badge pull-right"> 
  <!--  <form action="" method="get"><input type="text" placeholder="Search.." name="search">
        </form> </h2> -->
        </div>
  </div>


  <div class="col-sm-12">
    <?php 
  //echo'<pre>';
  //print_r($allpayments);
  //echo'</pre>';

  ?>
      <table class="table-width col_or">
        <thead>
          <tr class="table-active">
            <th class="col3">Worker Name</th>
            <th class="col2">Job Name</th>
            <th class="col2">Job Status</th>
            <th class="col2">Routing Number</th>
            <th class="col2">Account Number</th>
            <th class="col2">Client Amount</th>
            <th class="col3">Client Payment Status</th>
            <th class="col2">Worker Amount</th>
            <th class="col3">Worker Payment Status</th>
            
          </tr>
        </thead>
      <tbody>
          <?php if( count($allpayments) > 0 ):
          foreach ($allpayments as $paymentDetails) {
            $worker_name = $paymentDetails->name;
            $worker_id = $paymentDetails->worker_id;
            $client_id = $paymentDetails->client_id;
            $client_job_id = $paymentDetails->client_job_id;
            $work_payment_data = DB::table('workerpayment')->where('user_id', $worker_id)->first();
            ?>
          <tr>  
            <td><?php echo $worker_name; ?></td>
            <td><?php echo $paymentDetails->cat_name; ?></td>
            <td><?php echo $paymentDetails->job_status; ?></td>
            <td><?php if( !empty($work_payment_data) ): echo $work_payment_data->bank_routing_number; endif; ?></td>
            <td><?php if( !empty($work_payment_data) ): echo $work_payment_data->bank_account_number; endif; ?></td>
            <td>$<?php echo $paymentDetails->amount_paid;?></td>
            <td><label class="badge badge-success">Success</label></td>
            <td>$<?php echo $paymentDetails->amount_paid;?></td>
            <td><?php if( $paymentDetails->worker_payment_status == 1 ): echo '<label class="badge badge-success">Success</label>'; else : echo  '<label class="badge badge-danger">Rejected</label>'; endif; ?></td>
            <td>
              
            </td>
          </tr>
            <?php
          }
          else:
            echo'<tr><td colspan="6">No Record found</td></tr>';
          endif;  
          ?>    
        </tbody>
      </table>
            <div class="content pagi_div">    

    
</div>
</div>
  
@include('layouts.footer_admin') 
  
</div> 
<style type="text/css">
input[type="text"] {
    width: 79%;
}
.submit_btn {
  background: #25a6dc;
  color: #fff;
  font-weight: bold;
  border: none;
  padding: 6px 28px 7px 28px;
  border-radius: 5px;
}
.modal-title {
  text-align: center;
  font-size: 24px;
  font-weight: bold;
}
.markmodel {
    background: #25a6dc;
    border: none;
    padding: 7px 2px;
    border-radius: 5px;
    width: 100%;
    margin-bottom: 2px;
    font-size: 12px;
}
#reject_btn {
    background: #ff0000;
border: none;
padding: 7px 2px;
border-radius: 5px;
width: 100%;
margin-bottom: 2px;
font-size: 12px;
}
</style>
@stop