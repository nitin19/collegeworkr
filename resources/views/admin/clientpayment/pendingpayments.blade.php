@extends('layouts.defautladmin')

@section('title', 'Admincustomer')

@section('content')

<?php
//echo'<pre>';
//print_r($pending_paymentData);        
//echo'</pre>';
?>

<div class="content customers_detail">
    <div class="col-sm-12">
        <div class="adminjob">
        <h2>Clients Payment <span class="badge pull-right"> 
    <!-- <form action="" method="get"><input type="text" placeholder="Search.." name="search">
        <button type="submit"><i class="fa fa-search"></i></button></form> --></h2>
        </div>
    </div>
    <div class="col-sm-12">
          <table class="table-width col_or">
            <thead>
              <tr class="table-active">
                <th class="col4">Client Name</th>
                <th class="col5">Worker Name</th>
                <th class="col5">Job Tittle</th>
                <th class="col4">Approved Time</th>
                <th class="col4">Amount</th>
                <th class="col2">Action</th>
              </tr>
            </thead>
            <?php 
            //echo'<pre>';
            //print_r($pending_paymentData );
            //echo'</pre>';
            if( !empty($pending_paymentData) ): 
                foreach ($pending_paymentData as $paymentData) {
                    # code...
                    $hireid = $paymentData->hireid;
                    $client_id = $paymentData->user_id;
                    $client_job_id = $paymentData->client_job_id;
                    $worker_id = $paymentData->worker_id;
                    $hours_to_complete = $paymentData->hours_to_complete;
                    $mints_to_complete = $paymentData->mints_to_complete;
                    $updated_hours = $paymentData->updated_hours;
                    $updated_mints = $paymentData->updated_mints;
                    $price_per_hour = 35;
                    $price_per_mint = 35 / 60;
                    if($updated_hours !='' && $updated_mints !=''){
                        $total_time_mints = intval($updated_hours) * 60 + intval($updated_mints);
                        $approved_time_formated = $updated_hours .' : '. $updated_mints .'hr';
                    }else{
                        $total_time_mints = intval($hours_to_complete) * 60 + intval($mints_to_complete); 
                        $approved_time_formated = $hours_to_complete .' : '. $mints_to_complete .'hr';
                    }

                    $clientDetails = DB::table('users')
                                    ->where('id', '=', $client_id )
                                    ->first();
               
            ?>
                <tr>
                <td class="col4"><?php echo $clientDetails->name; ?></td>
                <td style="" class="col5"><?php echo $paymentData->name; ?></td>
                <td class="col5"><?php echo $paymentData->cat_name; ?></td>
                <td class="col4"><label class="badge badge-success"><?php echo $approved_time_formated; ?></label></td>
                <td class="col4"><?php $total_amount = $total_time_mints *  $price_per_mint;  
                                //setlocale(LC_MONETARY,"en_US");
                                echo $amountToPay = money_format("$%i", round($total_amount));  
                                ?>
                </td>
                <td class="col2"><a href="#" class="payNow btn btn-primary" data-payableamount="<?php echo  $total_amount; ?>" data-clientid="<?php echo $client_id; ?>" data-jobid="<?php echo $client_job_id; ?>" data-workerid="<?php echo $worker_id;  ?>" data-hireid="<?php echo $hireid; ?>" data-jobtitle="<?php echo $paymentData->cat_name; ?>"> Pay Now </a></td>
              </tr>
              <?php }
               else: 
                echo'<tr class="text-center"><td colspan="5">No Pending Payment</td></tr>';
            endif; ?>  

            </tbody>
          </table>




    </div>
        
@include('layouts.footer_admin') 
</div> 
</div>
<script type="text/javascript">
  jQuery(document).ready(function($){
    $('body').on('click', '.payNow', function(e){
      e.preventDefault();
      var payableamount  = $(this).data('payableamount');
      var clientid  = $(this).data('clientid');
      var worker_id  = $(this).data('workerid');
      var jobid  = $(this).data('jobid');
      var hireid  = $(this).data('hireid');
      var jobtitle  = $(this).data('jobtitle');
      if(confirm('Please confirm you want to process this payment.')){
      jQuery.ajax({ 
            method : 'GET',
            url  : "{{ url('/processclientpayments') }}",
            data:{'payableamount' : payableamount, 'clientid' : clientid, 'worker_id' : worker_id, 'jobid' : jobid , 'hireid' : hireid, 'jobtitle' : jobtitle }, 
            success :  function(resp) {
              //alert(resp)
              var data = jQuery.parseJSON( resp );
              alert( data.msg );
              
              console.log(data);
              window.location.reload();
              
              
            },
            error : function(resp){
              // var data = jQuery.parseJSON( resp );
               alert( 'Something went wrong.please try again.' );
               window.location.reload();
            }
      });
    }
    })
  })

</script>


@stop