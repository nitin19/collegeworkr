@extends('layouts.defautladmin')

@section('title', 'Client Payment')

@section('content')


<div class="content Contextua_sec customers_detail">
<?php
				$client_jobid = $client_payment->client_jobid;
		    	$client_user_id = $client_payment->user_id;

		    	$hire_info = DB::table('hire')
		    		 ->where('client_job_id', $client_jobid)
                     ->first();
                $worker_id = $hire_info->worker_id;
                $work_user = DB::table('users')->where('id', $worker_id)->first();
                $workername = $work_user->firstname.' '.$work_user->lastname;
                $workerimage = $work_user->image;    
                $job_heading_id = $client_payment->job_heading;
				$job_heading_info = DB::table('categories')->where('id', $job_heading_id)->get();
                $client_user = DB::table('users')->where('id', $client_user_id)->get();
?>
    <div class="col-sm-12">
        
            <div class="style-done">
                <div class="row">
                    <div class="col-sm-6 worker-profile">
                    @foreach($client_user as $client_users)
                    <?php 
                    $clientimage = $client_users->image;
                    $client_name = $client_users->firstname.' '.$client_users->lastname; ?>
                    
                    @if($clientimage=='')
                    <img src="{{ url('/public/admin/images/avatar.jpg') }}" class="profile-img1">
                    @else    
                    <img src="{{ url('/public/uploads/profile/') }}/{{ $clientimage }}" class="profile-img1">
                    @endif
                    <h6>{{ $client_name }}</h6>
                    @endforeach
                    </div>
                    
                    <div class="col-sm-6 client-profile">
                    @if($workerimage=='')
                    <img src="{{ url('/public/admin/images/avatar.jpg') }}" class="profile-img1">
                    @else    
                    <img src="{{ url('/public/uploads/profile/') }}/{{ $workerimage }}" class="profile-img1">
                    @endif
                        <h6>{{ $workername }}</h6>
                    </div>
                </div>
                <div class="profile-text">
                    <div class="row">
                        <div class="col-sm-4">
                            <p class="tital-pro">Job-Title :</p>
                        </div>
                        <div class="col-sm-8">
                            @foreach($job_heading_info as $job_heading_infos)
                            <?php $job_heading = $job_heading_infos->cat_name; ?>
                            <p>{{ $job_heading }}</p>
                            @endforeach
                        </div>
                    </div>
                    <div class="row">    
                        <div class="col-sm-4">
                            <p class="tital-pro">Total Amount :</p>
                        </div>
                        <div class="col-sm-8">
                            <p>{{ '$'.$client_payment->item_price }} 
                            <!-- <button type="button" class="btn btn-primary pull-right">Approved</button> -->
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <p class="tital-pro">Discription :</p>
                        </div>
                        <div class="col-sm-8">
                            <p>{{ $client_payment->job_description }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <p class="tital-pro">Feedback :</p>
                        </div>
                        <div class="col-sm-8">
                            <p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eleifend semper imperdiet. Phasellus scelerisque nisl ante, vitae sodales tellus faucibus a. Ut bibendum tincidunt leo sed maximus. Sed at lectus purus.</p>
                        </div>
                    </div>
                </div>
                </div>
            </div>
</div>
<style>
img.profile-img1 {
    height: 160px;
    width: 160px;
    border: 3px solid #808080;
    border-radius: 50%;
     -webkit-transition: .5s ease;
    transition: .5s ease;
     
}
button.btn.btn-primary.pull-right {
    background-color: #e74c3c;
    border: 1px solid #e74c3c;
    border-radius: 2px;
    padding: 4px 10px!important;
    margin-top: -13px;
}
img.profile-img1:hover {
-ms-transform: scale(1.05,1.05);
    -webkit-transform: scale(1.05,1.05);
    transform: scale(1.05,1.05);
    box-shadow: 0px 0px 20px #00000061;

}
.worker-profile {text-align: center;border-right: 1px solid #cccccc;}
.client-profile {text-align: center}

.profile-done h6 {
    font-size: 20px;
    text-transform: capitalize;
    padding: 15px 0px;
    color: #202020;
    font-weight: 700;
}
.profile-done {
    border: 1px solid #cccccc;
    padding: 40px;
}
.profile-text {
    padding-top: 40px;
}
.profile-text .row {
    /* border-bottom: 1px solid #ccc; */
    padding: 10px;
}
.tital-pro {
    color:#202020;
    font-weight: bold;
}
.textbox-back {background-color: transparent;}
.worker-profile h6 {
    font-size: 20px;
    margin-top: 15px;
    text-transform: capitalize;
    font-weight: bold;
    font-family: "proxima-semibold";
}
.client-profile h6 {
    font-size: 20px;
    margin-top: 15px;
    text-transform: capitalize;
    font-weight: bold;
    font-family: "proxima-semibold";
}
</style>
@stop