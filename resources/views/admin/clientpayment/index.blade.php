@extends('layouts.defautladmin')

@section('title', 'Admincustomer')

@section('content')


<div class="content customers_detail">
	<div class="col-sm-12">
	    <div class="adminjob">
		<h2>Clients Payment <span class="badge pull-right"> 
	<!-- <form action="" method="get"><input type="text" placeholder="Search.." name="search">
        <button type="submit"><i class="fa fa-search"></i></button></form> --></h2>
        </div>
	</div>
	<div class="col-sm-12">
		  <table class="table-width col_or">
		    <thead>
		      <tr class="table-active">
		        <th class="col4">Client Name</th>
		      	<th class="col5">Worker Name</th>
		        <th class="col5">Job Tittle</th>
		        <th class="col4">Amount</th>
		        <th class="col2">Action</th>
		      </tr>
		    </thead>
		    <?php
		    $count = 0;
		    ?>
		    @if($client_payment_info!='')
		    @foreach($client_payment_info as $client_payment)
		      <?php
		    	$client_jobid = $client_payment->client_jobid;
		    	$client_user_id = $client_payment->user_id;
		    	$hire_info = DB::table('hire')
		    		 ->where('client_job_id',$client_jobid)
                     ->first();
                ?>
                <?php
                if($hire_info!=''){
					$worker_id = $hire_info->worker_id;
				} else {
					$worker_id = '...';
				}
                $work_user = DB::table('users')->where('id', $worker_id)->first();
                if($work_user!=''){
					$workername = $work_user->firstname.' '.$work_user->lastname;
				} else {
					$workername = '...';
				}
                     
                $job_heading_id = $client_payment->job_heading;
				$job_heading_info = DB::table('categories')->where('id', $job_heading_id)->first();
				if($job_heading_info!=''){
					$job_heading = $job_heading_info->cat_name;
				} else {
					$job_heading = '...';
				}
                $client_user = DB::table('users')->where('id', $client_user_id)->first();
                if($client_user!=''){
					$client_name = $client_user->firstname.' '.$client_user->lastname;
				} else {
					$client_name = '...';
				}
                
                ?>
                
		        <tr>
		        <td class="col4">{{ $client_name }}</td>
		        <td style="color: #808080;" class="col5">{{ $workername }}</td>
		        <td class="col5">{{ $job_heading }}</td>
		        <td class="col4">{{ '$'.$client_payment->paid_amount }}</td>
		        <td class="col2"><a href="{{route('clientpayment.show',$client_payment->id)}}"><i class="fa fa-eye mr-3" aria-hidden="true"></i></a></td>
		      </tr>
@endforeach
@else
<tr>
	No Payment Here...
</tr>
@endif
		    </tbody>
		  </table>
	</div>
		
@include('layouts.footer_admin') 
  
</div> 
</div>
@stop