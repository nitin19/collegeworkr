@extends('layouts.defautladmin')

@section('title','Update User')

@section('content')
<div class="content Contextua_sec customers_detail">
    <h1>Edit Customer Profile</h1>
    @include('layouts.flash-message') 
<form method="POST" action="{{url('/')}}/adminupdate/customer" id="updateprofile" accept-charset="UTF-8" enctype='multipart/form-data'>
  {{ csrf_field() }}
  <div class="row bussiness">
<div class="col-sm-6"><label>First Name</label><input type="text" name="firstname" value="{{ $customer_info->firstname }}" required="required"></div>
<div class="col-sm-6"><label>Last Name</label><input type="text" name="lastname" value="{{ $customer_info->lastname }}" required="required"></div></div>
<div class="row bussiness">
<div class="col-sm-6"><label>Email Address</label><input type="text" name="email" value="{{ $customer_info->email }}" required="required"></div>
<div class="col-sm-6"><label>Phone</label><input type="text" name="phone" value="{{ $customer_info->phone }}" required="required"  onkeypress="this.value=this.value.replace(/[^0-9]/g,'');"></div>
</div>
<div class="row bussiness">
<div class="col-sm-6"><label>Password</label><input type="password" name="password" id="password"></div>    
<div class="col-sm-6"><label>Confirm Password</label><input type="password" name="confpwd" id="password"></div>
</div>
<div class="row bussiness">
  <div class="col-sm-6">
  <label>Profile Image</label>
  <input type="file" name="input_img">
<?php
if($customer_info->image==''){
  ?>
<img class="profileimage" src="{{ url('/public/admin/images/admin.jpg') }}">
<?php
}
else{
  ?>
<img class="profileimage" src="{{ url('/public/uploads/profile/')}}/{{ $customer_info->image }}">
<?php
}
?>
</div></div>
<div class="row bussiness">
<div class="col-sm-6"><label>City</label><input type="text" name="city" value="{{ $customer_info->city }}"></div>
<div class="col-sm-6"><label>State</label><input type="text" name="state" value="{{ $customer_info->state }}"></div></div>
<div class="row bussiness">
    <div class="col-sm-6"><label>Address</label><textarea name="address">{{ $customer_info->address }}</textarea>
</div>
	<div class="col-sm-6"><label>Address1</label><textarea name="address1">{{ $customer_info->address1 }}</textarea>
</div></div>
<div class="row bussiness"><div class="col-sm-6">
<input type="hidden" name="id" value="{{ $customer_info->id }}">
	<input type="submit" name="submit" value="Update" class="update_buss"></div></div>
</form>
<script type="text/javascript">
 $(document).ready( function() {
 $.validator.addMethod("alphaLetterNumber", function(value, element) {
     return this.optional(element) || value == value.match(/^[a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });

  $.validator.addMethod("alphaLetterNumberSpace", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });

  $.validator.addMethod("alphaLetter", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });
     $.validator.addMethod("valueNotEquals", function(value, element, arg){
     return arg !== value;
  }, "Value must not equal arg.");
 $( "#updateprofile" ).validate({
  rules: {
                firstname: {
                    required: true,
                    alphaLetter:true,
                    minlength: 2,
                    maxlength: 150
                },
                lastname: {
                    required: true,
                    alphaLetter: true,
                    minlength: 2,
                    maxlength: 60
                },
                email: {
                    required: true,
                    email: true
                },
                phone: {
                  required: true,
                  minlength: 10,
                  maxlength: 14
                },
                address: {
                    required: true,
                    minlength: 6,
                    maxlength: 60
                },
                address1: {
                    required: true,
                    minlength: 6,
                    maxlength: 60
                },
                city: {
                    required: true
                },
                state: {
                    required: true
                }
  },
  messages: {
              firstname: {
                  required: "Please enter your firstname.",
                   alphaLetter:"Only letters are allowed",
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 150 characters allowed."
                },
              lastname: {
                  required: "Please enter your lastname.",
                  alphaLetter:"Only letters are allowed",
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 60 characters allowed."
                },
             
              email: {
                  required: "Please enter your email.",
                  email: "Enter valid email."                 
                },
                phone: {
                  required: "Please enter your phone number.",
                  minlength: "Minimum 10 digits required.",
                  maxlength: "Maximum 14 digits required."
                },
                address: {
                  required: "Please enter your Address.",
                  minlength: "Minimum 6 characters required.",
                  maxlength: "Maximum 60 characters allowed."
                },
                address1: {
                  required: "Please enter your Address.",
                  minlength: "Minimum 6 characters required.",
                  maxlength: "Maximum 60 characters allowed."
                },
                city: {
                    required: "Please enter your City."
                },
                state: {
                    required: "Please enter your State."
                }
              },
             
         });  

 });
</script>
</div>
 </div>
<style type="text/css">
.bussiness label { width: 32% }	
.bussiness { margin: 10px 0 10px 0; }
textarea {
    border: 1px solid #25a6dc;
    border-radius: 5px;
    padding: 0px 10px;
    background-color: #e5f1f6;
}
#password {
    border: 1px solid #25a6dc;
    border-radius: 5px;
    padding: 0px 10px;
    background-color: #e5f1f6;
    height: 38px;
}
.update_buss {
    border: 1px solid #25a6dc;
    border-radius: 5px;
    padding: 0px 10px;
    background-color: #e5f1f6;	
    cursor: pointer;
    height: 36px;
}
</style> 
 @stop