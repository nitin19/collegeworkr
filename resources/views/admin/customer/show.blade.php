@extends('layouts.defautladmin')

@section('title', 'Profile')

@section('content')

<div class="content customers_detail">
	<div class="col-sm-12">
	    <div class="admincust">
		    <h2>Profile <a class="editprofile mar-profile" href="{{ route('admincustomer.edit', $admin_info->id)}} ">Edit Profiles</a></h2>
		</div>
    </div>
	<div class="col-sm-12">
		<div class="col-sm-3 proleft ">
<div class="profileleft">
<div class="profileimg">
	<?php
	$image = $admin_info->image;
	if($image==''){
	?>
	<img class="img-responsive" src="{{ url('/public/images/avatar.jpg') }}">
	<?php
	}
	else {
	?>
	<img class="img-responsive" src="{{ url('/public/uploads/profile/')}}/{{ $image }}">
	<?php
	}
	?>
</div>
<!-- <div class="profileinfo">
<h1><b>Name : </b> Dev </h1>
<h1><b>Gender : </b> male  </h1>
<h1><b>Email : </b> binarydata2017@gmail.com  </h1>
<h1><b>Phone Number : </b> 1234567890 </h1>
</div> -->
<!-- <div class="profilesocialicons">
<a href="#"><i class="fa fa-facebook"></i></a>
<a href="#"><i class="fa fa-twitter"></i></a>
<a href="#"><i class="fa fa-google-plus"></i></a>
<a href="#"><i class="fa fa-linkedin"></i></a>
<a href="#"><i class="fa fa-youtube"></i></a>
</div> -->

<?php
 $state_id = $admin_info->state;
                $statename = DB::table('state')
                	 ->where('id',$state_id)
                     /*->where('status', '1')
                     ->where('deleted', '0')*/
                     ->first();
                if($statename!=''){
                $state_name = $statename->name;
            	} else {
            	$state_name = '';
            	}
                ?>


</div>
</div>
<div class="col-sm-9">
<div class="profileright">
<div class="profileinfo-details">
<h1><img src="{{ url('/public/admin/images/basicinfo-icon.PNG') }}"> General Information</h1>
<div class="col-sm-6 profiledet-content">
<h3 class="capitalize"><b>Name : </b> {{ $admin_info->name }} {{ $admin_info->lastname }} </h3>
</div>
<div class="col-sm-6 profiledet-content">
<!-- <h3><b>Gender  : </b> {{ $admin_info->gender }} </h3> -->
</div>
<div class="col-sm-6 profiledet-content">
<h3><b>Email  : </b>  {{ $admin_info->email }} </h3>
</div>
<div class="col-sm-6 profiledet-content">
<h3><b>Phone Number : </b> {{ $admin_info->phone }} </h3>
</div>
</div>
<div class="profileinfo-details">
<h1><img src="{{ url('/public/admin/images/address-icon.png') }}">Address Information</h1>
<div class="col-sm-6 profiledet-content">
<h3 class="capitalize"><b>Address : </b>  {{ $admin_info->address }}  </h3>
</div>
<div class="col-sm-6 profiledet-content">
<h3 class="capitalize"><b>State   : </b>  {{ $state_name }} </h3>
</div>
<div class="col-sm-6 profiledet-content">
<h3 class="capitalize"><b>City   : </b>   {{ $admin_info->city }} </h3>
</div>
</div>
</div>  
</div> 
</div>		
@include('layouts.footer_admin')  
</div> 
</div>
@stop