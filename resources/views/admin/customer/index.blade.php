@extends('layouts.defautladmin')

@section('title', 'Admincustomer')

@section('content')

<div class="breadcrumbs" style="margin-top:20px;">
    <!--<div class="col-sm-12">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Welcome, <span class="color_text">{{ ucfirst($username) }}</span></h1>
            </div>
        </div>
    </div>-->
</div>
<div class="content">
    <div class="">
    	<div class="admincust">
    		<h2>Customers Detail 
        		<span class="badge pull-right"> 
        			<form action="" method="get" class="getform">
        			    <input type="text" placeholder="Search.." name="search">
                    </form>
                </span>
            </h2>
    	</div>
	</div>
	<div class="">
		  <table class="table-width col_or">
		    <thead>
		      <tr class="table-active">
		        <th class="col4">Name</th>
		      	<th class="col3">Email</th>
		        <th class="col3">Phone</th>
<!--		        <th>Package</th>
-->		        <th class="col3">Address</th>
		        <th class="col3">City</th>
		        <th class="col2">State</th>
		        <th class="col2">Action</th>
		      </tr>
		    </thead>
		    @foreach($customer_info as $customer)
		      <?php
		    	$customeruserid = $customer->id;
		    	$imagename = DB::table('users')
                     ->where('status', '1')
                     ->where('deleted', '0')
                     ->where('id','=',$customeruserid)
                     ->first();
                $image_users = $imagename->image;
                $state_id = $customer->state;
                $statename = DB::table('state')
                	 ->where('id',$state_id)
                     /*->where('status', '1')
                     ->where('deleted', '0')*/
                     ->first();
                if($statename!=''){
                $state_name = $statename->name;
            	} else {
            	$state_name = '';
            	}
                ?>
		      <tr class="{{ $customer->id }}">
		        <td class="col4">@if($image_users=='')
				<img src="{{ url('/public') }}/images/avatar.jpg" alt="John Doe" class="mr-2 rounded-circle " style="width:40px;height:40px">
				@else 
				<img src="{{ url('/public') }}/uploads/profile/{{ $image_users }}" alt="John Doe" class="mr-2 rounded-circle" style="width:40px;height:40px">
				@endif
				{{ $customer->name }} {{ $customer->lastname }}</td>
		        <td class="col3">{{ $customer->email }}</td>
		        <td class="col2">{{ $customer->phone }}</td>
<!--		        <td>{{ $customer->workertype }}</td>
-->		        <td class="col3">{{ $customer->address }}</td>
		        <td class="col3">{{ $customer->city }}</td>
		        <td class="col2">{{ $state_name }}</td>
		        <td  class="col3" id="{{ $customer->id }}"><a href="{{route('admincustomer.edit',$customer->id)}}"><i class="fa fa-edit mr-3"></i></a><a href="{{route('admincustomer.show',$customer->id)}}"><i class="fa fa-eye mr-3" aria-hidden="true"></i></a><i class="fa fa-trash mr-3" aria-hidden="true"></i></td>
		      </tr>      
@endforeach
		    </tbody>
		  </table>
	</div>
		<script type="text/javascript">
         jQuery('.fa-trash').click(function (e) {
         var pid = $(this).parent().attr("id");
         if(confirm("Are you want to remove this?")== true){
         var base_url = '<?php echo url('/'); ?>';
         jQuery.ajax({
            url: "{{url('/admincustomer/update')}}",
            type: 'GET',
            data: {'id': pid},
            success: function(response) {
             jQuery('.' +response).hide();     
             }        
        });
     }
    });
	</script>
<div class="content pagi_div">
    {{ $customer_info->appends(Request::only('s', 'show'))->links() }} 
</div>
@include('layouts.footer_admin') 
  
</div> 

    </div>
    @stop