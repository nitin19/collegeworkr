@extends('layouts.defautladmin')

@section('title', 'Adminworker')

@section('content')


        <div class="breadcrumbs" style="margin-top:20px;">
            <!--<div class="col-sm-12">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Welcome, <span class="color_text">{{ ucfirst($username) }}</span></h1>
                    </div>
                </div>
            </div>-->
            
        </div>
         
<div class="content">
    <div class="">
    	<div class="">
    	    <div class="admincust" style="padding:0px;">
    		    <h2>Workers Detail <span class="badge pull-right"> 
    			<form action="" method="get" class="getform"><input type="text" placeholder="Search.." name="search">
                                 </form></span></h2>
            </div>
    	</div>
    </div>
	<div class="">
		  <table class="table-width col_or">
		    <thead>
		      <tr class="table-active">
		        <th class="col4">Name</th>
		      	<th class="col3">Email</th>
		        <th class="col2">Phone</th>
		        <th class="col2">Tier</th>
                <th class="col3">Address</th>
                <th class="col2">City</th>
                <th class="col2">State</th>
		        <th class="col2">Action</th>
		      </tr>
		    </thead>
		    @foreach($customer_info as $customer)
		      <?php
		    	$customeruserid = $customer->id;
		    	$imagename = DB::table('users')
                     ->where('status', '1')
                     ->where('deleted', '0')
                     ->where('id','=',$customeruserid)
                     ->first();
                $image_users = $imagename->image;
                $state_id = $customer->state;
                $statename = DB::table('state')
                     ->where('id',$state_id)
                     /*->where('status', '1')
                     ->where('deleted', '0')*/
                     ->first();
                if($statename!=''){
                $state_name = $statename->name;
                } else {
                $state_name = '';
                }
                ?>
		      <tr class="{{ $customer->id }}">
		        <td>@if($image_users=='')
				<img src="{{ url('/public') }}/images/avatar.jpg" alt="John Doe" class="mr-2 rounded-circle" style="width:40px;height:40px">
				@else 
				<img src="{{ url('/public') }}/uploads/profile/{{ $image_users }}" alt="John Doe" class="mr-2 rounded-circle" style="width:40px;height:40px">
				@endif
				{{ $customer->firstname }} {{ $customer->lastname }}</td>
		        <td>{{ $customer->email }}</td>
		        <td>{{ $customer->phone }}</td>

		        <td>
                    <!--{{ $customer->workertype }} -->
                    <?php 

                    $feedback = DB::table('feedback')
                            ->where('worker_id',$customeruserid)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->sum('feedback_rating');
                    $feedback_count = DB::table('feedback')
                            ->where('worker_id',$customeruserid)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->count('feedback_rating');
                    $customer_feedback_count = DB::table('feedback')
                            ->where('worker_id',$customeruserid)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->count('worker_id');

                    $feedback;
                          $feedback_count;
                          $customer_feedback_count;
                          if($feedback!='0' && $feedback_count!='0' && $customer_feedback_count!='0'){
                              $feedback_average =  $feedback / $feedback_count;
                          } else {
                              $feedback_average = '';
                          }
                    ?>
                    @if($feedback_average <= 2)
                        Brownze
                        @elseif($feedback_average > 3 && $feedback_average <= 4)
                        Silver
                        @elseif($feedback_average > 4 && $feedback_average <= 5)
                        Gold
                        @else 
                        @endif
                </td>

                <td>{{ $customer->address }}</td>
                <td>{{ $customer->city }}</td>
                <td>{{ $state_name }}</td>
		        <td id="{{ $customer->id }}"><a href="{{route('adminworker.edit',$customer->id)}}"><i class="fa fa-edit mr-3"></i></a><a href="{{route('adminworker.show',$customer->id)}}"><i class="fa fa-eye mr-3" aria-hidden="true"></i></a><i class="fa fa-trash mr-3" aria-hidden="true"></i></td>
		      </tr>      
@endforeach
		    </tbody>
		  </table>
	</div>
	<script type="text/javascript">

		 jQuery('.fa-trash').click(function (e) {
         var pid = $(this).parent().attr("id");
         if(confirm("Are you want to remove this?")== true){
         var base_url = '<?php echo url('/'); ?>';
         jQuery.ajax({
            url: "{{url('/adminworker/update')}}",
            type: 'GET',
            data: {'id': pid},
            success: function(response) {
             jQuery('.' +response).hide();     
             }        
        });
     }
    });
	</script>
<div class="content pagi_div">		
    <!-- <ul class="pagination">
        <li><a href="#">Previous</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">Next</a></li>
    </ul> -->
    {{ $customer_info->appends(Request::only('s', 'show'))->links() }} 
</div>
@include('layouts.footer_admin') 
  
</div> 

    </div>
<style>
/*.content {
    margin-bottom: 0px!important;
}*/
.mr-3, .mx-3 {
    margin-right: .5rem!important;
}
</style>
    @stop