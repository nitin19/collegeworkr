@extends('layouts.defautladmin')

@section('title', 'Job Listing')

@section('content')

<div class="content Contextua_sec customers_detail">
	<div class="col-sm-12">
		<h2>Hiring Listing <span class="badge pull-right"> 
			<!--<form action="" method="get"><input type="text" placeholder="Search.." name="search">-->
   <!--                           <button type="submit"><i class="fa fa-search"></i></button></form>-->
                              </h2>
        
	</div>
	<div class="col-sm-12">
		  <table class="table-width col_or">
		    <thead>
		      <tr class="table-active">
		        <th>Job Title</th>
		      	<th>Assign By</th>
		      	<th>Assign To</th>
		      	<th>status</th>

<!-- <th>Package</th>
	        <th>Created At</th>
                <th>View</th> -->
		      </tr>
		    </thead>
		    <?php $count=count($hire_info); ?>
          	@if($count > '0')
		    @foreach($hire_info as $jobs)
		      <tr>
		        <td>{{ $jobs->cat_name }}</td>
		        <td>{{ $jobs->name }}</td>
		        <td>..</td>
		        <td>{{ $jobs->job_status }}</td>

		      </tr>      
@endforeach
@else
<tr>
	<td></td>
	<td></td>
	<td>No Jobs Here...</td>
	<td></td>
</tr>
@endif
		    </tbody>
		  </table>
	</div>
@include('layouts.footer_admin') 
  
</div> 

    </div>
    @stop