@extends('layouts.defautladmin')

@section('title', 'Job Listing')

@section('content')

<div class="content Contextua_sec customers_detail">
    <div class="">
        
        	<form id="adminhire" action="{{url('/adminhirecreate')}}" method="POST">
               {{ csrf_field() }}
               <div class="">
            <div class="col-sm-5">
            	<?php 
              //echo'<pre>';
              //print_r($jobs_info);
              //echo'</pre>';

              $count=count($jobs_info); 
              if($count > 0){
                ?>
              <select name="job_posts" id="job_posts" required="true" class="slecthire">
                <option value=""> Job </option>
                <option value="" disabled>Choose a job </option>


		        @foreach($jobs_info as $jobs)
		        <?php
                 $user_id =  $jobs->user_id;
                 $job_id =  $jobs->id;
                 $worker_required =  $jobs->worker_required;
                 $hirecount =  DB::table('hire')
                     ->where('client_job_id',$job_id)
                     ->where('deleted', '0') 
                     ->count();  
                 $username =  DB::table('users')
                     ->where('id',$user_id)
                     ->where('status', '1')
                     ->where('deleted', '0')
                     ->orderBy('users.id', 'desc') 
                     ->first();  
                 if($username!=''){
                  $user_name = $username->name;
                  $user_lastname = $username->lastname;
                } else {
                  $user_name = '...';
                  $user_lastname='...';
                }
 		         ?>
                   <?php if( $hirecount < $worker_required ) : ?> <option value="{{$jobs->id }}">{{ $jobs->cat_name }} ({{ $user_name }} {{ $user_lastname }})</option> <?php endif; ?>

                          
                @endforeach
                                
                </select>
<?php } else { ?>
              <select name="job_posts" id="job_posts2">        
                </select>
                <?php } ?>
            </div>

            <div class="col-sm-5">
                <select name="workers" id="workers" required="true" class="slecthire">
                   <option value="">Worker</option>
                   <option value="" disabled>Choose a worker</option>
                	@foreach($customer_info as $customer)
		            <?php               
		    	      $customeruser_id = $customer->id;
                ?>
                    <option value="{{ $customeruser_id }}">{{ $customer->firstname }} {{ $customer->lastname }}</option>
                    @endforeach
                 </select>
              </div>
            <div class="col-sm-2">
             <?php if($count > 0){ ?>
                <button type="submit" class="hirebtn" value="Assign">Assign</button>
                <?php } else { ?>
                 <button type="button" class="hirebtn" value="Assign">Assign</button>
                 <?php } ?>
            </div>
            <!--<input type="text" name="one" class="job-drop">
            <input type="text" name="one" class="worker">-->
        </div>
         </form>

  <div class="col-sm-12">
    <h2 style="margin-top:15px;">Hiring Listing <span class="badge pull-right"> 
      <!--<form action="" method="get"><input type="text" placeholder="Search.." name="search">-->
   <!--                           <button type="submit"><i class="fa fa-search"></i></button></form>-->
                              </h2>
        
  </div>

  <div class="col-sm-12">
      <table class="table-width col_or">
        <thead>
          <tr class="table-active">
            <th>Posted Job</th>
            <th>Customer</th>
            <th>WRKer</th>
            <th>Status</th>

<!-- <th>Package</th>
          <th>Created At</th>
                <th>View</th> -->
          </tr>
        </thead>
        <tbody>
        <?php $count=count($hire_info); ?>
            @if($count > '0')
        @foreach($hire_info as $hirejobs)
        <?php
        $job_user_id = $hirejobs->user_id;
        $job_user_info = DB::table('users')
                     ->where('id',$job_user_id)
                     ->where('status', '1')
                     ->where('deleted', '0')
                     ->first();
         ?>
          <tr>
            <td>{{ $hirejobs->cat_name }} </td>

            <?php if($job_user_info!=''){
                 $job_user_name = $job_user_info->name; 
                 $job_user_lastname = $job_user_info->lastname; ?>
            <td>{{ $job_user_name }} {{ $job_user_lastname }}</td>
            <?php } else { ?>
            <td>...</td>
            <?php } ?>
            <td>{{ $hirejobs->firstname }} {{ $hirejobs->lastname }}</td>
            <td style="text-transform: capitalize;">{{ $hirejobs->job_status }}</td>
          </tr>      
@endforeach
@else
<tr>
  <td></td>
  <td></td>
  <td>No Jobs Here...</td>
  <td></td>
</tr>
@endif
        </tbody>
      </table>
      <div class="" >{{$hire_info->render()}}</div>
  </div>
@include('layouts.footer_admin')

</div>

<script>
  jQuery(document).ready(function(){
    $('body').on('click', '.assign', function(e){
      var job_posts = $('#job_posts').val();
      var workers = $('#workers').val();
      job_posts = parseInt(job_posts);
      workers = parseInt(workers);
      if( $.isNumeric(job_posts) &&  $.isNumeric(workers) ){
          
      }else{
        e.preventDefault();
        alert('Please choose a job and worker from the list');
      }
      //alert( parseInt(job_posts));
      
        //if( job_posts == undefined && job_posts == null  )
    })

  });
</script>

<script type="text/javascript">
 jQuery(document).ready( function($) {
    $("#adminhire").validate({
        rules: {
                  job_posts: {
                      required: true
                  },
                  workers: {
                      required: true
                  }
                },
        messages: {
                  job_posts: {
                    required: "Please Select Job."
                  },
                  workers: {
                    required: "Please Select Worker."
                  }
                },
        submitHandler: function(form) {
            form.submit();
          }
    });
 });
 </script>   
<script>
$(".custom-select").each(function() {
  var classes = $(this).attr("class"),
      id      = $(this).attr("id"),
      name    = $(this).attr("name");
  var template =  '<div class="' + classes + '">';
      template += '<span class="custom-select-trigger">' + $(this).attr("placeholder") + '</span>';
      template += '<div class="custom-options">';
      $(this).find("option").each(function() {
        template += '<span class="custom-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
      });
  template += '</div></div>';
  
  $(this).wrap('<div class="custom-select-wrapper"></div>');
  $(this).hide();
  $(this).after(template);
});
$(".custom-option:first-of-type").hover(function() {
  $(this).parents(".custom-options").addClass("option-hover");
}, function() {
  $(this).parents(".custom-options").removeClass("option-hover");
});
$(".custom-select-trigger").on("click", function() {
  $('html').one('click',function() {
    $(".custom-select").removeClass("opened");
  });
  $(this).parents(".custom-select").toggleClass("opened");
  event.stopPropagation();
});
$(".custom-option").on("click", function() {
  $(this).parents(".custom-select-wrapper").find("select").val($(this).data("value"));
  $(this).parents(".custom-options").find(".custom-option").removeClass("selection");
  $(this).addClass("selection");
  $(this).parents(".custom-select").removeClass("opened");
  $(this).parents(".custom-select").find(".custom-select-trigger").text($(this).text());
});
</script>
<style>
.not-assing {
    text-align: center;
}
.custom-select {padding: 0px !important;}
input.assign {
    width: 100%;
    height: 45px;
    background-color: #24a6dc;
    border: 1px solid #24a6dc;
    border-radius: 5px;
    color: #fff;
    font-size: 18px;
    font-weight: bold;
}
/** Custom Select **/
.custom-select-wrapper {
  position: relative;
  display: inline-block;
  user-select: none;
  width:100%;
}
  .custom-select-wrapper select {height: 400px;
    overflow-y: scroll;
    display: none;
  }
  .custom-select {
    position: relative;
    display: inline-block;
  }
    .custom-select-trigger {
      position: relative;
      display: block;
      width: 100%;
      padding: 0 84px 0 22px;
      font-size: 22px;
      font-weight: 300;
      color: #fff;
      line-height: 45px;
      background: #24a6dc;
      border-radius: 4px;
      cursor: pointer;
    }
      .custom-select-trigger:after {
        position: absolute;
        display: block;
        content: '';
        width: 10px; height: 10px;
        top: 50%; right: 25px;
        margin-top: -3px;
        border-bottom: 1px solid #fff;
        border-right: 1px solid #fff;
        transform: rotate(45deg) translateY(-50%);
        transition: all .4s ease-in-out;
        transform-origin: 50% 0;
      }
      .custom-select.opened .custom-select-trigger:after {
        margin-top: 3px;
        transform: rotate(-135deg) translateY(-50%);
      }
      
  .custom-options {
    position: absolute;
    display: block;
   top: 40px; left: 0; right: 0;
    min-width: 100%;
    margin: 15px 0;
    border: 1px solid #b5b5b5;
    border-radius: 4px;
    box-sizing: border-box;
    box-shadow: 0 2px 1px rgba(0,0,0,.07);
    background: #fff;
    transition: all .4s ease-in-out;
    
    opacity: 0;
    visibility: hidden;
    pointer-events: none;
    transform: translateY(-15px);
    max-height: 400px;
    overflow-y: scroll;
    z-index:9;
  }
  
  .custom-select.opened .custom-options {
    opacity: 1;
    visibility: visible;
    pointer-events: all;
    transform: translateY(0);
  }
    .custom-options:before {
      position: absolute;
      display: block;
      content: '';
      bottom: 100%; right: 25px;
      width: 7px; height: 7px;
      margin-bottom: -4px;
      border-top: 1px solid #b5b5b5;
      border-left: 1px solid #b5b5b5;
      background: #fff;
      transform: rotate(45deg);
      transition: all .4s ease-in-out;
    }
    .option-hover:before {
      background: #f9f9f9;
    }
    .custom-option {
    position: relative;
    display: block;
    padding: 4px 12px;
    border-bottom: 1px solid #b5b5b5;
    font-size: 13px;
    font-weight: 600;
    color: #333333;
    line-height: 20px;
    cursor: pointer;
    transition: all .4s ease-in-out;
}
    /*.custom-option {
      position: relative;
      display: block;
      padding: 0 22px;
      border-bottom: 1px solid #b5b5b5;
      font-size: 18px;
      font-weight: 600;
      color: #b5b5b5;
      line-height: 47px;
      cursor: pointer;
      transition: all .4s ease-in-out;
    }*/
    .custom-option:first-of-type {
      border-radius: 4px 4px 0 0;
    }
    .custom-option:last-of-type {
      border-bottom: 0;
      border-radius: 0 0 4px 4px;
    }
    .custom-option:hover,
    .custom-option.selection {
      background: #f9f9f9;
    }
    </style>
</div>

     @stop