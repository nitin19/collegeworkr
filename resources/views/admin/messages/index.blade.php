@extends('layouts.defautladmin')

@section('title', 'Message Listing')

@section('content')

<div class="content customers_detail">
    @include('layouts.flash-message') 
  <div class="col-sm-12">
      <div class="adminjob">
            <h2>Message Listing <span class="badge pull-right"> 
                <!--<form action="" method="get"><input type="text" placeholder="Search.." name="search">-->
                 <!--<button type="submit"><i class="fa fa-search"></i></button></form>-->
            </h2>
        </div>
  </div>
  <div class="col-sm-12">
      <table class="table-width col_or">
        <thead>
          <tr class="table-active">
            <th class="col4">Name</th>
            <th class="col4">Email</th>
            <th class="col5">Message</th>
            <th class="col5">Date</th>
            <th class="col2">Action</th>
          </tr>
        </thead>
        @foreach($unseen_msg as $unseen_msg_update)
        <?php
       $msg_id = $unseen_msg_update->id;
       $update_qry = DB::table('homes')
            ->where('id', $msg_id)
            ->update(['seen_msg' => '1']);
        ?>
        @endforeach
        @foreach($mesage_info as $mesages)
          <tr class="{{ $mesages->id }}">
            <td class="col4">{{ ucfirst($mesages->name) }}</td>
            <td class="col4">{{ ($mesages->email) }}</td>
            <td class="col5">{{ ucfirst($mesages->message) }}</td>
            <td class="col5">{{ $mesages->created_at }}</td>
            <td class="col2" id="{{ $mesages->id }}"><a href="{{ route('adminmessages.show', $mesages->id) }}" data-toggle="tooltip" title="Reply"><i class="fa fa-reply mr-3" aria-hidden="true"></i></a><!-- <i class="fa fa-trash mr-3" aria-hidden="true"></i> --></td>
          </tr>      
            @endforeach
        <script type="text/javascript">
        jQuery('.fa-trash').click(function (e) {
         var pid = $(this).parent().attr("id");
         if(confirm("Are you want to remove this?")== true){
         var base_url = '<?php echo url('/'); ?>';
         jQuery.ajax({
            url: "{{url('/adminmessages/update')}}",
            type: 'GET',
            data: {'id': pid},
            success: function(response) {
             jQuery('.' +response).hide();     
             }        
        });
     }
    });
	</script>
        </tbody>
      </table>
      <div class="pagination_section">
              {{ $mesage_info->links() }}
      </div>
  </div>
<!-- <div class="content pagi_div">
    {{ $mesage_info->appends(Request::only('s', 'show'))->links() }} 
</div> -->
@include('layouts.footer_admin') 
  
</div> 

    </div>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
<style type="text/css">
  .Contextua_sec h2 {
    font-size: 2.25rem;
  }
.alert-success {
    text-align: center;
}
</style>
    @stop