@extends('layouts.defautladmin')

@section('title', 'Send Mail')

@section('content')

<div class="content Contextua_sec customers_detail">
	<div class="style-done">
		<h1>Send Mail</h1>
        
<form method="POST" action="{{ url('/adminmessages/send_mail') }}" accept-charset="UTF-8" id="sendmail" class="sendmail" enctype="multipart/form-data" novalidate="novalidate">
{{ csrf_field() }}
<div class="row bussiness">
<div class="col-sm-6">
	<label>Subject</label>
	<input type="text" name="subject" placeholder="Subject" required>
	<input type="hidden" name="email" value="{{ $mesage_info->email }}">
	<input type="hidden" name="name" value="{{ $mesage_info->name }}">
</div>

<div class="col-sm-6">
	<label class="level-top">Message</label>
	<textarea name="message" placeholder="Message" required></textarea>
</div></div><br>
<div class="row">
<div class="col-sm-12"><input class="update_buss" type="submit" name="submit" value="Send"></div></div>

</form>

<script type="text/javascript">
	$( "#sendmail" ).validate({
  rules: {
                subject: {
                    required: true,
                    minlength: 2,
                    maxlength: 150
                },
                message: {
                    required: true,
                    minlength: 2,
                    maxlength: 200
                }
  },
  messages: {
              subject: {
                  required: "Please enter your firstname.",
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 150 characters allowed."
                },
              message: {
                  required: "Please enter your lastname.",
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 200 characters allowed."
                }
              },
             
         });  
</script>

@include('layouts.footer_admin') 
  
</div> 
</div>
<style type="text/css">
.bussiness label {
    width: 15%;
}

.bussiness input[type="text"] {
    border: 1px solid #25a6dc;
    height: 38px;
    border-radius: 5px;
    padding: 0px 10px;
    background-color: #e5f1f6;
    width:100%;
}
.bussiness textarea {
    border: 1px solid #25a6dc;
    border-radius: 5px;
    padding: 0px 10px;
    background-color: #e5f1f6;
    width:100%;
}
</style>
    </div>
    @stop