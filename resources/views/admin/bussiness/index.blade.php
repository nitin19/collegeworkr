@extends('layouts.defautladmin')

@section('title','Bussiness Setting')

@section('content')
<div class="content Contextua_sec customers_detail">
<div class="style-done">    
@include('layouts.flash-message') 
<h1>Bussiness Setting</h1>
{{ Form::model($bussinesssetting_info, array('route' => array('adminbussiness.update', $bussinesssetting_info->id), 'id' => 'settingFrm', 'class' => 'settingform', 'method' => 'PUT')) }}
  {{ csrf_field() }}
  <div class="row bussiness">
<div class="col-sm-6"><label>Phone</label><input type="text" name="phone" value="{{ $bussinesssetting_info->phone }}" required="required" class="adminbssiness"></div>
<div class="col-sm-6"><label>Email</label><input class="change adminbssiness" type="email" name="emails" value="{{ $bussinesssetting_info->emails }}" required="required"></div></div>
<div class="row bussiness">
<div class="col-sm-6"><label>Facebook Url</label><input type="text" name="facebook_url" value="{{ $bussinesssetting_info->facebook_url }}" required="required" class="adminbssiness"></div>
<div class="col-sm-6"><label>Twitter Url</label><input type="text" name="twitter_url" value="{{ $bussinesssetting_info->twitter_url }}" required="required" class="adminbssiness"></div></div>
<div class="row bussiness">
<div class="col-sm-6"><label>Google Plus Url</label><input type="text" name="google_url" value="{{ $bussinesssetting_info->google_url }}" required="required" class="adminbssiness"></div>
<div class="col-sm-6"><label>Linkedin Url</label><input type="text" name="linkedin_url" value="{{ $bussinesssetting_info->linkedin_url }}" required class="adminbssiness"></div></div>
<div class="row bussiness">
<div class="col-sm-6"><label class="level-top">About Us</label><textarea name="about_us" required  class="admintextarea">{{ $bussinesssetting_info->about_us }}</textarea>
</div>
<div class="col-sm-6"><label class="level-top">Address</label><textarea name="address" required class="admintextarea">{{ $bussinesssetting_info->address }}</textarea>
</div>
</div>
<div class="row bussiness"><div class="col-sm-12">
<input type="hidden" name="id" value="{{ $bussinesssetting_info->id }}">
	<input type="submit" name="submit" value="Update" class="update_buss"></div></div>
{!! Form::close() !!}

 <script type="text/javascript">
 $(document).ready( function() {
    $("#settingFrm").validate({
        rules: {
                phone: {
                    required: true
                },
                emails: {
                    required: true
                },
            },
        messages: {
                phone: {
                    required: "This field is required"
                },
                emails: {
                    required: "This field is required"
                },
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

 });
</script>
</div>
</div>
 </div>
<style type="text/css">
.bussiness label { width: 32% }	

textarea {
    border: 1px solid #25a6dc;
    border-radius: 5px;
    padding: 0px 10px;
    background-color: #e5f1f6;
}
.update_buss {
    border: 1px solid #25a6dc;
    border-radius: 5px;
    padding: 0px 10px;
    	
    cursor: pointer;
    height: 36px;
}
</style> 
 @stop