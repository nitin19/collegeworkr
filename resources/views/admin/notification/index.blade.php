@extends('layouts.defautladmin')

@section('title', 'Notification')

@section('content')

<div class="content Contextua_sec customers_detail">
	<div class="col-sm-12">
		<h2>Notification<span class="badge pull-right"></h2>
        
	</div>
	<div class="col-sm-12">
		  <table class="table-width col_or">
		    <thead>
		      <tr class="table-active">
		        <th>Message</th>
		        <th>Created At</th>
		      </tr>
		    </thead>
		   <?php $count = count($notification_info); 
		  	//echo'<pre>';
		   //print_r($notification_info);
		   //echo'</pre>';

		   foreach($unseen_notification as $unseen_list) {
		  $update_seen = DB::table('new_notifications')
            ->where('notification_id', $unseen_list->notification_id)
            ->update(['read_by_Admin' => 1]);
		   } ?>
		   @if($count>0)
		    @foreach($notification_info as $notification_list)
		    <tr>
		    	<td> <?php if( $notification_list->notification_redirect_url !='' ): ?><a href="{{ url($notification_list->notification_redirect_url) }}"><?php echo $notification_list->message ;?></a> <?php else:  echo $notification_list->message; endif;?> </td>    
		    	<td><?php echo $notification_list->created_at; ?></td>    
		    </tr>      
@endforeach
@else 
<h3 class="norecordmsg">No Record Found</h3>
@endif
		    </tbody>
		  </table>
	</div>
<div class="content pagi_div">
    {{ $notification_info->appends(Request::only('s', 'show'))->links() }} 
</div>
@include('layouts.footer_admin') 
  
</div> 

    </div>
    @stop