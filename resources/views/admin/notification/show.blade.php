@extends('layouts.defautladmin')

@section('title', 'Job Listing')

@section('content')

<div class="content Contextua_sec customers_detail">
	<div class="col-sm-12">
		<h2>Job Listing <span class="badge pull-right"> 
			<!--<form action="" method="get"><input type="text" placeholder="Search.." name="search">-->
   <!--                           <button type="submit"><i class="fa fa-search"></i></button></form>-->
                              </h2>
        
	</div>
	<div class="col-sm-12">
		  <table class="table-width col_or">
		    <thead>
		      <tr class="table-active">
		        <th>Heading</th>
		      	<th>Description</th>
		        <th>Package</th>
		        <th>Created At</th>
		      </tr>
		    </thead>
		    @foreach($jobs_info as $jobs)
		      <tr>
		        <td>{{ $jobs->cat_name }}</td>
		        <td>{{ $jobs->job_description }}</td>
                <td>{{ ucfirst($jobs->worker_type) }}</td>
		        <td>{{ $jobs->created_at }}</td>
		        
		      </tr>      
@endforeach
		    </tbody>
		  </table>
	</div>
<div class="content pagi_div">
    {{ $jobs_info->appends(Request::only('s', 'show'))->links() }} 
</div>
@include('layouts.footer_admin') 
  
</div> 

    </div>
    @stop