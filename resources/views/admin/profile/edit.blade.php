@extends('layouts.defautladmin')

@section('title','Update Profile')

@section('content')

<div class="content Contextua_sec customers_detail col-sm-12 one">
    <div class="style-done">
<h1>Update Profile</h1>
@include('layouts.flash-message') 
<!-- <form method="POST" action="http://www.iebasketball.com/collegeworker/adminupdate/customer" id="updateprofile" accept-charset="UTF-8"> -->
  {{ Form::model($admin_info, array('route' => array('adminprofile.update', $admin_info->id), 'id' => 'updateprofile', 'class' => 'updateprofile', 'method' => 'PUT', 'files' => true)) }}
  {{ csrf_field() }}
  <div class="row bussiness">
<div class="col-sm-6"><label>First Name</label><input type="text" name="firstname" value="{{ $admin_info->firstname }}" required="required" class="adminbssiness"></div>

<div class="col-sm-6"><label>Last Name</label><input type="text" name="lastname" value="{{ $admin_info->lastname }}" required="required" class="adminbssiness"></div></div>
<div class="row bussiness">
<div class="col-sm-6"><label>Email Address</label><input type="text" name="email" value="{{ $admin_info->email }}" required="required" class="adminbssiness"></div>


<!-- <div class="row bussiness">
<div class="col-sm-6"><label>Password</label><input type="password" name="password" id="password"></div>    
<div class="col-sm-6"><label>Confirm Password</label><input type="password" name="confpwd" id="password"></div>
</div> -->

<div class="col-sm-6"><label>City</label><input type="text" name="city" value="{{ $admin_info->city }}" class="adminbssiness"></div>
</div>
<div class="row bussiness">
<div class="col-sm-12"><label>State</label><input type="text" name="state" value="{{ $admin_info->state }}" class="adminbssiness"></div></div>

<div class="row bussiness">
    <div class="col-sm-6"><label class="level-top">Address</label><textarea name="address" class="adminbssiness">{{ $admin_info->address }}</textarea>
</div>

  <div class="col-sm-6"><label class="level-top">Address1</label><textarea name="address1" class="adminbssiness">{{ $admin_info->address1 }}</textarea>
</div></div>
<div class="row bussiness">
     <label class="red-one">Profile Image</label>
  <div class="col-sm-12 bod-right">

<?php
if($admin_info->image==''){
  ?>
<img class="profileimage" src="{{ url('/public/admin/images/avatar.jpg') }}">
<?php
}
else{
  ?>
<img class="profileimage" src="{{ url('/public/uploads/profile/')}}/{{ $admin_info->image }}">
<?php
}
?>

  <input type="file" name="input_img">

</div>
</div>
<div class="row bussiness"><div class="col-sm-12">
<input type="hidden" name="id" value="{{ $admin_info->id }}">
  <input type="submit" name="submit" value="Update" class="update_buss"></div></div>
{!! Form::close() !!}
<script type="text/javascript">
 $(document).ready( function() {
 $.validator.addMethod("alphaLetterNumber", function(value, element) {
     return this.optional(element) || value == value.match(/^[a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });

  $.validator.addMethod("alphaLetterNumberSpace", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });

  $.validator.addMethod("alphaLetter", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });
     $.validator.addMethod("valueNotEquals", function(value, element, arg){
     return arg !== value;
  }, "Value must not equal arg.");
 $( "#updateprofile" ).validate({
  rules: {
                firstname: {
                    required: true,
                    alphaLetter:true,
                    minlength: 2,
                    maxlength: 150
                },
                lastname: {
                    required: true,
                    alphaLetter: true,
                    minlength: 2,
                    maxlength: 60
                },
                email: {
                    required: true,
                    email: true
                },
                address: {
                    required: true,
                    minlength: 6,
                    maxlength: 60
                },
                address1: {
                    required: true,
                    minlength: 6,
                    maxlength: 60
                },
                city: {
                    required: true
                },
                state: {
                    required: true
                }
  },
  messages: {
              firstname: {
                  required: "Please enter your firstname.",
                   alphaLetter:"Only letters are allowed",
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 150 characters allowed."
                },
              lastname: {
                  required: "Please enter your lastname.",
                  alphaLetter:"Only letters are allowed",
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 60 characters allowed."
                },
             
              email: {
                  required: "Please enter your email.",
                  email: "Enter valid email."                 
                },
            address: {
                  required: "Please enter your Address.",
                  minlength: "Minimum 6 characters required.",
                  maxlength: "Maximum 60 characters allowed."
                },
                address1: {
                  required: "Please enter your Address.",
                  minlength: "Minimum 6 characters required.",
                  maxlength: "Maximum 60 characters allowed."
                },
                city: {
                    required: "Please enter your City."
                },
                state: {
                    required: "Please enter your State."
                }
              },
             
         });  

 });
</script>
</div>
</div>
</div>
 
<style type="text/css">
.col-sm-9.one {
    padding-right: 15px;
}

.col-sm-3.bod-right {
    text-align: center;
    margin-top: 40px;
}

.bussiness label { width: 32% } 
.bussiness { margin: 10px 0 15px 0; }
textarea {
    border: 1px solid #25a6dc;
    border-radius: 5px;
    padding: 0px 10px;
    background-color: #e5f1f6;
}
#password {
    border: 1px solid #25a6dc;
    border-radius: 5px;
    padding: 0px 10px;
    background-color: #e5f1f6;
    height: 38px;
}
.update_buss {
    border: 1px solid #25a6dc;
    border-radius: 5px;
    padding: 0px 10px;
     
    cursor: pointer;
    height: 36px;
}
.col-sm-3.bod-right label {
    width: 100%;
    margin: 20px 0px;
}

</style> 
 @stop