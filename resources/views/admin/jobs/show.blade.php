@extends('layouts.defautladmin')

@section('title', 'Job Listing')

@section('content')

<!--<div class="content Contextua_sec customers_detail">
	<div class="col-sm-12">
		<h2>Job Listing <span class="badge pull-right"> 
			<!--<form action="" method="get"><input type="text" placeholder="Search.." name="search">-->
   <!--                           <button type="submit"><i class="fa fa-search"></i></button></form>-->
                             <!-- </h2>
        
	</div>
	<div class="col-sm-12">
		  <table class="table-width col_or">
		    <thead>
		      <tr class="table-active">
		        <th>Heading</th>
		      	<th>Description</th>
		        <th>Package</th>
		        <th>Created At</th>
		      </tr>
		    </thead>
		    @foreach($jobs_info as $jobs)
		      <tr>
		        <td>{{ $jobs->cat_name }}</td>
		        <td>{{ $jobs->job_description }}</td>
                <td>{{ ucfirst($jobs->worker_type) }}</td>
		        <td>{{ $jobs->created_at }}</td>
		        
<!--		      </tr>      -->
<!--@endforeach-->
		  <!--  </tbody>
		  </table>
	</div>
<div class="content pagi_div">
    {{ $jobs_info->appends(Request::only('s', 'show'))->links() }} 
</div>
@include('layouts.footer_admin') 
  
</div> 

    </div>-->
    <!--modified 1dec2018-->
    @foreach($jobs_info as $jobs)
<!--{{ $jobs->cat_name }} 
 {{ $jobs->job_description }}
{{ ucfirst($jobs->worker_type) }}
{{ $jobs->created_at }}-->
@endforeach

<div class="tab-pane active text-style" id="tab1">
        <div class="content customers_detail">
       <div class="col-md-12 rightcontent">
        
  <div class="">
    <div role="tabpanel" class="" id="openjobs">
      <div class="">
      	 <div class="">
      	    <div class="adminjob">
                <h2>Details</h2>
            </div>
           
            <?php 
            //echo'<pre>';
            //print_r($hireData);
            //echo'</pre>';

            ?>

          <table class="table-view">
          <tr class="headings"><th class="column1">Category</th><td>{{ $jobs->cat_name}}</td> </tr>  
          <tr class="headings"><th class="column1">Description</th><td>{{ $jobs->job_description }}</td> </tr>
          <tr class="headings"><th class="column1">Workertype</th><td> {{ ucfirst($jobs->worker_type) }}</td></tr>
          <tr class="headings"><th class="column1">Hours</th><td> {{ $jobs->start_time }}
          </td></tr>
          <tr class="headings"><th class="column1">Created at</th><td> {{ $jobs->created_at }}</td></tr>

           <tr class="headings"><th class="column1"><a href="{{url('/chathistory').'/'.$id}}" class="historybtn">Chat history</a></th></tr>
           
          </table></div></div>
          <?php if(!empty($hireData)):?>
            <div class="adminjob">
                <h2 class="text-center">Worker Details</h2>
              </div>
            <div class="table-responsive">
               
               
               <table class="table table-striped">
                  <tr>
                    <th>Worker ID</th>
                    <th>Worker Name</th>
                    <th>Job Status</th>
                    <th>Work Timesheet</th>
                    <th>Job Summary(Worker)</th>
                    <th>Future Appointments</th>
                    <th>Private Notes</th>
                    <th>Updated Timesheet</th>
                    
                  </tr>
                  <?php foreach( $hireData as $_hiredata  ): ?>
                    <tr>
                    <td><?php echo $_hiredata->worker_id; ?></td>
                    <td><?php echo $_hiredata->name; ?></td>
                    <td><?php echo $_hiredata->job_status; ?></td>
                    <td><?php if($_hiredata->hours_to_complete !=''): echo '<label class="badge badge-success">'. $_hiredata->hours_to_complete .':'. $_hiredata->mints_to_complete .'hr</label>'; endif; ?></td>
                    <td><small><?php echo $_hiredata->job_summary; ?></small></td>
                    <td><small><?php echo $_hiredata->future_appointments; ?></small></td>
                    <td><small><?php echo $_hiredata->private_notes; ?></small></td>
                    <td><?php if($_hiredata->updated_hours !=''): echo '<label class="badge badge-success">'. $_hiredata->updated_hours .':'. $_hiredata->updated_mints .'hr</label>'; endif; ?></td>
                    </tr>
                  <?php endforeach; ?>

               </table> 
               </div>  
          <?php endif;?>
        </div></div>
          
          <style>
table.table-view td {
    padding: 15px;
    font-size: 16px;
    color: #666666;
}
table.table-view td.column1 {
    width: 200px;
}
table.table-view a.view-table {
    display: block;
    width: 120px;
    border: 1px solid #cdcfd9;
    text-align: center;
    padding: 10px;
    border-radius: 5px;
    color: #1aa3df;
    font-size: 17px;
    background: #f3f5f8;
    text-decoration: none;
}
table.table-view tr.headings th {
    font-size: 15px;
    color: #333333;
    padding: 20px 15px;
}
table.table-view thead {
    background: #f6f6f6;
}
table.table-view td.column1 {
    font-size: 17px;
    color: #666666;
    font-family: "ProximaNova-Semibold";
}
table.table-view {
    border: 1px solid #e4e4e4;
    background-color: #f6f6f6;
    width: 100%;
}
.dashboard-right {
    background: #e5f1f6;
    padding: 25px 30px;
}
.tab-content {
    padding-left: 20px;
    padding-top: 44px;
}
.historybtn {
    background-color: #25a6dc;
    border: 1px solid #25a6dc;
    color: #fff;
    font-size: 14px;
    font-weight: 600;
    padding: 10px;
    border-radius: 5px;
    float: left;
    /*margin: 15px;*/
}
.historybtn:hover, .historybtn:active, .historybtn:focus  {color:#fff;text-decoration:none;}
</style>

    @stop