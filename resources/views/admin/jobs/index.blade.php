@extends('layouts.defautladmin')

@section('title', 'Job Listing')

@section('content')

<div class="content customers_detail">
	<div class="col-sm-12">
	    <div class="adminjob">
		    <h2>Job Listing <span class="badge pull-right"> 
			    <form action="" method="get" class="getform">
			        <p></p>
			        <!--<input type="text" placeholder="Search.." name="search">-->
                 <!--<button type="submit"><i class="fa fa-search"></i></button>-->
                </form>
            </h2>
        </div>
	</div>
	<div class="col-sm-12">
		  <table class="table-width col_or">
		    <thead>
		      <tr class="table-active">
		        <th class="col3">Heading</th>
		      	<th class="col4">Description</th>
		      	<th class="col4">Requested Worker</th>
		      	<th class="col3">Hours</th>

<!--		        <th>Package</th>
-->		        <th class="col4">Created At</th>
                <th class="col2">View</th>
		      </tr>
		    </thead>
		    <?php $count=count($jobs_info); ?>
          	@if($count > '0')
		    @foreach($jobs_info as $jobs)
		      <tr>
		        <td class="col3">{{ $jobs->cat_name }}</td>
		        <td class="col5">{{ $jobs->job_description }}</td>
		        <td class="col3">{{ $jobs->worker_required }}</td>
		        <td class="col3">{{ $jobs->start_time }}</td>
		        <td class="col4">{{ $jobs->created_at }}</td>
		       <td class="col1"><a href="{{route('adminjobs.show', $jobs->id)}}"><i class="fa fa-eye" style="font-size:15px"></i></a></td>
		      </tr>      
@endforeach
@else
<tr>
	<td></td>
	<td></td>
	<td>No Jobs Here...</td>
	<td></td>
</tr>
@endif

		    </tbody>
		  </table>
	</div>
<div class="content pagi_div">
    {{ $jobs_info->appends(Request::only('s', 'show'))->links() }} 
</div>
@include('layouts.footer_admin') 
  
</div> 

    </div>
    @stop