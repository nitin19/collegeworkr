@extends('layouts.defautladmin')

@section('title', 'Admincustomer')

@section('content')


<div class="content customers_detail">
	<div class="col-sm-12">
	    <div class="adminjob">
		<h2>WRKer Payments<span class="badge pull-right"> 
	<!--  <form action="" method="get"><input type="text" placeholder="Search.." name="search">
        </form> </h2> -->
        </div>
	</div>
	<div class="col-sm-12">
		  <table class="table-width col_or">
		    <thead>
		      <tr class="table-active">
		        <th class="col3">Worker Name</th>
		        <th class="col3">Account Number</th>
		        <th class="col3">Routing Number</th>
		        <th class="col2">Job Name</th>
		        <th class="col2">Job Status</th>
		        <th class="col2">Amount</th>
		        <th class="col3">Payment Status</th>
		        <th class="col2">Action</th>
		      </tr>
		    </thead>
			<tbody>
		    <?php
		    $count = 0;
		    ?>
		    <?php $count=count($workerpayment_info); ?>
          	@if($count > '0')
		    @foreach($workerpayment_info as $worker_payment)
		     <?php
		    
		    	$worker_userid = $worker_payment->worker_id;
                $work_user = DB::table('users')->where('id', $worker_userid)->get();
                 
                $work_payment_data = DB::table('workerpayment')->where('user_id', $worker_userid)->get();
                $worker_job_id = $worker_payment->client_job_id;
             
                $worker_job_status = $worker_payment->job_status;
                $work_job_data = DB::table('clientjobpost')->where('id', $worker_job_id)->first();
                $job_name_id = $work_job_data->job_heading;
                
                $work_cat_data = DB::table('categories')->where('id', $job_name_id)->first();
                $cat_name = $work_cat_data->cat_name;
                $job_payment_data = DB::table('client_payment')->where('client_jobid', $worker_job_id)->get();
                //print_r($job_payment_data);
             ?>
		        <tr>
		        <td class="col3">
		        @foreach($work_user as $work_users)
		        <?php $workername = $work_users->firstname.' '.$work_users->lastname; ?>
		        {{ $workername }}
		        @endforeach
		        </td>
		        @if($work_payment_data!='')
		        @foreach($work_payment_data as $work_payment_datas)
		        <td  class="col3" style="color: #808080;">{{ $work_payment_datas->bank_account_number }}</td>
		        <td style="color: #808080;">{{ $work_payment_datas->bank_routing_number }}</td>
		        @endforeach
		        @else
		        <td class="col3" style="color: #808080;"></td>
		        <td style="color: #808080;"></td>	
		        @endif
		        <td class="col2" style="color: #808080;">{{$cat_name}}</td>
		        <td class="col2" style="color: #808080;">{{$worker_job_status}}</td>
		        <td class="col2">
                @foreach($job_payment_data as $job_payment_datas)
                <?php 
                $job_amount = $job_payment_datas->paid_amount;
                ?>
		        {{ '$'.$job_amount }}
                @endforeach
                </td>
                @if($work_payment_data)
		        @foreach($work_payment_data as $work_payment_datas)
		        <td class="col3">{{ $work_payment_datas->payment_status }}</td>
		        <?php $transaction_id = $work_payment_datas->transaction_id; 
                      $payment_status = $work_payment_datas->payment_status;
		        ?>
		        <td class="col2">
		        @if($transaction_id=='' && $payment_status=='pending')
		         <button type="button" class="markmodel" data-toggle="modal" data-target="#myModal_{{$work_payment_datas->user_id}}">Mark Done</button>
				<!--model popup-->
						      <div class="modal fade" id="myModal_{{$work_payment_datas->user_id}}" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content--> 
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body" style="padding-bottom: 60px;">
        	<h4 class="modal-title">Payment</h4>
          {{ Form::model($work_payment_datas, array('route' => array('requestpayment.update', $work_payment_datas->id), 'id' => 'updatepayment', 'class' => 'updatepayment', 'method' => 'PUT', 'files' => true)) }}
          <!-- <label>Transaction Id</label> -->
          <input type="text" name="transactionid" required  class="payinput" placeholder="Enter Your Trancation Id...">
          @foreach($job_payment_data as $job_payment_datas)
          <?php 
          	$client_user_id = $job_payment_datas->user_id;
          	$job_id = $job_payment_datas->client_jobid;
          	$job_amount = $job_payment_datas->paid_amount;
          ?>
          	<input type="hidden" name="job_client_id" value="{{ $client_user_id }}">
          	<input type="hidden" name="job_id" value="{{ $job_id }}">
            <input type="hidden" name="job_amount" value="{{ $job_amount }}">
       
          @endforeach
          	<input type="hidden" name="id" value="{{ $work_payment_datas->id }}">
		<input class="paybtn" type="submit" name="submit" value="Save">
          {{ Form::close() }}
 
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>   


		        	@foreach($job_payment_data as $job_payment_datas)
		        	<button type="button" class="" id="reject_btn" client_user_id="{{ $job_payment_datas->user_id }}" job_id="{{ $job_payment_datas->client_jobid }}" worker_payment_id="{{ $work_payment_datas->id }}">Reject</button> 
                @endforeach
                  @elseif($work_payment_datas->payment_status=='approved')<button type="button">Payment Done</button>
		          @else<button type="reject_button" id="reject_btn">Rejected</button>
		       	@endif
		</td>

		        <!-- <td>@if($transaction_id=='')<button type="button" class="btn btn-info btn-lg">Reject</button> @else <button type="button">Payment Done @endif</td> -->
		        @endforeach
		        @else
		        <td></td>
		        <td></td>
		        @endif
		      </tr>  




@endforeach

@else
<tr>
	<td></td>
	<td></td>
	<td>No Payment Here...</td>
	<td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
@endif
		    </tbody>
		  </table>
		  		  <div class="content pagi_div">		

    {{ $workerpayment_info->appends(Request::only('s', 'show'))->links() }} 
</div>
	</div>
	
@include('layouts.footer_admin') 
  
</div> 
</div>
<script type="text/javascript">
$(document).ready(function() {
$("#reject_btn").click(function(){
    var client_user_id = $("#reject_btn").attr("client_user_id");
    var job_id = $("#reject_btn").attr("job_id");
    var worker_payment_id = $("#reject_btn").attr("worker_payment_id");

    jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/rejectpayment') }}",
            data :{'client_user_id': client_user_id, 'job_id': job_id, 'worker_payment_id': worker_payment_id},
            success :  function(resp) {
                if(resp=='true'){
                  window.location.href="{{url('/requestpayment')}}";

                }
             }
       });
});
});fxVBJnmj123eyfdFGH
</script>
<style type="text/css">
input[type="text"] {
    width: 79%;
}
.submit_btn {
	background: #25a6dc;
	color: #fff;
	font-weight: bold;
	border: none;
	padding: 6px 28px 7px 28px;
	border-radius: 5px;
}
.modal-title {
	text-align: center;
	font-size: 24px;
	font-weight: bold;
}
.markmodel {
    background: #25a6dc;
    border: none;
    padding: 7px 2px;
    border-radius: 5px;
    width: 100%;
    margin-bottom: 2px;
    font-size: 12px;
}
#reject_btn {
    background: #ff0000;
border: none;
padding: 7px 2px;
border-radius: 5px;
width: 100%;
margin-bottom: 2px;
font-size: 12px;
}
</style>
@stop