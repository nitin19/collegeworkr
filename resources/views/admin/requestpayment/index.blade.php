@extends('layouts.defautladmin')

@section('title', 'Admincustomer')

@section('content')


<div class="content customers_detail">
	<div class="col-sm-12">
	    <div class="adminjob">
		<h2>WRKer Payments<span class="badge pull-right"> 
	<!--  <form action="" method="get"><input type="text" placeholder="Search.." name="search">
        </form> </h2> -->
        </div>
	</div>


	<div class="col-sm-12">
		<?php 
	//echo'<pre>';
	//print_r($workerpayment_info);
	//echo'</pre>';

	?>
		  <table class="table-width col_or">
		    <thead>
		      <tr class="table-active">
		        <th class="col3">Worker Name</th>
		       	<th class="col2">Job Name</th>
		       	<th class="col2">Job Status</th>
		        <th class="col2">Routing Number</th>
		        <th class="col2">Account Number</th>
		        <th class="col2">Amount</th>
		        <th class="col3">Payment Status</th>
		        <th class="col2">Action</th>
		      </tr>
		    </thead>
			<tbody>
		  		<?php if( count($workerpayment_info) > 0 ):
		  		foreach ($workerpayment_info as $paymentDetails) {
		  			$worker_name = $paymentDetails->name;
		  			$worker_id = $paymentDetails->worker_id;
		  			$client_id = $paymentDetails->client_id;
		  			$client_job_id = $paymentDetails->client_job_id;
		  			$work_payment_data = DB::table('workerpayment')->where('user_id', $worker_id)->first();
		  			?>
		  		<tr>	
		  			<td><?php echo $worker_name; ?></td>
		  			<td><?php echo $paymentDetails->cat_name; ?></td>
		  			<td><?php echo $paymentDetails->job_status; ?></td>
		  			<td><?php if( !empty($work_payment_data) ): echo $work_payment_data->bank_routing_number; endif; ?></td>
		  			<td><?php if( !empty($work_payment_data) ): echo $work_payment_data->bank_account_number; endif; ?></td>
		  			<td>$<?php echo $paymentDetails->amount_paid;?></td>
		  			<td><?php echo 'success'; ?></td>
		  			<td>
		  				<button type="button" class="markmodel" data-toggle="modal" data-target="#paymentPopup" data-paymentid="{{ $paymentDetails->payment_id }}" data-workerid="{{$worker_id}}" data-jobtitle="<?php echo $paymentDetails->cat_name; ?>">Mark Done</button>
						<button type="button" class="reject_btn" id="reject_btn" client_user_id="{{ $client_id }}" job_id="{{ $client_job_id }}" worker_payment_id="{{ $paymentDetails->payment_id }}" worker_id="{{ $worker_id }}" jobtitle="<?php echo $paymentDetails->cat_name; ?>">Reject</button> 
		  			</td>
		  		</tr>
		  			<?php
		  		}
		  		else:
		  			echo'<tr><td colspan="6">No Record found</td></tr>';
		  		endif;	
		  		?>		
		    </tbody>
		  </table>
		  		  <div class="content pagi_div">		

    
</div>
	</div>
<!-- ================ payment popup start ================= -->
	<div class="modal fade" id="paymentPopup" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content--> 
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body" style="padding-bottom: 60px;">
        	<h4 class="modal-title">Payment</h4>
          	<!-- <label>Transaction Id</label> -->
          	<form>
          	<input type="text" name="transactionid" required  class="payinput transactionid" placeholder="Enter Your Transaction Id...">
          	<input type="hidden" name="payment_id" class="payment_id">
          	<input type="hidden" name="workerid" class="workerid">
          	<input type="hidden" name="jobtitle" class="jobtitle">
          	<input class="paybtn" type="button" name="submit" value="Save">
          </form>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div> 
<!-- =================== paymant popup ends =============== -->

	
@include('layouts.footer_admin') 
  
</div> 
<script type="text/javascript">
$(document).ready(function() {
$(".reject_btn").click(function(){
    var client_user_id = $(this).attr("client_user_id");
    var job_id = $(this).attr("job_id");
    var worker_id = $(this).attr("worker_id");
    var worker_payment_id = $(this).attr("worker_payment_id");
    var jobtitle = $(this).attr("jobtitle");
 
    jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/rejectpayment') }}",
            data :{'client_user_id': client_user_id, 'job_id': job_id, 'worker_payment_id': worker_payment_id, 'worker_id': worker_id, 'jobtitle' : jobtitle },
            success :  function(resp) {
                if(resp=='true'){
                  window.location.href="{{url('/requestpayment')}}";

                }
             }
       });
});
});
</script>
<script>
jQuery(document).ready(function($){
	$('body').on('click', '.markmodel', function(e){
		//var transactionid = $(this).data('transactionid');
		var payment_id = $(this).data('paymentid');
		var workerid = $(this).data('workerid');
		var jobtitle = $(this).data('jobtitle');
		$('.payment_id').val( payment_id );
		$('.workerid').val( workerid );
		$('.jobtitle').val( jobtitle );
	})
});
</script>


<script type="text/javascript">
$(document).ready(function() {
$(".paybtn").click(function(e){
	e.preventDefault();
    var transactionid = $('.transactionid').val();
    var payment_id = $('.payment_id').val();
    var workerid = $('.workerid').val();
    var jobtitle = $('.jobtitle').val();
   	
   	$.ajax({
            method : 'GET',
            url  : "{{ url('/updateworkerpayment')}}",
            data :{ 'transactionid': transactionid, 'payment_id': payment_id , 'workerid' : workerid, 'jobtitle' : jobtitle },
            success :  function(resp) {
                if(resp=='true'){
                  window.location.href="{{url('/requestpayment')}}"; 

                }
             }
       });
});
});
</script>

<style type="text/css">
input[type="text"] {
    width: 79%;
}
.submit_btn {
	background: #25a6dc;
	color: #fff;
	font-weight: bold;
	border: none;
	padding: 6px 28px 7px 28px;
	border-radius: 5px;
}
.modal-title {
	text-align: center;
	font-size: 24px;
	font-weight: bold;
}
.markmodel {
    background: #25a6dc;
    border: none;
    padding: 7px 2px;
    border-radius: 5px;
    width: 100%;
    margin-bottom: 2px;
    font-size: 12px;
}
#reject_btn {
    background: #ff0000;
border: none;
padding: 7px 2px;
border-radius: 5px;
width: 100%;
margin-bottom: 2px;
font-size: 12px;
}
</style>
@stop