@extends('layouts.clientdefault')

@section('title', 'Clientdashboard')

@section('content')
 <div class="col-md-9 dashboard-right">
    <div class="share_workers share-sweep1">
           <div class="row pro-heading">
              <div class="col-sm-12">
                <div class="pro-head">
                <h3>Gift Certificate</h3>
              </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="customer-gift">
                  <h3>Give the gift of Sweeps!</h3>
                  <p>Send your friends or loved ones a gift certificate to Sweeps and help them get more done.</p>

                </div>
                <div class="recipient-info">
                  <div class="col-md-12">
                  <h3>Recipient Information</h3>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                 </div>
                  <form>
                    <div class="form-group col-md-6">
                      <label>First Name</label>
                      <input type="text" name="" placeholder="Enter your first name" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                      <label>Last Name</label>
                      <input type="text" name="" placeholder="Enter your last name" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                      <label>Email Address</label>
                      <input type="email" name="" placeholder="Enter Price email" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                      <label>Value</label>
                      <input type="text" name="" placeholder="Enter Price Value" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                      <label>Credit Card Detail</label>
                      <input type="text" name="" placeholder="Enter your first name" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                      <label>Expiration Date</label>
                      <input type="text" name="" placeholder="Enter your first name" class="form-control">
                    </div>
                     <div class="form-group col-md-3">
                      <label>Security Code</label>
                      <input type="text" name="" placeholder="Code" class="form-control">
                    </div>
                    <div class="form-group col-md-9 checkbx-group">
                      <label class="checkbox-inline">
                        <input type="radio" name="radio1">Email to recipient
                        <input type="radio" name="radio1">Printable (emailed to you)
                      </label>
                      
                     
                    </div>
                    <div class="col-md-12">
                        <button class="purchase-btn">Purchase Gift Card</button>
                    </div>  
                  </form>

                </div>
                
              </div>
              
            </div>


        </div>
</div>
</div>
</section>
@stop