@extends('layouts.clientdefault')

@section('title', 'Clientdashboard')

@section('content')

<div class="col-md-9 dashboard-right">
      <div class="main-profile">
         <div class="upper-profile">
            <div class="row Frequent-Jobs">
                  <div class="col-md-12">
                    <h2>Account Information</h2>
                  </div>
            </div>
            <div class="row">
            <div class="col-md-9">
            <div class="row user-info custom-profile">
              <h3>Personal Info </h3>
               <form name="profileform" id="profileform" action="{{ route('profilecustomer.update',$usercustomer->id) }}" method="PUT" enctype="multipart/form-data">

                {{csrf_field()}}

                <input type="hidden" name="user_id"  value="{{$usercustomer->id}}">
            <div class="col-md-2 nopadding">
                @if($usercustomer->image !='')
                <img src="{{url('/public')}}/uploads/profile/{{$usercustomer->image}}" class="img-responsive center-block" id="blah">
                @else
                <img src="{{url('/public')}}/images/user-pic.png" class="img-responsive center-block" id="blah">
                @endif
             <div class="form-group" style="text-align: center;">
      <!--         <label style="color: #3399cc;font-size: 15px;text-decoration: none;font-weight: 400;display: block;">Upload Image</label> -->
              <div class="input-group">
                  <span class="input-group-btn">
                      <span class="btn btn-default btn-file">
                          Browse… <input type="file" id="imgInp" name="image">
                      </span>
                  </span>
                 
              </div>
          </div>
              </div>
              <div class="col-md-10">
               
                <div class="row">
                  <div class="form-group col-md-6">
                    <label>First Name</label>
                    <input type="text" name="firstname" placeholder="Enter your firstname" value="{{$usercustomer->firstname}}" class="form-control">
                </div>
                <div class="form-group col-md-6">
                    <label>Last Name</label>
                    <input type="text" name="lastname" placeholder="Enter your lastname" value="{{$usercustomer->lastname}}" class="form-control">
                </div>
                 <div class="form-group col-md-6">
                    <label>Email Address</label>
                    <input type="email" name="email" placeholder="Enter your email" value="{{$usercustomer->email}}" class="form-control" readonly="readonly">
                </div>
                 <div class="form-group col-md-6">
                    <label>Phone Number</label>
                    <input type="tel" name="phone" placeholder="Enter your phone number" value="{{$usercustomer->phone}}" class="form-control">
                </div>
                 <div class="form-group col-md-6">
                    <label>Invite Code</label>
                    <input type="text" name="invitecode" placeholder="Enter your code" value="{{$usercustomer->invitecode}}" class="form-control">
                </div>
                <div class="col-md-6">
                
              </div>
              <div class="col-md-12">
                <button type="submit" class="sve-btn">Update Information</button>
              </div>
              </div>
            </form>
            </div>
            </div>
            
          </div>
          <div class="col-md-3">
            <div class="pro-myaccount">
              <h2>My Account</h2>
              <ul>
                <li>Member Since: 
                 <?php
                    $createdate = explode(' ', $usercustomer->created_at);
                 ?>
                    {{date('d M Y',strtotime($createdate[0]))}}
                </li>
                <li>Balance: $0.00 </li>
                <li>Jobs Posted:
                  @if ($clientjobpost->count()){{$clientjobpost->count()}}
                  @else {{'No job posted yet'}}
                  @endif</li>
              </ul>
            </div>
          </div>
        </div>
        </div>
         
    </div>
</div>
</div>
</section>

<script type="text/javascript">
  
  function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#blah').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#imgInp").change(function() {
  readURL(this);
});
</script>

 <script>
jQuery(document).ready(function() {
  $.validator.addMethod("alphaLetter", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });
 
 $("#profileform").validate({
        rules: {
                firstname: {
                    required: true,
                    alphaLetter:true,
                    minlength: 2,
                    maxlength: 60
                },
                lastname: {
                    required: true,
                    alphaLetter: true,
                    minlength: 2,
                    maxlength: 60
                },
                phone: {
                    required: true,
                    minlength: 10,
                    maxlength: 12,
                    number:true
                }   
            },
        messages: {
              firstname: {
                  required: "Please enter your firstname.",
                  alphaLetter:"Only letters are allowed",
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 60 characters allowed."
                },
              lastname: {
                  required: "Please enter your lastname.",
                  alphaLetter:"Only letters are allowed",
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 60 characters allowed."
                },
              phone: {
                  required: "Please enter phone number.",
                  minlength: "Minimum 10 characters required.",
                  maxlength: "Maximum 12 characters allowed.",
                  number: "Please enter only number."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
    });

  });
</script>
@stop