@extends('layouts.clientdefault')

@section('title', 'Hour Details')

@section('content')
<div class="col-md-9 dashboard-right">
        <div class="tab-pane active text-style" id="tab1">
        <div class="row">
       <div class="col-md-12 rightcontent">
        
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="openjobs">
      <div class="tab_contentarea">
        <?php
        /* echo'<pre>';
        print_r( $hireData );
        echo'</pre>'; */
        $hireid = $_GET['hireid'];
        $worker_id = $hireData->worker_id;
        $client_job_id = $hireData->client_job_id;
        $userData =DB::table('users')
                         ->where('id','=',$worker_id)
                         ->first(); 
        //echo'<pre>';
        //print_r( $userData );
        //echo'</pre>';
        ?>
         <div class="customer-head">
             <h1> {{ $hireData->job_description }}, {{  $hireData->job_location }} </h1>
         </div>
        
        <div class="row">
        <div class="col-md-8 tablewrapper tablediv">
            <div class="customer-head">
                <h1>Details</h1>
            </div>
          <table class="table-striped">
                   
            <tr class="headings"><th class="column1">Category</th><td>{{ $hireData->cat_name }}</td> </tr>
            <tr class="headings"><th class="column1">Description</th><td>{{ $hireData->job_description }}</td> </tr>
            <tr class="headings"><th class="column1">Worker</th><td>{{ $userData->name }}</td> </tr>
            
         </table>
        </div>
      
     <div class="col-md-4" >
      	<table class="table-striped">
      		<?php if($hireData->hours_to_complete !='' ){?>
      		<tr><th style="padding: 5px;">Time Updated By WRKer</th><td style="padding: 5px;"> <label class="label label-success">{{ $hireData->hours_to_complete }} : {{ $hireData->mints_to_complete }} hr</label> </td> </tr>
      		
      		<?php if( $hireData->time_approved == 0 ){?>
      		<tr><th style="padding: 5px;"></th> 
      			<td style="padding: 5px;"><a href="#" data-toggle="modal" data-target="#hourDetailModal" class="approveTime btn btn-primary btn-sm">Approve / Edit </a></td>
      		</tr>
      		<?php }else{?>
      		<tr><th style="padding: 5px;">Approved Time</th> 
      			<td style="padding: 5px;"> <?php if( $hireData->updated_hours !='' ): echo '<label class="label label-success">'. $hireData->updated_hours .' : '. $hireData->updated_mints .' hr</label>';  else: echo '<label class="label label-success">'. $hireData->hours_to_complete .' : '. $hireData->mints_to_complete .' hr</label>'; endif; ?></td>
      		</tr>

      		<?php }?>

      	<?php } ?>
      	</table>

      </div>  
  </div>

<!--  hours details modal start   -->
<div id="hourDetailModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Timesheet</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            	<form>
            		<div class="row">
            			<div class="col-sm-4">
            				<div class="form-group">
            					<input type="number" class="hours form-control"  min="0" placeholder="Hours" value="{{ $hireData->hours_to_complete }}" readonly="true">
            				</div>
            			</div>
            			<div class="col-sm-4">
            				<div class="form-group">
            					<input type="number" class="mints form-control" min="0" max="59" placeholder="Minutes" value="{{ $hireData->mints_to_complete }}" readonly="false">
            				</div>
            			</div>
            			<div class="col-sm-4">
            				<label class="label label-default">Hours and Minutes</label>
            			</div>
            			<div class="col-sm-12">
            				<input type="checkbox" name="editable_input" class="editable_input"> Please check if you want to update hours
            			</div>
            			<div class="col-sm-3">
            				<div class="form-group">
            					<br />
            					<button type="button" class="btn btn-primary approveTimeSheet"> Approve </button>
            				</div>	
            			</div>
            		</div>
            	</form>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </div>
    </div>

  </div>
</div>
<!-- hours details modal end   -->
    </div>
   </div>
  </div>
 </div>
</div>
</div>
<script>
	jQuery(document).ready(function($){
		$('body').on('change', '.editable_input', function(e){
			if($(this).prop("checked") == true){
				//alert( 'change hours' );
				$('.hours').attr("readonly", false);
				$('.mints').attr("readonly", false);
			}else{
				$('.hours').attr("readonly", true);
				$('.mints').attr("readonly", true);
			} 
		})
	})
</script>
<script>
	jQuery(document).ready(function($){
		$('body').on('click', '.approveTimeSheet', function(e){
			if($('.editable_input').prop("checked") == true){
				var updatedHour = $('.hours').val();
				var updatedMints = $('.mints').val();
			}else{
				var updatedHour ='';
				var updatedMints = '';
			}
		jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/updatehourstatus') }}",
            data :{'hireid': '<?php echo $hireid; ?>' ,'clientjobid': '<?php echo $client_job_id; ?>','worker_id':'<?php echo  $worker_id; ?>', 'updatedHour' : updatedHour, 'updatedMints' : updatedMints , 'job_cat' : '<?php echo $hireData->cat_name; ?>'}, 
            success :  function(resp) {
              //alert(resp);
                if(resp=='true'){
                  window.location.href="{{url('/hourdetails?hireid='.$hireid )}}";
				}
             }
       });


		});
	})
</script>

        
@stop