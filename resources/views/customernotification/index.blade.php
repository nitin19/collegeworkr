@extends('layouts.clientdefault')

@section('title', 'Customer Notification')

@section('content')

<div class="col-sm-9 dashboard-right width80">
<div class="col-sm-12">
<div class="tab-section">
  <ul class="nav nav-tabs" role="tablist">
  <?php
      //echo $userId = Auth::id();
      $applied_worker_count = count($applied_worker);
      $hired_worker_count = count($hired_worker);
      $completed_worker_count = count($complte_job_worker);
      $cancelled_count_worker = count($cancelled_job_worker);
      //$all_applied_count_worker =$applied_worker_count-($hired_worker_count+$completed_worker_count+$cancelled_count_worker);
      $all_applied_count_worker = 0;
      if( $applied_worker_count > 0 ){
          foreach ($applied_worker as $__pendingjobs) {
            $job_id = $__pendingjobs->JobID; 
            $hire_count = DB::table('hire')
                                ->where('client_job_id',$job_id)
                                ->count();
            if( $hire_count == 0 ){
              $all_applied_count_worker++;
            }                    
          }
      }
?>

    <!-- <li role="presentation" class="active custom-width-noti"><a href="#newworker" aria-controls="newworker" role="tab" data-toggle="tab">New Worker<span>(<?php //echo $all_applied_count_worker; ?>)</span></a></li> -->
    <li role="presentation" class="active custom-width-noti"><a href="#newworker" aria-controls="newworker" role="tab" data-toggle="tab">Opens Jobs<span>(<?php echo $all_applied_count_worker; ?>)</span></a></li>


    
    <!-- <li role="presentation" class="custom-width-noti"><a href="#inprogress" aria-controls="inprogress" role="tab" data-toggle="tab">Inprogress Worker<span>(<?php //echo count($hired_worker);?>)</span></a></li> -->

     <li role="presentation" class="custom-width-noti"><a href="#inprogress" aria-controls="inprogress" role="tab" data-toggle="tab">Confirmed jobs<span>(<?php echo count($hired_worker);?>)</span></a></li>
    
    <li role="presentation" class="custom-width-noti"><a href="#completed" aria-controls="completed" role="tab" data-toggle="tab">Completed<span>(<?php echo count($complte_job_worker);?>)</span></a></li>

     <li role="presentation" class="custom-width-noti"><a href="#cancelled" aria-controls="cancelled" role="tab" data-toggle="tab" >Cancelled<span>(<?php echo count($cancelled_job_worker);?>)</span></a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="newworker">
      <div class="tab_contentarea">
        <h1>Opens Jobs</h1>
        <div class="tablewrapper tablediv">
          <table class="table-striped">
        <thead>
          <tr class="headings">
            <th class="column1 col3">Job Post Date</th>
            <th class="column2 col3">Type of Job</th>
            <th class="column3 col4">Description</th>
            <th class="column4 col6">Number of Required WRkers</th>
            <th class="column5 col4">Location</th>
          </tr>
        </thead>
        <tbody>
        <?php $count=count($applied_worker);
        ?>
          @if($count > '0')
          @foreach($applied_worker as $applied_workers)
          <?php  
          $cat_id = $applied_workers->JobHeading;
          $category_data = DB::table('categories')
                                ->where('id',$cat_id)
                                ->first();
          $cat_name = $category_data->cat_name;
          $job_id = $applied_workers->JobID;
          $workerid = $applied_workers->customer_id;
          $createdate = explode(' ', $applied_workers->created_at); 
          $hire_count = DB::table('hire')
                                ->where('client_job_id',$job_id)
                                ->count();
        ?>

        <!-- (Select count(*) from hire where clentjobid =  $applied_workers->JobID and workerid = $applied_workers->Workerid ) -->
        @if($hire_count==0)
        <tr class="familydata custom-reports">
          <td class="column1 col3" data-label="Job Post Date">{{date('d M Y',strtotime($createdate[0]))}}</td>
          <td class="column2 col3" data-label="Type of Job">{{$cat_name}}</td>
          <td class="column3 col4" data-label="Description">{{$applied_workers->job_description}}</td>
          <td class="column4 col6" data-label="Required WRkers">{{$applied_workers->worker_required}}</td>
          <td class="column5 col4" data-label="Location">{{$applied_workers->job_location}}</td>
        </tr> 
      @endif
      @endforeach
      @else       
      <tr>
         <td>
           No job posted yet.
         </td>
     </tr>
     @endif

        </tbody>
      </table>
         <div class="pagination_section">
              {{ $applied_worker->links() }}
           </div>
        </div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="inprogress">
      <div class="tab_contentarea">
        <h1>Confirmed jobs</h1>
        <div class="tablewrapper tablediv">
          <table class="table-striped">
        <thead>
          <tr class="headings">
            <th class="column1 col4">Worker Uid</th>
            <th class="column2 col4">Worker Name</th>
            <th class="column3 col5">Name of Job</th>
            <!-- <th class="column4">Worker Type</th> -->
            <th class="column5 col5">Location</th>
            <th class="column6 col2"></th>
          </tr>
        </thead>
        <tbody>
        <?php 
        //echo'<pre>';
        //print_r($hired_worker);
        //echo'</pre>';

        $count=count($hired_worker); ?>
          @if($count > '0')
          @foreach($hired_worker as $hired_workers)
        <?php  
           $cat_id = $hired_workers->JobHeading;
           $category_data = DB::table('categories')
                                ->where('id',$cat_id)
                                ->first();
          $cat_name = $category_data->cat_name;
          $job_id = $hired_workers->JobID;
          $workerid = $hired_workers->Workerid;
          $hire_id = $hired_workers->id;
          //=============================//
          $hireTableData = DB::table('hire')
                          ->where('id',$hire_id)
                          ->first();
          //echo'<pre>';
          //print_r($hireTableData);
          //echo'</pre>';
          $hours_to_complete = $hireTableData->hours_to_complete;
          $time_approved = $hireTableData->time_approved;
          //=============================//
          $client_id = $hired_workers->client_id;
          $worker_id = $hired_workers->Workerid;
          $completed_count = DB::table('hire')
                                ->where('client_job_id',$job_id)
                                ->where('worker_id',$workerid)
                                ->where('job_status','completed')
                                ->where('status','3')
                                ->count();
        ?>
        @if($completed_count == 0)
        <tr class="familydata custom-reports">
          <td class="column1 col4" data-label="Worker Uid">{{$hired_workers->Workerid}}</td>
          <td class="column2 col4" data-label="Worker Name">{{$hired_workers->WorkerFirstName}}</td>
          <td class="column3 col5" data-label="Name of Job">{{$cat_name}}</td>
          <!-- <td class="column4" data-label="Worker Type">{{$hired_workers->WorkerType}}</td> -->
          <td class="column5 col5" data-label="Location">{{$hired_workers->address1}},{{$hired_workers->address}}</td>
          <td class="column6 col2">
            <!-- <div class="col-sm-6 inprogress_btn_sec">
                <button type="button"  class="btn btn-primary" disabled="disabled">InProgress </button>
            </div> -->
            <div class="inprogress_btn_sec">
              <?php if( $hours_to_complete != '' ){
                echo'<a href="'. url('hourdetails?hireid='. $hire_id ) .'" class="btn btn-primary hourButton">View & update hours</a>';
              }?>  
              <?php if( $time_approved == 1 ) : ?>
                <button type="button" id="feedback__btn" class="btn btn-primary" data-toggle="modal" data-target="#myModal_{{$hire_id}}">Complete this Job</button>
              <?php endif; ?>
                  <!-- The Modal -->
                  <div class="modal" id="myModal_{{ $hire_id }}">
                    <div class="modal-dialog">
                      <div class="modal-content">
                      
                        <!-- Modal Header -->
                        <div class="modal-header">
                          <div class="col-sm-10">
                          <h4 class="modal-title" style="font-size: 28px;font-weight: bold;">Feedback</h4>
                          </div>
                          <div class="col-sm-2">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>
                        </div>
                        
                        <!-- Modal body -->
                        <div class="feedback_popup modal-body">
                          <h1 style="margin-top: 0px;">Please provide your feedback below:</h1>
                          <h2 style="font-size: 20px;">How do you rate your overall experience?</h2>
                          <form id="feedback" name="feedback" method="POST" action="{{url('/customerfeedback')}}" enctype="multipart/form-data">
                            <?php $client_id = Auth::user()->id; ?>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="client_user_id" value="{{$client_id}}">
                            <input type="hidden" name="worker_user_id" value="{{ $hired_workers->Workerid }}">
                            <input type="hidden" name="cat_id" value="{{ $cat_id }}">
                            <input type="hidden" name="rev_id" value="{{ $job_id}}">
                            <input type="hidden" name="completed_btn_status" value="3">
                            

                            <div class="star_rating">
                              <fieldset class="rating">
                                  <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="GOLD"></label>
                                  <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="SILVER"></label>
                                  <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="SILVER"></label>
                                  <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="BROWNZ"></label>
                                  <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="BROWNZ"></label>
                              </fieldset>
                            </div>
                            <div class="feedback_text">
                            <textarea id="feedback_text" name="feedback_text" placeholder="Write Your Feedback here..." minlength="10" maxlength="200"></textarea>
                            </div>
                            <div class="submit_btn">
                                 <button class="btn btn-primary" id="submit_btn" name="submit_btn" value="Submit">Submit</button>
                            </div>
                          </form>
                        </div>
                        
                        <!-- Modal footer -->
                        <!-- <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div> -->
                        
                      </div>
                    </div>
                  </div>
            </div>
            <div class="inprogress_btn_sec">
              <button type="button" id="cancelled_btn" class="btn btn-primary" status="4" user_id="{{$hired_workers->Workerid}}" client_job_id="{{$hired_workers->JobID}}">Cancel this Job</button>
            </div>
            <div class="inprogress_btn_sec">
              <a href="{{url('/')}}/chat/?hired_id={{$hire_id}}&&job_id={{$job_id}}&&client_id={{$client_id}}&&worker_id={{$worker_id}}" id="cancelled_btn" class="btn btn-primary">Start Chat With {{$hired_workers->WorkerFirstName}}</a>
            </div>
          </td> 
        </tr> 
        @endif
      @endforeach
      @else       
      <tr>
         <td>
           No job posted yet.
         </td>
     </tr>
     @endif
        </tbody>
      </table>
           <div class="pagination_section">
              {{ $hired_worker->links() }}
           </div>
        </div>
      </div>
    </div>

   <!--  <div role="tabpanel" class="tab-pane" id="awarded">3</div> -->
    <div role="tabpanel" class="tab-pane" id="completed">
      <div class="tab_contentarea">
        <h1>Completed Jobs</h1>
        <div class="tablewrapper tablediv">
          <table class="table-striped">
        <thead>
          <tr class="headings">
            <th class="column1 col4">Worker Uid</th>
            <th class="column2 col4">Worker Name</th>
            <th class="column3 col5">Name of Job</th>
            <!-- <th class="column4">Worker Type</th> -->
            <th class="column5 col5">Location</th>
            <th class="column6 col2"></th>
          </tr>
        </thead>
        <tbody>
        <?php $count=count($complte_job_worker); ?>
          @if($count > '0')
          @foreach($complte_job_worker as $complte_job_workers)
        <?php  $cat_id = $complte_job_workers->JobHeading;
           $category_data = DB::table('categories')
                                ->where('id',$cat_id)
                                ->first();
          $cat_name = $category_data->cat_name;
          $job_id = $complte_job_workers->JobID;
          $workerid = $complte_job_workers->Workerid;
          $cancelled_count = DB::table('hire')
                                ->where('client_job_id',$job_id)
                                ->where('worker_id',$workerid)
                                ->where('job_status','cancelled')
                                ->where('status','4')
                                ->count();
        ?>
        @if($cancelled_count == 0)
        <tr class="familydata custom-reports">
          <td class="column1 col4" data-label="Worker Uid">{{$complte_job_workers->Workerid}}</td>
          <td class="column2 col4" data-label="Worker Name">{{$complte_job_workers->WorkerFirstName}}</td>
          <td class="column3 col5" data-label="Name of Job">{{$cat_name}}</td>
          <!-- <td class="column4" data-label="Worker Type">{{$complte_job_workers->WorkerType}}</td> -->
          <td class="column5 col5" data-label="Location">{{$complte_job_workers->address1}},{{$complte_job_workers->address}}</td>
          <td class="column6 col2">
            <div class="inprogress_btn_sec">
                <button type="button"  class="btn btn-primary" disabled="disabled">Completed Job </button>
            </div>
          </td> 
        </tr> 
      @endif
      @endforeach
      @else       
      <tr>
         <td>
           No job posted yet.
         </td>
     </tr>
     @endif
        </tbody>
      </table>
      <div class="pagination_section">
              {{ $complte_job_worker->links() }}
           </div>
        </div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="cancelled">
      <div class="tab_contentarea">
        <h1>Cancelled Jobs</h1>
        <div class="tablewrapper tablediv">
          <table class="table-striped">
        <thead>
          <tr class="headings">
            <th class="column1 col4">Worker Uid</th>
            <th class="column2 col4">Worker Name</th>
            <th class="column3 col5">Name of Job</th>
            <!-- <th class="column4">Worker Type</th> -->
            <th class="column5 col5">Location</th>
            <th class="column6 col2"></th>
          </tr>
        </thead>
        <tbody>
        <?php $count=count($cancelled_job_worker); ?>
          @if($count > '0')
          @foreach($cancelled_job_worker as $cancelled_job_workers)
        <?php  $cat_id = $cancelled_job_workers->JobHeading;
           $category_data = DB::table('categories')
                                ->where('id',$cat_id)
                                ->first();
          $cat_name = $category_data->cat_name;
        ?>
        <tr class="familydata custom-reports">
          <td class="column1 col4" data-label="Worker Uid">{{$cancelled_job_workers->Workerid}}</td>
          <td class="column2 col4" data-label="Worker Name">{{$cancelled_job_workers->WorkerFirstName}}</td>
          <td class="column3 col5" data-label="Name of Job">{{$cat_name}}</td>
          <!-- <td class="column4" data-label="Worker Type">{{$cancelled_job_workers->WorkerType}}</td> -->
          <td class="column5 col5" data-label="Location">{{$cancelled_job_workers->address1}},{{$cancelled_job_workers->address}}</td>
          <td class="column6 col2">
            <div class="inprogress_btn_sec">
                <button type="button"  class="btn btn-primary" disabled="disabled">Cancelled Job </button>
            </div>
          </td> 
        </tr> 
      @endforeach
      @else       
      <tr>
         <td>
           No job posted yet.
         </td>
     </tr>
     @endif
        </tbody>
      </table>
      <div class="pagination_section">
              {{ $cancelled_job_worker->links() }}
           </div>
        </div>
      </div>
    </div>

  </div>

</div>
</div>
</div>
<script type="text/javascript">
jQuery(document).ready(function($) {
/*$("#completed_btn").click(function(){
    var completed_btn_status = $("#completed_btn").attr("status");
    var user_id = $("#completed_btn").attr("user_id");
    var client_job_id = $("#completed_btn").attr("client_job_id");
    //var proposal_id = $("#completed_btn").attr("proposal_id");
    jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/customercompleteupdate') }}",
            data :{'completed_btn_status': completed_btn_status,'user_id': user_id,'client_job_id':client_job_id},
            success :  function(resp) {
              if(resp=='true'){
                  window.location.href="{{url('/customernotification')}}";

              }
             }
       });
});*/
$("#cancelled_btn").click(function(){
    var cancelled_btn_status = $("#cancelled_btn").attr("status");
    var user_id = $("#cancelled_btn").attr("user_id");
    var client_job_id = $("#cancelled_btn").attr("client_job_id");
    //var proposal_id = $("#completed_btn").attr("proposal_id");
    jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/customercancelledupdate') }}",
            data :{'cancelled_btn_status': cancelled_btn_status,'user_id': user_id,'client_job_id':client_job_id},
            success :  function(resp) {
              //alert(resp);
                if(resp=='true'){
                  window.location.href="{{url('/customernotification')}}";

                }
             }
       });
});
});
</script>
<!-- <script type="text/javascript">
  $(document).ready(function() {
    $("#preview_email").modal({
        show: false,
        backdrop: 'static'
    });
    
    $("#click-me").click(function() {
       $("#preview_email").modal("show");             
    });
});
</script>
  <style type="text/css">
          a.btn.btn-lg {
    background: #3399cc;
    color: #fff;
}#click-me {
    background: #337ab7;
    border: 1px solid #337ab7;
    padding: 8px 17px;
    border-radius: 5px;
    color: #fff;
}
        </style>
 -->
<style type="text/css">
table.table-striped td.column1 {
    width: 139px;
}
#feedback_text {
    min-height: 158px;
    width: 100%;
    border-radius: 5px;
    border: 1px solid #ccc;
}
.feedback_popup{
    min-height: 350px !important;
}
.submit_btn {
    text-align: right;
    margin-top: 10px;
}
#feedback_text {
    padding: 10px;
}
#submit_btn {
    width: 100%;
    background: #26a9e2;
    border-color: #26a9e2;
    font-size: 24px;
    text-transform: uppercase;
}
#cancelled_btn,#feedback__btn {
    font-size: 12px;
}
#completed_btn {
    font-size: 12px;
}
table.table-striped {
width: 100%;
}
.pagination_section .pagination li a{
  padding: 3px 20px;
}
.hourButton{
  font-size: 12px;
  display: block;
  margin-bottom: 2px;
}
</style>
<style>
@import url(https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);

fieldset, label { margin: 0; padding: 0; }
h1 { font-size: 1.5em; margin: 10px; }

/****** Style Star Rating Widget *****/

.rating { 
  border: none;
  float: left;
}

.rating > input { display: none; } 
.rating > label:before { 
  margin: 5px;
  font-size: 1.25em;
  font-family: FontAwesome;
  display: inline-block;
  content: "\f005";
}

.rating > .half:before { 
  content: "\f089";
  position: absolute;
}

.rating > label { 
  color: #ccc; 
 float: right; 
}

/***** CSS Magic to Highlight Stars on Hover *****/



.rating > input[id="star2"]:checked ~ label, /* show gold star when clicked */
.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { color: #cd7f32;  } /* hover previous stars in list */


.rating > input[id="star1"]:checked ~ label, /* show gold star when clicked */
.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { color: #cd7f32;  } /* hover previous stars in list */



.rating > input[id="star3"]:checked ~ label, /* show gold star when clicked */
.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { color: #585553;  } /* hover previous stars in list */

.rating > input[id="star4"]:checked ~ label, /* show gold star when clicked */
.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { color: #585553;  } /* hover previous stars in list */



.rating > input[id="star5"]:checked ~ label, /* show gold star when clicked */
.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */


/* CLICK ACTION FINISHED */




.rating:not(:checked) > label[for="star4"]:hover, /* hover current star */
.rating:not(:checked) > label[for="star4"]:hover ~ label{ color: #585553;  } /* hover previous stars in list */


.rating:not(:checked) > label[for="star3"]:hover, /* hover current star */
.rating:not(:checked) > label[for="star3"]:hover ~ label{ color: #585553;  } /* hover previous stars in list */



.rating:not(:checked) > label[for="star5"]:hover, /* hover current star */
.rating:not(:checked) > label[for="star5"]:hover ~ label { color: #FFD700;  } /* hover previous stars in list */


.rating:not(:checked) > label[for="star1"]:hover, /* hover current star */
.rating:not(:checked) > label[for="star1"]:hover ~ label{ color: #cd7f32;  } /* hover previous stars in list */



.rating:not(:checked) > label[for="star2"]:hover, /* hover current star */
.rating:not(:checked) > label[for="star2"]:hover ~ label{ color: #cd7f32;  } /* hover previous stars in list */


.rating > input:checked + label:hover, /* hover current star when changing rating */
.rating > input:checked ~ label:hover,
.rating > label:hover ~ input:checked ~ label, /* lighten current selection */
.rating > input:checked ~ label:hover ~ label { color: #FFED85;  } 
.custom-width-noti a {
    width: 100%;
}
.custom-width-noti {
    width: 25%;
}

</style>
 @stop