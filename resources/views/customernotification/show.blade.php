@extends('layouts.clientdefault')

@section('title', 'Customer Notification')

@section('content')
  <div class="col-md-9 dashboard-right">
           <div class="job-history">
            <div class="row pro-heading">
              <div class="col-sm-12">
                <div class="pro-head">
                <h3>Job Notification</h3>
              </div>
              </div>
            </div>
    <table class="table table-striped history-table">
    <thead>
    <tr> <th>Worker Name</th>
      <th >rating</th>
      <th >comment</th>
      <th></th>

    </tr>
  </thead>
  <tbody>
    <?php  $count=count($job_listing); ?>
    @if($count > '0')
   
    <?php 
    $user_id=$job_listing->worker_userid;
    
    $user = DB::table('users')
                     ->where('id', '=', $user_id)
                     ->first();     
      ?>                  
    <tr>
      <td data-label="Name of Job">{{ $user->name }}</td>
  
      <td data-label="Description"></td>
      <td data-label="Location"></td>
   
    <td> 
   <?php 
     
      $hire_sec = DB::table('hire')
                     ->where('worker_id', '=', $user->id)
                     ->where('job_status', '=', 'hired')
                     ->where('status', '=', 2)
                     ->where('deleted', '=', 0)
                     ->get(); 
                     ?>
      @if(count($hire_sec) > '0')
      <div class="col-sm-6 inprogress_btn_sec">
      <button type="button"  class="btn btn-primary">InProgress </button>
      </div>
      <div class="col-sm-6 cancel_btn_sec">
      <a href="#" class="btn btn-primary cancel_btn">Canceled </a>
      </div>
      @else
     <button type="button"  class="btn btn-primary" data-toggle="modal" data-target="#preview_email">Hire </button>
      @endif

<div class="modal fade" id="preview_email" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">  
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
      </div>
         {{ Form::model($job_listing, array('route' => array('customernotification.update', $job_listing->worker_userid), 'id' => 'addproductform', 'class' => 'innerform', 'name' => 'addproductform','method' => 'PUT', 'files' => true)) }}
      <div class="modal-body">
          <input type="hidden" name="client_job_id" value="{{ $job_listing->client_jobid}}">
          <input type="hidden" name="worker_id" value="{{ $job_listing->worker_userid}}">
          <input type="hidden" name="proposal_id" value="{{ $job_listing->id}}">
          <input type="hidden" name="job_status" value="hired">


           <h3><b>Thankyou for Hire this job we will get back soon!</b><h3>
      </div>
      <div class="modal-footer">
        <input type="submit" name="Close" value="Close" class="btn_close_ne" id="click-me">       
      </div>
        {{ Form::close() }}
    </div>
  </div>
  </td>

    </tr>
    @else
    <tr>
     <td>
        No job notification yet.
     </td>
  </tr>
  @endif

  </tbody>

</table>
</div>

</div>
</div>
</section>
<script type="text/javascript">
  $(document).ready(function() {
    $("#preview_email").modal({
        show: false,
        backdrop: 'static'
    });
    
    $("#click-me").click(function() {
       $("#preview_email").modal("show");             
    });
});
</script>
  <style type="text/css">
          a.btn.btn-lg {
    background: #3399cc;
    color: #fff;
}#click-me {
    background: #337ab7;
    border: 1px solid #337ab7;
    padding: 8px 17px;
    border-radius: 5px;
    color: #fff;
}
        </style>
@stop