@extends('layouts.workerdefault')

@section('title', 'Worker Notification')

@section('content')
  <div class="col-md-9 dashboard-right">
           <div class="job-history">
            <div class="row pro-heading">
              <div class="col-sm-12">
                <div class="pro-head">
                <h3>Job Notification</h3>
              </div>
              </div>
            </div>
    <table class="table table-striped history-table">
    <thead>
    <tr> <th>Customer Name</th>
      <th ></th>
      <th >comment</th>
      <th></th>

    </tr>
  </thead>
  <tbody>
    <?php  $count=count($job_listing); ?>
    @if($count > '0')
   
    <?php 
    $user_id=$job_listing->client_userid;
    
    $user = DB::table('users')
                     ->where('id', '=', $user_id)
                     ->first();     
      ?>                  
    <tr>
      <td data-label="Name of Job">{{ $user->name }}</td>
  
      <td data-label="Description"></td>
      <td data-label="Location"></td>
   
    <td> 
   <?php 
     
      $hire_sec = DB::table('hire')
                     ->where('worker_id', '=', $user->id)
                     ->where('job_status', '=', 'hired')
                     ->where('status', '=', 1)
                     ->where('deleted', '=', 0)
                     ->get(); 
                     ?>
      @if(count($hire_sec) > '0')
         <div class="col-sm-6">
      <button type="button"  class="btn btn-primary">InProgress </button>
      </div>
      <div class="col-sm-6">
      <a href="#" class="btn btn-primary cancel_btn">Canceled </a>
      </div>
      @else
      <div class="col-sm-6">
      <button type="button"  class="btn btn-primary">InProgress </button>
      </div>
      <div class="col-sm-6">
      <a href="#" class="btn btn-primary cancel_btn">Canceled </a>
      </div>
      @endif
  </td>

    </tr>
    @else
    <tr>
     <td>
        No job notification yet.
     </td>
  </tr>
  @endif

  </tbody>

</table>
</div>

</div>
</div>
</section>
<script type="text/javascript">
  $(document).ready(function() {
    $("#preview_email").modal({
        show: false,
        backdrop: 'static'
    });
    
    $("#click-me").click(function() {
       $("#preview_email").modal("show");             
    });
});
</script>
  <style type="text/css">
          a.btn.btn-lg {
    background: #3399cc;
    color: #fff;
}#click-me {
    background: #337ab7;
    border: 1px solid #337ab7;
    padding: 8px 17px;
    border-radius: 5px;
    color: #fff;
}
        </style>
@stop