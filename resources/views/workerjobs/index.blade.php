@extends('layouts.workerdefault')

@section('title', 'Worker Jobs')

@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css" rel="stylesheet">
     <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>

        <div class="tab-pane active text-style" id="tab1">
        <div class="row">
       <div class="col-md-12 rightcontent">
        <!-- <p>You will be able to compete for and work jobs once you finish your application</p>
 -->
        <div class="tab-section">
  <ul class="nav nav-tabs worktabtab" role="tablist">
    <li role="presentation" class="active custom-width-job"><a href="#openjobs" aria-controls="openjobs" role="tab" data-toggle="tab">Open Jobs <span> <?php echo count($clientjobpost);?> </span></a></li>
    <?php
     $appliedjobpostcount = count($appliedjobpost);
     //$acceptjobpostcount = count($acceptjobpost);
     $acceptjobpostcount = count($hired_info);
     $inprogressjobpostcount = count($inprogressjobpost);
     $completejobpostcount = count($completejobpost);
     $cancelledjobpostcount = count($cancelledjobpost);

      //$all_applied_count_worker = ($acceptjobpostcount+$completejobpostcount+$cancelledjobpostcount)-$appliedjobpostcount;
      $all_applied_count_worker = ($acceptjobpostcount+$completejobpostcount)-$appliedjobpostcount;
    ?>
    <!-- <li role="presentation" class="custom-width-job"><a href="#competing" aria-controls="competing" role="tab" data-toggle="tab">Competing For<span>(<?php //echo $all_applied_count_worker;?>)</span></a></li> -->

 <!--    <li role="presentation"><a href="#awarded" aria-controls="awarded" role="tab" data-toggle="tab">Awarded<span>(0)</span></a></li> -->
    <li role="presentation" class="custom-width-job"><a href="#confirmed" aria-controls="confirmed" role="tab" data-toggle="tab">Confirmed <span>(<?php echo $acceptjobpostcount;?>)</span></a></li>
    <!-- <li role="presentation" class="custom-width-job"><a href="#inprogress" aria-controls="inprogress" role="tab" data-toggle="tab">Inprogress <span>(<?php //echo count($inprogressjobpost);?>)</span></a></li> -->
    <li role="presentation" class="custom-width-job"><a href="#completed" aria-controls="completed" role="tab" data-toggle="tab">Completed<span>(<?php echo count($completejobpost);?>)</span></a></li>
     <!-- <li role="presentation" class="custom-width-job"><a href="#cancelled" aria-controls="cancelled" role="tab" data-toggle="tab">Cancelled<span>(<?php //echo count($cancelledjobpost);?>)</span></a></li> -->
  </ul>
  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="openjobs">
      <div class="tab_contentarea">
        <h1>Open Jobs</h1>
        <div class="tablewrapper tablediv">
          <table class="table-striped" style="width: 100%;">
        <thead>
          <tr class="headings">
            <th class="column1 col4">Job Type</th>
            <th class="column2 col4">Job Author Name</th>
            <th class="column3 col6">Description</th>
            <th class="column4 col4">Time Estimation</th>
            <th class="column5 col2">Detail</th>
          </tr>
        </thead>

        <tbody>
          <?php $count=count($clientjobpost); ?>
          @if($count > '0')
          @foreach($clientjobpost as $clientjob)

           <?php 

         $catid = $clientjob->job_heading;
         $author_id = $clientjob->user_id;
         $categories   = DB::table('categories')
                            ->where(['id'=>$catid, 'status' => '1','deleted' => '0'])
                            ->get();
         /*$cat_name = $categories->cat_name;*/ 
         /*$timedate=$clientjob->created_at;*/
         $timedate=$clientjob->created_at;
         $createdate = explode(' ', $clientjob->start_date);   
         $time=date('h:i A', strtotime($timedate));
         //$author_id = Auth::user()->id;
         $authorname = DB::table('users')
                            ->where('id','=',$author_id)
                            ->where('status','=','1')
                            ->first();
       
    ?>
     <tr class="familydata custom-reports wow fadeInLeft animated" data-wow-delay="2s">
            @foreach($categories as $categories_data)
            <?php 
            if($categories!=''){
            $cat_name = $categories_data->cat_name; ?>
            <td class="column1 col4" data-label="Job Type"><span>{{ $cat_name }} in {{ $clientjob->job_location }}</span></td>
            <?php } else { ?>
            <td class="column1 col4" data-label="Job Type"><span>---</span></td>
            <?php } ?>
            @endforeach
            @if($authorname!= '')
            <td class="column2 col4" data-label="Job Author Name">{{ $authorname->name }}</td>
            @else
            <td class="column2 col4" data-label="Job Author Name">...</td>
            @endif
            <td class="column3 col6" data-label="Description">{{ $clientjob->job_description }}</td>
            <td class="column4 col4" data-label="Time Estimation"> {{date('d M Y ',strtotime($createdate[0]))}} at {{ $time }} <br>{{ $clientjob->time_estimate }} hours </td>
            <td class="column5 col2" data-label="Detail"><a href="{{ route('workerjobs.show', $clientjob->id )}}" class="view-table">View Detail </a></td>
          </tr>
          @endforeach
          @else
      <tr>
         <td>
           No job posted yet.
         </td>
     </tr>
   @endif
        </tbody>
      </table>

        </div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="competing">
      <div class="tab_contentarea">
        <h1>Competing Jobs</h1>
        <div class="tablewrapper tablediv">
          <table class="table-striped" style="width: 100%;">
        <thead>
          <tr class="headings">
            <th class="column1 col4">Job Type</th>
            <th class="column2 col4">Job Author Name</th>
            <th class="column3 col6">Description</th>
            <th class="column4 col4">Time Estimation</th>
            <th class="column5 col2">Detail</th>
          </tr>
        </thead>
        <tbody>
          <?php $count=count($hired_info); ?>
          @if($count > '0')
          @foreach($hired_info as $hiredinfo)

           <?php 

      $catname = $hiredinfo->cat_name;
      $job_location = $hiredinfo->job_location;
      $job_description = $hiredinfo->job_description;
      $job_time = $hiredinfo->time_estimate;
      $job_user = $hiredinfo->user_id;
      $authornames = DB::table('users')
                            ->where('id','=', $job_user)
                            ->where('status',1)
                            ->get();
    //print_r($author_name);
      
    ?>
     <tr class="familydata custom-reports">
            <td class="column1 col4" data-label="Job Type"><span>{{ $catname }} in {{ $job_location }}</span></td>
            @if($authornames!='')
            @foreach($authornames as $authorname)
            <td class="column2 col4" data-label="Job Author Name">{{ $authorname->name }}</td>
            @endforeach
            @else
            <td class="column2 col6" data-label="Job Author Name">...</td>
            @endif
            <td class="column3 col4" data-label="Description">{{ $job_description }}</td>
            <td class="column4 col4" data-label="Time Estimation"> {{ $job_time }} hours</td>
            <td class="column5 col2" data-label="Detail"><a href="#" class="view-table">Accept Job</a></td>
          </tr>
          @endforeach
          @else
      <tr>
         <td>
           No job posted yet.
         </td>
     </tr>
   @endif
        </tbody>
      </table>
        </div>
      </div>
    </div>

   <!--  <div role="tabpanel" class="tab-pane" id="awarded">3</div> -->


    <div role="tabpanel" class="tab-pane" id="confirmed">
      <div class="tab_contentarea">
        <h1>Confirmed Jobs</h1>
        <div class="tablewrapper tablediv">
          <table class="table-striped" style="width: 100%;">
        <thead>
          <tr class="headings">
            <th class="column1 col4">Job Type</th>
            <th class="column2 col4">Job Author Name</th>
            <th class="column3 col6">Description</th>
            <th class="column4 col4">Time Estimation</th>
            <th class="column5 col2">Detail</th>
          </tr>
        </thead>
        <tbody>
          <?php
            //echo'<pre>';
            //print_r($hired_info);
            //echo'</pre>';
          //echo  Auth::user()->name;
          ?> 
          <?php $count=count($hired_info); ?>
          @if($count > '0')
          @foreach($hired_info as $hiredinfo)
    <?php
      $hireid = $hiredinfo->hireid;
      $catname = $hiredinfo->cat_name;
      $client_job_id = $hiredinfo->client_job_id;
      $job_heading = $hiredinfo->job_heading;
      $job_location = $hiredinfo->job_location;
      $job_description = $hiredinfo->job_description;
      $job_time = $hiredinfo->time_estimate;
      $job_user = $hiredinfo->user_id;
      $authornames = DB::table('users')
                            ->where('id','=', $job_user)
                            ->where('status',1)
                            ->get();
      $hiredata = DB::table('hire')
                      ->where('worker_id','=', $hiredinfo->worker_id)
                      ->where('status',2)
                      ->first();
    //print_r($author_name);
      
      
    ?>
     <tr class="familydata custom-reports">
            <td class="column1 col4" data-label="Job Type"><span><a href="{{ route('workerjobs.show', $hiredinfo->client_job_id )}}">{{ $catname }} in {{ $job_location }}</a></span></td>
            @if($authornames!='')
            @foreach($authornames as $authorname)
            <td class="column2 col4" data-label="Job Author Name">{{ $authorname->name }}</td>
            @endforeach
            @else
            <!--<td class="column2" data-label="Job Author Name">...</td>-->
            @endif
            <td class="column3 col6" data-label="Description">{{ $job_description }}</td>
            <td class="column4 col4" data-label="Time Estimation"> {{ $job_time }} hours</td>
            <td class="column5 col2" data-label="Detail"><a href="{{url('/')}}/chat/?hired_id={{$hireid}}&&job_id={{$hiredinfo->client_job_id}}&&client_id={{$hiredinfo->user_id}}&&worker_id={{$hiredinfo->worker_id}}" class="view-table">Start Chat</a> <br />
              <?php
              if( $hiredinfo->hours_to_complete =='' && $hiredinfo->mints_to_complete =='' ): 
                ?>
              <a href="#" class="view-table updateWorkHourID" data-toggle="modal" data-target="#updateWorkHour" data-hireid="{{$hireid}}" data-jobauthor="<?php echo $job_user; ?>" data-clientjobid="<?php echo $client_job_id; ?>" data-jobheading="<?php echo $job_heading; ?>">Update WRK Hours</a> 
            <?php else: 
            ?>
              <a href="#"  class="view-table viewHourDetails"  data-toggle="modal" data-target="#hourDetailModal" data-hireid="{{$hireid}}"  data-mints="<?php echo $hiredinfo->mints_to_complete ?>" data-hours="<?php echo $hiredinfo->hours_to_complete; ?>" data-updatedhours="<?php echo $hiredinfo->updated_hours; ?>" data-updatedmints="<?php echo $hiredinfo->updated_mints; ?>" data-timeapproved="<?php echo $hiredinfo->time_approved;?>">View Hours </a> 
            <?php endif;?>
            </td>
          </tr>
          @endforeach
          @else
      <tr>
         <td>
           No job posted yet.
         </td>
     </tr>
   @endif
        </tbody>
      </table>
        </div>
      </div>
    </div>
 
    <div role="tabpanel" class="tab-pane" id="completed">
      <div class="tab_contentarea">
        <h1>Completed Jobs</h1>
        <div class="tablewrapper tablediv">
          <table class="table-striped" style="width: 100%;">
        <thead>
          <tr class="headings">
            <th class="column1 col4">Job Type1</th>
            <th class="column2 col4">Job Author Name</th>
            <th class="column3 col6">Description</th>
            <th class="column4 col4">Time Estimation</th>
            <!--<th class="column5 "></th>-->
            <th class="column6 col2">Detail</th>
          </tr>
        </thead>
        <tbody>
          <?php $count=count($completejobpost); ?>
          @if($count > '0')
          @foreach($completejobpost as $completejob)

           <?php 

            //$catid = $completejob->job_heading;
            //$author_id = $completejob->user_id;
            /* $categories   = DB::table('categories')
                            ->where(['id'=>$catid, 'status' => '1','deleted' => '0'])
                            ->first(); */
            //$cat_name = $categories->cat_name; 
            /*$timedate=$completejob->created_at;*/
         
            $timedate=$completejob->created_at;
            $createdate = explode(' ', $completejob->start_date);   
            $time=date('h:i A', strtotime($timedate));
      
            /* $authornames = DB::table('users')
                            ->where('id','=', $author_id)
                            ->where('status',1)
                            ->get(); */
            //print_r($author_name);
      
          ?>
     <tr class="familydata custom-reports">
            <td class="column1 col4" data-label="Job Type"><span>{{ $completejob->cat_name }} in {{ $completejob->job_location }}</span></td>
            <td class="column2 col4" data-label="Job Author Name">{{ $completejob->name }}</td>
            <td class="column3 col6" data-label="Description">{{ $completejob->job_description }}</td>
            <td class="column4 col4" data-label="Time Estimation"> {{date('d M Y ',strtotime($createdate[0]))}} <br> at {{ $time }} <br>{{ $completejob->time_estimate }} hours</td> 
            <td class="column5 col2" data-label="Detail"><a href="#" class="view-table">Completed Job</a></td>
          </tr>
          @endforeach
          @else
      <tr>
         <td>
           No job posted yet.
         </td>
     </tr>
   @endif
        </tbody>
      </table>
        </div>
      </div>
    </div>
<!--     <div role="tabpanel" class="tab-pane" id="cancelled">
           <div class="tab_contentarea">
        <h1>Cancelled Jobs</h1>
        <div class="tablewrapper tablediv">
          <table class="table-striped" style="width: 100%;">
        <thead>
          <tr class="headings">
            <th class="column1">Job Type</th>
            <th class="column2">Job Author Name</th>
            <th class="column3">Description</th>
            <th class="column4">Time Estimation</th>
            <th class="column5">Detail</th>
          </tr>
        </thead>
        <tbody>
          <?php //$count=count($cancelledjobpost); ?>
          @if($count > '0')
          @foreach($cancelledjobpost as $cancelledjob)

           <?php 

      //$catid = $cancelledjob->job_heading;
      //$author_id = $cancelledjob->user_id;
      //$categories   = DB::table('categories')
                            //->where(['id'=>$catid, 'status' => '1','deleted' => '0'])
                            //->first();
         //$cat_name = $categories->cat_name; 
         /*$timedate=$cancelledjob->created_at;*/
         //$timedate=$cancelledjob->created_at;
         //$createdate = explode(' ', $cancelledjob->start_date);   
         //$time=date('h:i A', strtotime($timedate));
      
      //$authornames = DB::table('users')
                            //->where('id','=', $author_id)
                            //->where('status',1)
                            //->get();
    //print_r($author_name);
      
    ?> 
     <tr class="familydata custom-reports">
            <td class="column1" data-label="Job Type"><span>{{ $cat_name }} in {{ $cancelledjob->job_location }}</span></td>
            @if($authornames!='')
            @foreach($authornames as $authorname)
            <td class="column2" data-label="Job Author Name">{{ $authorname->name }}</td>
            @endforeach
            @else
            <td class="column2" data-label="Job Author Name">...</td>
            @endif
            <td class="column3" data-label="Description">{{ $cancelledjob->job_description }}</td>
            <td class="column4" data-label="Time Estimation"> {{date('d M Y ',strtotime($createdate[0]))}} <br> at {{ $time }} <br>{{ $cancelledjob->time_estimate }} hours</td>
            <td class="column5" data-label="Detail"><a href="#" class="view-table">Cancelled Job</a></td>
          </tr>
          @endforeach
          @else
      <tr>
         <td>
           No job posted yet.
         </td>
     </tr>
   @endif
        </tbody>
      </table>
        </div>
      </div>
    </div> -->
  </div>

</div>

<div class="row pagination-list">
  <div class="col-md-6 col-sm-6">
      <nav aria-label="Page navigation example">
       <div class="pagination_section">
  <!-- {{ $client_pagination }} -->
      </div>
  </nav>
  </div>
  <div class="col-md-6 col-sm-6">
    <ul class="list-inline select-pages">
      <?php  $id = Auth::user()->id;
      $count_total = DB::table('clientjobpost')
                           ->where('user_id', '!=', $id)->count(); ?>
      <!-- <li><h4> {{ $count_total }} Worker Total</h4></li> -->
      <li>
    <!--       <form name="searchForm" id="searchForm" method="GET" action="{{ route('workerjobs.index') }}" role="form" class="srchfrm">
                <?php $count=DB::table('clientjobpost')->count();
                            
                     if ($count > 5){ ?>
                   <span>Showing:</span>
                        <select name="show" id="show" onchange="this.form.submit()">
                        <option value="5"  {{ ( $show == '5' ) ? 'selected' : '' }}>5</option>
                        <option value="10" {{ ( $show == '10' ) ? 'selected' : '' }}>10</option>
                        <option value="20" {{ ( $show == '20' ) ? 'selected' : '' }}>20</option>
                        </select>
                        <?php }?>
                  </form> -->
      </li>
    </ul>
  </div>
  
</div> 
<a href="{{url('/workeradminchat')}}" class="support-chat">Message us <i class="fa fa-envelope" aria-hidden="true"></i></a>
</div>



<!-- <div class="col-md-3">
                <div class="become-cont">
                  <h4>Become a CollegeWRK</h4>
                  <p>Complete your Sweeps Profile to become an active Sweeper and work the jobs you want.</p>
                </div>
              </div> -->

<!--<div class="row">
 <div class="col-md-9"></div> 
 <div class="col-md-3 nopadding"></div>  
</div>-->



</div>
</div>
</div>
</div>
<!-- update work hours modal start  -->

<div id="updateWorkHour" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Timesheet</h4>
      </div>
      <div class="modal-body">
        <p>Time spent working on the job. Do not include travel time to and from the job.</p> <br />
        <form>
          <div class="form-group">
            <div class="row">
              <input type="hidden" value="" class="idToUpdate">
              <input type="hidden" value="" class="clientjobid">
              <input type="hidden" value="" class="jobauthor">
              <input type="hidden" value="" class="jobheading">
                <div class="col-sm-4">
                  <!--<select class="form-control hours" name="hours" required>
                    <option value=""> Hours</option>
                      <?php for($i =0; $i<=23; $i++ ):?>
                      <option value="<?php echo $i;?>"><?php echo $i; ?></option>
                      <?php endfor;?>
                  </select> -->
                  <input type="number" class="form-control hours" name="hours" min="0" placeholder="Hours" required >
                </div>
                <div class="col-sm-4">
                  <!--<select class="form-control mints" name="mints" required>
                    <option value=""> Mints</option>
                      <?php for($j =0; $j<=59; $j++ ):?>
                      <option value="<?php echo $j;?>"><?php echo $j; ?></option>
                      <?php endfor;?>
                  </select> -->
                  <input type="number" class="form-control mints" name="mints" min = "0" max="59" placeholder="Minutes" required > 

                </div>
                <div class="col-sm-4">
                  <label class="label label-default">Hours and Minutes</label>
                </div>
                <div class="col-sm-12">
                  <br />
                  <h4>Job Summary (Required)</h4>
                  <p>Summarize the job and overall experience. Job Posters see this.(30 characters minimum)</p> <br />
                    <textarea class="job_summary form-control" name="job_summary" placeholder="Tell us about the job here"></textarea> <br />
                </div>
                <div class="col-sm-12">
                  <h4>Future Appointment(s)</h4>
                  <p>Is a follow-up appointment scheduled? Should we get in touch with the job Poster about future jobs?</p><br />
                    <textarea class="future_appointement form-control" name="future_appointement" placeholder="If a follow-up appointment scheduled? please list the exact start time"></textarea> <br />
                </div>
                <div class="col-sm-12">
                  <h4>Private Notes</h4>
                  <p>Feedback on fellow WRKers, Job Poster or anything else. Only seen by CollegeWRK admins.</p><br />
                    <textarea class="private_notes form-control" name="private_notes" placeholder="Private Notes about the Job"></textarea> <br />
                </div>
                <div class="col-sm-3">
                    <button type="button" class="btn update_work_hours"> Save </button> 
                </div>

            </div>

              
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </div>
    </div>

  </div>
</div>
<!-- update work hours modal ends  -->


<!-- hours details modal start   -->
<div id="hourDetailModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Timesheet</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12 hoursData">
            
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </div>
    </div>

  </div>
</div>
<!-- hours details modal end   -->



<script type="text/javascript">
$(document).ready(function() {
$("#completed_btn").click(function(){
    var completed_btn_status = $("#completed_btn").attr("status");
    var user_id = $("#completed_btn").attr("user_id");
    var client_job_id = $("#completed_btn").attr("client_job_id");
    
      jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/completeupdate') }}",
            data :{'completed_btn_status': completed_btn_status,'user_id': user_id,'client_job_id':client_job_id},
            success :  function(resp) {
              //alert(resp);
                if(resp=='true'){
                  window.location.href="{{url('/workerjobs')}}";

                }
             }
       });
});
$("#cancelled_btn").click(function(){
    var cancelled_btn_status = $("#cancelled_btn").attr("status");
    var user_id = $("#cancelled_btn").attr("user_id");
    var client_job_id = $("#cancelled_btn").attr("client_job_id");
    
    jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/cancelledupdate') }}",
            data :{'cancelled_btn_status': cancelled_btn_status,'user_id': user_id,'client_job_id':client_job_id},
            success :  function(resp) {
              //alert(resp);
                if(resp=='true'){
                  window.location.href="{{url('/workerjobs')}}";

                }
             }
       });
});
//==========================================//
$(".updateWorkHourID").click(function(){
var hireid = $(this).data('hireid');
var clientjobid = $(this).data('clientjobid');
var jobauthor = $(this).data('jobauthor');
var jobheading = $(this).data('jobheading');
//alert( hireid );
$('.idToUpdate').val( hireid );
$('.clientjobid').val( clientjobid );
$('.jobauthor').val( jobauthor );
$('.jobheading').val( jobheading );
});

$(".viewHourDetails").click(function(){
  var mints = $(this).data('mints');
  var hours = $(this).data('hours');
  var time_approved = $(this).data('time_approved');
  var updatedhours = $(this).data('updatedhours');
  var updatedmints = $(this).data('updatedmints');
  var timeapproved = $(this).data('timeapproved');

  if( ( updatedhours !='' && updatedhours != undefined) && (updatedmints != '' && updatedmints != undefined)){
    var dataToDisplay = '<p>Time updated by you : <label class="label label-success">'+ hours +' : '+ mints +'hr </label></p><br /><p>Approved Time : <label class="label label-success"> '+ updatedhours + ' : '+  updatedmints +'hr</p>';
  }else{
    if( timeapproved == 1 ){
      var dataToDisplay = 'Approved Time : <label class="label label-success">'+ hours + ':'+  mints +'hr</label></p>';
    }else{
      var dataToDisplay = 'Time updated by you : <label class="label label-success">'+ hours + ':'+  mints +'hr</label></p>';
    }
    
  }
  $('.hoursData').html( dataToDisplay );

});



$(".update_work_hours").click(function(){
  var idToUpdate = $('.idToUpdate').val();
  var hours = $('.hours').val();
  var mints = $('.mints').val();
  var clientjobid = $('.clientjobid').val();
  var jobauthor = $('.jobauthor').val();
  var jobheading = $('.jobheading').val();
  var job_summary = $('.job_summary').val();
  var future_appointement = $('.future_appointement').val();
  var private_notes = $('.private_notes').val();
  if( (hours !='' && hours != undefined) && (mints !='' && mints != undefined ) && ( job_summary !='' && job_summary != undefined )){
    if( job_summary.length > 30 ){
    jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/updateworkedhour') }}",
            data :{'idToUpdate' : idToUpdate, 'mints': mints, 'hours' : hours, 'clientjobid' : clientjobid, 'jobauthor' : jobauthor, 'jobheading' : jobheading, 'job_summary' : job_summary, 'future_appointement' : future_appointement, 'private_notes' : private_notes }, 
            success :  function(resp) {
              //alert(resp);
              if(resp=='true'){
                alert('Updated successfully');
                  window.location.href="{{url('/workerjobs')}}";

                }else{
                  alert('There is an error while updating the time. Please try again.');
                }
              
             }
       });
  }else{
    alert('Job summary must be 30 characters minimum.');
  }
  }else{
    alert('Please Enter Hour, mints & job summary');
  }
});
//==========================================//
});
</script>
<style type="text/css">
  .view-table {
    background: #26a9e2;
    color: #fff;
    border: none;
    padding: 10px 10px 10px 10px;
    font-size: 15px;
    border-radius: 5px;
}
#feedback_text {
    min-height: 158px;
    width: 100%;
    border-radius: 5px;
    border: 1px solid #ccc;
}
.feedback_popup{
    min-height: 350px !important;
}
.submit_btn {
    text-align: right;
    margin-top: 10px;
}
#feedback_text {
    padding: 10px;
}
#submit_btn {
    width: 100%;
    background: #26a9e2;
    border-color: #26a9e2;
    font-size: 24px;
    text-transform: uppercase;
}
.support-chat {
    text-align: center;
    font-size: 16px;
    background-color: #3399cc;
    padding: 10px 20px;
    display: inline-block;
    border-radius: 20px;
    float: right;
    margin-top: 20px;
    color: #fff;
    text-decoration: none;
}
</style>
<style>
@import url(http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);

fieldset, label { margin: 0; padding: 0; }
h1 { font-size: 1.5em; margin: 10px; }

/****** Style Star Rating Widget *****/

.rating { 
  border: none;
  float: left;
}

.rating > input { display: none; } 
.rating > label:before { 
  margin: 5px;
  font-size: 1.25em;
  font-family: FontAwesome;
  display: inline-block;
  content: "\f005";
}

.rating > .half:before { 
  content: "\f089";
  position: absolute;
}

.rating > label { 
  color: #ccc; 
 float: right; 
}

/***** CSS Magic to Highlight Stars on Hover *****/



.rating > input[id="star2"]:checked ~ label, /* show gold star when clicked */
.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { color: #cd7f32;  } /* hover previous stars in list */


.rating > input[id="star1"]:checked ~ label, /* show gold star when clicked */
.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { color: #cd7f32;  } /* hover previous stars in list */



.rating > input[id="star3"]:checked ~ label, /* show gold star when clicked */
.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { color: #585553;  } /* hover previous stars in list */

.rating > input[id="star4"]:checked ~ label, /* show gold star when clicked */
.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { color: #585553;  } /* hover previous stars in list */



.rating > input[id="star5"]:checked ~ label, /* show gold star when clicked */
.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */


/* CLICK ACTION FINISHED */




.rating:not(:checked) > label[for="star4"]:hover, /* hover current star */
.rating:not(:checked) > label[for="star4"]:hover ~ label{ color: #585553;  } /* hover previous stars in list */


.rating:not(:checked) > label[for="star3"]:hover, /* hover current star */
.rating:not(:checked) > label[for="star3"]:hover ~ label{ color: #585553;  } /* hover previous stars in list */



.rating:not(:checked) > label[for="star5"]:hover, /* hover current star */
.rating:not(:checked) > label[for="star5"]:hover ~ label { color: #FFD700;  } /* hover previous stars in list */


.rating:not(:checked) > label[for="star1"]:hover, /* hover current star */
.rating:not(:checked) > label[for="star1"]:hover ~ label{ color: #cd7f32;  } /* hover previous stars in list */



.rating:not(:checked) > label[for="star2"]:hover, /* hover current star */
.rating:not(:checked) > label[for="star2"]:hover ~ label{ color: #cd7f32;  } /* hover previous stars in list */


.rating > input:checked + label:hover, /* hover current star when changing rating */
.rating > input:checked ~ label:hover,
.rating > label:hover ~ input:checked ~ label, /* lighten current selection */
.rating > input:checked ~ label:hover ~ label { color: #FFED85;  } 
.custom-width-job {
    width: 16.66%;
}
.custom-width-job a {
    width: 100%;
}


</style>
@stop