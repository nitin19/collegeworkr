@extends('layouts.workerdefault')

@section('title', 'Workerdashboard')

@section('content')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
  <?php
  //echo'<pre>';
  //print_r( $workerjobpreference );
  //echo'</pre>';

  ?>
  <div class="" id="tab2">
    <div  class="main-profile">
    <div class="upper-profile uper_sec">
      <div class="row pro-heading">
        <div class="col-sm-12">
          <div class="pro-head">
          <?php $user_address = Auth::user()->address;
          if($user_address=='') { ?>
            <h4 class="addres-alert">Please Complete your profile</h4>
            <?php } else { ?>
            <h4> </h4>
            <?php } ?>
          <h3>Account Information</h3>
        </div>
        </div>
      </div>
        <div class="row">
        <div class="col-md-9">
            <div class="row user-info custom-profile">
              <h3>Personal Info </h3>

         <form id="profileform" method="POST" action="{{ url('/workerprofileupdate', Auth::user()->id)}}" enctype="multipart/form-data">
               {{ csrf_field() }}
                <input type="hidden" name="user_id"  value="">
            <div class="col-md-2 nopadding">

            <?php $user_profile = Auth::user()->image;?>
             @if($user_profile !='')
            <img src="{{url('/public')}}/uploads/profile/{{$userworker->image}}" class="img-responsive center-block" id="profileimg">
             @else
            <img src="{{url('/public')}}/images/avatar.jpg" class="img-responsive center-block" id="profileimg">
             @endif
          
            <div class="form-group" style="text-align: center;">
      <!--         <label style="color: #3399cc;font-size: 15px;text-decoration: none;font-weight: 400;display: block;">Upload Image</label> -->
              <div class="input-group">
                  <span class="input-group-btn">
                      <span class="btn btn-default btn-file">
                          Browse… <input type="file" id="imgInp" name="image">
                      </span>
                  </span>
                 
              </div>
          </div>              </div>
              <div class="col-md-1">
              </div>
              <div class="col-md-9">
               
                <div class="row">
                  <div class="form-group">
                    <div class="col-md-6">
                      <label>First Name</label>
                      <input type="text" name="firstname" placeholder="Enter your firstname" value="{{Auth::user()->firstname}}" class="form-control">
                    </div>
                    <div class="col-md-6">
                      <label>Last Name</label>
                      <input type="text" name="lastname" placeholder="Enter your lastname" value="{{Auth::user()->lastname}}" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                     <div class="col-md-12">
                        <label>Email Address</label>
                        <input type="email" name="email" placeholder="Enter your email" value="{{Auth::user()->email}}" class="form-control" readonly="readonly">
                    </div>
                     
                  </div>
                  <div class="form-group">
                    <div class="col-md-6">
                        <label>Phone Number</label>
                        <input type="tel" name="phone" placeholder="Enter your phone number" value="{{Auth::user()->phone}}" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label>Address</label>
                        <input type="text" id="address" name="address" placeholder="Enter Your Full Address" value="{{Auth::user()->address}}" class="form-control" required>
                    </div>
                    <!-- <div class="col-md-6">
                        <label>Invite Code</label>
                        <input type="text" name="invitecode" placeholder="Enter your code" value="{{Auth::user()->invitecode}}" class="form-control" readonly="readonly">
                    </div> -->
                  </div>
              <div class="form-group">
                <div class="col-md-12">
                  <button type="submit" class="sve-btn">Update Information</button>
                </div>
              </div>
              </div>
            </div>
            </div>
          </form>
   
          </div>
          <div class="col-md-3">
            <div class="pro-myaccount">
              <h2>My Account</h2>
              <ul>
                
                <li>Member Since:</li>
                <li>Balance: $0.00 </li>
                <!-- <li>Jobs Posted:
                  
                </li> -->
                <li>
                  <?php 
                    $id = Auth::user()->id;
                    $feedback = DB::table('feedback')
                            ->where('worker_id',$id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->sum('feedback_rating');
                    //echo $feedback ;

                     $feedback_count = DB::table('feedback')
                            ->where('worker_id',$id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->count('feedback_rating');
                     $customer_feedback_count = DB::table('feedback')
                            ->where('worker_id',$id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->count('user_id');

                    $feedback;
                          $feedback_count;
                          $customer_feedback_count;
                          if($feedback!='0' && $feedback_count!='0' && $customer_feedback_count!='0'){
                              $feedback_average =  $feedback / $feedback_count;
                          } else {
                              $feedback_average = '';
                          }
                    ?>


                  Rating: <span>
            
                        @if($feedback_average <= 2)
                        {{$feedback_average}}
                        <img src="{{url('/public')}}/images/brownz.png">
                        ({{$customer_feedback_count}}) 
                        @elseif($feedback_average > 3 && $feedback_average <= 4)
                        {{$feedback_average}}
                        <img src="{{url('/public')}}/images/silver.png">
                        ({{$customer_feedback_count}}) 
                        @elseif($feedback_average > 4 && $feedback_average <= 5)
                        {{$feedback_average}}
                        <img src="{{url('/public')}}/images/gold.png">
                        ({{$customer_feedback_count}})
                        @else 
                        @endif

                      </span>
                </li>
              </ul>
            </div>
          </div>
        </div>
<script type="text/javascript">
$(document).ready(function(){
  $(".social_link_btn").click(function(){
      $(".address_error_message").html("Please Fill Your Address");
  });
});
</script>
<style type="text/css">
  .pro-myaccount {
    background: #3399cc;
    padding: 20px 15px;
    margin-top: 16px;
    min-height: 356px;
}
.pro-myaccount h2 {
    font-size: 20px;
    color: #fff;
    font-family: "ProximaNova-Semibold";
}  
.pro-myaccount ul {
    margin-top: 10px;
}
.pro-myaccount li {
    font-size: 13px;
    color: #ffffff;
    list-style: none;
    padding: 7px 0;
}
.address_error_message {
    color: #ff0000;
    font-size: 13px;
    font-style: italic;
}
.addres-alert {
    text-align: center;
    color: red;
    font-weight: bold;
}
</style>
<!-- <div id="google-reviews"></div>
 -->
<!-- <link rel="stylesheet" href="https://cdn.rawgit.com/stevenmonson/googleReviews/master/google-places.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.rawgit.com/stevenmonson/googleReviews/6e8f0d79/google-places.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAmECi5THi1W3CHDdmC2bI6q6dDZNRAY-I&signed_in=true&libraries=places"></script>

<script>
jQuery(document).ready(function( $ ) {
   $("#google-reviews").googlePlaces({
        placeId: 'ChIJp2QxV_sJVFMR1DEp1x_16F8' //Find placeID @: https://developers.google.com/places/place-id
      , render: ['reviews']
      , min_rating: 4
      , max_rows:4
   });
});
</script> -->


      <div class="row user-about">
        <div class="col-sm-12">
          <h4>ABOUT ME :</h4>
          <?php $user_about = Auth::user()->worker_about; ?>
          <p>{{ $user_about }} </p>
        </div>
      </div>
      <div class="row ">
        @if(Auth::user()->facebook_id!='' && Auth::user()->google_id ==NULL && Auth::user()->address ==NULL || Auth::user()->google_id!='' && Auth::user()->facebook_id ==NULL && Auth::user()->address ==NULL)
        <div class="col-md-12">
          <a cwlass="updt-btn social_link_btn">Update Account Information</a>
        </div>
        @else
        <div class="col-md-12">
          <a href="#info_profile" class="updt-btn">Update Account Information</a>
        </div>
        @endif
      </div>
      
      
    </div>
    <div class="lower-profile" id="info_profile">
      <div class="row pro-heading">
        <div class="col-sm-12">
          <div class="pro-head">
          <h3>Update Profile Information</h3>
        </div>
        </div>
      </div>
      <div class="row jb-prefrence" >
        <div class="col-md-12">
              <div class="panel with-nav-tabs panel-default panel-preference">
          <div class="panel-heading">
                  <ul class="nav nav-tabs">
                      <li class="active custom-width-done"><a href="#tab1default" data-toggle="tab">Account Information</a></li>
                      <li class="custom-width-done"><a href="#tab2default" data-toggle="tab">Job preference</a></li>
                     <!-- <li class="custom-width-done"><a href="#tab3default" data-toggle="tab">Onboarding</a></li> -->
                      
                  </ul>
          </div>
<div class="panel-body">
  <div class="tab-content">
    <div class="tab-pane fade in active" id="tab1default">
     <form class="profileFrm" id="profileFrm" action="{{ url('workerdashboard/store') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
     <div class="about-tb">
      <h4>About You</h4>
      <div class="row tp-rw-1">
        <div class="col-sm-3">
        <!--   <img src="{{ url('/public') }}/images/user-pic.png" class="img-responsive"> -->
        <?php $user_image = Auth::user()->image; ?>
         @if($user_image !='')
            <div class="browseimage nopadding" id="browse_img">
               <img src="{{ url('/public') }}/uploads/profile/{{ $user_image }}" style="width:150px;height:150px;" class="img-responsive" id="profileimg">
                <div class="form-group" style="text-align: center;">
              <div class="input-group">
                  <span class="input-group-btn">
                      <span class="btn-default btn-file">
                      <input type="file" id="image" name="image">
                      </span>
                  </span>
                 
              </div>
          </div>
                 </div>
                @else
            <img src="{{url('/public')}}/images/avatar.jpg" style="width:150px;height:150px;" class="img-responsive" id="profileimg">
           @endif
             
      </div>
        <div class="col-sm-9">
          <?php 
           $user_first_name = Auth::user()->firstname;
           $user_slogan = Auth::user()->worker_slogan;
           $user_about = Auth::user()->worker_about;
           $user_qualification = Auth::user()->worker_qualification; 
           $user_worker_school = Auth::user()->worker_school;
           $user_attend = Auth::user()->worker_attend;
          
          ?>
          <h5> {{$user_first_name}} </h5>
          <h6>Add a Profile Photo</h6>
          <p>In your profile photo, we recommend a high-resolution, well-lit photo of your smiling face (without sunglasses). Recommended dimensions are 300x300 pixels.</p>
        </div>
        <div class="col-md-12 tp-rw-col1">
          <h6>Slogan</h6>
          <p class="dr-clr">In a few words, describe yourself and your ambitions. This will be visible and prominent on your profile. (20 - 100 characters)</p>
          <input type="text" name="worker_slogan" placeholder="I am Painter & intersted in doing paintings. And want work." class="form-control" value="{{ $user_slogan }}">
        </div>
         <div class="col-md-12 tp-rw-col1">
          <h6>About</h6>
          <p class="dr-clr">In a few words, describe yourself and your ambitions. This will be visible and prominent on your profile. (20 - 100 characters)</p>
        <textarea placeholder="" class="form-control" rows="5" name="worker_about">{{ $user_about }}</textarea>
        </div>
      </div>
    </div>

  <div class="university-info">
    <h4>College/University Info</h4>
    <div class="row">
      <div class="form-group  col-sm-12">
          <label>What school do you attend? </label>
        <select class="form-control" id="worker_attend" name="worker_attend"> 
           <option>Select a School</option>
            @foreach ($worker_attend as $worker_attends)
            <option value="{{ $worker_attends->name }}" {{ ($user_attend ==  $worker_attends->name ) ? 'selected' : '' }}>{{ $worker_attends->name }}</option>
          @endforeach
          </select>
        </div>
        <div class="form-group">
        <div class="col-md-6" style="overflow: visible !important">
          <label>Your school email address (.edu) </label>
          <input type="email" name="worker_school" placeholder="youremail@university.edu" class="form-control" value="{{ $user_worker_school }}">
        </div>
        <div class="col-md-6">
          <label>What are you studying? </label>
          <input type="text" class="form-control" name="worker_qualification" id="worker_qualification" value="{{ $user_qualification }}">
        <!--<select class="form-control" id="worker_qualification" name="worker_qualification"> 
          <option>Select Your Primary Major</option>
            @foreach ($worker_qualification as $worker_qualifications)
            <option value="{{ $worker_qualifications->id }}" {{ ( $user_qualification ==  $worker_qualifications->id ) ? 'selected' : '' }}>{{ $worker_qualifications->name }}</option>
          @endforeach
          </select> -->
        </div>
      </div>
    </div>
  </div>
<div class="pers-info">
  <h4>Personal Info</h4>
  <?php 
  $user_dob = Auth::user()->dob; 
  $user_phone = Auth::user()->phone; 
  $user_gender = Auth::user()->gender; 
  $user_worker_crime = Auth::user()->worker_crime; 
  $user_address = Auth::user()->address; 
  $user_address1 = Auth::user()->address1; 
  $user_city = Auth::user()->city; 
  $user_state = Auth::user()->state; 
  $user_zip_code = Auth::user()->zip_code; 
  $user_worker_college = Auth::user()->worker_college; 
  ?>
  <div class="row">
    <div class="col-md-6">
      <label>Birthday</label>
      <div class="date">

        <div class="input-group input-append date" id="datePicker">
             <input type="text" class="form-control" name="dob"  value="{{ $user_dob }}"/>
           <span class="input-group-addon add-on"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></span> 
        </div>
    </div>
    </div>
  
  <div class="col-md-6">
    <label>Phone Number</label>
    <input type="tel" name="phone" placeholder="Phone Number" class="form-control" value="{{ $user_phone }}">
  </div>
   <div class="col-md-12 gender-input">
    <label class="head-gen">Gender</label>
    <label class="radio-inline">
    <input type="radio" name="gender" checked="checked" value="Male" <?php if($user_gender=="Male"){ echo "checked";}?>> Male
    </label>
     <label class="radio-inline">
    <input type="radio" name="gender" value="Female" <?php if($user_gender=="Female"){ echo "checked";}?>> Female
    </label>
  </div>
   <div class="col-md-12 gender-input">
    <label class="head-gen">Have you ever been convicted of a crime?</label>
    <label class="radio-inline">
      <input type="radio" name="worker_crime" value="Yes" <?php if ($user_worker_crime=="Yes"){echo "checked";} ?>>Yes
    </label>
     <label class="radio-inline">
     <input type="radio" name="worker_crime" checked="checked" value="No" <?php if ($user_worker_crime=="No"){echo "checked";}?>> No
     </label>
  </div>
  <div class="col-md-6">
    <label>Address 1</label>
    <input type="text" name="address" placeholder="Address" class="form-control" value="{{ $user_address }}">
  </div>
  <div class="col-md-6">
    <label>Address 2</label>
    <input type="text" name="address1" placeholder="Address" class="form-control" value="{{ $user_address1 }}">
  </div>
  <div class="col-md-5">
    <label>City</label>
    <input type="text" name="city" placeholder="City" class="form-control"  value="{{ $user_city }}"> 
  </div>
  <div class="col-md-5">
    <label>State</label>
    <select class="form-control" id="state" name="state"> 
      <option>Select Your State</option>
          @foreach ($state as $states)
          <option value="{{ $states->id }}" {{ ($user_state ==  $states->id ) ? 'selected' : '' }}>{{ $states->name }}</option>
        @endforeach
        </select>
  </div>
  <div class="col-md-2">
    <label>Zip code</label>
    <input type="text" name="zip_code" placeholder="Zip code" class="form-control" value="{{ $user_zip_code }}">
  </div>
   <div class="form-group col-md-6">
    <label>How'd you hear about CollegeWRK?</label>
    <select class="form-control" id="worker_college" name="worker_college"> 
         <option>Select One</option>>
          @foreach ($worker_college as $worker_colleges)
          <option value="{{ $worker_colleges->id }}" {{ ($user_worker_college ==  $worker_colleges->id ) ? 'selected' : '' }}>{{ $worker_colleges->name }}</option>
        @endforeach
        </select>
  </div>
  <div class="form-group col-md-6 refer-code">
    <label>Do you have a Referral Code?</label>
     <input type="text" name="" placeholder="" class="form-control"><button class="apl-btn">Apply</button>
  </div>
</div>
</div>
<div class="col-md-12">
 <button type="submit" class="updt-btn" id="save_info_work">Save Information</button>
</div>
</form>
</div>
<div class="tab-pane fade" id="tab2default">
  
  <div class="panel with-nav-tabs perf-content">
    <div class="panel-heading">
            <ul class="nav nav-tabs" id="mytabs" role="tablist">
                <li class="active">
                <a href="#tab1primary" data-toggle="tab" role="tab">Campuses</a></li>
                <li><a href="#tab2primary" data-toggle="tab" role="tab">Categories</a></li>
                <li><a href="#tab3primary" data-toggle="tab" role="tab">Equipment</a></li>
                <li><a href="#tab4primary" data-toggle="tab" role="tab">Notifications</a></li>
             </ul>
    </div>
  <div class="panel-body">
      <div class="tab-content">
          <div class="tab-pane fade in active" id="tab1primary">
            <div class="travel-bonus">
              <div class="row">
                <div class="col-md-12">
                  <h3>Become a WRKer</h3>
                  <p>Complete your CollegeWRK profile to become an active WRKer and work the jobs you want.</p>
                  <h3>Travel Bonuses</h3>
                  <p>You are responsible for your own transportation to and from jobs.</p>

                  <p>Travel bonuses are only paid for jobs > 10 miles from the nearest campus and are clearly displayed on jobs. Note, that travel bonuses are not based on where you are coming from, rather where the job address is.</p>

                  <p>If you work a job that has unexpected or a lot of travel involved, please communicate with the customer proactively about adding travel cost as an expense in your timesheet.</p>
                  <h4>Where do you want to work?</h4>
                  <p>Select all campuses you may want to work. You will receive notifications based on these preferences and can edit them at any point.</p>

               <form class="jobpre" id="jobpre" action=""  enctype="multipart/form-data">
                 {{ csrf_field() }}
                  <div class="form-group">
                  <select class="form-control selectpicker locationSelect" name="location[]" required="required" multiple="true"  id="location">
                    <option>Asheville</option>
                    <option>Boone</option>
                    <option>Chapel Hill</option>
                    <option>Charlotte</option>
                    <option>Greensboro</option>
                    <option>Greenville</option>
                    <option>Raleigh</option>
                    <option>Wilmington</option>
                    <option>Durham</option>
                    <option>Virtual</option>
                    </select>
                  <button class="sve-btn" type="button" id="save_new">Save</button>
                  <button class="sve-btn save_new1" type="button"  id="changetabbutton">Continue</button>
                   </div>
                </form>
                 </div>
              </div>
            </div>
         
          </div>
          <div class="tab-pane fade" id="tab2primary">
            <div class="travel-bonus catg-pro">
              <div class="row">
                <div class="col-md-12">
                  <h4>What kind of jobs do you want to work?</h4>
                  <p>Select all the categories you may want to work and include a brief summary of your experience. You will receive notifications based on these preferences and can edit them at any point. Jobs are awarded based on these responses and they are visible on your public profile, so be concise, candid, and compelling.</p>

  @foreach ($categories as $category)
  
  <?php
    $user_id = Auth::user()->id;
    $cat_id  = $category->id;
    $workerexperience = DB::table('worker_categories_exprience')
                        ->where(['user_id' => $user_id,'cat_id' => $cat_id,'status' => '1','deleted'=>'0'])
                        ->first();
  ?>

  <?php if($workerexperience !=''){ ?>
  <div class="event-box">
    <h2 class="btnleft-head">{{$category->cat_name}}</h2>
      <div class="checkbox checkbox-toggle">
        <label>
          <input data-toggle="toggle" type="checkbox" class="chk_toogle_{{$category->id}}" onchange="valueChanged_{{$category->id}}()" checked="checked">
        </label>
      </div>
    <h6>{{$category->cat_rate}}</h6>
    <p>* {{$category->cat_description}}</p>
    <div id="event_form_{{$category->id}}">
    <form class="cat_form" action="#" method="" id="exp_form_{{$category->id}}">

          <input type="hidden" name="categoryid" value="{{$category->id}}" id="categoryid_{{$category->id}}">
          <input type="hidden" name="user_id" value="{{Auth::user()->id}}" id="user_id_{{$category->id}}" >

    <div class="row">
      <div class="form-group col-md-12">
        <label class="price-radio">What level of experience do you have?</label>
        <label class="radio-inline"><input type="radio" name="experience" class="experience" <?php if($workerexperience->experience =="none") {echo "checked";}?> value="none">None</label>
         <label class="radio-inline"><input type="radio" name="experience" class="experience" <?php if($workerexperience->experience =="some") {echo "checked";}?> value="some">Some</label>
         <label class="radio-inline"><input type="radio" name="experience" class="experience" <?php if($workerexperience->experience =="professional") {echo "checked";}?> value="professional">Professional</label>
      </div>
      <div class="form-group col-md-12">
        <label>Summarize your skills, experience, and interest in {{$category->cat_name}} jobs (10 - 110 characters).</label>
        <textarea class="form-control" name="additionalinfo" id="additionalinfo_{{$category->id}}" rows="5">{{$workerexperience->additional_info}}</textarea>

      </div>
      <div class="col-md-12">
       <button type="button" class="updt-btn" id="save_info_work_{{$category->id}}">Save Information</button>
      </div>
    </div>
    </form>
  </div>
  </div>

    <script type="text/javascript">
      function valueChanged_{{$category->id}}(){
        
        if($('.chk_toogle_{{$category->id}}').is(":unchecked")){   
            
          $("#event_form_{{$category->id}}").hide();
            var user_id            = jQuery('#user_id_{{$category->id}}').val();
            var catid              = jQuery('#categoryid_{{$category->id}}').val();
            var status             = '0';
            var deleted            = '1';          
            jQuery.ajax({
            method : 'GET',
            url  : "{{url('/updtworkexp')}}",
            data:{ 'user_id': user_id, 'catid': catid,'status': status ,'deleted': deleted },
            success :  function(resp) {
             /* alert(resp);*/
                          }
                    });
         }else{
            
            $("#event_form_{{$category->id}}").show();
            var user_id            = jQuery('#user_id_{{$category->id}}').val();
            var catid              = jQuery('#categoryid_{{$category->id}}').val();
            var status             = '1';
            var deleted            = '0';          
            jQuery.ajax({
            method : 'GET',
            url  : "{{url('/updtworkexp')}}",
            data:{ 'user_id': user_id, 'catid': catid,'status': status ,'deleted': deleted },
            success :  function(resp) {
              /*alert(resp);*/
                          }
                    });

         }
      }
    </script>

<?php }else{ ?>
    <div class="event-box">
    <h2 class="btnleft-head">{{$category->cat_name}}</h2>
      <div class="checkbox checkbox-toggle">
        <label>
          <input data-toggle="toggle" type="checkbox" class="chk_toogle_{{$category->id}}" onchange="valueChanged_{{$category->id}}()">
        </label>
      </div>
    <h6>{{$category->cat_rate}}</h6>
    <p>* {{$category->cat_description}}</p>
    <div id="event_form_{{$category->id}}" style="display: none;">
    <form class="cat_form" action="#" method="" id="exp_form_{{$category->id}}">

          <input type="hidden" name="categoryid" value="{{$category->id}}" id="categoryid_{{$category->id}}">
          <input type="hidden" name="user_id" value="{{Auth::user()->id}}" id="user_id_{{$category->id}}" >

    <div class="row">
      <div class="form-group col-md-12">
        <label class="price-radio">What level of experience do you have?</label>
        <label class="radio-inline"><input type="radio" name="experience" class="experience" value="none">None</label>
         <label class="radio-inline"><input type="radio" name="experience" class="experience" value="some">Some</label>
         <label class="radio-inline"><input type="radio" name="experience" class="experience" value="professional">Professional</label>

      </div>
      <div class="form-group col-md-12">
        <label>Summarize your skills, experience, and interest in {{$category->cat_name}} jobs (10 - 110 characters).</label>
        <textarea class="form-control" name="additionalinfo" id="additionalinfo_{{$category->id}}" rows="5"></textarea>

      </div>
      <div class="col-md-12">
       <button type="button" class="updt-btn" id="save_info_work_{{$category->id}}">Create Endoresement</button>
      </div>
    </div>
    </form>
  </div>
  </div>
    <script type="text/javascript">
      function valueChanged_{{$category->id}}(){
        if($('.chk_toogle_{{$category->id}}').is(":checked"))   
            $("#event_form_{{$category->id}}").show();
        else
            $("#event_form_{{$category->id}}").hide();
          }
    </script>

   <?php } ?>

    <script >
     jQuery(document).ready(function(){

       var exp_form = jQuery("#exp_form_{{$category->id}}").validate({
        rules: {
                experience: {
                    required: true,
                  },
                additionalinfo: {
                    required: true,
                }
               },
        messages: {
              experience: {
                  required: "Please select one."
                },
              additionalinfo: {
                 required: "Please enter about your experience."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });
      jQuery("#save_info_work_{{$category->id}}").click(function(){ 
         if (exp_form.form()) {

         var user_id            = jQuery('#user_id_{{$category->id}}').val();
         var catid              = jQuery('#categoryid_{{$category->id}}').val();
         var experience         = jQuery('.experience:checked').val();
         var additionalinfo     = jQuery('#additionalinfo_{{$category->id}}').val();
         jQuery.ajax({
          method : 'GET',
          url  : "{{url('/workerexperience')}}",
          data:{ 'user_id': user_id, 'catid': catid,'experience': experience ,'additionalinfo': additionalinfo },
          success :  function(resp) {
                        }
                    });
                  }
              });
    });
    </script>

    @endforeach
         <div class="cont-btn"><button class="sve-btn" id="changetabbutton_sec">Continue</button></div>
       </div>
    </div>

  </div>
</div>
          <div class="tab-pane fade" id="tab3primary"> 
            <div class="travel-bonus equipment_category">
              <div class="row">
                <div class="col-md-12">
                  <h4>Equipment Bonuses</h4>
                  <p>Equipment bonuses are only paid when they are specifically requested on jobs and you are awarded the equipment.
                  When competing on a job with an equipment bonus you can indicate whether you can provide the equipment, and if you are willing to work without the bonus.</p>
                  <p>Competing for equipment does not guarantee you will be awarded it. You must be awarded the job and the equipment to receive the bonus. Equipment bonuses can add up, from $7-$40+ per item, and having equipment will make you more likely to be awarded jobs. Thus, some Sweepers invest in equipment just to receive the bonuses!</p>
                  <h3>Do you have any equipment?</h3>
                  <p>Equipment bonuses are only paid when they are specifically requested on jobs and you are awarded the equipment.
                    When competing on a job with an equipment bonus you can indicate whether you can provide the equipment, and if you are willing to work without the bonus.</p>
                    <a href="#"><h3 style="text-align: center;">Skip this step >></h3></a>
                    @foreach ($equipment as $equipments)
                        <?php
                          $user_id = Auth::user()->id;
                          $equ_id  = $equipments->id;
                          $workerequipments = DB::table('worker_equipment')
                                              ->where(['user_id' => $user_id,'equ_id' => $equ_id,'status' => '1','deleted'=>'0'])
                                              ->first();
                          if($workerequipments !=''){ ?>

                      <div class="event-box">
                          <h2 class="btnleft-head">{{$equipments->equ_name}}</h2>
                           <div class="checkbox checkbox-toggle">
                                <label>
                                  <input data-toggle="toggle" type="checkbox" class="chk_toogle_equ_{{$equipments->id}}" onchange="valueChanged_equ_{{$equipments->id}}()" checked="checked">
                                </label>
                              </div>
                          <h6>{{$equipments->equ_price}}</h6>
                          <p>{{$equipments->equ_description}}</p>
                      <div id="event_form_equ_{{$equipments->id}}">
                      <form class="equ_form" action="#" method="" id="equ_form_{{$equipments->id}}">
                         <input type="hidden" name="equ_id" value="{{$equipments->id}}" id="equ_id_{{$equipments->id}}">
                         <input type="hidden" name="user_id" value="{{Auth::user()->id}}" id="user_id_{{$equipments->id}}" >
                      <div class="row">
                        <div class="form-group col-md-12">
                          <label>Please describe your {{$equipments->equ_name}} (10 - 30 characters).</label>
                          <textarea class="form-control" name="equlinfo" id="equlinfo_{{$equipments->id}}" rows="5">{{$workerequipments->description}}</textarea>
                        </div>
                        <div class="col-md-12">
                         <button type="button" class="updt-btn" id="save_info_equ_{{$equipments->id}}">Create Equipment</button>
                        </div>
                      </div>
                      </form>
                    </div>
                       </div>

          <script type="text/javascript">
          function valueChanged_equ_{{$equipments->id}}(){
        
         if($('.chk_toogle_equ_{{$equipments->id}}').is(":unchecked")){   
            
          $("#event_form_equ_{{$equipments->id}}").hide();
            var user_id            = jQuery('#user_id_{{$equipments->id}}').val();
            var equ_id              = jQuery('#equ_id_{{$equipments->id}}').val();
            var status             = '0';
            var deleted            = '1';          
            jQuery.ajax({
            method : 'GET',
            url  : "{{url('/updtworkequ')}}",
            data:{ 'user_id': user_id, 'equ_id': equ_id,'status': status ,'deleted': deleted },
            success :  function(resp){
             /*alert(resp);*/
                          }
                    });
             }else{
            $("#event_form_equ_{{$equipments->id}}").show();
            var user_id            = jQuery('#user_id_{{$equipments->id}}').val();
            var equ_id              = jQuery('#equ_id_{{$equipments->id}}').val();
            var status             = '1';
            var deleted            = '0';          
            jQuery.ajax({
            method : 'GET',
             url  : "{{url('/updtworkequ')}}",
             data:{ 'user_id': user_id, 'equ_id': equ_id,'status': status ,'deleted': deleted },
             success :  function(resp) {
              /*alert(resp);*/
                          }
                    });

                   }
                }
              </script>
            <?php } else { ?>
             <div class="event-box">
                     <h2 class="btnleft-head">{{$equipments->equ_name}}</h2>
                     <div class="checkbox checkbox-toggle">
                          <label>
                            <input data-toggle="toggle" type="checkbox" class="chk_toogle_equ_{{$equipments->id}}" onchange="valueChanged_equ_{{$equipments->id}}()">
                          </label>
                        </div>
                    <h6>{{$equipments->equ_price}}</h6>
                    <p>{{$equipments->equ_description}}</p>
                      <div id="event_form_equ_{{$equipments->id}}" style="display: none;">
                      <form class="equ_form" action="#" method="" id="equ_form_{{$equipments->id}}">

                        <input type="hidden" name="equ_id" value="{{$equipments->id}}" id="equ_id_{{$equipments->id}}">
                         <input type="hidden" name="user_id" value="{{Auth::user()->id}}" id="user_id_{{$equipments->id}}" >
                      <div class="row">
                        <div class="form-group col-md-12">
                          <label>Please describe your {{$equipments->equ_name}} (10 - 30 characters).</label>
                          <textarea class="form-control" name="equlinfo" id="equlinfo_{{$equipments->id}}" rows="5"></textarea>
                        </div>
                        <div class="col-md-12">
                         <button type="button" class="updt-btn" id="save_info_equ_{{$equipments->id}}">Create Equipment</button>
                        </div>
                      </div>
                      </form>
                    </div>
                       </div>
                     <script type="text/javascript">
                      function valueChanged_equ_{{$equipments->id}}(){
                        if($('.chk_toogle_equ_{{$equipments->id}}').is(":checked"))   
                            $("#event_form_equ_{{$equipments->id}}").show();
                        else
                            $("#event_form_equ_{{$equipments->id}}").hide();
                          }
                    </script>

                   <?php } ?>
     <script>
      jQuery(document).ready(function(){
       var equ_form = jQuery("#equ_form_{{$equipments->id}}").validate({
        rules: {
                equlinfo: {
                    required: true
                }
               },
        messages: {
                equlinfo: {
                 required: "Please describe."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });
      jQuery("#save_info_equ_{{$equipments->id}}").click(function(){ 
        if (equ_form.form()) {
         var user_id            = jQuery('#user_id_{{$equipments->id}}').val();
         var equ_id             = jQuery('#equ_id_{{$equipments->id}}').val();
         var equlinfo           = jQuery('#equlinfo_{{$equipments->id}}').val();

         jQuery.ajax({
          method : 'GET',
          url  : "{{url('/workerequipment')}}",
          data:{ 'user_id': user_id, 'equ_id': equ_id,'equlinfo': equlinfo },
          success :  function(resp) {
             
                        }
                    });
                  }
              });
    });
    </script>
                  @endforeach

                 <div class="cont-btn"><button class="sve-btn" id="changetabbutton_new">Continue</button></div>
                </div>
              </div>

            </div>
          </div>
          <div class="tab-pane fade" id="tab4primary">
            <div class="travel-bonus equipment_category">
              <div class="row">
                <div class="col-md-12">
                  <h4>Become a WRKer</h4>
                  <p>Complete your CollegeWRK Profile to become an active WRKer and work the jobs you want.</p>
                    <h3>Manage your Messages</h3>
                  <p>Adjust your CollegeWRK notifications to work for you.</p>
                    

                  <div class="event-box">
                    <h2 class="btnleft-head">Important Notifications</h2>
                     
                    <p style="margin-top: 15px;">Job reminders and notifications cannot be turned off, and we may message you with questions, updates, and about potential jobs.</p>
                    <h2 class="btnleft-head">New Job Alert</h2>
                     
                    <p style="margin-top: 15px;"><strong>Recommended :</strong> Get notified immediately when jobs are posted that match your campus and category preferences.</p>
                    <div class="row">
                    <div class="col-md-12">
                    <div class="btn-group fl-width" id="status" data-toggle="buttons">
                      <label class="btn btn-default btn-on btn-lg active">
                      <input type="radio" value="1" name="multifeatured_module[module_id][status]" checked="checked">Yes</label>
                      <label class="btn btn-default btn-off btn-lg ">
                      <input type="radio" value="0" name="multifeatured_module[module_id][status]">No</label>
                    </div>
                    <span class="tgl-btn">text</span>
                  </div>
                  </div>
                  <div class="row">
                  <div class="col-md-12">
                    <div class="btn-group fl-width" id="status" data-toggle="buttons">
                      <label class="btn btn-default btn-on btn-lg active">
                      <input type="radio" value="1" name="multifeatured_module[module_id][status]" checked="checked">Yes</label>
                      <label class="btn btn-default btn-off btn-lg ">
                      <input type="radio" value="0" name="multifeatured_module[module_id][status]">No</label>
                    </div>
                     <span class="tgl-btn">Email</span>
                   </div>
                 </div>
                    <!-- <h2 class="btnleft-head">Daily Digests</h2>
                     <p>Receive a (nearly) daily email summary of open jobs and updates.</p>
                     <div class="row">
                      <div class="col-md-12">
                     <div class="btn-group fl-width" id="status" data-toggle="buttons">
                      <label class="btn btn-default btn-on btn-lg active">
                      <input type="radio" value="1" name="multifeatured_module[module_id][status]" checked="checked">Yes</label>
                      <label class="btn btn-default btn-off btn-lg ">
                      <input type="radio" value="0" name="multifeatured_module[module_id][status]">No</label>
                     </div>
                     <span class="tgl-btn">Email</span>
                   </div> -->
                    </div>
                    </div>
                  <div class="cont-btn"><a href="#"><button>Continue</button></a></div>
              </div>
              </div>

            </div>
          </div>
      </div>
  </div>
</div>
</div>
                  <div class="tab-pane fade" id="tab3default"></div>
              </div>
          </div>
      </div>
        </div>
      </div>
    </div>
  </div>
    
   
  </div>
  <div class="tab-pane text-style" id="tab3">
  
  </div>

</div>
</div>
</div>
</div>
</section>
<script type="text/javascript">
  
  function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#profileimg').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#image").change(function() {
  readURL(this);
});
</script>
<script>
  $(document).ready(function () {

    var mySelect = $('#first-disabled2');

    $('#special').on('click', function () {
      mySelect.find('option:selected').prop('disabled', true);
      mySelect.selectpicker('refresh');
    });

    $('#special2').on('click', function () {
      mySelect.find('option:disabled').prop('disabled', false);
      mySelect.selectpicker('refresh');
    });

    $('#basic2').selectpicker({
      liveSearch: true,
      maxOptions: 1
    });
  });
</script>

<script>
 $(document).ready( function() {
    $("#profileFrm").validate({
        rules: {
                worker_slogan: {
                    required: true,
                    minlength: 2,
                    maxlength: 100
                   },
                worker_about: {
                    required: true,
                    minlength: 2,
                    maxlength: 500
                   },
                 worker_attend: {
                    required: true
                   },
                 worker_school: {
                    required: true,
                    email: true
                   },
                  worker_qualification: {
                    required: true
                   },
                   phone: {
                    required: true,
                    number: true,
                    minlength: 10,
                    maxlength: 12
                   },
                   gender: {
                    required: true
                   },
                   address: {
                    required: true
                   },
                   city: {
                    required: true
                   },
                   state : {
                    required: true
                   },
                   zip_code : {
                    required: true
                   }
                },
        messages: {
                worker_slogan: {
                  required: "This is required field.", 
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 100 characters allowed."
                   },
                  worker_about: {
                  required: "This is required field.", 
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 500 characters allowed."
                   },
                  worker_attend: {
                  required: "This is required field."
                   },
                  worker_school: { 
                  required: "This field is required.",
                  email: "Enter valid email address."
                  },
                  worker_qualification: {
                  required: "This is required field."
                   },
                  phone: {
                  required: "This field is required.",
                  number: "Enter only number",
                  maxlength: "Maximum 14 numbers are allowed."
                   },
                  gender: {
                  required: "This is required field."
                   },
                  address: {
                  required: "This is required field."
                   },
                  city: {
                  required: "This is required field."
                   },
                  state: {
                  required: "This is required field."
                   },
                   zip_code: {
                  required: "This is required field."
                   }
                },
        submitHandler: function(form) {
            form.submit();
          }
        });

     // $("#jobpre").validate({
     //    rules: {
     //            location: {
     //                required: true
     //               }
     //            },
     //    messages: {
     //            location: {
     //              required: "Please let us know where you would like to work.."
     //               }
     //            },
     //       submitHandler: function(form) {
     //        form.submit();
     //      }
     //    });

  $('.save_new1').hide();
  $('#save_new').click(function(){
    var location       = jQuery('#location').val();
   // var location = $('#location').find("option:selected").val();
    //alert( location );
     $.ajax({
              url: "{{url('/jobpreference')}}",
              type: "get",
              data: {'location': location},
               success: function(response){
               //alert(response); 
               if( response == 'true' ){
                  alert('Location updated');
               }else{
                  alert('Something went wrong. Please try again.');
               }
               $('#save_new').hide();
               $('.save_new1').show();
               
        }
    });
  });
 });
</script>
<script>
$(document).ready(function() {
  $('.lower-profile').hide();
  $('.updt-btn').click(function(){
    $('.lower-profile').show();
    $('.uper_sec').hide();
  });
    $('#datePicker')
        .datepicker({
            format: 'yyyy-mm-dd'
        })
        .on('changeDate', function(e) {
            // Revalidate the date field
            $('#eventForm').formValidation('revalidateField', 'date');
        });

    $('#eventForm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'The name is required'
                    }
                }
            },
            date: {
                validators: {
                    notEmpty: {
                        message: 'The date is required'
                    },
                    date: {
                        format: 'MM/DD/YYYY',
                        message: 'The date is not a valid'
                    }
                }
            }
        }
    });
});
</script>
<script>
    $(document).ready(function() {

     $('#browsebtn').on('click', function() {
             $('#image').click();
       });

    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img style="margin:0 10px;width:150px;height:150px;">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };
    $('#image').on('change', function() {
         jQuery('#nofile').empty(); 
          jQuery('#browse_img').empty(); 
        imagesPreview(this, '#browse_img');
      });
  
  });
</script>

<script type="text/javascript">
  $('#changetabbutton').click(function(e) {
  e.preventDefault();
  var link = $('#mytabs .active').next().children('a').attr('href');

  $('#mytabs a[href="' + link + '"]').tab('show');
});
$('#changetabbutton_sec').click(function(e) {
  e.preventDefault();
  var link = $('#mytabs .active').next().children('a').attr('href');

  $('#mytabs a[href="' + link + '"]').tab('show');
});
$('#changetabbutton_new').click(function(e) {
  e.preventDefault();
  var link = $('#mytabs .active').next().children('a').attr('href');

  $('#mytabs a[href="' + link + '"]').tab('show');
});
</script>
<script type="text/javascript">
  
  function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#profileimg').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#imgInp").change(function() {
  readURL(this);
});
</script>

 <script>
jQuery(document).ready(function() {
  $.validator.addMethod("alphaLetter", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });
 
 $("#profileform").validate({
        rules: {
                firstname: {
                    required: true,
                    alphaLetter:true,
                    minlength: 2,
                    maxlength: 60
                },
                lastname: {
                    required: true,
                    alphaLetter: true,
                    minlength: 2,
                    maxlength: 60
                },
                phone: {
                    required: true,
                    minlength: 10,
                    maxlength: 12,
                    number:true
                }   
            },
        messages: {
              firstname: {
                  required: "Please enter your firstname.",
                  alphaLetter:"Only letters are allowed",
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 60 characters allowed."
                },
              lastname: {
                  required: "Please enter your lastname.",
                  alphaLetter:"Only letters are allowed",
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 60 characters allowed."
                },
              phone: {
                  required: "Please enter phone number.",
                  minlength: "Minimum 10 characters required.",
                  maxlength: "Maximum 12 characters allowed.",
                  number: "Please enter only number."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
    });

  });
</script>
<?php if( !empty($workerjobpreference) ):?>
<script>
  $( window ).load(function() {
    var location = '<?php echo $workerjobpreference->location; ?>';
    var locationArray = location.split(',');
    //$('#$location').val( locationArray );
    $('.selectpicker').selectpicker();
    $('.locationSelect').selectpicker('val', locationArray);
  })
</script>
<?php endif; ?> 


<style>
.tooltip-inner {margin-top:8px;}
</style>
<style type="text/css">
  #image {
    color: #fff;
}
.datepicker-dropdown.datepicker-orient-left {
    left: 400px !important;
}
.rating_sec {
    box-shadow: 0 0 11px #ccc;
    padding: 5px 10px 5px 10px;
    border-radius: 4px;
    font-size: 13px;
    vertical-align: middle;
}
.rating_sec img {
    width: 13px;
    vertical-align: middle;
    margin-top: -3px;
}
.custom-width-done {
  /*  width: 33.33%; */
    width: 50%;
    text-align: center;
    border-left: 1px solid #e4e4e4;
}
.custom-width-done a:hover {
text-decoration: none;
    background-color: #1aa4df!important;
    color: #fff!important;
  }
/*  img#profileimg {
    border: 5px solid #fff;
    box-shadow: 0px 0px 20px #000000;
}*/
.rating_logo_sec img {
    width: 125px;
}
.worker_per_hour_rate {
    font-size: 20px !important;
}
input {text-transform: capitalize;}
input[type="email"] {text-transform: lowercase;}
/*.custom-width-done:hover {
    text-decoration: none;
    background-color: #1aa4df!important;
    color: #fff!important;
}*/
.main-profile .tp-rw-col1 {
    float: left;
}
@media screen and (max-width:700px){
.main-profile ul.nav-tabs li a {font-size:12px;}
}

#jobpre .dropdown-menu{
  left: 0px !important;
  
}

#jobpre .form-group{
  overflow: inherit !important;
}
#jobpre .glyphicon-ok:before{
    content: "\f00c";
    display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
}
</style>

@stop