   <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./"><img src="http://iebasketball.com/collegeworker/public/admin/images/newlogo.png" alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src="http://iebasketball.com/collegeworker/public/admin/images/logo2.png" alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <div class="media p-3">
                            <?php
                           $image = Auth::user()->image;
                            if($image=='') {
                            ?>
						    <img src="{{ url('/public') }}/admin/images/admin.jpg" alt="John Doe" class="mr-3 rounded-circle" style="width:60px;">
                            <?php
                        }
                        else {
                            ?>
                            <img src="{{ url('/public') }}/uploads/profile/{{ $image }}" alt="John Doe" class="mr-3 rounded-circle" style="width:60px;">
                            <?php
                        }
                            ?>
						    <div class="media-body">
							    <p style="margin-bottom: 0px;""><a href="index.html" style="color: #25a6dc;font-size: 1.0625rem;"><?php echo Auth::user()->name; ?> </a></p>
							    <p><a href="#" style="font-size: 0.9rem;color:#ffffff;">Admin</a></p>  
						    </div>
						</div>
                    </li>
                    <h3 class="men-title">Menus</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown active">
                        <a href="http://iebasketball.com/collegeworker/admindashboard"> <i class="fa fa-home pr-2"></i>Dashboard</a>
                       </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-cube pr-2"></i>Packages<span class="badge bad_bac">3</span></a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="packages.html">Manage Packages</a></li>   
                            <li><i class="fa fa-table"></i><a href="tables-data.html">Data Table</a></li>
                        </ul>   
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="http://iebasketball.com/collegeworker/admincustomer" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-camera-retro pr-2"></i>Customers<span class="badge bad_bac2">9</span></a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table pr-2"></i><a href="customers.html">Manage Customers</a></li>
                            <li><i class="fa fa-table pr-2"></i><a href="tables-data.html">Data Table</a></li>
                        </ul>
                    </li>  
                        <li class="menu-item-has-children dropdown">
                        <a href="http://iebasketball.com/collegeworker/adminworker" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-briefcase pr-2"></i>Workers<span class="badge bad_bac3">4</span></a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="workers.html">Manage Workers</a></li>
                            <li><i class="fa fa-table"></i><a href="tables-data.html">Data Table</a></li>
                        </ul>
                    </li>
                  
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-user pr-2"></i>Payment Management</a>
                       <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="tables-basic.html">Basic Table</a></li>
                            <li><i class="fa fa-table"></i><a href="tables-data.html">Data Table</a></li>
                        </ul>
                    </li>

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-briefcase pr-2"></i></i>Job Requests</a>   
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="tables-basic.html">Basic Table</a></li>
                            <li><i class="fa fa-table"></i><a href="tables-data.html">Data Table</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-envelope pr-2"></i>Email Verification<span class="badge bad_bac4">8</span></a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="tables-basic.html">Basic Table</a></li>
                            <li><i class="fa fa-table"></i><a href="tables-data.html">Data Table</a></li>
                        </ul>
                    </li>
                    
                    <h3 class="menu-title"> Settings-pages</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-window-close pr-2"></i>Settings</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="tables-basic.html">Basic Table</a></li>
                            <li><i class="fa fa-table"></i><a href="tables-data.html">Data Table</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside>
  <style>
    .nav>li>a:focus, .nav>li>a:hover {
    text-decoration: none;
    background-color: #1e282c !important;
    outline: none;
}
</style>