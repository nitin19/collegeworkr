@extends('layouts.workerdefault')

@section('title', 'Worker Notification')

@section('content')
  <div class="col-md-9 dashboard-right">
           <div class="job-history">
            <div class="row pro-heading">
              <div class="col-sm-12">
                <div class="pro-head">
                <h3>Job Notification</h3>
              </div>
              </div>
            </div>
    <table class="table table-striped history-table">
    <thead>
    <tr> <th>Customer Name</th>
      <th >Name of Job</th>
      <th >Description</th>
      <th >Required Worker</th>
      <th>Action</th>

    </tr>
  </thead>
  <tbody>
    <?php $count=count($notification); ?>
    @if($count > '0')
    @foreach($notification as $notifiy)
    <?php 
    $user_id=$notifiy->client_userid;
    $client_jobid=$notifiy->client_jobid;
    $clientjobpost   = DB::table('clientjobpost')
                     ->where('id', '=', $client_jobid)
                     ->where('status', '=', 1)
                     ->where('deleted', '=', 0)
                     ->first();
            $catid=$clientjobpost->job_heading;
            $categories = DB::table('categories')
                     ->where('id', '=', $catid)
                     ->where('deleted', '=', 0)
                     ->first();
           $cat_name = $categories->cat_name; 
           $createdate = explode(' ', $clientjobpost->created_at);
           $client_payment = DB::table('client_payment')
                     ->where('user_id', '=', $user_id)
                     ->first();

    $user = DB::table('users')
                     ->where('id', '=', $user_id)
                     ->first();     
      ?>                  
    <tr>
      <td data-label="Name of Job">{{ $user->name }}</td>
      <td data-label="Name of Job">{{$cat_name}} in {{$clientjobpost->job_location}}</td>
      <td data-label="Description">{{$clientjobpost->job_description}}</td>
      <td data-label="Location">{{$clientjobpost->worker_required}}</td>
   
    <td>  <a href="{{ route('workernotification.show', $notifiy->id )}}"  class="btn btn-primary">View</a> 
     
</td>

    </tr>
    @endforeach
    @else
    <tr>
     <td>
        No job notification yet.
     </td>
  </tr>
  @endif

  </tbody>

</table>
     <div class="pagination_section">

                               {!! $notification->render() !!} 
                </div>
</div>

</div>
</div>
</section>
<!-- <script type="text/javascript">
  $(document).ready(function() {
    $("#preview_email").modal({
        show: false,
        backdrop: 'static'
    });
    
    $("#click-me").click(function() {
       $("#preview_email").modal("show");             
    });
});
</script>
  <style type="text/css">
          a.btn.btn-lg {
    background: #3399cc;
    color: #fff;
}#click-me {
    background: #337ab7;
    border: 1px solid #337ab7;
    padding: 8px 17px;
    border-radius: 5px;
    color: #fff;
}
        </style>
 -->
 @stop