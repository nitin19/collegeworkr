    <div id="right-panel" class="right-panel">
<?php
$message_count = DB::table('homes')->where('status','1')->where('deleted','0')->where('seen_msg','0')->count();
//$unseen_count = DB::table('notification')->where('status_read','0')->count();
$unseen_count = DB::table('new_notifications')->where('read_by_admin','0')->where('is_for_admin','1')->get();
?>
        <header id="header" class="header">

            <div class="header-menu">

                <div class="col-lg-6 col-md-6">
                    <h6>Dashboard</h6>
                </div>

                <div class="col-lg-5 col-md-4">
                    <div class="header-left float-right">



                       <div class="dropdown for-notification">
                          <a class="btn btn-secondary dropdown-toggle" id="notification" data-toggle="dropdown" role="button" aria-expanded="true">
                            <i class="fa fa-bell-o" aria-hidden="true"></i>
                            @if(count($unseen_count)>0)
                            <span class="count bg-danger">{{ count($unseen_count) }}</span>@endif
                          </a>

                          <ul class="dropdown-menu headerdropdown how dropdown-customer dropdownNotifications" role="menu" >
                           
                            @if(count($unseen_count) > 0)
                            <p class="text-right"><a href="{{url('/adminnotification')}}"> View all</a></p>
                            @foreach($unseen_count as $notify)
                                <li><a href="<?php if( $notify->notification_redirect_url !='' ): echo url($notify->notification_redirect_url);  endif;?>" class="" notify="{{$notify->notification_id}}" data-redirecturl="<?php if( $notify->notification_redirect_url !='' ): echo url($notify->notification_redirect_url);  endif;?>"><?php echo $notify->message; ?></a></li>
                            @endforeach
                            @else
                             <li>no new notification</li>
                            @endif
                          </ul>  
                             
                        </div>

                    <div class="dropdown for-message">
                        <a class="dropdown-toggle" id="notification" href="{{url('/adminmessages')}}"
                                id="message">
                          <i class="fa fa-envelope-o" aria-hidden="true"></i>
                          @if($message_count>0)
                            <span class="count bg-primary">
                            {{ $message_count }}</span>@endif
                          </a>
                           
                        </div>
                        
                    </div>
                </div>
                 <div class="col-lg-1 col-md-2">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">
            <?php echo $name = Auth::user()->name; ?>
            <span class="caret"></span>
            </a>
            <ul class="dropdown-menu headerdropdown how dropdown-customer" role="menu" >
            <li>
            <a href="{{ url('/admindashboard') }}" class="logount_hed_sec">Dashboard</a>
            </li>
            <li>
            <a href="{{ route('logout') }}" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();" class="logount_hed_sec">
            Logout </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
            </form>
            </li>
            </ul>                                            
            </div>
            </div>
<style type="text/css">


.dropdown-customer {
    padding: 5px 0px;
    background: #fff;
    -webkit-box-shadow: 0 0 20px 0 rgba(0,0,0,.3);
    box-shadow: 0 0 20px 0 rgba(0,0,0,.3);
    border-radius: 5px;
    border: none;
    opacity: 1;
    position: absolute;
    left: auto;
    right: 0px;
    visibility: visible;
    overflow: visible;
    padding: 0px 0;
    -webkit-transform: translateY(20px);
    transform: translateY(20px);
    width:100%;
}
.dropdown-customer:before {
    content: "";
    border: 10px solid transparent;
    border-left-color: #fff;
    border-top-color: #fff;
    position: absolute;
    border-radius: 3px;
    top: -8px;
    right: 15px;
    -webkit-box-shadow: -5px -5px 12px -2px rgba(0,0,0,.3);
    box-shadow: -4px -4px 12px -2px rgba(0,0,0,.3);
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg);
    z-index: 1002;
}
.dropdown-customer li {
    display: block;
    padding: 0px 0px;
    border-bottom: 1px solid #eee;
    border-radius: 5px;
    float: left;
    width: 100%;
}
.dropdown-customer li a {
    padding: 10px 15px;
    border-radius: 3px 3px 3px 3px;
    float: left;
    width: 100%;
    font-size: 14px;
}
.dropdown-customer li a:hover {
    background-color:#eee;
    font-size: 14px;
    color:#808080;
}
.dropdown-customer {
    left: -33px !important;
    top: 45px !important;
    border-radius: 5px !important;
}
@media screen and (max-width: 991px) and (min-width: 576px) {
    .dropdown-customer {left: -60px !important;}
}
@media (max-width: 575.99px){
aside.left-panel .navbar .navbar-brand {float: none !important;}
.navbar .navbar-nav li {padding: 0px 15px;}
li.menu-item-has-children.dropdown.active {padding: 0px 15px;}
aside.left-panel .navbar .navbar-brand {
    width: 100%;
    margin-top: 0px !important;
    text-align: center;
    padding: 5px 0px;
}
.dropdown-customer {left: -39px !important;}
}
/*.panel-body {height:auto !important;}*/
.dropdownNotifications:before{
    right: 70%;

}
.dropdownNotifications{
    top: 10px;
     padding: 5px;
     max-width: 500px;
    width: 205px;
}
.headerdropdown{
	max-height: 400px;
	overflow-y: auto;
}
</style>
<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>
<script>
jQuery(document).ready(function($){
	$('body').on('click','#notification', function(e){
		 jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/readallnotifications') }}",
            data :{ 'field_name' : 'read_by_admin', 'user_id' : 422 , 'user_field' : ''},
            success :  function(resp) {
                if(resp =='true'){
                 $('.count').html('0');
               }
             }
       });

	});

});
</script>

<script type="text/javascript">
jQuery(document).ready(function($) {
$(".notity").click(function(e){
  e.preventDefault();
    var noti = $(this).attr("notify");
    var redirecturl = $(this).data("redirecturl");
    jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/notitysec') }}",
            data :{'noti': noti, 'field_name' : 'read_by_admin'},
            success :  function(resp) {
                if(resp =='true'){
                  if( redirecturl !='' && redirecturl != undefined && redirecturl != null ){
                    window.location.href= redirecturl ;
                  }else{
                    window.location.href="{{url('/adminnotification')}}";
                  }
               }
             }
       });
});
});
</script>
        </header>