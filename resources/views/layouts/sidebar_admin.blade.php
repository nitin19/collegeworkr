   <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="{{ url('/admindashboard/') }}"><img src="{{url('/')}}/public/admin/images/newlogo.png" alt="Logo"></a>
                <a class="navbar-brand hidden" href="{{ url('/admindashboard/') }}"><img src="{{url('/')}}/public/admin/images/logo2.png" alt="Logo"></a>
            </div>  

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <div class="media p-3">
<?php
    $customer_count = DB::table('users')
                      ->where('user_role', 'customer')
                      ->where('status', '1')
                      ->where('deleted', '0')->count();
    $worker_count = DB::table('users')
                    ->where('user_role', 'worker')
                    ->where('status', '1')
                    ->where('deleted', '0')->count();                 
                           $image = Auth::user()->image;
                           $current_userid = Auth::user()->id;
                           if($image=='') {
                            ?>
						    <img src="{{ url('/public') }}/admin/images/admin.jpg" alt="John Doe" class="mr-2 rounded-circle" style="width:55px; height: 55px;object-fit: cover;">
                            <?php
                        }
                        else {
                            ?>
                            <img src="{{ url('/public') }}/uploads/profile/{{ $image }}" alt="John Doe" class="mr-2 rounded-circle" style="width:55px; height: 55px;object-fit: cover;">
                            <?php
                        }
                        ?>
						    <div class="media-body">
							    <p style="margin-bottom: 0px;""><a href="{{ url('/adminprofile') }}" style="color: #25a6dc;font-size: 1.0625rem;"><?php echo ucfirst(Auth::user()->name); ?> </a></p>
							    <p><a href="#" style="font-size: 0.9rem;color:#ffffff;">Admin</a></p>  
						    </div>
						</div>
                    </li>
                    <!-- <h3 class="men-title">Menus</h3> --><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown active">
                        <a href="{{ url('/') }}/admindashboard"> <i class="fa fa-home pr-2"></i>Dashboard</a>
                       </li>

                    <!-- <li class="menu-item-has-children dropdown">
                        <a href="{{ url('/clientpayment') }}" class="dropdown-toggle"> <i class="fa fa-user pr-2"></i>Client Payment</a>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="{{ url('/requestpayment') }}" class="dropdown-toggle"> <i class="fa fa-user pr-2"></i>Request Payment</a>
                    </li>  -->

                    
                    <!-- <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-cube pr-2"></i>Packages<span class="badge bad_bac">3</span></a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table pr-2"></i><a href="packages.html">Manage Packages</a></li>   
                            
                        </ul>   
                    </li> -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-user pr-2"></i>Payment Management</a>
                       <ul class="sub-menu children dropdown-menu">
                            <!-- <li><i class="fa fa-table pr-2"></i><a href="#">Basic Table</a></li> -->
                            <li class="menu-item-has-children dropdown">
                                <a href="{{ url('/pendingpayments') }}" class="dropdown-toggle"> <i class="fa fa-user pr-2"></i>Pending Payments</a>
                            </li>
                            <li class="menu-item-has-children dropdown">
                              <a href="{{ url('/requestpayment') }}" class="dropdown-toggle"> <i class="fa fa-user pr-2"></i>Request Payment</a>
                            </li> 
                            <li>
                                 <a href="{{ url('/adminallpayments') }}" class="dropdown-toggle"> <i class="fa fa-user pr-2"></i> Payments</a>
                            </li>


                            <!--<li class="menu-item-has-children dropdown">
                              <a href="{{ url('/clientpayment') }}" class="dropdown-toggle"> <i class="fa fa-user pr-2"></i>Client Payment</a>
                            </li> -->
                             
                        </ul>
                    </li>
            
                    <li class="menu-item-has-children dropdown">
                        <a href="{{ url('/') }}/admincustomer"  class="dropdown-toggle"> <i class="fa fa-camera-retro pr-2"></i>Customers<span class="badge bad_bac2"><?php echo $customer_count; ?></span></a>
                           <!--<ul class="sub-menu children dropdown-menu">
                            <li><a href="{{ url('/') }}/admincustomer"><i class="fa fa-table pr-2"></i>Manage Customers</a></li>
                            
                        </ul> -->
                    </li>  
                        <li class="menu-item-has-children dropdown">
                        <a href="{{ url('/') }}/adminworker"  class="dropdown-toggle"> <i class="fa fa-briefcase pr-2"></i>Workers<span class="badge bad_bac3"><?php echo $worker_count; ?></span></a>
                        <!--<ul class="sub-menu children dropdown-menu">
                            <li><a href="{{ url('/') }}/adminworker"><i class="fa fa-table  pr-2"></i>Manage Workers</a></li>
                            
                        </ul>-->
                    </li>
                  
                    
                    <li class="menu-item-has-children dropdown">
                        <a href="{{ url('/adminmessages') }}" class="dropdown-toggle"> <i class="fa fa-user pr-2"></i>Messages</a>
                    </li>
                    
                    <li class="menu-item-has-children dropdown">
                            <a href="{{ url('/adminjobs') }}" class="dropdown-toggle"> <i class="fa fa-user pr-2"></i>Jobs</a>
                    </li>  
                    
                    <li class="menu-item-has-children dropdown">
                        <a href="{{url('/adminhire')}}"> <i class="fa fa-address-card pr-2" aria-hidden="true"></i>Assign Job</a>
                      </li>
                    <!-- <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-briefcase pr-2"></i></i>Job Requests</a>   
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table pr-2"></i><a href="#">Basic Table</a></li>
                            
                        </ul>
                    </li> -->
                    <!-- <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-envelope pr-2"></i>Email Verification<span class="badge bad_bac4">8</span></a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table pr-2"></i><a href="#">Basic Table</a></li>
                            
                        </ul>
                    </li> -->
                    
                    <!-- <h3 class="menu-title"> Settings-pages</h3> --><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-window-close pr-2"></i>Settings</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><a href="{{ url('/adminbussiness')}}"><i class="fa fa-table pr-2"></i>Bussiness</a></li>
                            <li><a href="{{ url('/adminprofile') }}"><i class="fa fa-table pr-2"></i>Profile</a></li>
                        </ul>
                    </li>

                     <li class="menu-item-has-children dropdown">
                        <a href="{{ url('/support') }}" class="dropdown-toggle"> <i class="fa fa-comments-o pr-2"></i>Support</a>
                    </li>

                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside>
<style>
    .nav>li>a:focus, .nav>li>a:hover {
    text-decoration: none;
    background-color: #1e282c !important;
    outline: none;
}
</style>