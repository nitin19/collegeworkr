@extends('layouts.default')
@section('title', 'Home')
@section('content')
<script type="text/javascript">
  document.addEventListener('DOMContentLoaded',function(event){
  // array with texts to type in typewriter
  var dataText = [ "From the Classroom,to the Community",];
  
  // type one text in the typwriter
  // keeps calling itself until the text is finished
  function typeWriter(text, i, fnCallback) {
    // chekc if text isn't finished yet
    if (i < (text.length)) {
      // add next character to h1
     document.querySelector(".header-title").innerHTML = text.substring(0, i+1) +'<two aria-hidden="true"></two>';

      // wait for a while and call this function again for next character
      setTimeout(function() {
        typeWriter(text, i + 1, fnCallback)
      }, 100);
    }
    // text finished, call callback if there is a callback function
    else if (typeof fnCallback == 'function') {
      // call callback after timeout
      setTimeout(fnCallback, 700);
    }
  }
  // start a typewriter animation for a text in the dataText array
   function StartTextAnimation(i) {
     if (typeof dataText[i] == 'undefined'){
        setTimeout(function() {
          StartTextAnimation(0);
        }, 20000);
     }
     // check if dataText[i] exists
    if (i < dataText[i].length) {
      // text exists! start typewriter animation
     typeWriter(dataText[i], 0, function(){
       // after callback (and whole text has been animated), start next text
       StartTextAnimation(i + 1);
     });
    }
  }
  // start the text animation
  StartTextAnimation(0);
});
</script>


<div class="iuk">
 <div class="container">
  <div class="headerWRK-text">
   <h1 class="header-title">From the Classroom,
   to the Community</h1>
   <p>CollegeWRK connects you with respectful, reliable students from your community to offer you a helping hand. Post a job to our dashboard for a simple way to find help with moving, cleaning, events, and more.Are you a student looking for a job with fair pay and flexible hours? Become a WRKer!</p>
   <ul class="list-inline">
          @if(Auth::check())
          <!-- <li><button class="post-btn" data-toggle="modal" data-target="#demo-modal-3">Post A Job</button> </li>
          <li><button class="apply-btn" data-toggle="modal" data-target="#myModal-2">Apply For Work</button></li> -->
          @else
          <li><button class="post-btn wow fadeIn" data-toggle="modal" data-target="#demo-modal-3">Join Our Team</button></li>
          <li><button class="apply-btn wow fadeOut" data-toggle="modal" data-target="#myModal-2">Post A Job</button></li>
          @endif
        </ul>
   </div>     
 </div>
</div>
<style>
.google_plus_btn {
    padding: 12px 169px 12px 169px!important;}
.facebook_btn {
    background-color: #2d4373 !important;
    padding: 14px 175px 14px 175px;}
button.post-btn {
    transition: .9s ease;
    -webkit-animation-name: hvr-pulse;
    animation-name: hvr-pulse;
}
 button.post-btn:hover {
    webkit-animation-name: hvr-pulse;
    animation-name: hvr-pulse;
    -webkit-animation-duration: 1s;
    animation-duration: 1s;
    -webkit-animation-timing-function: linear;
    animation-timing-function: linear;
    -webkit-animation-iteration-count: infinite;
    animation-iteration-count: infinite;
}
button.apply-btn {
    transition: .9s ease;
    -webkit-animation-name: hvr-pulse;
    animation-name: hvr-pulse;
}
 button.apply-btn:hover {
    webkit-animation-name: hvr-pulse;
    animation-name: hvr-pulse;
    -webkit-animation-duration: 1s;
    animation-duration: 1s;
    -webkit-animation-timing-function: linear;
    animation-timing-function: linear;
    -webkit-animation-iteration-count: infinite;
    animation-iteration-count: infinite;
}
.get-started button.post-btn {
    transition: .9s ease;
    -webkit-animation-name: hvr-pulse;
    animation-name: hvr-pulse;
}
.iuk {
    background-image: url("./public/images/slide.jpg");
    animation-name: hhh;
    animation-duration: 9s;
    animation-iteration-count: infinite;
    padding:170px 0px;
}
@keyframes  hhh {
     20%  {background-image: url("./public/images/slide.jpg");}
    60%  {background-image: url("./public/images/banner2.jpg");}
    100% {background-image: url("./public/images/college_wrk-Banner.jpg");}
}
.iuk h1{
font-size: 70px;
    font-family: 'ProximaNovaA-Bold';
    line-height: 64px;
    text-align: center;
   color:#ffffff;
   max-width:780px;
   margin:0 auto;
  }
  .other-color {
    color: #ff8638;
    display: block;
}
.iuk p{
  font-size: 28px;
    color: #ffffff;
    padding-top: 15px;
    padding-bottom: 40px;
    text-align: center;
  }
  .iuk ul {
    list-style: none;
    position: relative;
    text-align: center;
}
@media screen and (max-width: 980px){
.iuk h1 {font-size: 55px;max-width: 100%;line-height: 55px;}
.iuk p {padding-top: 10px;line-height: 32px;}
}

@media screen and (max-width: 600px){
.iuk {padding: 100px 0px;}
.iuk h1 {font-size: 40px;max-width: 100%;line-height: 45px;}
.iuk p {font-size: 22px;width: 500px;margin: 0 auto;}
}
@media screen and (max-width: 500px){
.iuk p {width: 100%;}
}
@media screen and (max-width: 425px){
.iuk h1 {font-size: 33px;line-height: 40px;}
.iuk p {font-size: 20px;line-height: 27px;}
.iuk p {font-size: 20px;line-height: 27px;padding-bottom: 30px;}
.list-inline>li {padding-bottom: 16px;}
}
@media screen and (max-width: 375px){
.iuk h1 {font-size: 30px;line-height: 40px;}
.iuk p {font-size: 18px;}
.apply-btn {height: 45px;font-size: 19px;}
.post-btn {height: 45px;font-size: 19px;}
}
@media screen and (max-width: 375px){
.iuk h1 {font-size: 27px;line-height: 40px;}
}
</style>

<section class="about">
  <div class="container">
    <div class="row">
      <div class="col-md-3 wow fadeInLeft">
        <h2>Why Choose  <br>
</h2>
<h3> CollegeWRK?</h3>
      </div>
      <div class="col-md-9 wow fadeInRight">
        <h4>CollegeWRK makes it easy for you to get more done with the help of honest and reliable students from your community.</h4>
        <p>At CollegeWRK, we have created a simple and painless process to help you find quality workers straight from the classroom to the community. Have the reassurance that your job will be taken care of with up-to-date notifications of your listing’s status on your own personal CollegeWRK dashboard. Your CollegeWRK dashboard helps you stay informed and connected with your WRKers, all while granting access to our dedicated support staff to address any questions or concerns you may have during the process. Finally, our WRKers are dependable, enthusiastic, and courteous. Feel safe and secure knowing that WRKers are hard-working local students committed to establishing financial independence and learning new skills. With our Book Now, Pay Later feature and competitive pricing, we make it easier than ever to get more done.</p>
      </div>
    </div>
    <div class="row about-btm">
      <div class="col-md-4 col-sm-4 wow fadeInDown">
        <div class="Guarantee-cont">
          <div class="gaur-img">
            <img src="{{ url('/public') }}/images/right.png" alt="">
          </div>
          <div class="guar-text">
              <h6>Verified Activity</h6>
              <p>Stay connected with 24/7 updates on the status of your posted jobs.</p>
      </div>
</div>
      </div>
      <div class="col-md-4 col-sm-4 wow fadeInDown">
          <div class="Guarantee-cont">
          <div class="gaur-img">
            <img src="{{ url('/public') }}/images/gaur.png" alt="">
          </div>
          <div class="guar-text">
              <h6>Dedicated Support</h6>
              <p>We're here to help. We will answer your questions by phone, text, chat, or email support whenever you need it, seven days a week.</p>
      </div>
</div>
      </div>
      <div class="col-md-4 col-sm-4 wow fadeInDown">
          <div class="Guarantee-cont">
          <div class="gaur-img">
            <img src="{{ url('/public') }}/images/education.png" alt="">
          </div>
          <div class="guar-text">
              <h6>Local College Students</h6>
              <p>Our WRKers have undergone background checks and have been interviewed to ensure that each WRKer embodies our values and understands our mission.</p>
      </div>
</div>
      </div>
    </div>
  </div>
</section>
<section class="services">
  <div class="container-fluid">
    <h2 class="wow fadeInRight">Our <span>Services>></span></h2>
    <p class="tp-service wow fadeInRight">Choose from a variety of services to get quality work done by responsible individuals. What do you need help with today? </p>
    <div class="row">
      <div class="col-md-3 col-sm-4 wow fadeInLeft tab-left nopadding">
    <nav class="nav-sidebar">
    <ul class="nav tabs">
          <li class="active"><a href="#tab1" data-toggle="tab"><img src="{{ url('/public') }}/images/moving_color.png" alt="" class="blue"><img src="{{ url('/public') }}/images/moving_white.png" alt="" class="white"> Moving</a></li>
          <li class=""><a href="#tab2" data-toggle="tab"><img src="{{ url('/public') }}/images/yard_work_color.png" alt="" class="blue"><img src="{{ url('/public') }}/images/yard_work_white.png" class="white"> Yard Work</a></li>
          <li class=""><a href="#tab8" data-toggle="tab"><img src="{{ url('/public') }}/images/calendar_color.png" alt="" class="blue"><img src="{{ url('/public') }}/images/calendar_white.png" alt="" class="white"> Events</a></li>
          <li class=""><a href="#tab3" data-toggle="tab"><img src="{{ url('/public') }}/images/cleaning_color.png" alt="" class="blue"><img src="{{ url('/public') }}/images/cleaning_white.png" alt="" class="white">Cleaning</a></li>
          <li class=""><a href="#tab4" data-toggle="tab"><img src="{{ url('/public') }}/images/painting_color.png" alt="" class="blue"><img src="{{ url('/public') }}/images/painting_white.png" alt="" class="white">Painting</a></li>
          <li class=""><a href="#tab5" data-toggle="tab"><img src="{{ url('/public') }}/images/tutoring_color.png" alt="" class="blue"><img src="{{ url('/public') }}/images/tutoring_white.png" alt="" class="white">Tutoring</a></li>
          <li class=""><a href="#tab6" data-toggle="tab"><img src="{{ url('/public') }}/images/technology_color.png" alt="" class="blue"><img src="{{ url('/public') }}/images/technology_whiter.png" alt="" class="white">Technology</a></li>
          <li class=""><a href="#tab7" data-toggle="tab"><img src="{{ url('/public') }}/images/odd_jobs.png" alt="" class="blue"><img src="{{ url('/public') }}/images/odd_jobs-white.png" alt="" class="white">Odd Jobs</a></li>
       <!--    <li class=""><a href="#tab8" data-toggle="tab"><img src="{{ url('/public') }}/images/8.png" alt="" class="blue"><img src="{{ url('/public') }}/images/8-white.png" alt="" class="white">Odd Jobs</a></li>   -->                             
    </ul>
  </nav>
    
</div>
<!-- tab content -->
<div class="col-md-9 col-sm-8 wow fadeInRight tab-right nopadding">
<div class="tab-content ">
<div class="tab-pane active text-style" id="tab1">
  <img src="{{ url('/public') }}/images/moving.jpg" alt="" class="servicesimage">
 <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusm od tempor incididunt ut labore et dolore magna aliqua. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
  -->
       
      
</div>
<div class="tab-pane text-style" id="tab2">
  <img src="{{ url('/public') }}/images/yard-work.jpg" alt="" class="servicesimage">
 <!-- <p>tempor incididunt ut labore et dolore magna aliqua. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> -->
 
 
</div>

<div class="tab-pane text-style" id="tab8">
  <img src="{{ url('/public') }}/images/yard-work.jpg" alt="" class="servicesimage">
   <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusm od tempor incididunt ut labore et dolore magna aliqua. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> -->
 
  
</div> 

<div class="tab-pane text-style" id="tab3">
  <img src="{{ url('/public') }}/images/cleaning.jpg" alt="" class="servicesimage">
  <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusm inting and typesetting industry. Lore m Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galpe specimen book.</p>
  -->
  
</div>
<div class="tab-pane text-style" id="tab4">
  <img src="{{ url('/public') }}/images/painting.jpg" alt="" class="servicesimage">
  <!--  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusm od tempor incididunt ut labore et dolore magna aliqua. Lorem </p>
  -->
</div>
<div class="tab-pane text-style" id="tab5">
  <img src="{{ url('/public') }}/images/tutoring.jpg" alt="" class="servicesimage">
  <!--  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusm od tempor incididunt ut labore et dolore magna aliqua. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
  -->
  
</div>
<div class="tab-pane text-style" id="tab6">
  <img src="{{ url('/public') }}/images/technology.jpg" alt="" class="servicesimage">
   <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusm od tempor incididunt ut labore et dolore magna aliqua. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
  -->
  
</div>
<div class="tab-pane text-style" id="tab7">
  <img src="{{ url('/public') }}/images/odd-jobs.jpg" alt="" class="servicesimage">
  <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusm od tempor incididunt ut labore et dolore magna aliqua. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
  -->
  
</div>

</div>
    </div>
    </div>
  </div>
  
</section>
<section class="howitwork">
  <div class="container">
    <h2 class="wow fadeInLeft">Posting a Job:How We Work</h2>
    <p class="hiw-tp wow fadeInLeft">In three easy steps, post a job to our CollegeWRK dashboard. Here, local students can browse listings and accept jobs. </p>
    <div class="row">
      <div class="col-sm-4 wow fadeInUp">
      <div class="work-inner">
        <img src="{{ url('/public') }}/images/post.png" alt="">
        <h3>Post your Job</h3>
        <p>Get an estimate and post your job listing on the CollegeWRK dashboard. WRKers in your area are notified and matched to your specific job. </p>
      </div>
      </div>
      <div class="col-sm-4 wow fadeInUp">
        <div class="work-inner"> 
        <img src="{{ url('/public') }}/images/confirm.png" alt="">
        <h3>Confirm your job</h3>
        <p>Get notified immediately when your job has been accepted by a WRKer in your area, and verify the final details such as the estimated price, hours, and duties to be performed by your WRKer. </p>
      </div>
      </div>
      <div class="col-sm-4 wow fadeInUp">
        <div class="work-inner">
        <img src="{{ url('/public') }}/images/work.png" alt="">
        <h3>Get to Work</h3>
        <p>At your selected date and time, sit back and wait for your WRKer to arrive ready to work. Don’t forget about our 24/7 dedicated support staff who are available to answer any questions or concerns you may have during the process. </p>
      </div>
      </div>
    </div>
  </div>
  
</section>
<section class="get-started">
  <div class="container">
    <div class="row">
      <div class="col-md-12 wow fadeInLeft">
        <h2>Ready to get started?</h2>
        <p><b>Post a job</b> to your CollegeWRK dashboard now to connect with a local college student ready to help you accomplish your tasks. We take the pain out of moving, home & office projects to make the entire process simple, safe, and productive. While we are not professionals (and not the best fit to move pianos or priceless antiques), our WRKers are smart, strong, and eager to make your busy days run smoothly. 

<br><b>If you are a student,</b> find out more about becoming a WRKer by clicking below to access your own personal CollegeWRKer dashboard, where you can view and accept jobs after an initial background screening and short interview process.</br> </p>
<ul class="list-inline">
        @if(Auth::check())
           <!-- <li><button class="post-btn" data-toggle="modal" data-target="#demo-modal-3">Post A Job</button> </li>
          <li><button class="apply-btn" data-toggle="modal" data-target="#myModal-2">Apply For Work</button></li> -->
          @else
          <li><button class="post-btn" data-toggle="modal" data-target="#demo-modal-3">Join Our Team</button> </li>
          <li><button class="apply-btn" data-toggle="modal" data-target="#myModal-2">Post A Job</button></li>
          @endif
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="testimonial">
 <div class="container theme_columns_carousel">
    
    <!--*-*-*-*-*-*-*-*-*-*- BOOTSTRAP CAROUSEL *-*-*-*-*-*-*-*-*-*-->

    <div id="testimonial_2_columns_text_carousel" class="carousel slide testimonial_columns_text_carousel_wrapper" data-ride="carousel" data-interval="3000" data-pause="hover">
      <h3>Customer Feedback</h3>
      <h1>See what others  <br>have to say:</h1>
      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">

        <!--========= First Slide =========-->
        <div class="item active">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 hidden-xs">
              <div class="testimonial_columns_text_carousel_caption">
                 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <div class="author-info">
                  <img src="{{ url('/public') }}/images/client-1.jpg" alt="">
                <div class="author-cont">
               <h4>Rashed ca.</h4>
                <a href="#">Marketing Manager</a>
              </div>
               </div>
                
              </div>
            </div>
            
            <div class="col-xs-12 col-sm-6 col-md-6 ">
              <div class="testimonial_columns_text_carousel_caption">
                 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <div class="author-info">
                  <img src="{{ url('/public') }}/images/client-2.jpg" alt="">
                <div class="author-cont">
                <h4>Maria Rose</h4>
                <a href="#">Certified Engineer</a>
              </div>
               </div>
                
              </div>
            </div>
          </div>
        </div>

        <!--========= Second Slide =========-->
        <div class="item">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 ">
              <div class="testimonial_columns_text_carousel_caption">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <div class="author-info">
                  <img src="{{ url('/public') }}/images/client-1.jpg" alt="">
                <div class="author-cont">
              <h4>Rashed ca.</h4>
                <a href="#">Marketing Ma nager</a>
              </div>
               </div>
              </div>
            </div>
            
            <div class="col-xs-12 col-sm-6 col-md-6 hidden-xs">
              <div class="testimonial_columns_text_carousel_caption">
                 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <div class="author-info">
                  <img src="{{ url('/public') }}/images/client-2.jpg" alt="">
                <div class="author-cont">
                 <h4>Maria Rose</h4>
                <a href="#">Certified Engineer</a>
              </div>
               </div>
                
              </div>
            </div>
          </div>
        </div>

        <!--========= Third Slide =========-->
        <div class="item">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="testimonial_columns_text_carousel_caption">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
               <div class="author-info">
                  <img src="{{ url('/public') }}/images/client-2.jpg" alt="">
                <div class="author-cont">
                <h4>Maria Rose</h4>
                <a href="#">Certified Engineer</a>
              </div>
               </div>
                
              </div>
            </div>
            
            <div class="col-xs-12 col-sm-6 col-md-6 hidden-xs">
              <div class="testimonial_columns_text_carousel_caption">
                 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <div class="author-info">
                  <img src="{{ url('/public') }}/images/client-1.jpg" alt="">
                <div class="author-cont">
                <h4>Rashed ca.</h4>
                <a href="#">Marketing Manager</a>
              </div>
               </div>
                
              </div>
            </div>
          </div>
        </div>

      </div>

      <!--========= Indicators =========-->
      <ol class="carousel-indicators testimonial_columns_text_carousel_indicators">
        <li data-target="#testimonial_2_columns_text_carousel" data-slide-to="0" class="active"></li>
        <li data-target="#testimonial_2_columns_text_carousel" data-slide-to="1"></li>
        <li data-target="#testimonial_2_columns_text_carousel" data-slide-to="2"></li>
      </ol>

    </div>
  </div>
</section>

<section class="contact-us">
  <div class="container">
    <div class="row">
      <div class="col-md-12 wow fadeInDown">
        <div class="contact-inner">
          <h2>Write to Us</h2>
          <p>We want to make your experience with CollegeWRK exceed your expectations. Have a suggestion to make that happen?<br>
Send us a message now to let us know your thoughts! We appreciate our customers’ honest feedback. </p>
     <form class="settingform" id="contactFrm" action="{{ url('home/store') }}" method="POST" enctype="multipart/form-data">
                             {{ csrf_field() }}

  <div class="form-group col-md-6">
    <input type="text" name="name" class="form-control" placeholder="Full Name" id="name" value="" required minlength="2" maxlength="81">
  </div>
  <div class="form-group col-md-6">
    <input type="email" name="email" class="form-control" placeholder="Your Email Address" id="email" value="" required maxlength="181">
  </div>
  <div class="form-group col-md-12">
    <textarea placeholder="Write Message" rows="3" class="form-control" name="message" id="message"  required maxlength="5001"></textarea>
  </div>
  <div class="col-md-12" style="text-align: center;">
  <button type="submit">Send Message</button>
</div>
</form>

        </div>
      </div>
    </div>
  </div>
</section>
<section class="other-info">
  <div class="container">
    <div class="row">
      <h2 class="google_review">Google Reviews</h2>
      <div id="google-reviews"></div>
      <link rel="stylesheet" href="https://cdn.rawgit.com/stevenmonson/googleReviews/master/google-places.css">
      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> -->
      <script src="https://cdn.rawgit.com/stevenmonson/googleReviews/6e8f0d79/google-places.js"></script>
      <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAmECi5THi1W3CHDdmC2bI6q6dDZNRAY-I&signed_in=true&libraries=places"></script>
      <script>
        jQuery(document).ready(function( $ ) {
           $("#google-reviews").googlePlaces({
            //ChIJp2QxV_sJVFMR1DEp1x_16F8
                placeId: 'ChIJ480zVfTuDzkRmuggkae-kmQ' //Find placeID @: https://developers.google.com/places/place-id
              , render: ['reviews']
              , min_rating: 4
              , max_rows:4
           });
        });
      </script>
    </div>
  </div>
</section>
<section class="other-info">
  <div class="container">
    <div class="row">
                <?php
            $bussiness_info = DB::table('bussinesssetting')
                                ->get();

          ?>
      @foreach($bussiness_info as $bussiness_infos)
      <?php
           $bussiness_phone_number = $bussiness_infos->phone;
           $bussiness_email = $bussiness_infos->emails;
           $bussiness_address = $bussiness_infos->address;
      ?>
      <div class="col-md-4 col-sm-6 wow fadeInLeft">
        <div class="oth-inner other-icon">
          <i class="fa fa-phone" aria-hidden="true"></i>
          <div class="oth-text">
            <h5>(Phone) Call Us</h5>
          <h4>{{$bussiness_phone_number}}</h4>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 wow fadeInDown ">
         <div class="oth-inner other-icon">
          <i class="fa fa-envelope" aria-hidden="true"></i>
          <div class="oth-text">
            <h5>(Mail) Send us an Email</h5>
            <h4>{{$bussiness_email}}</h4>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-4 wow fadeInRight">
        <div class="oth-inner">
          <!-- <i class="fa fa-home" aria-hidden="true"></i> -->
          <div class="oth-text">
            <!-- Begin Mailchimp Signup Form -->
            <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
            <style type="text/css">
                #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
            </style>
            <div id="mc_embed_signup">
              <form action="https://CollegeWRK.us19.list-manage.com/subscribe/post?u=0f7aa9196520bd891f7bcaa87&amp;id=c3258ec84b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate extre-class" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
                    <!-- <h2>Subscribe to our mailing list</h2>
                    <div class="indicates-required"><span class="asterisk">*</span> indicates required</div> -->
                    <div class="mc-field-group">
                      <label for="mce-EMAIL">(Newsletter signup) Want to stay up to date with CollegeWRK news and events? Enter your email to recieve our newsletter! But don’t worry, we’ll only send you what’s important.<span class="asterisk">*</span>
                    </label>
                      <input type="email" value="" name="EMAIL" class="required email input-box" id="mce-EMAIL" placeholder="Email address*">
                      <input type="submit" value="Go" name="subscribe" id="mc-embedded-subscribe" class="button go-btn">
                    </div>
                    <div id="mce-responses" class="clear">
                      <div class="response" id="mce-error-response" style="display:none"></div>
                      <div class="response" id="mce-success-response" style="display:none"></div>
                    </div>    
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_0f7aa9196520bd891f7bcaa87_c3258ec84b" tabindex="-1" value=""></div>
                    <div class="clear"></div>
                </div>
              </form>
            </div>
            <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
            <script type='text/javascript'>
              (function($) {
                  window.fnames = new Array(); 
                  window.ftypes = new Array();
                    fnames[0]='EMAIL';
                    ftypes[0]='email';
                    /*fnames[1]='FNAME';
                    ftypes[1]='text';*/
                    /*fnames[2]='LNAME';
                    ftypes[2]='text';
                    fnames[3]='ADDRESS';
                    ftypes[3]='address';
                    fnames[4]='PHONE';
                    ftypes[4]='phone';
                    fnames[5]='BIRTHDAY';
                    ftypes[5]='birthday';*/
                    fnames[6]='MMERGE6';
                    ftypes[6]='number';
              }(jQuery));
              var $mcj = jQuery.noConflict(true);
            </script>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>

<div id="google-reviews"></div>
</section>
 <script>
 $(document).ready( function() {

    $("#contactFrm").validate({
        rules: {
                name: {
                    required: true,
                    minlength: 2,
                    maxlength: 80
                },
                email: {
                    required: true,
                    email: true,
                    maxlength: 180
                },
                messages: {
                  required: true,
                  maxlength: 5000
                }
            },
        messages: {
                name: {
                  required: "This is required field.", 
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 80 characters allowed."
                },
                email: {
                  required: "This is required field.",   
                  email: "Enter valid email",
                  maxlength: "Maximum 180 characters allowed."
                },
                message: {
                  required: "This is required field.",  
                  maxlength: "Maximum 5000 characters allowed."
                }  
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

 });
</script>
    <script>
      wow = new WOW(
        {
          animateClass: 'animated',
          offset:       100,
          callback:     function(box) {
            console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
          }
        }
      );
      wow.init();
      document.getElementById('moar').onclick = function() {
        var section = document.createElement('section');
        section.className = 'section--purple wow fadeInDown';
        this.parentNode.insertBefore(section, this);
      };
   </script>
   <style type="text/css">
   img.servicesimage {
    width: 100%;
    min-height: 593px;
   }
   .review-item {
    margin: 5px;
   }
   .google_review {
    text-align: center;
    font-size: 40px;
    font-weight: bold;
    padding-bottom: 50px;
   }
   </style>
@stop