<div class="col-sm-12 text-center p-3 coppy_right" style="background-color: #ffffff">
             <i class="fa fa-copyright" aria-hidden="true"></i>2019 College WRK. All rights reserved.<span class="pull-right"><a href="http://www.binarydata.in/" target="_blank">Power-by : Binary Data </a></span>
    </div>
    <div class="overlay">
    <div class="overlay-content">
        <p style="text-align: center" ><i class="fa fa-spinner fa-spin"></i></p>
    </div>
</div>

<script type="text/javascript">
    $(document).ajaxStart(function() {
        $('.overlay').show();
    }).ajaxStop(function() {
        $('.overlay').hide();
    });
</script> 
<style>
.overlay {
  /* Height & width depends on how you want to reveal the overlay (see JS below) */    
  height: 100%;
  width: 100%;
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  display: none;
  z-index: 9999999;
  background-color: rgb(0,0,0); /* Black fallback color */
  background-color: rgba(0,0,0, 0.8); /* Black w/opacity */
  overflow-x: hidden; /* Disable horizontal scroll */
  transition: 0.5s; /* 0.5 second transition effect to slide in or slide down the overlay (height or width, depending on reveal) */
}

/* Position the content inside the overlay */
.overlay-content{
  position: relative;
  top: 35%; /* 25% from the top */
  width: 100%; /* 100% width */
  text-align: center; /* Centered text/links */
  margin-top: 30px; /* 30px top margin to avoid conflict with the close button on smaller screens */
  
}
.overlay-content i{
  font-size: 50px;
  color: #fff !important; 
}
</style>