<section class="dashboard_header">
  <div class="container-fluid">
    <div class="row">
    <div class="col-sm-3 logodiv">
      <a href="{{url('/')}}">
      <img src="{{ url('/public') }}/images/newlogo.png" class="img-responsive">
      </a>
    </div>
    <div class="col-sm-9 dashboard_rgtheader">
      <div class="col-sm-4 col-xs-12 header_left">
        <h3>WRKer Dashboard</h3>
      </div>
      <div class="mobile_navdiv">
       <div class="col-sm-8 col-xs-9 header_right text-right">
        
        
        <div class="col-sm-12 ma-hu">
         <?php  $id = Auth::user()->id;
          $count_total = DB::table('new_notifications')
                      ->where('worker_id', '=', $id)
                      ->where('read_by_worker', '=', 0)
                      ->get(); ?>

    <ul class="dashboardright">
      @if(Auth::user()->facebook_id!='' && Auth::user()->google_id ==NULL && Auth::user()->address ==NULL || Auth::user()->google_id!='' && Auth::user()->facebook_id ==NULL && Auth::user()->address ==NULL)
      <li class="custdshbrd"><a class="btn btn-lg social_link_btn"> Customer Dashboard</a></li>
      @else
      <li class="custdshbrd"><a href="{{url('customerdashboard')}}" class="btn btn-lg"> Customer Dashboard</a></li>
      @endif
     <li class="notification">
     <div class="dropdown">
          <!-- <button class="dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-globe" ></i></span></button> -->
          <button class="dropdown-toggle headerNotification" type="button" data-toggle="dropdown"><img src="{{url('/public')}}/images/nitification-icon.png"></span></button>
          <ul class="dropdown-menu  notifaction-colwrk">
              <div class="headnoti">
                  Notification
              </div>
              <div class="notibody">
            @if(count($count_total) > 0)
            @foreach($count_total as $notify)
            <li><a href="<?php if( $notify->notification_redirect_url !='' ): echo url($notify->notification_redirect_url);  endif;?>" id="notity" class="title" notify="{{$notify->notification_id}}" data-redirecturl="<?php if( $notify->notification_redirect_url !='' ): echo url($notify->notification_redirect_url);  endif;?>"><?php echo $notify->message_worker; ?> </a></li>
            @endforeach
            @endif
            </div>
          </ul>
     </div>
     <span class="counter">{{count($count_total)}}</span></li>
       <li class="dropdown prflimgdiv">
      @if(Auth::user()->image !='')
         <img src="{{url('/public')}}/uploads/profile/{{Auth::user()->image}}" class="profilpic">
      @else
         <img src="{{url('/public')}}/images/avatar.jpg" class="profilpic">
      @endif
     </li>
            
             @if(Auth::check())

                 <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->firstname }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu dropdown-customer" role="menu">
                                    <li>
                                        <a href="{{url('/')}}/workerprofile" class="logount_hed_sec">
                                            Profile
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="logount_hed_sec">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown prflimgdiv  last-client">
                                <a class="cutmer-dash-tog"><i class="fa fa-bars" aria-hidden="true"></i></a>
                            </li>
        @endif
     </ul>
    </div>
      </div>
    <div class="col-xs-3 nav-toggle">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
    <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
  </button>
</div>
  
  </div>
  </div>
</div>
</div>
</section>
  
<section class="dashboard-main">
  <div class="container-fluid">
    <div class="row dashboard-rw">
      <div class="col-md-3 sidebar-left nopadding" id="nav-div1">
          <nav class="nav-sidebar">
          @if(Auth::user()->facebook_id!='' && Auth::user()->google_id ==NULL && Auth::user()->address ==NULL || Auth::user()->google_id!='' && Auth::user()->facebook_id ==NULL && Auth::user()->address ==NULL)
          <ul class="nav tabs">
                <li class=""><a class="social_link_btn"><i class="fa fa-briefcase" aria-hidden="true"></i><span>Jobs</span></a></li>
                <li class=""><a class="social_link_btn"><i class="fa fa-user" aria-hidden="true"></i>Profile</a></li>
                <li class=""><a class="social_link_btn"><i class="fa fa-share-alt" aria-hidden="true"></i>Share CollegeWRK</a></li>
                <li class=""><a class="social_link_btn"><i class="fa fa-usd" aria-hidden="true"></i>CollegeWRK Pay</a></li>
                <li class=""><a class="social_link_btn"><i class="fa fa-usd" aria-hidden="true"></i>Payments</a></li>  
                <li class=""><a class="social_link_btn"><i class="fa fa-question-circle" aria-hidden="true"></i>Help</a></li>
          </ul>
          @else
          <ul class="nav tabs">
                <li class=""><a href="{{url('workerjobs')}}"><i class="fa fa-briefcase" aria-hidden="true"></i><span>Jobs</span></a></li>
                
                <li class=""><a href="{{ url('workershare')}}"><i class="fa fa-share-alt" aria-hidden="true"></i>Share CollegeWRK</a></li>
                <li class=""><a href="{{ url('workerpay')}}"><i class="fa fa-usd" aria-hidden="true"></i>CollegeWRK Pay</a></li>
                <li class=""><a href="{{ url('workerpayment')}}"><i class="fa fa-usd" aria-hidden="true"></i>Payments</a></li>  
                <li class=""><a href="{{url('workerprofile')}}"><i class="fa fa-user" aria-hidden="true"></i>Profile</a></li>
                <!--<li class=""><a href="{{ url('workerhelp')}}"><i class="fa fa-question-circle" aria-hidden="true"></i>Help </a></li> -->
                <li class=""><a href="{{ url('workeradminchat')}}"><i class="fa fa-question-circle" aria-hidden="true"></i>Help </a></li>
          </ul>
          @endif
        </nav>
    </div>
<div class="col-md-9 dashboard-right width80">
  <div class="tab-content ">
<script>
$(document).ready(function(){
    $(".cutmer-dash-tog").click(function(){
        $("#nav-div1").fadeToggle("slow");
    });
});
</script>
<script>
jQuery(document).ready(function($){
  $('body').on('click','.headerNotification', function(e){
     jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/readallnotifications') }}",
            data :{ 'field_name' : 'read_by_worker', 'user_id' : {{ $id }} , 'user_field' : 'worker_id'},
            success :  function(resp) {
              //alert( resp );
                if(resp =='true'){
                 $('.counter').html('0');
               }
             }
       });

  });

});
</script>


<script type="text/javascript">
$(document).ready(function() {
$(".notity").click(function(e){
  e.preventDefault();
    var noti = $(this).attr("notify");
    var redirecturl = $(this).data("redirecturl");
    jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/notitysec') }}",
            data :{'noti': noti, 'field_name' : 'read_by_worker'},
            success :  function(resp) {
                if(resp=='true'){
                  if( redirecturl !='' && redirecturl != undefined && redirecturl != null ){
                    window.location.href= redirecturl ;
                  }else{
                    window.location.href="{{url('/workerjobs')}}";
                  }
               }
             }
       });
});
});
</script>
<style type="text/css">
  .dashboardright li {
    display: inline-block;
}
  .dashboardright button.dropdown-toggle {
    background: transparent;
    border: none;
}
.dropdown-menu {
    left: -70px !important;
}
.dropdown-customer {
    padding: 0px;
}
.dropdown-customer li {
    width: 100%;
}
.dropdown-customer li a {
    padding: 10px 15px;
    border-radius: 3px 3px 3px 3px;
}
.dropdown-customer:before {
    content: "";
    border: 10px solid transparent;
    border-left-color: #fff;
    border-top-color: #fff;
    position: absolute;
    border-radius: 3px;
    top: -8px;
    right: 15px;
    -webkit-box-shadow: -5px -5px 12px -2px rgba(0,0,0,.3);
    box-shadow: -4px -4px 12px -2px rgba(0,0,0,.3);
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg);
    z-index: 1002;
}
.dropdown-customer li {
    display: block;
    padding: 0px 0px;
    border-bottom: 1px solid #eee;
}
.logodiv a {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 95px;
}
@media screen and (max-width: 991px){
.event-box .btn-group-lg>.btn, .btn-lg {
    font-size: 13px;
}
.container {
    width: auto !important;
}
section.dashboard_header {
    border-bottom: 1px solid #ccc;
}
}
@media screen and (max-width: 980px) {
.col-md-3.sidebar-left.nopadding {
    width: 100% !important;
}
}

@media screen and (max-width: 870px){
.custdshbrd a {
    margin-right: 5px;
}
.notification {
    margin-right: 5px;
}
}
@media screen and (max-width: 767px){
    .col-sm-3.logodiv {
    width: 25%;
    margin: 0 auto;
}
    .logodiv a {
    height: auto;
}
.header_right {
    text-align: center;
    width: 100%;
}

.header_left h3 {
    font-size: 18px;
    padding: 10px 0px;
}
.col-sm-8.col-xs-9.header_right.text-right {
    padding: 0px;
    position:relative;
}
.col-sm-12.ma-hu {
    padding: 0px;
    
}
.dashboardright {
    padding: 10px 0px 0px 0px!important;
}
.dropdown.prflimgdiv.last-client {
    position: absolute;
    right: 30px;
    top: 0px;
}
a.cutmer-dash-tog {
    position: absolute;
    top: 8px;
    display: block;
    right:0px;
}
}
@media screen and (max-width: 480px){
.col-sm-3.logodiv {
    width: 40%;
    margin: 0 auto;
}
.logodiv img {
    height: 50px;
    padding: 10px 0px;
}
a.cutmer-dash-tog {
    position: absolute;
    top: -35px;
    display: block;
    right: -35px;
}
}


.notifaction-colwrk {
    position: absolute;
    top: 150%;
    left: 50% !important;
    transform: translateX(-50%);
    width: 350px;
    margin: 0;
    padding: 0;
    
}
.notifaction-colwrk>li>a {
    white-space: unset;
}
.notifaction-colwrk::before {
    display: block;
    content: "";
    position: absolute;
    top: -25px;
    border: 13px solid transparent;
    border-bottom-color: transparent;
    border-bottom-style: solid;
    border-bottom-width: 13px;
    border-bottom: 13px solid #2789fd;
    transform: translate(-50%);
    left: 50%;
}
.headnoti {
    background-color: #2789fd;
    color: #fff;
    padding: 10px;
    border-radius: 2px 2px 0 0;
}
.notview {
    float: right;
    color: #fff;
    text-decoration: underline;
    font-weight: bold;
}
.notifaction-colwrk li:nth-child(odd) {
    background-color: #2789fd0f;
}
.notibody {
    max-height: 400px;
    overflow-y: scroll;
}
.notibody li {
    width: 100%;
}
.notibody a {
    width: 100%;
    display: block;
    padding: 5px 15px;
    color: #666666;
}
@media screen and (max-width:480px){
.notifaction-colwrk {width: 270px;}
}
@media screen and (max-width: 375px){
.dashboardright li {
    padding-right: 0px;
}
.custdshbrd a {
    margin-right: 0px;
    font-size: 10px;
    padding: 6px 2px;
}
li.notification img {
    width: 75%;
}
.dashboardright li {
    padding-right: 0px;
}
.profilpic {
    border-radius: 50px;
    height: 30px;
    width: 30px;
}
}
input#estimatehrs {
    margin-right: 7px;
}
/*.panel-body {height:auto !important;overflow:auto;}*/
</style>