<!--<script type="text/javascript" src="https://js.stripe.com/v2/"></script> -->
<script src="https://js.stripe.com/v3/"></script>
<?php
  $bussinesssetting = DB::table('bussinesssetting')
                      ->first();

?>
<div class="top-header">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-5 th-left">
            <ul class="list-inline">
              <li><a href="tel:{{ $bussinesssetting->phone }}" class="five-link"><img src="{{ url('/public') }}/images/phone.png" alt=""><span> {{ $bussinesssetting->phone }} </span></a></li>
              <li><a href="mailto:{{ $bussinesssetting->emails }}" class="six-link"><img src="{{ url('/public') }}/images/email.png" alt=""><span>{{ $bussinesssetting->emails }}</span></a></li>
            </ul>
          </div>
          <div class="col-md-6 col-sm-7 th-right">
            <ul class="list-inline">
              <li><a href="{{ $bussinesssetting->twitter_url }}" target="_blank" class="one-link"><i class="fa fa-twitter" aria-hidden="true"></i>Twitter</a></li>
              <li><a href="{{ $bussinesssetting->facebook_url }}" target="_blank" class="two-link"><i class="fa fa-facebook" aria-hidden="true"></i>Facebook</a></li>
              <li><a href="{{ $bussinesssetting->google_url }}" target="_blank" class="three-link"><i class="fa fa-google-plus" aria-hidden="true"></i>Google Plus</a></li>
              <li><a href="{{ $bussinesssetting->linkedin_url }}" target="_blank" class="four-link"><i class="fa fa-linkedin" aria-hidden="true"></i>Linkedin</a></li>

            </ul>
          </div>
        </div>
      </div>
    </div>

  <nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ url('/') }}/home"><img src="{{ url('/public') }}/images/logo.png" alt=""></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
     
      <ul class="nav navbar-nav navbar-right">
        
        <li><a href="{{url('/pricing')}}" class="col-pri">Pricing</a></li>
       <li><a href="{{url('/aboutus')}}" class="col-pri">About Us</a></li> 
        @if(Auth::check())
        <!-- <li class="post-job"><a href="#" data-toggle="modal" data-target="#demo-modal-4">Post A Job</a></li> -->
        <li class="post-job"><a href="{{url('/customerdashboard')}}" class="launch-modal_after_login">Post A Job</a></li>
        <li class="apply-work"><a class="cll-apply" href="{{url('/customerdashboard')}}">Apply For Work</a></li>
        @else
        <!-- <li class="post-job"><a href="#" data-toggle="modal" data-target="#demo-modal-3">Post A Job</a></li> -->
        <li class="post-job"><a href="#" class="launch-modal_before_login" data-toggle="modal" data-backdrop="static" data-target="#demo-modal-3">Post A Job</a></li>
        <li><a class="apply-work" href="#" data-toggle="modal" data-target="#myModal-2">Join Our Team</a></li>
        @endif
       
         @if(Auth::check())

                 <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->firstname }} <span class="caret"></span>
                                    <?php
                                      //$userdata = Auth::user();
                                      //print_r($userdata);
                                    $user__role = Auth::user()->user_role;

                                    ?>
                                </a>

                                <ul class="dropdown-menu dropdown-customer" role="menu">
                                    <li>
                                      <?php if( $user__role == 'admin' ):?>
                                         <a href="{{ url('/admindashboard') }}" class="logount_hed_sec">Dashboard</a>
                                      <?php elseif( $user__role == 'customer' ):?>
                                         <a href="{{ url('/customerdashboard') }}" class="logount_hed_sec">Dashboard</a>
                                      <?php else:?>
                                          <a href="{{ url('/workerjobs') }}" class="logount_hed_sec">Dashboard</a>
                                      <?php endif;?>  
                                       
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="logount_hed_sec">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                     @else
          <li class="signin"><a href="#" data-toggle="modal" data-backdrop="static" data-target="#myModal">Sign In</a></li>
        @endif
         </ul>
       </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>



@if(Auth::check())
<!-- Modal -->
<div class="modal multi-step signup-popup" id="demo-modal-4">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title step-1" data-step="1">Get Started</h4>
                <h4 class="modal-title step-2" data-step="2">When</h4>
                <h4 class="modal-title step-3" data-step="3">Reserve Your Job</h4>
                <h4 class="modal-title step-4" data-step="4">Completed</h4>
                
                <div class="m-progress">
                    <div class="m-progress-bar-wrapper">
                        <div class="m-progress-bar">
                        </div>
                    </div>
                    <div class="m-progress-stats">
                        <span class="m-progress-current">
                        </span>
                        <span >of</span>
                        <span class="m-progress-total">
                        </span>
                    </div>
                    <div class="m-progress-complete">
                        Completed
                    </div>
                </div>

            </div>
      <?php 
            $events= DB::table('categories')
                        ->where('status',1)
                        ->where('deleted',0)
                        ->get();
            
      ?>            
  <div class="modal-body step-1" data-step="1">
       <?php   $id = Auth::user()->id; ?>
           <form class="" id="savedetails" method="GET" action="">
            <input type="hidden" name="userid" id="userid" value="{{$id}}" >
            <h3>Complete Your Details</h3>
            <div class="form-group">
              <label>What type of job is it?</label><span class="astrik">*</span>
            <select class="form-control" name="job_type" id="job_type">
                <option value ="default">Select Job</option>
                @foreach($events as $event)
                <option value = "{{$event->id}}">{{$event->cat_name}}</option>
               @endforeach
              </select>
            </div>
            <div id="equipment_sec">
            <!--Data through AJAX -->
            </div>
            <div class="form-group red">
              <label>Select Location</label><span class="astrik">*</span>
              <p>Start typing your street address here</p>
               <input type="text" name="street" id="street" placeholder="Street" class="form-control uprcls" style="margin-bottom: 10px;">
               <input type="text" name="address" id="address" placeholder="Address" class="form-control uprcls">
            </div>
            <div class="form-group">
              <label>Tell us about your job</label><span class="astrik">*</span>
              <textarea placeholder="" class="form-control" rows="5" name="about_job" id="about_job"></textarea> 
            </div>
            <div class="form-group">
              <!-- <label>Which type of College worker you need?</label><span class="astrik">*</span> -->
              <input type="hidden" name="workertype" class="workertype" value="bronze">
              <!-- <ul class="list-inline">
                <li> <input type="radio" name="workertype" class="workertype" value="bronze"> Bronze</li>
                <li> <input type="radio" name="workertype" class="workertype" value="silver">Silver</li>
                <li> <input type="radio" name="workertype" class="workertype" value="gold">Gold</li>
               
              </ul> -->

            </div>
            <div class="form-group">
              <label>How many CollegeWRK?</label><span class="astrik">*</span>
              <select class="form-control" name="collegewrk" id="collegewrk">
                <option value="default">Select CollegeWRK</option>
                @for ($i = 1; $i <= 10; $i++)
                  <option value = "{{$i}}">{{ $i }}</option> 
                @endfor
              </select>
            </div>
            <div class="form-group">
              <label>Describe your ideal Workers (optional)</label>
              <textarea placeholder="Describe about your ideal Workers " rows="5" class="form-control" id="sweeps_desc"></textarea>
            </div>

             <div class="submit-button">
            <!-- <button class="bck-btn">Back</button><button class="next-btn">Next</button> -->
          </div>
           
          </form>
    </div>

  <div class="modal-body step-2" data-step="2">
     <form class="" id="savetimings">
            <input type="hidden" name="userid" id="userid" value="" >
            <input type="hidden" name="lastjobid" id="lastjobid" value="" >
            <h3>Select Date & Time <span>*</span></h3>
             <div class="form-group">
        <div class="date">
          <input type="text" class="form-control" name="jobdate" id="jobdate">

        </div>
    </div>
            <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1default" data-toggle="tab">Morning</a></li>
                            <li><a href="#tab2default" data-toggle="tab">Afternoon</a></li>
                            <li><a href="#tab3default" data-toggle="tab">Evening</a></li>
                            
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1default">
                          <ul class="list-inline">
                          <li> <input type="radio" name="jobtime" class="job_time" value="9:00"  > 9:00 AM</li>
                          <li> <input type="radio" name="jobtime" class="job_time" value="10:00" >10:00 AM</li>
                          <li> <input type="radio" name="jobtime" class="job_time" value="11:00" >11:00 AM</li>
                          <li> <input type="radio" name="jobtime" class="job_time" value="12:00" >12:00 AM</li>
                        </ul>
                      </div>
                       <div class="tab-pane fade" id="tab2default">
                          <ul class="list-inline">
                         <li> <input type="radio" name="jobtime" class="job_time" value="1:00" > 1:00 PM</li>
                         <li> <input type="radio" name="jobtime" class="job_time" value="2:00" > 2:00 PM</li>
                         <li> <input type="radio" name="jobtime" class="job_time" value="3:00" > 3:00 PM</li>
                         <li> <input type="radio" name="jobtime" class="job_time" value="4:00" > 4:00 PM</li>
                        </ul>
                      </div>
                        <div class="tab-pane fade" id="tab3default">
                          <ul class="list-inline">
                          <li> <input type="radio" name="jobtime" class="job_time" value="5:00" > 5:00 PM</li>
                          <li> <input type="radio" name="jobtime" class="job_time" value="6:00" > 6:00 PM</li>
                          <li> <input type="radio" name="jobtime" class="job_time" value="7:00" > 7:00 PM</li>
                          <li> <input type="radio" name="jobtime" class="job_time" value="8:00" > 8:00 PM</li>
                          <li> <input type="radio" name="jobtime" class="job_time" value="9:00" > 9:00 PM</li>
                         </ul>
                      </div>
                        
                    </div>
                </div>
            </div>
            <h4>Don't see the time you need?</h4>
            <div class="form-group">
              <label>Estimate how long you will need CollegeWRK</label><span>*</span>
              <p>This helps us schedule Workers. You will not be charged
                 based on this estimate, only for the actual time Workers work.</p>
               <!-- <input type="text" name="estimatehrs" id="estimatehrs" placeholder="Enter estimate hours" class=" col-sm-6"><span>hours</span> -->
               <input type="text" name="estimatehrs" id="estimatehrs" placeholder="Enter estimate hours" class=" col-sm-6"><span>hours</span>
            </div>
            
            <div class="form-group">
              <label>Any scheduling details? (optional)</label>
              <textarea placeholder="" name="scheduling_details" id="scheduling_details" rows="5" class="form-control"></textarea>
            </div>
            <div class="form-group">
              <label>Any additional directions or locations? (optional)</label>
              <textarea placeholder="Add additional information." name="additional_info" id="additional_info" class="form-control" rows="5"></textarea> 
            </div>
            
             <div class="submit-button">
           <!--  <button class="bck-btn">Back</button><button class="next-btn">Next</button> -->
          </div>
           
          </form>

            </div>
            <div class="modal-body step-3" data-step="3">
                         <div class="reserve-job">
              <h5>Please Review the Information Below</h5>
              <div class="row">
               <div class="col-md-12">
                <div class="paym-cont">
                <ul class="paym-list payment_sec" style="float: left;width: 100%;">
                    <li><img src="{{ url('/public') }}/images/pay-1.png"/ class=""> Seamless payments with no up-front charges</li>
                    <li><img src="{{ url('/public') }}/images/pay-2.png"/ class=""> Secure, private data management</li>
                    <!--<li><img src="{{ url('/public') }}/images/pay-3.png"/ class=""> $500 Guarantee</li> -->
                    <li><img src="{{ url('/public') }}/images/pay-4.png"/ class=""> Dedicated support 7 days a week</li>
                  </ul>
                  <p style="color: #333;padding-bottom: 10px;">We require a valid credit card to reserve a job, but there are NO UPFRONT CHARGES. You will only be charged after your job is confirmed, completed, and you review CollegeWRK time sheets; or if a confirmed job is canceled within 24 hours of its start time.</p>
                </div>
                <div class="res-dtl">
                  <h6>Reservation Details</h6>

                  <ul class="addrs-lt">
                    <li><img src="{{ url('/public') }}/images/loc.png"/><div class="resr-txt" id="jobaddr"></div></li>
                    <li><img src="{{ url('/public') }}/images/msg.png"/><div class="resr-txt" id="fnldate"></div></li>
                  </ul>
                </div>
                <div class="res-dtl estimate-dv">
                  <h6>Estimate</h6>
                  <ul class="addrs-lt estmimate-price">
                    <li>
                      <div class="estim-left">
                      <h4>Labor</h4>
                      <p id = "workeramttxt"></p>
                    </div>
                    <div class="estim-right">
                      <h4 id="workeramt"></h4>
                    </div>
                    </li>
                    <li>
                      <div class="estim-left e">
                      <h4>Equipment Cost</h4>
                      <p id = "equipmentamttxt"></p>
                    </div>
                      <div class="estim-right">
                        <h4 id="equipmentamt"></h4>
                      </div>
                    </li>

                    <li id="refferal_section"></li>
                    <li>
                      <div class="estim-left">
                        <h5>TOTAL</h5>
                      </div>
                      <div class="estim-right nw-est">
                        <h4 id="totalamt"></h4>
                      </div>
                    </li>
<!--                      <li>
                      <div class="estim-left due-n">
                      <h5>Due Now</h5>
                    </div>
                    <div class="estim-right due-right">
                      <h4>$0</h4>
                    </div>
                    </li> -->
                    
                  </ul>
                 
                </div>
                 
               </div>
               
             </div>
           </div>

  <!--<form class="reserve-job" action="" method="get" id="paymentFrm">
   <div class="row">
   <div class="col-md-12">
   <div class="cont-detail-form payment-form">
   <h4>Payment Details</h4>
   <div class="cont-form-inner">
    <div class="payment-errors"></div>
    <input type="hidden" name="clientpay" class="clientpay" value="" id="clientpay">
    <input type="hidden" name="userid" id="userid" value="" >
    <input type="hidden" name="lastjobid" id="lastjobid" value="" >
    <div class="form-group">
    <div class="col-sm-6">
        <label>Name</label>
        <input type="text" class="form-control paymentname" name="paymentname" size="50" />
    </div>
    <div class="col-sm-6">
        <label>Email</label>
        <input type="text" class="form-control paymentemail" name="paymentemail" size="50" />
    </div>
  </div>
  <div class="form-group">
  <div class="col-sm-6">
        <label>Card Number</label>
        <input type="text" class="form-control card-number" name="card_num" size="20" autocomplete="off"/>
   </div>
    <div class="col-sm-2 custom-padd">
        <label>CVC</label>
        <input type="text" class="form-control card-cvc" name="cvc" size="4" autocomplete="off"/>
   </div>
   <div class="col-sm-4">
      <label>Expiration (MM/YYYY)</label>
      <div class="col-sm-4 custom-padd">
        
        <input type="text" class="form-control card-expiry-month" name="exp_month" size="2"/>
      </div>
      <div class="col-sm-1">  <span> / </span></div>
      <div class="col-sm-7 custom-padd">  <input type="text" class="form-control card-expiry-year" name="exp_year" size="4"/></div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-6">
      <label>How did you hear about CollegeWRK</label>
      <select class="form-control">
        <option>Please select one</option>
        <option>Advertisement</option>
        <option>Friends</option>
        <option>Others</option>
      </select>
    </div>
    <div class="col-sm-6">
      <label>Have a promo or referral code?</label>
      <input type="text" class="form-control" name="referralcode" placeholder="Enter your promo code" class="form-control" id="referralcode">
    </div>
  </div>
  </div>
</div>    
  <input type="checkbox" name="trmchk" ><span class="pay-agree">I agree to CollegeWRK' <a href="#" class="Privacy_Policy">Terms of Use and Privacy Policy</a>, and that the entered information is accurate and complete.</span>    
  
  </div>
</div>
</form> -->
<!-- ========= STRIPE CUSTOMER CREATE FORM START ======== -->
   <form class="reserve-job"  method="post" id="paymentFrm">
   <div class="row">
   <div class="col-md-12">
   <div class="cont-detail-form payment-form">
   <h4>Payment Details</h4>
   <div class="cont-form-inner">
    <div class="payment-errors"></div>
    <input type="hidden" name="clientpay" class="clientpay" value="" >
    <input type="hidden" name="userid" id="userid" value="" >
    <input type="hidden" name="lastjobid" id="lastjobid" value="" >
   <!-- stripe form for payments -->
  <div class="col-sm-6 model-bot">
        <label>Name</label>
        <input type="text" class="form-control paymentname" name="paymentname" size="50" />
    </div>
    <div class="col-sm-6 model-bot">
        <label>Email</label>
        <input type="text" class="form-control paymentemail" name="paymentemail" size="50" />
    </div>

    <div class="col-sm-12">
     
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-row">
            <label for="card-element">
              Credit or debit card
            </label>
          <div id="card-element" style="width: 100% !important;">
            <!-- A Stripe Element will be inserted here. -->
          </div>
          <!-- Used to display form errors. -->
          <div id="card-errors" role="alert"></div>
      </div>
      <button style="display: none;" class="stripPaymentButton">Submit Payment</button>
    
    </div> <br /> 
   
    <!-- stripe form for payments ends  --> 
  <!--<div class="col-sm-6 model-bot">
        <label>Card Number</label>
        <input type="text" class="form-control card-number" name="card_num" size="20" autocomplete="off"/>
   </div>
    <div class="col-sm-2 model-bot">
        <label>CVC</label>
        <input type="text" class="form-control card-cvc" name="cvc" size="4" autocomplete="off"/>
   </div>
   <div class="col-sm-4 model-bot">
       <label>Expiration (MM/YYYY)</label>
       <div class="row">
          <div class="col-sm-4">
            <input type="text" class="form-control card-expiry-month" name="exp_month" size="2"/>
          </div>
          <div class="col-sm-1">  <span> / </span></div>
          <div class="col-sm-6">  <input type="text" class="form-control card-expiry-year" name="exp_year" size="4"/>
          </div>
      </div>
    </div> -->
    <div class="col-sm-6 model-bot">
      <label>How did you hear about CollegeWRK</label>
      <select class="form-control hear_from">
        <option>Please select one</option>
        <option>Advertisement</option>
        <option>Friends</option>
        <option>Others</option>
      </select>
    </div>
    <div class="col-sm-6 model-bot">
      <label>Have a promo or referral code?</label>
      <input type="text" class="form-control promo_code" name="" placeholder="Enter your promo code" class="form-control">
    </div>
  </div>
</div>    
  <input type="checkbox" name="trmchk" class="modelchick agreeTerms" ><span class="pay-agree">I agree to CollegeWRK' Terms of Use and Privacy Policy, and that the entered information is accurate and complete.</span>    
  <!--   <button type="submit" id="payBtn">Submit Payment</button> -->
  </div>
</div>
</form>


<!-- ========= STRIPE CUSTOMER CREATE FORM ENDS ======== -->


  </div>
   <div class="modal-body step-4" data-step="4">
   <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
               <div class="thank_you_img_icon">
                 <img src="{{url('/public')}}/images/tick.png">
             </div>
             <h2 class="thank_you_head">Thank you for your submission</h2>
             <p class="thank_you_text">(You may close this page)</p>
             <hr />
             <h3 style="margin-bottom: 0px;">Book Now, Pay later</h3>
             <p>We require a valid credit card to reserve a job, but there are NO UPFRONT CHARGES. You will only be charged after your job is confirmed, completed and you review Sweeper time sheets; or if a confirmed job is canceled within 24 hours of its start time.</p>
             <div class="close_btn_sec">
                  <button type="button" class="btn btn-default cls-btn" data-dismiss="modal">Close</button>
             </div>


</div>

           
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-default cls-btn" data-dismiss="modal">Close</button> -->
                <button type="button" class="btn btn-primary step step-2 bck-btn" data-step="2" onclick="sendEvent('#demo-modal-4', 1)">Back</button>
                <button type="button" class="btn btn-primary step step-1 next-btn" data-step="1" id="step2">Next</button>
                <button type="button" class="btn btn-primary step step-3 bck-btn" data-step="3" onclick="sendEvent('#demo-modal-4', 2)">Back</button>
                <button type="button" class="btn btn-primary step step-2 next-btn" data-step="2" id="step3">Continue</button>
              <!--   <button type="button" class="btn btn-primary step step-4 bck-btn" data-step="4" onclick="sendEvent('#demo-modal-3', 3)">Back</button> -->
                <button type="button" class="btn btn-primary step step-3 next-btn" data-step="3" id="payBtn">Reserve Job</button>
            <!--     <button type="button" class="btn btn-primary step step-5 bck-btn" data-step="5" onclick="sendEvent('#demo-modal-3', 4)">Back</button> -->
              <!--   <button type="button" class="btn btn-primary step step-4 next-btn" data-step="4" id="payBtn">Reserve Job</button> -->
               
            </div>
        </div>
    </div>
</div>
@else
<div class="modal multi-step signup-popup" id="demo-modal-3">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title step-1" data-step="1">Basic Information</h4>
                <h4 class="modal-title step-2" data-step="2">Detail Information</h4>
                <h4 class="modal-title step-3" data-step="3">When</h4>
                <h4 class="modal-title step-4" data-step="4">Reserve Your Job</h4>
                <h4 class="modal-title step-5" data-step="5">Completed</h4>
                
                <div class="m-progress">
                    <div class="m-progress-bar-wrapper">
                        <div class="m-progress-bar">
                        </div>
                    </div>
                    <div class="m-progress-stats">
                        <span class="m-progress-current">
                        </span>
                        <span >of</span>
                        <span class="m-progress-total">
                        </span>
                    </div>
                    <div class="m-progress-complete">
                        Completed
                    </div>
                </div>

            </div>
            <div class="modal-body step-1" data-step="1">
              
              
            <!--   <button class="ext-account google_btn">Login with Google+ </button>
              <button class="ext-account facebook_btn">Login with Facebook</button>  -->
            

         <form class="" id="saveuser" method="GET" action="">
    `       <a href="#" data-toggle="modal" data-target="#myModal" data-dismiss="modal" class="sing_text_sec">
                <button class="bluemainbtn">Sign in to an existing account</button>
              </a>
              <div class="google_plus_btn_sec">
                  <a href="{{url('/redirect/google')}}" class="redmainbtn">Sign in with Google+</a>
              </div>
              <p style="text-align: center;">or</p>
              <div class="facebook_btn_sec">
              <a href="{{url('/facebookredirect/facebook')}}" class="skymainbtn">Sign in with Facebook</a>
          </div>
            <div class="form-group row">
                <div class="col-sm-6">
                  <label>First Name</label><span class="astrik">*</span>
                  <input type="text" name="firstname" id="firstname" placeholder="Enter your first name" class="form-control uprcls">
                </div>
                <div class="col-sm-6">
                  <label>Last Name</label><span class="astrik">*</span>
                  <input type="text" name="lastname" id="lastname" placeholder="Enter your last name" class="form-control uprcls">
                </div>
            </div>
            <div class="form-group">
                     <label>Email Address</label><span class="astrik">*</span>
                  <input type="email" name="email" id="email" placeholder="Enter your email" class="form-control">
                </div>
            <div class="form-group row">
               
                <div class="col-sm-6">
                    <label>Password</label><span class="astrik">*</span>
                    <input type="password" name="password" id="password" placeholder="Enter your password" class="form-control">
                </div>
                 <div class="col-sm-6">
              <label>Confirm Password</label><span class="astrik">*</span>
              <input type="password" name="confpwd" placeholder="Confirm your password" class="form-control"> 
            </div>
            </div>
            
            
            <input type="hidden" name="user_role" id="user_role" value="customer" class="form-control">
            <div class="submit-button"></div>
      </form>
    </div>
      <?php 
            $events= DB::table('categories')
                        ->where('status',1)
                        ->where('deleted',0)
                        ->get();
            
      ?>
    <div class="modal-body step-2" data-step="2">
         
          <form class="" id="savedetails" method="GET" action="">
            <input type="hidden" name="userid" id="userid" value="" >
            <h3>Complete Your Details</h3>
            <div class="form-group">
              <label>What type of job is it?</label><span class="astrik">*</span>
            <select class="form-control" name="job_type" id="job_type">
                <option value ="default">Select Job</option>
                @foreach($events as $event)
                <option value = "{{$event->id}}">{{$event->cat_name}}</option>
               @endforeach
              </select>
            </div>
            <div id="equipment_sec">
            <!--Data through AJAX -->
            </div>
            <div class="form-group">
              <label>Select Location</label><span class="astrik">*</span>
              <p>Start typing your street address here</p>
                <input type="text" name="street" id="street" placeholder="Street" class="form-control uprcls" style="margin-bottom: 10px;"> 
               <input type="text" name="address" id="address" placeholder="Address" class="form-control uprcls">
            </div>
            <div class="form-group">
              <label>Tell us about your job</label><span class="astrik">*</span>
              <textarea placeholder="" class="form-control" rows="5" name="about_job" id="about_job"></textarea> 
            </div>
            <div class="form-group">
              <!-- <label>Which type of College worker you need?</label><span class="astrik">*</span> -->
              <input type="hidden" name="workertype" class="workertype" value="bronze">
              <!-- <ul class="list-inline">
                <li> <input type="radio" name="workertype" class="workertype" value="bronze"> Bronze</li>
                <li> <input type="radio" name="workertype" class="workertype" value="silver">Silver</li>
                <li> <input type="radio" name="workertype" class="workertype" value="gold">Gold</li>
               
              </ul> -->

            </div>
            <div class="form-group">
              <label>How many CollegeWRK?</label><span class="astrik">*</span>
              <select class="form-control" name="collegewrk" id="collegewrk">
                <option value="default">Select CollegeWRK</option>
                @for ($i = 1; $i <= 10; $i++)
                  <option value = "{{$i}}">{{ $i }}</option> 
                @endfor
              </select>
            </div>
            <div class="form-group">
              <label>Describe your ideal Workers (optional)</label>
              <textarea placeholder="Describe about your ideal Workers " rows="5" class="form-control" id="sweeps_desc"></textarea>
            </div>

             <div class="submit-button">
            <!-- <button class="bck-btn">Back</button><button class="next-btn">Next</button> -->
          </div>
           
          </form>
            </div>
            <div class="modal-body step-3" data-step="3">
               
          <form class="" id="savetimings">
            <input type="hidden" name="userid" id="userid" value="" >
            <input type="hidden" name="lastjobid" id="lastjobid" value="" >
            <h3>Select Date & Time <span>*</span></h3>
             <div class="form-group">
        <div class="date">
          <input type="text" class="form-control" name="jobdate" id="jobdate">
      <!--       <div class="input-group input-append date" id="datePicker">
              <input type="text" class="form-control" name="jobdate" id="jobdate"/>   
               <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span> 
            </div> -->
        </div>
    </div>
            <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1default" data-toggle="tab">Morning</a></li>
                            <li><a href="#tab2default" data-toggle="tab">Afternoon</a></li>
                            <li><a href="#tab3default" data-toggle="tab">Evening</a></li>
                            
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1default">
                          <ul class="list-inline">
                          <li> <input type="radio" name="jobtime" class="job_time" value="9:00"  > 9:00 AM</li>
                          <li> <input type="radio" name="jobtime" class="job_time" value="10:00" >10:00 AM</li>
                          <li> <input type="radio" name="jobtime" class="job_time" value="11:00" >11:00 AM</li>
                          <li> <input type="radio" name="jobtime" class="job_time" value="12:00" >12:00 AM</li>
                        </ul>
                      </div>
                       <div class="tab-pane fade" id="tab2default">
                          <ul class="list-inline">
                         <li> <input type="radio" name="jobtime" class="job_time" value="1:00" > 1:00 PM</li>
                         <li> <input type="radio" name="jobtime" class="job_time" value="2:00" > 2:00 PM</li>
                         <li> <input type="radio" name="jobtime" class="job_time" value="3:00" > 3:00 PM</li>
                         <li> <input type="radio" name="jobtime" class="job_time" value="4:00" > 4:00 PM</li>
                        </ul>
                      </div>
                        <div class="tab-pane fade" id="tab3default">
                          <ul class="list-inline">
                          <li> <input type="radio" name="jobtime" class="job_time" value="5:00" > 5:00 PM</li>
                          <li> <input type="radio" name="jobtime" class="job_time" value="6:00" > 6:00 PM</li>
                          <li> <input type="radio" name="jobtime" class="job_time" value="7:00" > 7:00 PM</li>
                          <li> <input type="radio" name="jobtime" class="job_time" value="8:00" > 8:00 PM</li>
                          <li> <input type="radio" name="jobtime" class="job_time" value="9:00" > 9:00 PM</li>
                         </ul>
                      </div>
                        
                    </div>
                </div>
            </div>
            <h4>Don't see the time you need?</h4>
            <div class="form-group">
              <label>Estimate how long you will need CollegeWRK</label><span>*</span>
              <p>This helps us schedule Sweepers. You will not be charged
based on this estimate, only for the actual time Sweepers work.</p>
               <input type="text" name="estimatehrs" id="estimatehrs" placeholder="Enter estimate hours" class=" col-sm-6"><span>hours</span>
            </div>
            
            <div class="form-group">
              <label>Any scheduling details? (optional)</label>
              <textarea placeholder="" name="scheduling_details" id="scheduling_details" rows="5" class="form-control"></textarea>
            </div>
            <div class="form-group">
              <label>Any additional directions or locations? (optional)</label>
              <textarea placeholder="Add additional information." name="additional_info" id="additional_info" class="form-control" rows="5"></textarea> 
            </div>
            
             <div class="submit-button">
           <!--  <button class="bck-btn">Back</button><button class="next-btn">Next</button> -->
          </div>
           
          </form>
            </div>
            <div class="modal-body step-4" data-step="4">
              <div class="reserve-job">
              <h5>Please Review the Information Below</h5>
              <div class="row">
               <div class="col-md-12">
                <div class="paym-cont">
                <ul class="paym-list payment_sec" style="float: left;width: 100%;">
                    <li><img src="{{ url('/public') }}/images/pay-1.png"/ class=""> Seamless payments with no up-front charges</li>
                    <li><img src="{{ url('/public') }}/images/pay-2.png"/ class=""> Secure, private data management</li>
                    <!--<li><img src="{{ url('/public') }}/images/pay-3.png"/ class=""> $500 Guarantee</li> -->
                    <li><img src="{{ url('/public') }}/images/pay-4.png"/ class=""> Dedicated support 7 days a week</li>
                  </ul>
                  <p style="color: #333;padding-bottom: 10px;">We require a valid credit card to reserve a job, but there are NO UPFRONT CHARGES. You will only be charged after your job is confirmed, completed, and you review CollegeWRK time sheets; or if a confirmed job is canceled within 24 hours of its start time.</p>
                </div>
                <div class="res-dtl">
                  <h6>Reservation Details</h6>

                  <ul class="addrs-lt">
                    <li><img src="{{ url('/public') }}/images/loc.png"/><div class="resr-txt" id="jobaddr"></div></li>
                    <li><img src="{{ url('/public') }}/images/msg.png"/><div class="resr-txt" id="fnldate"></div></li>
                  </ul>
                </div>
                <div class="res-dtl estimate-dv">
                  <h6>Estimate</h6>
                  <ul class="addrs-lt estmimate-price">
                    <li>
                      <div class="estim-left">
                      <h4>Labor</h4>
                      <p id = "workeramttxt"></p>
                    </div>
                    <div class="estim-right">
                      <h4 id="workeramt"></h4>
                    </div>
                    </li>
                    <li>
                      <div class="estim-left e">
                      <h4>Equipment Cost</h4>
                      <p id = "equipmentamttxt"></p>
                    </div>
                      <div class="estim-right">
                        <h4 id="equipmentamt"></h4>
                      </div>
                    </li>
                    <li id="referral_section">
                      
                    </li>
                    <li>
                      <div class="estim-left">
                        <h5>TOTAL</h5>
                      </div>
                      <div class="estim-right nw-est">      
                        <h4 id="totalamt"></h4>
                      </div>
                    </li>
<!--                      <li>
                      <div class="estim-left due-n">
                      <h5>Due Now</h5>
                    </div>
                    <div class="estim-right due-right">
                      <h4>$0</h4>
                    </div>
                    </li> -->
                    
                  </ul>
                 
                </div>
                 
               </div>
               
             </div>
           </div>

  <!--<form class="reserve-job" action="" method="get" id="paymentFrm">
   <div class="row">
   <div class="col-md-12">
   <div class="cont-detail-form payment-form">
   <h4>Payment Details</h4>
   <div class="cont-form-inner">
    <input type="hidden" name="clientpay" class="clientpay" value="" id="clientpay">
    <input type="hidden" name="userid" id="userid" value="" >
    <input type="hidden" name="lastjobid" id="lastjobid" value="" >
    <div class="form-group">
    <div class="col-sm-6">
        <label>Name</label>
        <input type="text" class="form-control paymentname" name="paymentname" size="50" />
    </div>
    <div class="col-sm-6">
        <label>Email</label>
        <input type="text" class="form-control paymentemail" name="paymentemail" size="50" />
    </div>
  </div>
  <div class="form-group">
  <div class="col-sm-6">
        <label>Card Number</label>
        <input type="text" class="form-control card-number" name="card_num" size="20" autocomplete="off"/>
   </div>
    <div class="col-sm-2 custom-padd">
        <label>CVC</label>
        <input type="text" class="form-control card-cvc" name="cvc" size="4" autocomplete="off"/>
   </div>
 
 
   <div class="col-sm-4">
     
      <label>Expiration (MM/YYYY)</label>
    
      <div class="col-sm-4 custom-padd">
        <input type="text" class="form-control card-expiry-month" name="exp_month" size="2"/>
      </div>
      <div class="col-sm-1">  <span> / </span></div>
      <div class="col-sm-5 custom-padd">  <input type="text" class="form-control card-expiry-year" name="exp_year" size="4"/></div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-6">
      <label>How did you hear about CollegeWRK</label>
      <select class="form-control">
        <option>Please select one</option>
        <option>Advertisement</option>
        <option>Friends</option>
        <option>Others</option>
      </select>
    </div>
    <div class="col-sm-6">
      <label>Have a promo or referral code?</label>
      <input type="text" class="form-control" name="referralcode" placeholder="Enter your promo code" class="form-control" id="referralcode">
    </div>
  </div>
  </div>
</div>    
  <input type="checkbox" name="trmchk" ><span class="pay-agree">I agree to CollegeWRK' Terms of Use and Privacy Policy, and that the entered information is accurate and complete.</span>    
 
  </div>
</div>
</form> -->
<!-- ========= STRIPE CUSTOMER CREATE FORM START ======== -->
   <form class="reserve-job"  method="post" id="paymentFrm">
   <div class="row">
   <div class="col-md-12">
   <div class="cont-detail-form payment-form">
   <h4>Payment Details</h4>
   <div class="cont-form-inner">
    <div class="payment-errors"></div>
    <input type="hidden" name="clientpay" class="clientpay" value="" >
    <input type="hidden" name="userid" id="userid" value="" >
    <input type="hidden" name="lastjobid" id="lastjobid" value="" >
   <!-- stripe form for payments -->
  <div class="col-sm-6 model-bot">
        <label>Name</label>
        <input type="text" class="form-control paymentname" name="paymentname" size="50" />
    </div>
    <div class="col-sm-6 model-bot">
        <label>Email</label>
        <input type="text" class="form-control paymentemail" name="paymentemail" size="50" />
    </div>

    <div class="col-sm-12">
     
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-row">
            <label for="card-element">
              Credit or debit card
            </label>
          <div id="card-element" style="width: 100% !important;">
            <!-- A Stripe Element will be inserted here. -->
          </div>
          <!-- Used to display form errors. -->
          <div id="card-errors" role="alert"></div>
      </div>
      <button style="display: none;" class="stripPaymentButton">Submit Payment</button>
    
    </div> <br /> 
   
    <!-- stripe form for payments ends  --> 
  <!--<div class="col-sm-6 model-bot">
        <label>Card Number</label>
        <input type="text" class="form-control card-number" name="card_num" size="20" autocomplete="off"/>
   </div>
    <div class="col-sm-2 model-bot">
        <label>CVC</label>
        <input type="text" class="form-control card-cvc" name="cvc" size="4" autocomplete="off"/>
   </div>
   <div class="col-sm-4 model-bot">
       <label>Expiration (MM/YYYY)</label>
       <div class="row">
          <div class="col-sm-4">
            <input type="text" class="form-control card-expiry-month" name="exp_month" size="2"/>
          </div>
          <div class="col-sm-1">  <span> / </span></div>
          <div class="col-sm-6">  <input type="text" class="form-control card-expiry-year" name="exp_year" size="4"/>
          </div>
      </div>
    </div> -->
    <div class="col-sm-6 model-bot">
      <label>How did you hear about CollegeWRK</label>
      <select class="form-control hear_from">
        <option>Please select one</option>
        <option>Advertisement</option>
        <option>Friends</option>
        <option>Others</option>
      </select>
    </div>
    <div class="col-sm-6 model-bot">
      <label>Have a promo or referral code?</label>
      <input type="text" class="form-control promo_code" name="" placeholder="Enter your promo code" class="form-control">
    </div>
  </div>
</div>    
  <input type="checkbox" name="trmchk" class="modelchick agreeTerms" ><span class="pay-agree">I agree to CollegeWRK' Terms of Use and Privacy Policy, and that the entered information is accurate and complete.</span>    
  <!--   <button type="submit" id="payBtn">Submit Payment</button> -->
  </div>
</div>
</form>


<!-- ========= STRIPE CUSTOMER CREATE FORM ENDS ======== -->


</div>
 <div class="modal-body step-5" data-step="5">
             <div class="thank_you_img_icon">
                 <img src="{{url('/public')}}/images/tick.png">
             </div>
             <h2 class="thank_you_head">Thank you for your submission</h2>
             <p class="thank_you_text">(You may close this page)</p>
             <div class="close_btn_sec">
                  <button type="button" class="btn btn-default cls-btn" data-dismiss="modal">Close</button>
             </div>
            </div>
           
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-default cls-btn" data-dismiss="modal">Close</button> -->
                <button type="button" class="btn btn-primary step step-2 bck-btn" data-step="2" onclick="sendEvent('#demo-modal-3', 1)">Back</button>
                <button type="button" class="btn btn-primary step step-1 next-btn" data-step="1" id="step1">Next</button>
                <button type="button" class="btn btn-primary step step-3 bck-btn" data-step="3" onclick="sendEvent('#demo-modal-3', 2)">Back</button>
                <button type="button" class="btn btn-primary step step-2 next-btn" data-step="2" id="step2">Continue</button>
                <button type="button" class="btn btn-primary step step-4 bck-btn" data-step="4" onclick="sendEvent('#demo-modal-3', 3)">Back</button>
                <button type="button" class="btn btn-primary step step-3 next-btn" data-step="3" id="step3">Continue</button>
            <!--     <button type="button" class="btn btn-primary step step-5 bck-btn" data-step="5" onclick="sendEvent('#demo-modal-3', 4)">Back</button> -->
                <button type="button" class="btn btn-primary step step-4 next-btn" data-step="4" id="payBtn">Reserve Job</button>
               
            </div>
        </div>
    </div>
</div>
@endif

<div class="modal fade signup-popup hello" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">SIGN IN</h4>
        </div>
        <div class="modal-body">
          <div class="empty_space"></div>
          
               <!-- <button class="ext-account google_btn">Login with Google+ </button>
              <button class="ext-account facebook_btn">Login with Facebook</button> -->
           

           <form class="login" id="login"  name="login" action="{{ route('login') }}" method="POST" enctype="multipart/form-data">
               <div class="google_plus_btn_sec">
               <a href="{{url('/redirect/google')}}" class="redmainbtn">Sign in with Google+</a>
          </div>
          <div class="facebook_btn_sec one-of-thing">
              <a href="{{url('/facebookredirect/facebook')}}" class="skymainbtn">Sign in with Facebook</a>
          </div>
           <p style="text-align: center;">or</p>
             {{ csrf_field() }}
           
            <div class="form-group">
               <label>Email</label><span class="astrik">*</span>
               <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
               @if ($errors->has('email'))
                 <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                   </span>
                 @endif
             </div>
            <div class="form-group">
               <label>Password</label><span class="astrik">*</span>
               <input id="password" type="password" class="form-control" name="password" required>
                @if ($errors->has('password'))
                  <span class="help-block">
                     <strong>{{ $errors->first('password') }}</strong>
                   </span>
                @endif
             </div>
            <div class="row">
            <div class="col-sm-6">
               <label class="Agree-msg"><input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>Remember Me</label>
            </div>
            <div class="col-sm-6 forgot-pwd">
            <a href="#" data-toggle="modal" data-target="#myModal-3" data-dismiss="modal" class="">Forgot Password?</a>
          </div>
           </div>
             <div class="submit-button">
      
            <button type="submit" class="smb-btn"> Sign In </button>

          </div>
         
          </form>
        </div>
      
      </div>
      
    </div>
  </div>

<div class="modal fade signup-popup" id="myModal-3" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Forgot your password?</h4>
        </div>
        <div class="modal-body">
        <form class="form-horizontal" method="POST" action="{{ route('password.email') }}" id="forget_password">
                        {{ csrf_field() }}           
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" id="forg_sec">
              <label>Email</label><span class="astrik">*</span>
              <input id="forgetemail" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Enter your email"  required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

            </div>
           <div class="submit-button">
            <button type="submit" class="reset-btn">Send me reset password instructions</button>
          </div>
         
          </form>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>
<div class="modal fade signup-popup" id="myModal-2" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">SIGN UP</h4>
        </div>
        <div class="modal-body">
              
            <!--   <button class="ext-account google_btn">Login with Google+ </button>
              <button class="ext-account facebook_btn">Login with Facebook</button> -->
                
       <form class="work_sec" id="saveuser_sec"  name="work" action="{{url('/store')}}" method="POST" enctype="multipart/form-data">
           <a href="#" data-toggle="modal" data-target="#myModal" data-dismiss="modal" class="sing_text_sec">
               <button class="bluemainbtn">Sign in to an existing account</button>
              </a>
              <div class="google_plus_btn_sec other-class">
               <a href="{{url('/redirect/google')}}" class="redmainbtn">Sign in with Google+</a>
              </div>
              <p style="text-align: center;">or</p> 
              <div class="facebook_btn_sec">
              <a href="{{url('/facebookredirect/facebook')}}" class="skymainbtn">Sign in with Facebook</a>
              </div>
               {{ csrf_field() }}
            <div class="form-group row">
                <div class="col-sm-6">
                      <label>First Name</label><span>*</span>
                      <input type="text" name="firstname" placeholder="Enter your First Name" class="form-control uprcls">
                </div>
                <div class="col-sm-6">
                  <label>Last Name</label><span>*</span>
                  <input type="text" name="lastname" id="lastname" placeholder="Enter your Last Name" class="form-control uprcls">
                </div>
            </div>
            
            <div class="form-group">
              <label>Email Address</label><span>*</span>
              <input type="email" name="email" id="email" placeholder="Enter your Email Address" class="form-control">
            </div>
            <div class="form-group row">
                <div class="col-sm-6">
                      <label>Password</label><span>*</span>
                    <input type="password" name="password" id="passwordnew" placeholder="Enter your password" class="form-control">
                </div>
                <div class="col-sm-6">
                  <label>Confirm Password</label><span>*</span>
              <input type="password" name="confpwd" placeholder="Confirm password" class="form-control"> 
                </div>
            </div>
            
                <div id="locationField">
      
    </div>

            <div class="form-group">
              <label>Address</label><span>*</span>
              <input type="text" name="address" placeholder="Enter Your Address" id="address" class="form-control" value=""> 
              <!-- <input type="text" class="form-control" id="address" placeholder="Enter Your Address" name="address"  value="{{ old('address')}}" onfocusout="GetLocation()"> -->
            </div>
            <!-- <div class="form-group">
              <label>Address2</label><span>*</span>
              <input type="text" name="address2" placeholder="Enter Your Address2" id="address2" class="form-control" value=""> 
            </div> -->
            <input type="hidden" name="latitude" id="latitude" value="">
            <input type="hidden" name="longitude" id="longitude" value="">

             <label class="Agree-msg"><input type="checkbox" name="trmchk" class="chmgr" >I Agree To The <a href="#">Terms And Conditions</a></label>
             <input type="hidden" name="user_role" value="worker" id="user_role" class="form-control">
             <div class="submit-button">
            <button class="smb-btn" type="submit">Submit</button>
       
          </div>
           
          </form>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmECi5THi1W3CHDdmC2bI6q6dDZNRAY-I"></script> 
<script type="text/javascript">
$("#referralcode").focusout(function(){
    var referralcode = $("#referralcode").val();
    var clientpay = $("#clientpay").val();
    jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/checkrefferalcode') }}",
            data:{'referralcode':referralcode, 'clientpay':clientpay},
            success :  function(resp) {
              data=jQuery.parseJSON(resp);
              if(data.check_status==2) {
                var myTotal = data.refferal_total_amount;
                $("#totalamt").html('$'+myTotal);

              $("#referral_section").html('<div class="estim-left e"><h4>Refferal Discount</h4><p id = "refferalamttxt"></p></div><div class="estim-right"><h4 id="refferalamt">$20</h4></div>');
              $("#refferaltotalamt").text('$'+total_value);
              alert('Enter Refferal Code Amount Applied in your total payment');
            } else {

              alert('Enter Refferal Code Invalid');
              return false;
             }
            }
       });
    
    
});
</script>
  <script>
  $( function() {
    $( "#jobdate" ).datepicker({
      showweek : true,
      minDate: new Date(), 
     
    });

  } );
  </script>
  
<script>
$(document).ready(function() {
    $('.autoplay').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
});
});
</script>  
  
  <script>
$(document).ready(function() {


  $("#job_type").change(function(){
    var sel_option = $(this).find('option:selected');
    var cat_id = sel_option.val();
    jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/getequipment') }}",
            data:{'cat_id':cat_id},
            success :  function(resp) {
             $("#equipment_sec").html(resp);    
             }
       });
 
});


  $.validator.addMethod("alphaLetterNumber", function(value, element) {
     return this.optional(element) || value == value.match(/^[a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });

  $.validator.addMethod("alphaLetterNumberSpace", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });

  $.validator.addMethod("alphaLetter", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });
     $.validator.addMethod("valueNotEquals", function(value, element, arg){
     return arg !== value;
  }, "Value must not equal arg.");

$( "#saveuser_sec" ).validate({
  rules: {
                firstname: {
                    required: true,
                    alphaLetter:true,
                    minlength: 2,
                    maxlength: 150
                },
                lastname: {
                    required: true,
                    alphaLetter: true,
                    minlength: 2,
                    maxlength: 60
                },
                password: {
                    required: true,
                    minlength: 2,
                    maxlength: 15
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: "{{ url('checkuseremail') }}",
                        type: "GET"
                    }
                },
                confpwd: {
                    required: true,
                    minlength: 6,
                    maxlength: 15,
                    equalTo: "#passwordnew"
                },
                address: {
                    required: true,
                    minlength: 6,
                    maxlength: 60
                },
                /*address2: {
                    required: true,
                    minlength: 6,
                    maxlength: 60
                },*/
                trmchk:{
                  required:true
                }
  },
  messages: {
              firstname: {
                  required: "Please enter your firstname.",
                   alphaLetter:"Only letters are allowed",
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 150 characters allowed."
                },
              lastname: {
                  required: "Please enter your lastname.",
                  alphaLetter:"Only letters are allowed",
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 60 characters allowed."
                },
             
              email: {
                  required: "Please enter your email.",
                  email: "Enter valid email.",
                  remote: "Email already used."                 
                },
          
                 password: {
                  required: "Please enter password.",
                  minlength: "Minimum 6 characters required.",
                  maxlength: "Maximum 15 characters allowed."
                },
                confpwd: {
                  required: "Please enter confirm password.",
                  minlength: "Minimum 6 characters required.",
                  maxlength: "Maximum 15 characters allowed.",
                  equalTo: "Password and confirm password must be same."
                },
                address: {
                  required: "Please enter your Address.",
                  minlength: "Minimum 6 characters required.",
                  maxlength: "Maximum 60 characters allowed."

                },
                /*address2: {
                  required: "Please enter your Address.",
                  minlength: "Minimum 6 characters required.",
                  maxlength: "Maximum 60 characters allowed."

                },*/
                trmchk:{
                  required:"Agree terms and conditions."
                }
              },
         });

  /*$( "#forget_password" ).validate({
  rules: { email: {
                    required: true,
                    email: true
                }
           },
  messages: {  email: {
                  required: "Please enter your email.",
                  email: "Enter valid email"
                
                }
              },
         });*/

$( "#login" ).validate({
  rules: { email: {
                  required: true,
                  email: true,
                  remote: {
                        url: "{{ url('checkloginemail') }}",
                        type: "GET",
                        global: false,
                    }
                },
                password: {
                    required: true,
                    minlength: 2,
                    maxlength: 15
                    /*remote: {
                        url: "{{ url('checkloginpassword') }}",
                        type: "GET"
                    }*/
                }
           },
  messages: {  email: {
                  required: "Please enter your email.",
                  email: "Enter valid email",
                  remote: "These Email Not Registered"
                 
                },
          
                 password: {
                  required: "Please enter password.",
                  minlength: "Minimum 6 characters required.",
                  maxlength: "Maximum 15 characters allowed."
                  /*remote: "These Password Not Match"*/
                }
              },
         });

 var step1_form = $("#saveuser").validate({
        rules: {
                firstname: {
                    required: true,
                    alphaLetter:true,
                    minlength: 2,
                    maxlength: 150
                },
                lastname: {
                    required: true,
                    alphaLetter: true,
                    minlength: 2,
                    maxlength: 60
                },
                password: {
                    required: true,
                    minlength: 2,
                    maxlength: 15
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: "{{ url('checkuseremail') }}",
                        type: "GET"
                    }
                },
                
                confpwd: {
                    required: true,
                    minlength: 6,
                    maxlength: 15,
                    equalTo: "#password"
                },
                location: {
                    required: true,
                    minlength: 2,
                    maxlength: 150
                }      
            },
        messages: {
              firstname: {
                  required: "Please enter your firstname.",
                  alphaLetter:"Only letters are allowed",
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 150 characters allowed."
                },
              lastname: {
                  required: "Please enter your lastname.",
                  alphaLetter:"Only letters are allowed",
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 60 characters allowed."
                },
             
              email: {
                  required: "Please enter your email.",
                  email: "Enter valid email.",
                  remote: "Email already used."
                },
          
                 password: {
                  required: "Please enter password.",
                  minlength: "Minimum 6 characters required.",
                  maxlength: "Maximum 15 characters allowed."
                },
                confpwd: {
                  required: "Please enter confirm password.",
                  minlength: "Minimum 6 characters required.",
                  maxlength: "Maximum 15 characters allowed.",
                  equalTo: "Password and confirm password must be same."
                },
                location: {
                  required: "This field is required.",
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 150 characters allowed."
                },
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

  var step2_form = $("#savedetails").validate({
        rules: {
            job_type: {
                    valueNotEquals: "default" 
                },
            collegewrk: {
                    valueNotEquals: "default"
                },
/*            workertype: {
                    required:true
                }, */       
            /*street: {
                    required: true
                },*/
            address: {
                    required: true
                },
            about_job: {
                    required: true
                }
            },
        messages: {
            
            job_type: {
                  valueNotEquals: "Please select type of job."
                },
                collegewrk: {
                  valueNotEquals: "Please select number of worker(s)."
                },
/*                workertype: {
                  required: "Please select worker type."
                },*/
                /*street: {
                  required: "Enter your street address."
                },*/
                address: {
                  required: "Enter your address."
                },
                about_job: {
                  required: "Enter something about job."
                }

            },
        submitHandler: function(form) {
            form.submit();
          }
        });

    var step3_form = $("#savetimings").validate({
        rules: {
            jobdate: {
                    required: true 
                },
            jobtime: {
                    required: true
                },
            estimatehrs: {
                  required:true,
                  number:true
                }
            },
        messages: {
            
            jobdate: {
                  required: "Please confirm your date"
                },
            jobtime: {
                    required: "Please select your time."
                },
            estimatehrs: {
                  required: "Please enter estimate hours.",
                  number: "Please enter only number."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

  var step4_form = $("#paymentFrm").validate({
        rules: {
            paymentname: {
                required: true 
                },
            paymentemail: {
                required: true
                },
            card_num: {
                required:true,
                minlength:16,
                maxlength:20
                },
            cvc:{
                required:true,
                maxlength:3
            },
            exp_month:{
                required: true,
                minlength:2
            },
            exp_year:{
                required : true,
                maxlength: 4
            },
            /*referralcode: {
              required : true,
              remote:{
                     url: "{{ url('/checkrefferalcode') }}",
                     type: "GET"
                    } 
            },*/
            trmchk:{
              required:true
            }
            },
        messages: {
            
            paymentname: {
                  required: "Please enter your name."
                },
            paymentemail: {
                    required: "Please enter your email."
                },
            card_num: {
                  required: "Please enter 16-digits card number.",
                  minlength: "Minimum 16-digits are required.",
                  maxlength: "Maximum 20-digits are required."
                },
            cvc: {
                  required: "Please enter CVC number.",
                  maxlength: "Maximum 3-digits are required."
                },
            exp_month: {
                  required: "Please enter expiry month of card.",
                  minlength: "Minimum 2-digits are required."
                },
            exp_year: {
                  required: "Please enter expiry year of card.",
                  maxlength: "Maximum 4-digits are required."
                },
            /*referralcode: {
                  required: "Please enter Refferal code",
                  remote: "Enter Code is Invalid" 
            },*/
            trmchk: {
              required:"Agree term and conditions."
            }
            },
        submitHandler: function(form) {
            //form.submit(); //== commented 15-02-2019
          }
        });  

  $("#step1").click(function() {
       if (step1_form.form()) {

           var firstname    = jQuery('#firstname').val();
           var lastname     = jQuery('#lastname').val();
           var email        = jQuery('#email').val();
           var password     = jQuery('#password').val();
           var user_role     = jQuery('#user_role').val();
           jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/storeclient') }}",
            data:{ 'firstname': firstname, 'lastname': lastname,'email': email ,'password': password,'user_role': user_role },
            success :  function(resp) {
              if(resp > 0){
                $('input[name="userid"]').val(resp);
                sendEvent('#demo-modal-3', 2);
              }

           }
           });
      }
    });

   $("#step2").click(function() {

        var myCheckboxes = new Array();
        $(".che_sec:checked").each(function() {
           myCheckboxes.push($(this).val());
        });
       var equ_ids = myCheckboxes.join();
      if (step2_form.form()) {
           var userid         = jQuery('#userid').val();
           var job_type       = jQuery('#job_type').val();
           var worker_type    = jQuery('.workertype').val();
          /* var street         = jQuery('#street').val();*/
           var address1       = jQuery('#address').val(); 
           var job_location   = jQuery('#street').val() +','+ jQuery('#address').val();
           var job_desc       = jQuery('#about_job').val();
           var worker_required      = jQuery('#collegewrk').val();
           var worker_desc          = jQuery('#sweeps_desc').val();
           var equipment_needed     = equ_ids;

           jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/jobstore') }}",
            data:{'userid':userid,'job_type': job_type,'address1': address1, 'job_location': job_location,'job_desc': job_desc ,'worker_type':worker_type,'worker_required': worker_required,'worker_desc':worker_desc, 'equipment_needed': equipment_needed},
            success :  function(resp) {
              if(resp){
                $('input[name="lastjobid"]').val(resp);                 
                sendEvent('#demo-modal-3', 3);
              }else{
                alert('error on response');
              }

           }
       });
  
      }
    });

    $("#step3").click(function() {
      if (step3_form.form()) {
           var userid         = jQuery('#userid').val();
           var jobid          = jQuery('#lastjobid').val();
           var start_date     = jQuery('#jobdate').val() ;
           var start_time     = jQuery('.job_time:checked').val();
           var time_estimate  = jQuery('#estimatehrs').val();
           var scheduling_details  = jQuery('#scheduling_details').val();
           var additional_info = jQuery('#additional_info').val();
           //alert(referralcode);
           var amount =''; 
          jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/updatejob') }}",
            data:{'userid':userid,'jobid': jobid, 'start_date': start_date,'start_time': start_time ,'time_estimate': time_estimate,'scheduling_details':scheduling_details, 'additional_info': additional_info},
            success :  function(resp) {
               var obj = jQuery.parseJSON(resp);
               $("#jobaddr").html(obj.job_location); 
               $("#fnldate").html(obj.start_date + ' at ' + obj.start_time);

              /*if(obj.worker_type == 'gold'){
                var amount = obj.worker_required * obj.time_estimate * 25;
                $("#workeramttxt").html('1 Gold Worker for 1 hours @ $25/hour');
              }else if(obj.worker_type == 'silver'){
                var amount = obj.worker_required * obj.time_estimate * 20;
                $("#workeramttxt").html('1 Silver Worker for 1 hours @ $20/hour');*/
             /* }else{*/
                var amount = obj.worker_required * obj.time_estimate * 35;
                $("#workeramttxt").html('1 Worker for 1 hours @ $35/hour');
              /*}*/ 
                $("#workeramt").html('$'+amount);
                $("#equipmentamt").html('$'+obj.equipmentamount);

                  var totalcost = amount+obj.equipmentamount;
                 $("#totalamt").html('$'+totalcost);
                 $('input[name="clientpay"]').val(totalcost);

                sendEvent('#demo-modal-3', 4);
             

           }
       });
     
      }
    });
 
  $("#payBtn").click(function() {
       if (step4_form.form()) {

          /*Stripe.createToken({
            number: $('.card-number').val(),
            cvc: $('.card-cvc').val(),
            exp_month: $('.card-expiry-month').val(),
            exp_year: $('.card-expiry-year').val()
            }, stripeResponseHandler); */
            $('.stripPaymentButton').trigger('click');

      }
    });  
 

    $('#datePicker')
        .datepicker({
            format: 'mm/dd/yyyy'
        })
        .on('changeDate', function(e) {
            // Revalidate the date field
            $('#eventForm').formValidation('revalidateField', 'date');
        });

    $('#eventForm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your name'
                    }
                }
            },
            date: {
                validators: {
                    notEmpty: {
                        message: 'The date is required'
                    },
                    date: {
                        format: 'MM/DD/YYYY',
                        message: 'The date is not a valid'
                    }
                }
            }
        }
    });

    
});
</script>

<script>

/*Stripe.setPublishableKey('{{env('STRIPE_PUB_KEY')}}');

//callback to handle the response from stripe
function stripeResponseHandler(status, response) {

    if (response.error) {
         //enable the submit button
        //$('#payBtn').removeAttr("disabled");
        //display the errors on the form
        $(".payment-errors").html(response.error.message);
    } else {
        var form$ = $("#paymentFrm");
        //get token id
        var token = response['id'];
        //insert the token into the form
        form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
        //submit form to the server

           var stripeToken  = jQuery('input[name="stripeToken"]').val();
           var userid       = jQuery('#userid').val();
           var lastjobid    = jQuery('#lastjobid').val();
           var clientpay    = jQuery('.clientpay').val();
           var name         = jQuery('.paymentname').val();
           var email        = jQuery('.paymentemail').val();
           var cardnumber   = jQuery('.card-number').val();
           var cardcvc      = jQuery('.card-cvc').val();
           var cardexpirymonth      = jQuery('.card-expiry-month').val();
           var cardexpiryyear       = jQuery('.card-expiry-year').val();
           
           jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/reciept') }}",
            data:{ 'stripeToken': stripeToken, 'userid': userid, 'lastjobid':lastjobid,'clientpay':clientpay,'paymentname': name ,'paymentemail': email,'card_num': cardnumber ,'cvc': cardcvc,'exp_month': cardexpirymonth ,'exp_year': cardexpiryyear },
            success :  function(resp) {
       
               sendEvent('#demo-modal-3', 5);
           }
          });
        //form$.get(0).submit();
    }
} */

</script>

<script src="{{ url('/public') }}/js/multi-step-modal.js"></script>
<script type="text/javascript">
$( "#forget_password" ).validate({
  rules: { email: {
                    required: true,
                    email: true,
                    remote:{
                     url: "{{ url('/checkforgetemail') }}",
                     type: "GET"
                    }
                }
           },
  messages: {  email: {
                  required: "Please enter your email.",
                  email: "Enter valid email",
                  remote : "Email not exists in database ."
                
                }
              },
          submitHandler: function(form) {
            var forgetemail = $('#forgetemail').val();
            jQuery.ajax({
                 method : 'GET',
                 url  : "{{ url('/forgetpassword') }}",
                 data:{'forgetemail':forgetemail},
                 success :  function(resp) {
                  if(resp=='true'){
                    alert('Your Details are send to your email.');
                  }
                  else{
                    alert('Some problem exists. Please try again.');
                  }
/*                   if(resp > 0){
                      $('input[name="lastjobid"]').val(resp);                 
                      sendEvent('#demo-modal-3', 3);
                   }*/
                 }
            });
          } 
});
</script>
<script>
sendEvent = function(sel, step) {
    $(sel).trigger('next.m.' + step);
}
</script>
<script type="text/javascript">
$(document).ready(function(){
  $('.launch-modal_before_login').click(function(){
    $('#demo-modal-3').modal({
      backdrop: 'static'
    });
  }); 
  $('.launch-modal_after_login').click(function(){
    $('#demo-modal-4').modal({
      backdrop: 'static'
    });
  });
});
</script>

<style type="text/css">
/*.panel-body {height:auto !important;}*/
.google_plus_btn {
    padding: 15px 172px 15px 172px;
    font-size: 18px;
    color: #fff;
    font-weight: bold;
    border-radius: 5px;
    display:inline-block;
}
.google_plus_btn:hover {
    color: #fff;
    text-decoration: none;
}
.google_plus_btn_sec {
    text-align: center;
    
}
.empty_space {
    padding-top: 40px;
}
.che_sec {
    height: 13px !important;
}
input[type="checkbox"] {margin-top:2px;}
/*.facebook_btn {
    background-color: #2d4373 !important;
    padding: 15px 162px 15px 162px;
    border-radius: 5px;
    color: #fff;
}*/
.facebook_btn {
    background-color: #2d4373 !important;
    padding: 15px 170px 15px 170px;
    border-radius: 5px;
    color: #fff;
    font-size: 17px;
    font-weight: 600;
}
.facebook_btn:hover{
  color: #fff;
}
.facebook_btn_sec {
    text-align: center;
    margin-top: 20px;
    margin-bottom: 20px;
}
.thank_you_img_icon {
    text-align: center;
}
.thank_you_img_icon img {
    width: 60px;
}
.thank_you_head {
    text-align: center;
    font-weight: bold;
    color: #1ba4e0;
}
.close_btn_sec {
    text-align: center;
    margin-top: 20px;
}
.close_btn_sec .cls-btn {
    background: #1ba4e0;
    color: #fff;
    font-weight: bold;
}
.thank_you_text {
    text-align: center;
    color: #000 !important;
    font-size: 20px !important;
}
 .new_cont {text-align: center;margin-top: 5%;}.thanky_sec {color: #ff8638;}.colr_text {color: #ff8638;}.back_home_btn { background: #ff8638;color: #fff;text-decoration: navajowhite; padding: 9px 17px; border-radius: 26px; font-weight: 500;}
.dropdown-customer {
    padding: 5px 0px;
    background: #fff;
    -webkit-box-shadow: 0 0 20px 0 rgba(0,0,0,.3);
    box-shadow: 0 0 20px 0 rgba(0,0,0,.3);
    border-radius: 5px;
    border: none;
    opacity: 1;
    position: absolute;
    left: auto;
    right: 0px;
    visibility: visible;
    overflow: visible;
    padding: 0px 0;
    -webkit-transform: translateY(20px);
    transform: translateY(20px);
    width:100%;
}
.dropdown-customer:before {
    content: "";
    border: 10px solid transparent;
    border-left-color: #fff;
    border-top-color: #fff;
    position: absolute;
    border-radius: 3px;
    top: -8px;
    right: 15px;
    -webkit-box-shadow: -5px -5px 12px -2px rgba(0,0,0,.3);
    box-shadow: -4px -4px 12px -2px rgba(0,0,0,.3);
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg);
    z-index: 1002;
}
.dropdown-customer li {
    display: block;
    padding: 0px 0px;
    border-bottom: 1px solid #eee;
    border-radius: 5px;
}
.dropdown-customer li a {
    padding: 10px 15px;
    border-radius: 3px 3px 3px 3px;
    
}
.dropdown-customer {
    left: -85px !important;
    top: 45px !important;
    border-radius: 5px !important;
}
.help-block strong {
    color: #ff0000;
    font-size: 13px;
    font-weight: 600;
    font-style: italic;
}
.bluemainbtn {
    background: #12b6d9;
    color: #fff;
    font-size: 16px;
    padding: 10px;
    display: block;
    margin: 0 auto;
    border: none;
    border-radius: 4px;
    margin-top: 15px;
    margin-bottom: 7px;
    font-weight: bold;
    width: 100%;
}
.redmainbtn {
    background: rgb(234, 67, 53);
    color: #fff;
    font-size: 16px;
    padding: 10px;
    display: block;
    margin: 0 auto;
    border: none;
    border-radius: 4px;
    margin-top: 15px;
    margin-bottom: 7px;
    font-weight: bold;
    width: 100%;
}
.redmainbtn:hover, .redmainbtn:focus, .redmainbtn:active {color: #fff;text-decoration:none;}
.skymainbtn {
    background: #2d4373;
    color: #fff;
    font-size: 16px;
    padding: 10px;
    display: block;
    margin: 0 auto;
    border: none;
    border-radius: 4px;
    margin-top: 15px;
    margin-bottom: 7px;
    font-weight: bold;
    width: 100%;
}
.skymainbtn:hover, .redmainbtn:focus, .redmainbtn:active {color: #fff;text-decoration:none;}
.chmgr {
    margin-top: 3px !important;
}
@media screen and (max-width:575px) {
    .bluemainbtn {font-size: 14px;}
    .redmainbtn {font-size: 14px;}
    .skymainbtn {font-size: 14px;}
}

.StripeElement {
  background-color: white;
  height: 40px;
  padding: 10px 12px;
  border-radius: 4px;
  border: 1px solid transparent;
  box-shadow: 0 1px 3px 0 #e6ebf1;
  -webkit-transition: box-shadow 150ms ease;
  transition: box-shadow 150ms ease;
}

.StripeElement--focus {
  box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid {
  border-color: #fa755a;
}

.StripeElement--webkit-autofill {
  background-color: #fefde5 !important;
}

 </style>
<script>
// Create a Stripe client.
var stripe = Stripe('{{env('STRIPE_PUB_KEY')}}'); 

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    lineHeight: '18px',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission.
var form = document.getElementById('paymentFrm');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

// Submit the form with the token ID.
function stripeTokenHandler(token) { 
  // Insert the token ID into the form so it gets submitted to the server
  //alert( token );
  //console.log( token );
  var form = document.getElementById('paymentFrm');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);
  //===============================//
    var userid = $('#userid').val();
    var lastjobid = $('#lastjobid').val();
    var paymentname = $('.paymentname').val();
    var paymentemail = $('.paymentemail').val();
    var hear_from = $('.hear_from').val();
    var promo_code = $('.promo_code').val();
    var totalamt = $('input[name="clientpay"]').val();
      jQuery.ajax({ 
            method : 'GET',
            url  : "{{ url('/createuserstripe') }}",
            data:{'stripe_token': token.id, 'userid' : userid, 'lastjobid' : lastjobid, 'paymentname' : paymentname,  'hear_from' : hear_from, 'promo_code' : promo_code ,'paymentemail' : paymentemail, 'totalamt' : totalamt },
            success :  function(resp) {
              //alert( resp );
              console.log(resp);
              if( resp == 'success' ){
                sendEvent('#demo-modal-3', 5);
              }else{
                 alert( 'Something went wrong. please try again.' ); 
              }
              
            },
            error : function(resp){
              alert( 'Something went wrong. please try again.' ); 
              console.log(resp);
            }
      });
 //==============================//
  // Submit the form
  //form.submit();
}
</script>