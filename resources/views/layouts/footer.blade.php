<?php
$html = file_get_contents('https://www.instagram.com/collegewrk_/');

            $html = strstr($html,'window._sharedData = ')   ;
            $html = explode("</script>", $html);
            $html = $html[0];
            $html = str_replace("window._sharedData = ","",$html);
            $html = strstr($html,'"edge_owner_to_timeline_media');
            $html = explode(',"edge_saved_media"',$html);
            $html = '{'.$html[0].'}';
            $html = json_decode($html,true);
            $image_count = count($html['edge_owner_to_timeline_media']['edges']);

 ?>
 <div class="container">
    <div class="row ft-tp-row">
         <?php
         $bussinesssetting_info = DB::table('bussinesssetting')
                      ->first();
                      ?>
      <div class="col-md-3 col-sm-6 follow-foot wow fadeInLeft">
        <h2>College<span>WRK</span></h2>
        <ul class="footer-address">
        <li>
<a href="tel:{{ $bussinesssetting_info->phone }}"><i class="fa fa-phone" aria-hidden="true"></i>{{ $bussinesssetting_info->phone }}</a></li>
        <li><a href="mailto:{{ $bussinesssetting_info->emails }}"><i class="fa fa-envelope-o" aria-hidden="true"></i>{{ $bussinesssetting_info->emails }}</a></li>
        <!-- <li><i class="fa fa-home" aria-hidden="true"></i>{{ $bussinesssetting_info->address }}</li> -->
        </ul>
        <h3>Follow Us</h3>
        <ul class="social-link">
          <li><a href="{{ $bussinesssetting_info->facebook_url }}" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a></li>
         <li><a href="{{ $bussinesssetting_info->twitter_url }}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i> Twitter</a></li>
         <li><a href="{{ $bussinesssetting_info->google_url }}" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i>Google Plus</a></li>
              <li><a href="{{ $bussinesssetting_info->linkedin_url }}" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i>Linkedin</a></li>
        </ul>
      </div>
      <div class="col-md-3 col-sm-6  wow fadeInLeft">
        <h2>Services</h2>
        <ul class="site-links">
          <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Moving</a></li>
          <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Yard Work</a></li>
          <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Events</a></li>
          <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Cleaning</a></li>
          <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Painting</a></li>
          <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Tutoring</a></li>
          <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Technology</a></li>
          <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Odd Jobs</a></li>
        </ul>

      </div>
      <div class="col-md-3 col-sm-6 wow fadeInRight">
        <h2>News Updates</h2>
        <ul class="news-links">
          <li>EVERYDAY ESSENTIALS. Add some colour to your outfit. Bold | Blue | Pure silk Online now… <br><a href="#">https://t.co/fKrJCwNIrx</a><br>4 Days Ago</li>
          <li>EVERYDAY ESSENTIALS. Add some colour to your outfit. Bold | Blue | Pure silk Online now…<br> <a href="#">https://t.co/fKrJCwNIrx</a><br>4 Days Ago</li>
          <li>EVERYDAY ESSENTIALS. Add some colour to your outfit. Bold | Blue | Pure silk Online now…<br><a href="#">https://t.co/fKrJCwNIrx</a><br>4 Days Ago</li>
        </ul>
      </div>
      <div class="col-md-3 col-sm-6 wow fadeInRight">
        <h2>Instagram</h2>
        <ul class="insta-images">
          <?php
          for($i=0; $i<$image_count; $i++){
             $instsrc = $html['edge_owner_to_timeline_media']['edges'][$i]['node']['thumbnail_resources'][0]['src'];
             ?>
             
             <li><a href="#"><img src="{{$instsrc}}" class="img-responsive" alt=""></a></li>
            <?php
            }
          ?>
          

          </ul>
      </div>
    </div>
  </div>
    <div class="copyright-section">
      <div class="container">
    <div class="row ">
      <div class="col-md-8">
        <h2>Contact Us</h2>
        <p>{{ $bussinesssetting_info->about_us }}</p>
      </div>
       <div class="col-md-4">
        <ul class="list-inline copright-text">
          <li>© 2019 CollegeWRK</li>
          <li>Designed By:<a href="http://www.binarydata.in/" target="_blank">Binary Data</a></li>
        </ul>
      </div>
    </div>
  </div>
  </div>


<div class="overlay">
    <div class="overlay-content">
        <p style="text-align: center" ><i class="fa fa-spinner fa-spin"></i></p>
    </div>
</div>

<script type="text/javascript">
    $(document).ajaxStart(function() {
        $('.overlay').show();
    }).ajaxStop(function() {
        $('.overlay').hide();
    });
</script> 
<style>
.overlay {
  /* Height & width depends on how you want to reveal the overlay (see JS below) */    
  height: 100%;
  width: 100%;
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  display: none;
  z-index: 9999999;
  background-color: rgb(0,0,0); /* Black fallback color */
  background-color: rgba(0,0,0, 0.8); /* Black w/opacity */
  overflow-x: hidden; /* Disable horizontal scroll */
  transition: 0.5s; /* 0.5 second transition effect to slide in or slide down the overlay (height or width, depending on reveal) */
}

/* Position the content inside the overlay */
.overlay-content{
  position: relative;
  top: 35%; /* 25% from the top */
  width: 100%; /* 100% width */
  text-align: center; /* Centered text/links */
  margin-top: 30px; /* 30px top margin to avoid conflict with the close button on smaller screens */
  
}
.overlay-content i{
  font-size: 50px;
  color: #fff !important; 
}
</style>