<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="msapplication-TileColor" content="#e24416">
    <meta name="theme-color" content="#ffffff">
    <link rel="apple-touch-icon" sizes="180x180" href="{{url('/public')}}/favicon-icon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{url('/public')}}/favicon-icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('/public')}}/favicon-icon/favicon-16x16.png">
    <link rel="manifest" href="{{url('/public')}}/favicon-icon/site.webmanifest">
    <link rel="mask-icon" href="{{url('/public')}}/favicon-icon/safari-pinned-tab.svg" color="#e17200">


    <!--  {{ Html::favicon( '/public/images/fav_icon.png' ) }} -->

    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
     <link href="{{ url('/public') }}/css/bootstrap.min.css" rel="stylesheet">
     <link href="{{ url('/public') }}/css/style.css" rel="stylesheet">
     <link href="{{ url('/public') }}/css/pricing-style.css" rel="stylesheet">
     <link href="{{ url('/public') }}/css/aboutus.css" rel="stylesheet">
     <link href="{{ url('/public') }}/css/animate.css" rel="stylesheet">
     <link href="{{ url('/public') }}/css/newstyle.css" rel="stylesheet">
     <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800" rel="stylesheet">
     <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
     <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
     <link href="https://fonts.googleapis.com/css?family=Charmonman|Dancing+Script" rel="stylesheet">
     
     <script src="{{ url('/public') }}/js/jquery.min.js"></script>
     <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
     <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
     <script src="{{ url('/public') }}/js/jquery.validate.min.js"></script>
     <script src="{{ url('/public') }}/js/jquery-validate.bootstrap-tooltip.min.js"></script>
     <script src="{{ url('/public') }}/js/bootstrap.min.js"></script>
     <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet">
<!--      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script> -->
     <script src="{{ url('/public') }}/js/wow.js"></script>
     <script src="{{ url('/public') }}/js/multi-step-modal.js"></script>
<!--      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" /> -->
     
  </head>

<body>

 <header>@include('layouts.header')</header>
 <main> @yield('content') </main>
 <footer>@include('layouts.footer') </footer> 

</body>


</html>

