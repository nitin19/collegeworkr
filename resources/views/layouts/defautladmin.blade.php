<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<title>@yield('title')</title>
   
    <link rel="stylesheet" href="{{ url('/public') }}/admin/css/bootstrap.min.css">
     <link rel="stylesheet" href="{{ url('/public') }}/admin/css/dashoboard.css">
    
    <link rel="stylesheet" href="{{ url('/public') }}/admin/css/font-awesome.min.css">
   
    <link rel="stylesheet" href="{{ url('/public') }}/admin/scss/style.css">
<link rel="stylesheet" href="{{ url('/public/admin/css/') }}/profile.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <script src="{{ url('/public') }}/admin/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="{{ url('/public') }}/admin/js/plugins.js"></script>
    <script src="{{ url('/public') }}/admin/js/main.js"></script>

<script type="text/javascript" src="{{url('/public/admin/js/')}}/jquery.validate.min.js"></script>

    <script src="{{ url('/public') }}/admin/js/lib/chart-js/Chart.bundle.js"></script>
    <script src="{{ url('/public') }}/admin/js/dashboard.js"></script>
    <script src="{{ url('/public') }}/admin/js/widgets.js"></script>
    <script src="{{ url('/public') }}/admin/js/lib/vector-map/jquery.vmap.js"></script>
    <script src="{{ url('/public') }}/admin/js/lib/vector-map/jquery.vmap.min.js"></script>
    <script src="{{ url('/public') }}/admin/js/lib/vector-map/jquery.vmap.sampledata.js"></script>
    <script src="{{ url('/public') }}/admin/js/lib/vector-map/country/jquery.vmap.world.js"></script>
    <script>
        ( function ( $ ) {
            "use strict";

            jQuery( '#vmap' ).vectorMap( {
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#1de9b6',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: [ '#1de9b6', '#03a9f5' ],
                normalizeFunction: 'polynomial'
            } );
        } )( jQuery );
    </script>
</head>
<body>
@include('layouts.sidebar_admin')
 @include('layouts.header_admin')
 <main> @yield('content')
 </main>

<!-- <footer>@include('layouts.footer_admin') </footer> -->

</body>

</html>
<?php
$userid = Auth::id();
$userinfo = DB::table('users')->where('id', '=', $userid)->first();
$userrole = $userinfo->user_role;
if($userrole!='admin') {
    if($userrole=='customer'){
    $site_url = url("/customerdashboard");
    ?>
    <script>
        window.location.href='<?php echo $site_url; ?>';
    </script>
    <?php
    }
    else if($userrole=='worker'){
    $site_url = url("/workerjobs");
    ?>
    <script>
        window.location.href='<?php echo $site_url; ?>';
    </script>
    <?php
    }
    else {
        $site_url = url("/");
    ?>
    <script>
        window.location.href='<?php echo $site_url; ?>';
    </script>
    <?php
    }
}
?>
