<!--<script type="text/javascript" src="https://js.stripe.com/v2/"></script> -->
<script src="https://js.stripe.com/v3/"></script>
<section class="dashboard_header">
  <div class="container-fluid">
    <div class="row">
    <div class="col-sm-3 logodiv">
      <a href ="{{url('/')}}"><img src="{{url('/public')}}/images/newlogo.png" class="img-responsive"></a>
    </div>
    <div class="col-sm-9 dashboard_rgtheader">
      <div class="col-sm-4 col-xs-12 header_left">
       <h3>Customer Dashboard</h3>
        
      </div>
      <div class="mobile_navdiv">
      <div class="col-sm-8 col-xs-9 header_right text-right">
        
        
        <div class="col-sm-12 ma-hu">
         <?php  $id = Auth::user()->id;
          /* code commented 06-02-2019 
          $count_total = DB::table('notification')
                      ->where('client_userid', '=', $id)
                      ->where('job_status', '=', 'hired')
                      ->where('status_read', '=', 0)
                      ->get(); */ 
                       $count_total = DB::table('new_notifications')
                      ->where('client_id', '=', $id)
                      ->where('read_by_client', '=', 0)
                      ->get();
?>
            <ul class="dashboardright">
              <li class="custdshbrd"><a href="{{url('workerjobs')}}" class="btn btn-lg">WRK<span style="text-transform: lowercase;">er</span> Dashboard</a></li>
             <li class="notification"> 

           <div class="dropdown">
          <!-- <button class="dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-globe" ></i></span></button> -->
          <button class="dropdown-toggle headerNotification" type="button" data-toggle="dropdown"><img src="{{url('/public')}}/images/nitification-icon.png" class="motiing"></span></button>
          <ul class="dropdown-menu notifaction-colwrk">
              <div class="headnoti">
                  Notification
              </div>
              <div class="notibody">
            @if(count($count_total) > 0)
            @foreach($count_total as $notify)
             <li><a href="<?php if( $notify->notification_redirect_url !='' ): echo url($notify->notification_redirect_url);  endif;?>" id="notity" class="title " notify="{{$notify->notification_id}}" data-redirecturl="<?php if( $notify->notification_redirect_url !='' ): echo url($notify->notification_redirect_url);  endif;?>"><?php echo $notify->message; ?></a></li>
            @endforeach
            @endif
            </div>
          </ul>
        </div>
     <span class="counter">{{count($count_total)}}</span>
     
   </li>
     <li class="dropdown prflimgdiv">
      @if(Auth::user()->image !='')
         <img src="{{url('/public')}}/uploads/profile/{{Auth::user()->image}}" class="profilpic">
      @else
         <img src="{{url('/public')}}/images/avatar.jpg" class="profilpic">
      @endif
     </li>
    @if(Auth::check())

     <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->firstname }}<span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-customer" role="menu">
                        <li>
                            <a href="{{url('/')}}/customerdashboard" class="logount_hed_sec">
                                Dashboard
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();" class="logount_hed_sec">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="dropdown prflimgdiv last-client">
                  <a class="cutmer-dash-tog"><i class="fa fa-bars" aria-hidden="true"></i></a>
                </li>
    @endif
     </ul>
   </div>
    </div>
    <div class="col-xs-3 nav-toggle">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
    <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
  </button>
</div>
</div>
    </div>
  </div>
  </div>
</section>
<section class="dashboard-main">
  <div class="container-fluid">
    <div class="row dashboard-rw">
      <div class="col-md-3 sidebar-left nopadding" id="nav-div1">
          <nav class="nav-sidebar">
          <ul class="nav tabs">
            <?php   $id = Auth::user()->id; ?>
                <li class=""><a href="{{url('customerdashboard')}}" ><i class="fa fa-briefcase" aria-hidden="true"></i><span>Dashboard</span></a></li>
                
                <li class=""><a href="{{url('/customernotification')}}""><i class="fa fa-user" aria-hidden="true"></i>Current jobs</a></li>
                <!-- <li class=""><a href="{{url('customerpay')}}"><i class="fa fa-usd" aria-hidden="true"></i>CollegeWRK Pay</a></li> -->
                <li class=""><a href="{{url('customerpayment')}}"><i class="fa fa-usd" aria-hidden="true"></i>Payment</a></li>
                <li class=""><a href="{{url('customershare')}}"><i class="fa fa-share-alt" aria-hidden="true"></i>Share CollegeWRK</a></li>
                <li class=""><a href="{{url('customerjobhistory')}}"><i class="fa fa-clock-o" aria-hidden="true"></i>Job History</a></li>
                <!-- <li class=""><a href="{{url('customergift')}}"><i class="fa fa-gift" aria-hidden="true"></i>Gift Certificate</a></li>  -->
                <li class=""><a href="{{route('customerdashboard.edit',$id)}}""><i class="fa fa-user" aria-hidden="true"></i>Profile</a></li>
                <!--<li class=""><a href="{{ url('customerhelp') }}"><i class="fa fa-question-circle" aria-hidden="true"></i>Help </a></li> -->
                <li class=""><a href="{{ url('customeradminchat') }}"><i class="fa fa-question-circle" aria-hidden="true"></i>Help </a></li> 
          </ul>
        </nav>
    </div>


<!-- Modal -->
<div class="modal multi-step signup-popup" id="demo-modal-3">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title step-1" data-step="1">Get Started</h4>
                <h4 class="modal-title step-2" data-step="2">When</h4>
                <h4 class="modal-title step-3" data-step="3">Reserve Your Job</h4>
                <h4 class="modal-title step-4" data-step="4">Completed</h4>
                
                <div class="m-progress">
                    <div class="m-progress-bar-wrapper">
                        <div class="m-progress-bar">
                        </div>
                    </div>
                    <div class="m-progress-stats">
                        <span class="m-progress-current">
                        </span>
                        <span >of</span>
                        <span class="m-progress-total">
                        </span>
                    </div>
                    <div class="m-progress-complete">
                        Completed
                    </div>
                </div>

            </div>
      <?php 
            $events= DB::table('categories')
                        ->where('status',1)
                        ->where('deleted',0)
                        ->get();
            
      ?>            
  <div class="modal-body step-1" data-step="1">

           <form class="" id="savedetails" method="GET" action="">
            <input type="hidden" name="userid" id="userid" value="{{$id}}" >
            <h3>Complete Your Details</h3>
            <div class="form-group">
              <label>What type of job is it?</label><span class="astrik">*</span>
            <select class="form-control" name="job_type" id="job_type">
                <option value ="default">Select Job</option>
                @foreach($events as $event)
                <option value = "{{$event->id}}">{{$event->cat_name}}</option>
               @endforeach
              </select>
            </div>
            <div id="equipment_sec">
            <!--Data through AJAX -->
            </div>
            <div class="form-group">
              <label>Select Location</label><span class="astrik">*</span>
              <p>Start typing your street address here</p>
               <input type="text" name="street" id="street" placeholder="Street Address" class="form-control uprcls" style="margin-bottom: 10px;">
               <input type="text" name="address" id="address" placeholder="Apartment/Suite #" class="form-control uprcls">
            </div>
            <div class="form-group">
              <label>Tell us about your job</label><span class="astrik">*</span>
              <textarea placeholder="" class="form-control" rows="5" name="about_job" id="about_job"></textarea> 
            </div>
            <div class="form-group">
              <!-- <label>Which type of College worker you need?</label><span class="astrik">*</span> -->
              <input type="hidden" name="workertype" class="workertype" value="bronze">              <!-- <ul class="list-inline">
                <li> <input type="radio" name="workertype" class="workertype" value="bronze"> Bronze</li>
                <li> <input type="radio" name="workertype" class="workertype" value="silver">Silver</li>
                <li> <input type="radio" name="workertype" class="workertype" value="gold">Gold</li>
               
              </ul> -->

            </div>
            <div class="form-group">
              <label>How many WRKers do you need?</label><span class="astrik">*</span>
              <select class="form-control" name="collegewrk" id="collegewrk">
                <option value="default">Select the number of WRKers you'll need</option>
                @for ($i = 1; $i <= 10; $i++)
                  <option value = "{{$i}}">{{ $i }}</option> 
                @endfor
              </select>
            </div>
            <div class="form-group">
              <label>Describe your ideal WRKer (optional)</label>
              <textarea placeholder="Describe your ideal WRKer" rows="5" class="form-control" id="sweeps_desc"></textarea>
            </div>

             <div class="submit-button">
            <!-- <button class="bck-btn">Back</button><button class="next-btn">Next</button> -->
          </div>
           
          </form>
    </div>

  <div class="modal-body step-2" data-step="2">
     <form class="" id="savetimings">
            <input type="hidden" name="userid" id="userid" value="" >
            <input type="hidden" name="lastjobid" id="lastjobid" value="" >
            <h3>Select Date & Time <span>*</span></h3>
             <div class="form-group">
        <div class="date">
          <input type="text" class="form-control" name="jobdate" id="jobdate">

        </div>
    </div>
            <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1default" data-toggle="tab">Morning</a></li>
                            <li><a href="#tab2default" data-toggle="tab">Afternoon</a></li>
                            <li><a href="#tab3default" data-toggle="tab">Evening</a></li>
                            
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1default">
                          <ul class="list-inline">
                          <li> <input type="radio" name="jobtime" class="job_time" value="9:00"  > 9:00 AM</li>
                          <li> <input type="radio" name="jobtime" class="job_time" value="10:00" >10:00 AM</li>
                          <li> <input type="radio" name="jobtime" class="job_time" value="11:00" >11:00 AM</li>
                          <li> <input type="radio" name="jobtime" class="job_time" value="12:00" >12:00 AM</li>
                        </ul>
                      </div>
                       <div class="tab-pane fade" id="tab2default">
                          <ul class="list-inline">
                         <li> <input type="radio" name="jobtime" class="job_time" value="1:00" > 1:00 PM</li>
                         <li> <input type="radio" name="jobtime" class="job_time" value="2:00" > 2:00 PM</li>
                         <li> <input type="radio" name="jobtime" class="job_time" value="3:00" > 3:00 PM</li>
                         <li> <input type="radio" name="jobtime" class="job_time" value="4:00" > 4:00 PM</li>
                        </ul>
                      </div>
                        <div class="tab-pane fade" id="tab3default">
                          <ul class="list-inline">
                          <li> <input type="radio" name="jobtime" class="job_time" value="5:00" > 5:00 PM</li>
                          <li> <input type="radio" name="jobtime" class="job_time" value="6:00" > 6:00 PM</li>
                          <li> <input type="radio" name="jobtime" class="job_time" value="7:00" > 7:00 PM</li>
                          <li> <input type="radio" name="jobtime" class="job_time" value="8:00" > 8:00 PM</li>
                          <li> <input type="radio" name="jobtime" class="job_time" value="9:00" > 9:00 PM</li>
                         </ul>
                      </div>
                        
                    </div>
                </div>
            </div>
            <h4>Don't see the time you need?</h4>
            <div class="form-group">
              <label>Estimate how long you will need CollegeWRK</label><span>*</span>
              <p>This helps us schedule Workers. You will not be charged
                 based on this estimate, only for the actual time Workers work.</p>
               <input type="text" name="estimatehrs" id="estimatehrs" placeholder="Enter estimate hours" class=" col-sm-6"><span>hours</span>
            </div>
            
            <div class="form-group">
              <label>Any scheduling details? (optional)</label>
              <textarea placeholder="" name="scheduling_details" id="scheduling_details" rows="5" class="form-control"></textarea>
            </div>
            <div class="form-group">
              <label>Any additional directions or locations? (optional)</label>
              <textarea placeholder="Add additional information." name="additional_info" id="additional_info" class="form-control" rows="5"></textarea> 
            </div>
            
             <div class="submit-button">
           <!--  <button class="bck-btn">Back</button><button class="next-btn">Next</button> -->
          </div>
           
          </form>

            </div>
            <div class="modal-body step-3" data-step="3">
                         <div class="reserve-job">
              <h5>Please Review the Information Below</h5>
              <div class="row">
               <div class="col-md-12">
                <div class="paym-cont">
                <ul class="paym-list payment_sec">
                    <li><img src="{{ url('/public') }}/images/pay-1.png"/ class=""> Seamless payments with no up-front charges</li>
                    <li><img src="{{ url('/public') }}/images/pay-2.png"/ class=""> Secure, private data management</li>
                   <!--  <li><img src="{{ url('/public') }}/images/pay-3.png"/ class=""> $500 Guarantee</li> -->
                    <li><img src="{{ url('/public') }}/images/pay-4.png"/ class=""> Dedicated support 7 days a week</li>
                  </ul>
                  <p>We require a valid credit card to reserve a job, but there are NO UPFRONT CHARGES. You will only be charged after your job is confirmed, completed, and you review CollegeWRK time sheets; or if a confirmed job is canceled within 24 hours of its start time.</p>
                </div>
                <div class="res-dtl">
                  <h6>Reservation Details</h6>

                  <ul class="addrs-lt">
                    <li><img src="{{ url('/public') }}/images/loc.png"/><div class="resr-txt" id="jobaddr"></div></li>
                    <li><img src="{{ url('/public') }}/images/msg.png"/><div class="resr-txt" id="fnldate"></div></li>
                  </ul>
                </div>
                <div class="res-dtl estimate-dv">
                  <h6>Estimate</h6>
                  <ul class="addrs-lt estmimate-price">
                    <li>
                      <div class="estim-left">
                      <h4>Labor</h4>
                      <p id = "workeramttxt"></p>
                    </div>
                    <div class="estim-right">
                      <h4 id="workeramt"></h4>
                    </div>
                    </li>
                    <li>
                      <div class="estim-left e">
                      <h4>Equipment Cost</h4>
                      <p id = "equipmentamttxt"></p>
                    </div>
                      <div class="estim-right">
                        <h4 id="equipmentamt"></h4>
                      </div>
                    </li>
                    <li>
                      <div class="estim-left">
                        <h5>TOTAL</h5>
                      </div>
                      <div class="estim-right nw-est">
                        <h4 id="totalamt"></h4>
                      </div>
                    </li>
<!--                      <li>
                      <div class="estim-left due-n">
                      <h5>Due Now</h5>
                    </div>
                    <div class="estim-right due-right">
                      <h4>$0</h4>
                    </div>
                    </li> -->
                    
                  </ul>
                 
                </div>
                 
               </div>
               
             </div>
           </div>

   <form class="reserve-job"  method="post" id="paymentFrm">
    <input type="hidden" class="final_amount_job">
   <div class="row">
   <div class="col-md-12">
   <div class="cont-detail-form payment-form">
   <h4>Payment Details</h4>
   <div class="cont-form-inner">
    <div class="payment-errors"></div>
    <input type="hidden" name="clientpay" class="clientpay" value="" >
    <input type="hidden" name="userid" id="userid" value="" >
    <input type="hidden" name="lastjobid" id="lastjobid" value="" >
    
    <!-- stripe form for payments -->
    <?php
    $__id = Auth::user()->id;
    $stripeDetails = DB::table('user_payment_credentials')
                        ->where('user_id', '=', $__id )
                        ->first();
    if( empty($stripeDetails) ):                    
    ?>
    <div class="col-sm-6 model-bot">
        <label>Name</label>
        <input type="text" class="form-control paymentname" name="paymentname" size="50" />
    </div>
    <div class="col-sm-6 model-bot">
        <label>Email</label>
        <input type="text" class="form-control paymentemail" name="paymentemail" size="50" />
    </div>

    <div class="col-sm-12">
     
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-row">
            <label for="card-element">
              Credit or debit card
            </label>
          <div id="card-element" style="width: 100% !important;">
            <!-- A Stripe Element will be inserted here. -->
          </div>
          <!-- Used to display form errors. -->
          <div id="card-errors" role="alert"></div>
      </div>
      <button style="display: none;" class="stripPaymentButton">Submit Payment</button>
    
    </div> <br /> 
  <?php else : ?>
    <div class="col-sm-6 col-xs-6">
        <h4 style="padding-left:0px !important; ">Your Card</h1>
    </div>
     <div class="col-sm-6 col-xs-6">
       <h4 class="text-right" style="padding-right:0px !important;">  ...<?php echo $stripeDetails->last_digit_cc; ?></h1>
     </div> 
  <?php endif; ?>  
    <!-- stripe form for payments ends  --> 
  <!--<div class="col-sm-6 model-bot">
        <label>Card Number</label>
        <input type="text" class="form-control card-number" name="card_num" size="20" autocomplete="off"/>
   </div>
    <div class="col-sm-2 model-bot">
        <label>CVC</label>
        <input type="text" class="form-control card-cvc" name="cvc" size="4" autocomplete="off"/>
   </div>
   <div class="col-sm-4 model-bot">
       <label>Expiration (MM/YYYY)</label>
       <div class="row">
          <div class="col-sm-4">
            <input type="text" class="form-control card-expiry-month" name="exp_month" size="2"/>
          </div>
          <div class="col-sm-1">  <span> / </span></div>
          <div class="col-sm-6">  <input type="text" class="form-control card-expiry-year" name="exp_year" size="4"/>
          </div>
      </div>
    </div> -->
    <div class="col-sm-6 model-bot">
      <label>How did you hear about CollegeWRK</label>
      <select class="form-control hear_from">
        <option>Please select one</option>
        <option>Advertisement</option>
        <option>Friends</option>
        <option>Others</option>
      </select>
    </div>
    <div class="col-sm-6 model-bot">
      <label>Have a promo or referral code?</label>
      <input type="text" class="form-control promo_code" name="" placeholder="Enter your promo code" class="form-control">
    </div>
  </div>
</div>    
  <input type="checkbox" name="trmchk" class="modelchick agreeTerms" ><span class="pay-agree">I agree to CollegeWRK' Terms of Use and Privacy Policy, and that the entered information is accurate and complete.</span>    
  <!--   <button type="submit" id="payBtn">Submit Payment</button> -->
  </div>
</div>
</form>
  </div>
   <div class="modal-body step-4" data-step="4">
   
  <h1>Thank you !!! Your Job has been saved.</h1>
  <hr />
  <h3 style="margin-bottom: 0px;">Book Now, Pay later</h3>
  <p>We require a valid credit card to reserve a job, but there are NO UPFRONT CHARGES. You will only be charged after your job is confirmed, completed and you review Sweeper time sheets; or if a confirmed job is canceled within 24 hours of its start time.</p>


</div>

           
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-default cls-btn" data-dismiss="modal">Close</button> -->
                <button type="button" class="btn btn-primary step step-2 bck-btn" data-step="2" onclick="sendEvent('#demo-modal-3', 1)">Back</button>
                <button type="button" class="btn btn-primary step step-1 next-btn" data-step="1" id="step2">Next</button>
                <button type="button" class="btn btn-primary step step-3 bck-btn" data-step="3" onclick="sendEvent('#demo-modal-3', 2)">Back</button>
                <button type="button" class="btn btn-primary step step-2 next-btn" data-step="2" id="step3">Continue</button>
              <!--   <button type="button" class="btn btn-primary step step-4 bck-btn" data-step="4" onclick="sendEvent('#demo-modal-3', 3)">Back</button> -->
                <button type="button" class="btn btn-primary step step-3 next-btn" data-step="3" id="<?php if(empty($stripeDetails)): echo 'payBtn'; else: echo'updatePOStStatus'; endif; ?>">Reserve Job</button>
            <!--     <button type="button" class="btn btn-primary step step-5 bck-btn" data-step="5" onclick="sendEvent('#demo-modal-3', 4)">Back</button> -->
              <!--   <button type="button" class="btn btn-primary step step-4 next-btn" data-step="4" id="payBtn">Reserve Job</button> -->
               
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    $(".cutmer-dash-tog").click(function(){
        $("#nav-div1").fadeToggle("slow");
    });
});
</script>

<script>
  $( function() {
    $( "#jobdate" ).datepicker({
      showweek : true,
      minDate: new Date(), 
     
    });
  } );
</script>
<script>
jQuery(document).ready(function($){
  $('body').on('click','.headerNotification', function(e){
     jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/readallnotifications') }}",
            data :{ 'field_name' : 'read_by_client', 'user_id' : {{ $id }} , 'user_field' : 'client_id'},
            success :  function(resp) {
                if(resp =='true'){
                 $('.counter').html('0');
               }
             }
       });

  });

});
</script>
 <script>
$(document).ready(function() {

   $(".notity").click(function(e){
    e.preventDefault();
    var noti = $(this).attr("notify");
     var redirecturl = $(this).data("redirecturl");
    jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/notitysec') }}",
            data :{'noti': noti, 'field_name' : 'read_by_client' },
            success :  function(resp) {
                if(resp=='true'){
                  if( redirecturl !='' && redirecturl != undefined && redirecturl != null ){
                    window.location.href= redirecturl ;
                  }else{
                    window.location.href="{{url('/customernotification')}}";
                  }
               }

             }
       });
 
});

  $("#job_type").change(function(){

    var sel_option = $(this).find('option:selected');
    var cat_id = sel_option.val();
    jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/getequipment') }}",
            data:{'cat_id':cat_id},
            success :  function(resp) {
             $("#equipment_sec").html(resp);    
             }
       });
 
});

  $.validator.addMethod("alphaLetterNumber", function(value, element) {
     return this.optional(element) || value == value.match(/^[a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });

  $.validator.addMethod("alphaLetterNumberSpace", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z0-9]+$/) && value.match(/[a-zA-Z0-9]/);
    });

  $.validator.addMethod("alphaLetter", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });
     $.validator.addMethod("valueNotEquals", function(value, element, arg){
     return arg !== value;
  }, "Value must not equal arg.");

  var step2_form = $("#savedetails").validate({
        rules: {
            job_type: {
                    valueNotEquals: "default" 
                },
            collegewrk: {
                    valueNotEquals: "default"
                },
            /*workertype: {
                    required:true
                }, */       
            street: {
                    required: true
                },
            address: {
                    required: true
                },
            about_job: {
                    required: true
                }
            },
        messages: {
            
            job_type: {
                  valueNotEquals: "Please select type of job."
                },
                collegewrk: {
                  valueNotEquals: "Please select number of worker(s)."
                },
               /* workertype: {
                  required: "Please select worker type."
                },*/
                street: {
                  required: "Enter your street address."
                },
                address: {
                  required: "Enter your address."
                },
                about_job: {
                  required: "Enter something about job."
                }

            },
        submitHandler: function(form) {
            form.submit();
          }
        });

    var step3_form = $("#savetimings").validate({
        rules: {
            jobdate: {
                    required: true 
                },
            jobtime: {
                    required: true
                },
            estimatehrs: {
                  required:true,
                  number:true
                }
            },
        messages: {
            
            jobdate: {
                  required: "Please confirm your date"
                },
            jobtime: {
                    required: "Please select your time."
                },
            estimatehrs: {
                  required: "Please enter estimate hours.",
                  number: "Please enter only number."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

  var step4_form = jQuery("#paymentFrm").validate({
        rules: {
            paymentname: {
                required: true 
                },
            paymentemail: {
                required: true
                },
            card_num: {
                required:true,
                minlength:16,
                maxlength:20
                },
            cvc:{
                required:true,
                maxlength:3
            },
            exp_month:{
                required: true,
                minlength:2
            },
            exp_year:{
                required : true,
                maxlength: 4
            },
            trmchk:{
              required:true
            }
            },
        messages: {
            
            paymentname: {
                  required: "Please enter your name."
                },
            paymentemail: {
                    required: "Please enter your email."
                },
            card_num: {
                  required: "Please enter 16-digits card number.",
                  minlength: "Minimum 16-digits are required.",
                  maxlength: "Maximum 20-digits are required."
                },
            cvc: {
                  required: "Please enter CVC number.",
                  maxlength: "Maximum 3-digits are required."
                },
            exp_month: {
                  required: "Please enter expiry month of card.",
                  minlength: "Minimum 2-digits are required."
                },
            exp_year: {
                  required: "Please enter expiry year of card.",
                  maxlength: "Maximum 4-digits are required."
                },
            trmchk: {
              required:"Agree term and conditions."
            }
            },
        submitHandler: function(form) {
            //form.submit();
          }
        });  

   $("#step2").click(function() {

        var myCheckboxes = new Array();
        $(".che_sec:checked").each(function() {
           myCheckboxes.push($(this).val());
        });
       var equ_ids = myCheckboxes.join();
      if (step2_form.form()) {
           var userid         = jQuery('#userid').val();
           var job_type       = jQuery('#job_type').val();
           var worker_type    = jQuery('.workertype').val();
           var street         = jQuery('#street').val();
           var address1       = jQuery('#address').val(); 
           var job_location   = jQuery('#street').val() +','+ jQuery('#address').val();
           var job_desc       = jQuery('#about_job').val();
           var worker_required      = jQuery('#collegewrk').val();
           var worker_desc          = jQuery('#sweeps_desc').val();
           var equipment_needed     = equ_ids;

           jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/jobstore') }}",
            data:{'userid':userid,'job_type': job_type,'address': street ,'address1': address1, 'job_location': job_location,'job_desc': job_desc ,'worker_type':worker_type,'worker_required': worker_required,'worker_desc':worker_desc, 'equipment_needed': equipment_needed},
            success :  function(resp) {
              if(resp > 0){
                $('input[name="lastjobid"]').val(resp);                 
                sendEvent('#demo-modal-3', 2);
              }

           }
       });
  
      }
    });

    $("#step3").click(function() {
      if (step3_form.form()) {
           var userid         = jQuery('#userid').val();
           var jobid          = jQuery('#lastjobid').val();
           var start_date     = jQuery('#jobdate').val() ;
           var start_time     = jQuery('.job_time:checked').val();
           var time_estimate  = jQuery('#estimatehrs').val();
           var scheduling_details  = jQuery('#scheduling_details').val();
           var additional_info = jQuery('#additional_info').val();
           var amount =''; 
          jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/updatejob') }}",
            data:{'userid':userid,'jobid': jobid, 'start_date': start_date,'start_time': start_time ,'time_estimate': time_estimate,'scheduling_details':scheduling_details, 'additional_info': additional_info},
            success :  function(resp) {
               var obj = jQuery.parseJSON(resp);
               $("#jobaddr").html(obj.job_location); 
               $("#fnldate").html(obj.start_date + ' at ' + obj.start_time);
/*
              if(obj.worker_type == 'gold'){
                var amount = obj.worker_required * obj.time_estimate * 25;
                $("#workeramttxt").html('1 Gold Worker for 1 hours @ $25/hour');
              }else if(obj.worker_type == 'silver'){
                var amount = obj.worker_required * obj.time_estimate * 20;
                $("#workeramttxt").html('1 Silver Worker for 1 hours @ $20/hour');
              }else{*/
                var amount = obj.worker_required * obj.time_estimate * 35;
                $("#workeramttxt").html('1 Worker for 1 hours @ $35/hour');
              /*}*/ 
                $("#workeramt").html('$'+amount);
                $("#equipmentamt").html('$'+obj.equipmentamount);
                
                var totalcost = amount+obj.equipmentamount;
                 $("#totalamt").html('$'+totalcost);
                 $('input[name="clientpay"]').val(totalcost);

                sendEvent('#demo-modal-3', 3);
             

           }
       });
     
      }
    });

  $("#payBtn").click(function() {
       if (step4_form.form()) {
          /*Stripe.createToken({
            number: $('.card-number').val(),
            cvc: $('.card-cvc').val(),
            exp_month: $('.card-expiry-month').val(),
            exp_year: $('.card-expiry-year').val()
            }, stripeResponseHandler); */
            //alert('submit payments');
        $('.stripPaymentButton').trigger('click');

      }
    });  
});
</script>

<!--<script>
//set your publishable key
Stripe.setPublishableKey('{{env('STRIPE_PUB_KEY')}}');

//callback to handle the response from stripe
function stripeResponseHandler(status, response) {

    if (response.error) {
         //enable the submit button
        //$('#payBtn').removeAttr("disabled");
        //display the errors on the form
        $(".payment-errors").html(response.error.message);
    } else {
        var form$ = $("#paymentFrm");
        //get token id
        var token = response['id'];
        //insert the token into the form
        form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
        //submit form to the server

           var stripeToken  = jQuery('input[name="stripeToken"]').val();
           var userid       = jQuery('#userid').val();
           var lastjobid    = jQuery('#lastjobid').val();
           var clientpay    = jQuery('.clientpay').val();
           var name         = jQuery('.paymentname').val();
           var email        = jQuery('.paymentemail').val();
           var cardnumber   = jQuery('.card-number').val();
           var cardcvc      = jQuery('.card-cvc').val();
           var cardexpirymonth      = jQuery('.card-expiry-month').val();
           var cardexpiryyear       = jQuery('.card-expiry-year').val();
           
           jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/reciept') }}",
            data:{ 'stripeToken': stripeToken, 'userid': userid, 'lastjobid':lastjobid,'clientpay':clientpay,'paymentname': name ,'paymentemail': email,'card_num': cardnumber ,'cvc': cardcvc,'exp_month': cardexpirymonth ,'exp_year': cardexpiryyear },
            success :  function(resp) {
       
               sendEvent('#demo-modal-3', 4);
           }
          });
        //form$.get(0).submit();
    }
}

</script> -->

<script src="{{ url('/public') }}/js/multi-step-modal.js"></script>
<script>
sendEvent = function(sel, step) {
    $(sel).trigger('next.m.' + step);
}
</script>
<style type="text/css">
  .dashboardright li {
    display: inline-block;
}
  .dashboardright button.dropdown-toggle {
    background: transparent;
    border: none;
}
.dropdown-menu {
    left: -70px !important;
    min-width:100%;
}
.che_sec {
    height: 13px !important;
}
.dropdown-customer {
    padding: 5px 0px;
    background: #fff;
    -webkit-box-shadow: 0 0 20px 0 rgba(0,0,0,.3);
    box-shadow: 0 0 20px 0 rgba(0,0,0,.3);
    border-radius: 5px;
    border: none;
    opacity: 1;
    position: absolute;
    left: auto;
    right: 0px;
    visibility: visible;
    overflow: visible;
    padding: 0px 0;
    -webkit-transform: translateY(20px);
    transform: translateY(20px);
}
.dropdown-customer:before {
    content: "";
    border: 10px solid transparent;
    border-left-color: #fff;
    border-top-color: #fff;
    position: absolute;
    border-radius: 3px;
    top: -8px;
    right: 15px;
    -webkit-box-shadow: -5px -5px 12px -2px rgba(0,0,0,.3);
    box-shadow: -4px -4px 12px -2px rgba(0,0,0,.3);
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg);
    z-index: 1002;
}
.dropdown-customer li {
    display: block;
    padding: 0px 0px;
    border-bottom: 1px solid #eee;
}
.dropdown-customer li a.logount_hed_sec {
    padding: 10px 15px;
    border-radius: 3px 3px 3px 3px;
}
.dropdown-customer {
    left: -70px !important;
}
@media screen and (max-width:991px;){
  #nav-div1 {
    width: 100% !important;
}
}

.notifaction-colwrk {
    position: absolute;
    top: 150%;
    left: 50% !important;
    transform: translateX(-50%);
    width: 350px;
    margin: 0;
    padding: 0;
    
}
.notifaction-colwrk>li>a {
    white-space: unset;
}
.notifaction-colwrk::before {
    display: block;
    content: "";
    position: absolute;
    top: -25px;
    border: 13px solid transparent;
    border-bottom-color: transparent;
    border-bottom-style: solid;
    border-bottom-width: 13px;
    border-bottom: 13px solid #2789fd;
    transform: translate(-50%);
    left: 50%;
}
.headnoti {
    background-color: #2789fd;
    color: #fff;
    padding: 10px;
    border-radius: 2px 2px 0 0;
}
.notview {
    float: right;
    color: #fff;
    text-decoration: underline;
    font-weight: bold;
}
.notifaction-colwrk li:nth-child(odd) {
    background-color: #2789fd0f;
}
.notibody {
    max-height: 400px;
    overflow-y: scroll;
}
.notibody a {
    width: 100%;
    display: block;
    padding: 5px 15px;
    color: #666666;
}
@media screen and (max-width:480px){
.notifaction-colwrk {width: 270px;}
}
input#estimatehrs {
    margin-right: 7px;
}
/*.panel-body {height:auto !important;overflow:auto;}*/

.StripeElement {
  background-color: white;
  height: 40px;
  padding: 10px 12px;
  border-radius: 4px;
  border: 1px solid transparent;
  box-shadow: 0 1px 3px 0 #e6ebf1;
  -webkit-transition: box-shadow 150ms ease;
  transition: box-shadow 150ms ease;
}

.StripeElement--focus {
  box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid {
  border-color: #fa755a;
}

.StripeElement--webkit-autofill {
  background-color: #fefde5 !important;
}

</style>
<script>
// Create a Stripe client.
var stripe = Stripe('{{env('STRIPE_PUB_KEY')}}'); 

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    lineHeight: '18px',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission.
var form = document.getElementById('paymentFrm');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

// Submit the form with the token ID.
function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  //alert( token );
  //console.log( token );
  var form = document.getElementById('paymentFrm');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);
  //===============================//
    var userid = $('#userid').val();
    var lastjobid = $('#lastjobid').val();
    var paymentname = $('.paymentname').val();
    var paymentemail = $('.paymentemail').val();
    var hear_from = $('.hear_from').val();
    var promo_code = $('.promo_code').val();
    var totalamt = $('input[name="clientpay"]').val();
    //alert(totalamt);
      jQuery.ajax({ 
            method : 'GET',
            url  : "{{ url('/createuserstripe') }}",
            data:{'stripe_token': token.id ,'stripe_data': token, 'userid' : userid, 'lastjobid' : lastjobid, 'paymentname' : paymentname,  'hear_from' : hear_from, 'promo_code' : promo_code ,'paymentemail' : paymentemail, 'totalamt' : totalamt },
            success :  function(resp) {
              //alert( resp );
              console.log(resp);
              if( resp == 'success' ){
                sendEvent('#demo-modal-3', 4);
              }else{
                 alert( 'Something went wrong. please try again.' ); 
              }
              
            },
            error : function(resp){
              alert( 'Something went wrong. please try again.' ); 
               console.log(resp);
            }
      });
 //==============================//
  // Submit the form
  //form.submit();
}
</script>

<script>
jQuery(document).ready(function($){
  $('body').on('click', '#updatePOStStatus', function(){
    var userid = $('#userid').val();
    var lastjobid = $('#lastjobid').val();
    var paymentname = $('.paymentname').val();
    var paymentemail = $('.paymentemail').val();
    var hear_from = $('.hear_from').val();
    var promo_code = $('.promo_code').val();
    var totalamt = $('input[name="clientpay"]').val();
    //alert(totalamt);
    if($('.agreeTerms').prop("checked") == true){
    jQuery.ajax({ 
            method : 'GET',
            url  : "{{ url('/updatepoststatus') }}",
            data:{ 'userid' : userid, 'lastjobid' : lastjobid, 'paymentname' : paymentname,  'hear_from' : hear_from, 'promo_code' : promo_code ,'paymentemail' : paymentemail, 'totalamt' : totalamt },
            success :  function(resp) {
              //alert( resp );
              //console.log(resp);
              if( resp == 'success' ){
                sendEvent('#demo-modal-3', 4);
              }else{
                 alert( 'Something went wrong. please try again.' ); 
              }
              
            },
            error : function(resp){
              alert( 'Something went wrong. please try again.' ); 
               console.log(resp);
            }
      });
  }else{
    alert('Please accept term and conditions');
  }
  }); 

})
</script>