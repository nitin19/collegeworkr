@extends('layouts.clientdefault')

@section('title', 'Clientdashboard')

@section('content')

<div class="col-md-9 dashboard-right">
      <div class="main-profile">
         <div class="upper-profile">
            <div class="row Frequent-Jobs">
                  <div class="col-md-12">
                    <h2>Account Information</h2>
                  </div>
            </div>

        @if (\Session::has('success'))
             <div class="alert alert-success alert-dismissable">
               <a href="#" class="close" data-dismiss="alert" aria-label="close" style="right:0px;">×</a>
               {!! \Session::get('success') !!}
              </div>
        @endif

         @if (\Session::has('error'))
            <div class="alert alert-danger alert-dismissable">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
               {!! \Session::get('error') !!}
            </div>
        @endif

            <div class="row">
            <div class="col-md-9">
            <div class="row user-info custom-profile">
              <h3>Personal Info </h3>
          {{ Form::model($usercustomer, array('route' => array('customerdashboard.update', $usercustomer->id), 'id' => 'profileform', 'method' => 'PUT', 'files' => true)) }}
               
                <input type="hidden" name="user_id"  value="{{$usercustomer->id}}">
            <div class="col-md-2 nopadding">
                @if($usercustomer->image !='')
                <img src="{{url('/public')}}/uploads/profile/{{$usercustomer->image}}" class="img-responsive center-block" id="profileimg">
                @else
                <img src="{{url('/public')}}/images/avatar.jpg" class="img-responsive center-block" id="profileimg">
                @endif
             <div class="form-group" style="text-align: center;">
      <!--         <label style="color: #3399cc;font-size: 15px;text-decoration: none;font-weight: 400;display: block;">Upload Image</label> -->
              <div class="input-group">
                  <span class="input-group-btn">
                      <span class="btn btn-default btn-file">
                          Browse… <input type="file" id="imgInp" name="image">
                      </span>
                  </span>
                 
              </div>
          </div>
              </div>
              <div class="col-md-1">
              </div>
              <div class="col-md-9">
               
                <div class="row">
                  <div class="form-group" style="overflow: visible !important;">
                    <div class="col-md-6">
                      <label>First Name</label>
                      <input type="text" name="firstname" placeholder="Enter your firstname" value="{{$usercustomer->firstname}}" class="form-control">
                    </div>
                    <div class="col-md-6">
                      <label>Last Name</label>
                      <input type="text" name="lastname" placeholder="Enter your lastname" value="{{$usercustomer->lastname}}" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                     <div class="col-md-12">
                        <label>Email Address</label>
                        <input type="email" name="email" placeholder="Enter your email" value="{{$usercustomer->email}}" class="form-control" readonly="readonly">
                    </div>
                     
                  </div>
                  <div class="form-group" style="overflow: visible !important;float: left;width: 100%;">
                    <div class="col-md-6">
                        <label>Phone Number</label>
                        <input type="tel" name="phone" placeholder="Enter your phone number" value="{{$usercustomer->phone}}" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label>Invite Code</label>
                        <input type="text" name="invitecode" placeholder="Enter your code" value="{{$usercustomer->invitecode}}" class="form-control" readonly="readonly">
                    </div>
                  </div>
              <div class="form-group">
                <div class="col-md-12">
                  <button type="submit" class="sve-btn">Update Information</button>
                </div>
              </div>
              </div>
              {!! Form::close() !!}
            </div>
            </div>
            
          </div>
          <div class="col-md-3">
            <div class="pro-myaccount">
              <h2>My Account</h2>
              <ul>
                  <?php
                    $createdate = explode(' ', $usercustomer->created_at);
                    $mydate = date('d M Y',strtotime($createdate[0]));
                 ?>
                <li>Member Since:{{date('d M Y',strtotime($createdate[0]))}}</li>
                <!--<li>Balance: $0.00 </li> -->
                <li>Jobs Posted:
                  @if ($clientjobpost->count()){{$clientjobpost->count()}}
                  @else {{'No job posted yet'}}
                  @endif
                </li>
                <!--<li>Rating: 
                <?php 
                    $feedback;
                    $feedback_count;
                    $customer_feedback_count;
                    if($feedback!='0' && $feedback_count!='0' && $customer_feedback_count!='0'){ 
                    $feedback_average =  $feedback / $feedback_count;
                    } else {
                      $feedback_average = '';
                    }
                ?> 
                  @if($feedback_average <= 2)
                  {{$feedback_average}}
                  <img src="{{url('/public')}}/images/brownz.png">
                  ({{$customer_feedback_count}})
                  @elseif($feedback_average > 2 && $feedback_average <= 3)
                  {{$feedback_average}}
                  <img src="{{url('/public')}}/images/silver.png">
                  ({{$customer_feedback_count}}) 
                  @elseif($feedback_average > 3 && $feedback_average <= 4)
                  {{$feedback_average}}
                  <img src="{{url('/public')}}/images/silver.png">
                  ({{$customer_feedback_count}}) 
                  @elseif($feedback_average > 4 && $feedback_average <= 5)
                  {{$feedback_average}}
                  <img src="{{url('/public')}}/images/gold.png">
                  ({{$customer_feedback_count}})
                  @else 
                  @endif

                </li> -->
              </ul>
            </div>
          </div>
        </div>
        </div>
         
    </div>
</div>
</div>
</section>

<script type="text/javascript">
  
  function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#profileimg').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#imgInp").change(function() {
  readURL(this);
});
</script>

 <script>
jQuery(document).ready(function() {
  $.validator.addMethod("alphaLetter", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });
 
 $("#profileform").validate({
        rules: {
                firstname: {
                    required: true,
                    alphaLetter:true,
                    minlength: 2,
                    maxlength: 60
                },
                lastname: {
                    required: true,
                    alphaLetter: true,
                    minlength: 2,
                    maxlength: 60
                },
                phone: {
                    required: true,
                    minlength: 10,
                    maxlength: 12,
                    number:true
                }   
            },
        messages: {
              firstname: {
                  required: "Please enter your firstname.",
                  alphaLetter:"Only letters are allowed",
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 60 characters allowed."
                },
              lastname: {
                  required: "Please enter your lastname.",
                  alphaLetter:"Only letters are allowed",
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 60 characters allowed."
                },
              phone: {
                  required: "Please enter phone number.",
                  minlength: "Minimum 10 characters required.",
                  maxlength: "Maximum 12 characters allowed.",
                  number: "Please enter only number."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
    });

  });
</script>
<style>
.tooltip-inner {margin-top:8px;}
</style>
@stop