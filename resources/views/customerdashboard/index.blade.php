@extends('layouts.clientdefault')

@section('title', 'Clientdashboard')

@section('content')

   <div class="col-md-9 dashboard-right">

          <div class="cut-dash-inner">
                <div class="homedas">
                    <div class="col-sm-6">
                        <h2>Your Current Jobs</h2>
                    </div>
                    <div class="col-sm-6">
                        <p class="welcome-text">
                            <span style="color: #3399cc;font-weight:bold;">Welcome, </span>{{ucfirst($usercustomer->firstname)}} {{ucfirst($usercustomer->lastname)}}
                        </p>
                   </div>
                </div>
                 <div class="homepagetable">
                   <!--<div class="col-sm-6">
                    
                  </div>
                   <div class="col-sm-6">
                    <p class="welcome-text"><span style="color: #3399cc;font-weight:bold;">Welcome, </span>{{ucfirst($usercustomer->firstname)}} {{ucfirst($usercustomer->lastname)}}</p>
                   </div>-->
                   <div class="row">
                     <div class="col-sm-12">
                  <?php 
                    $count=count($recentjobs);    
                  ?>
                @if($count > '0')

                    <div class="recent_jobs">
                      <table class="table table-striped history-table">
                      <thead>
                      <tr>
                        <th class="col5">Job Post Date</th>
                        <th class="col5">Type of Job and Location</th>
                        <th class="col5">Description</th>
                        <th class="col5">Number of Required Workers</th>
                      </tr>

                    </thead>
                    <tbody>
                    @foreach($recentjobs as $clientjob)
                      <?php 
                       $catid = $clientjob->job_heading;
                       $user_categories   = DB::table('categories')
                                              ->where(['id'=>$catid, 'status' => '1','deleted' => '0'])
                                              ->first();
                        $cat_name = $user_categories->cat_name; 
                         $createdate = explode(' ', $clientjob->created_at);   

                      ?>
                      <tr>
                        <td  class="col5" data-label="Job Post Date">{{date('d M Y',strtotime($createdate[0]))}}</td>
                        <td  class="col5" data-label="Name of Job">{{$cat_name}} <span style="text-transform:lowercase;">at</span> {{$clientjob->job_location}}</td>
                        <td  class="col5" data-label="Description">{{$clientjob->job_description}}</td>
                        <td  class="col5" data-label="Required Worker">{{$clientjob->worker_required}}</td>
                      </tr>
                      @endforeach

                    </tbody>
                  </table>
                </div> 
               @else              
                <div class="table-error-mesg">
                    <p>You Don't Have Any Upcoming Jobs <button data-toggle="modal" data-target="#demo-modal-3" class="swe-btn pull-right">Book Now</button></p>
                    
                </div>
              @endif

                     </div>
                   </div>
                  </div>
                 <div class="row Frequent-Jobs">
                  <div class="col-md-12">
                    <h2>Our Most Frequent Jobs</h2>
                  </div>
                 </div>

                 <div class="row dash-services">
                  @foreach( $categories as $category )
                  <div class="col-md-3 col-sm-6 col1">
                    <a id="category_popup_link" href="#" data-toggle="modal" data-target="#demo-modal-3">
                    <div class="ser-col{{$category->id}}">
                    <div class="left-ser">
                      <h5>{{$category->cat_name}}</h5>
                      <h2>$35<span>/ hour</span></h2>
                    </div>
                    <div class="right-ser">
                      <img src="{{url('/public')}}/images/{{$category->cat_image}}" class="center-block">
                    </div>
                    </div>
                  </a>
                  </div>
                  @endforeach

                 </div>
                 <div class="row getintouch">
                  <div class="col-md-6">
                    <div class="get-touch">
                    <h2>Get in touch!</h2>
                    <ul>
                      <li>1. Click on message icon     <a href="{{url('/#contact-us')}}"><i class="fa fa-envelope" aria-hidden="true"></i>
</a></li>
                      <li>2. Send us a message detailing the nature of your job.</li>
                      <li>3. We will get back to you ASAP!</li>
                    </ul>
                  </div>
                  </div>

                   <div class="col-md-6">
                    <div class="recent-job">
                      <div class="recent-job-head">
                    <h2>Your Most Recent Jobs<button class="swe-btn pull-right" onclick="location.href = '{{url('')}}/customerjobhistory'" style="padding: 6px 10px 6px 10px;    margin-top: -3px;">View All</button></h2>
                        <!--  <div class="sweep-btn">
                  <button class="swe-btn" onclick="location.href = '{{url('')}}/customerjobhistory'">View All</button>
                </div> -->
              </div>

                 <?php 
                    $count=count($recentjobs);    
                 ?>
                @if($count > '0')
                    <div class="recent_jobs">
                      <ul>
                    @foreach($recentjobs as $clientjob)
                      <?php 
                       $catid = $clientjob->job_heading;
                       $user_categories   = DB::table('categories')
                                              ->where(['id'=>$catid, 'status' => '1','deleted' => '0'])
                                              ->first();
                        $cat_name = $user_categories->cat_name; 
                         $createdate = explode(' ', $clientjob->created_at);   

                      ?>
                      <li>
                       <b>{{$cat_name}}</b> at {{$clientjob->job_location}} on <b>{{date('d M Y',strtotime($createdate[0]))}} </b>
                     </li>
                      @endforeach
              </ul>
                </div> 

              @else                  
               <p>We Don't Have Any History Yet</p>
               <button data-toggle="modal" data-target="#demo-modal-3">Book Now</button>
              @endif
                  </div>
                 </div>
          </div>

          <div class="row Frequent-Jobs">
                  <div class="col-md-12">
                    <h2>Meet the Workers</h2>
                  </div>
                 </div>
                 <div class="row Sweepers-row">
                  <div class="col-md-8">

                  @if(count($reserved_job_data)>0)
                  @foreach($reserved_job_data as $reserved_job)
                  <?php 
                  $worker_id = $reserved_job->worker_userid;
                  $worker_data = DB::table('users')
                            ->where(['id'=>$worker_id])
                            ->get();

                   ?>
                   @foreach($worker_data as $workerdata)
                   <?php 
                   $worker_qualification_id = $workerdata->worker_qualification;
                   $worker_user_id = $workerdata->id;
                   $worker_quali_data = DB::table('worker_qualification')
                            ->where(['id'=>$worker_qualification_id])
                            ->get();
                   ?>
                   <div class="Sweepers-detail">
                    <div class="sweep-image">
                      @if($workerdata->image!='')
                      <img src="{{url('/public')}}/uploads/profile/{{$workerdata->image}}"/>
                      @else
                      <img src="{{url('/public')}}/images/avatar.jpg"/>
                      @endif
                    </div>
                    <div class="sweep-content">
                      
                    <h5>{{$workerdata->firstname}}{{$workerdata->lastname}}</h5>
                    <?php 
                    $feedback = DB::table('feedback')
                            ->where('user_id',$worker_user_id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->sum('feedback_rating');
                    $feedback_count = DB::table('feedback')
                            ->where('user_id',$worker_user_id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->count('feedback_rating');
                    $customer_feedback_count = DB::table('feedback')
                            ->where('user_id',$worker_user_id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->count('user_id');

                    $feedback;
                          $feedback_count;
                          $customer_feedback_count;
                          if($feedback!='0' && $feedback_count!='0' && $customer_feedback_count!='0'){
                              $feedback_average =  $feedback / $feedback_count;
                          } else {
                              $feedback_average = '';
                          }
                    ?>

                    <h6><span>Rating:</span> <span class="rating_sec">
            
                        @if($feedback_average <= 2)
                        {{$feedback_average}}
                        <img src="{{url('/public')}}/images/brownz.png">
                        ({{$customer_feedback_count}}) 
                        @elseif($feedback_average > 3 && $feedback_average <= 4)
                        {{$feedback_average}}
                        <img src="{{url('/public')}}/images/silver.png">
                        ({{$customer_feedback_count}}) 
                        @elseif($feedback_average > 4 && $feedback_average <= 5)
                        {{$feedback_average}}
                        <img src="{{url('/public')}}/images/gold.png">
                        ({{$customer_feedback_count}})
                        @else 
                        @endif

                      </span>
                    </h6> 
                    <!-- <img src="{{url('/public')}}/images/star.png"><span class="rating-text">5 Average based on 39, 481 rating</span> -->
                    @foreach($worker_quali_data as $worker_quali_datas)
                    <h4><i class="fa fa-graduation-cap" aria-hidden="true"></i>{{$worker_quali_datas->name}}<span>(Peace Studies and Conflict Resolution)</span></h4>
                    @endforeach
    
                    <p>{{ str_limit($workerdata->worker_about, 100) }}</p>
                  </div>
                  </div>
                  @endforeach
                  @endforeach
                  @endif

                  <!-- <div class="Sweepers-detail">
                    <div class="sweep-image">
                      <img src="{{url('/public')}}/images/customer.png"/>
                    </div>
                    <div class="sweep-content">
                    <h5>Tyler D.</h5>
                    <img src="{{url('/public')}}/images/star.png"><span class="rating-text">5 Average based on 39, 481 rating</span>
                    <h4><i class="fa fa-graduation-cap" aria-hidden="true"></i>University of North Carolina-Chapel Hill<span>(Peace Studies and Conflict Resolution)</span></h4>
                    <p>I graduated from UNC Chapel hill with a degree in Peace, War, and Defense.</p>
                  </div>
                  </div> -->

                  <!--<div class="sweep-btn">-->
                      <!--<div class="recent-job">
                          <div class="recent-job-head">
                            
                          
                          </div>
                      </div>-->
                    <!-- <div class="recent-job">
                      <div class="recent-job-head">
                         <h2>Your Most Recent <a href="{{url('/customernotification')}}" class="swe-btn pull-right" style="margin-top: -3px;">View All</a> -->
                            <!--<button class="swe-btn pull-right" onclick="location.href = 'https://collegewrk.com/customernotification'">View All</button>-->
                         <!-- </h2> -->
                        <!--  <div class="sweep-btn">
                        <button class="swe-btn" onclick="location.href = 'https://collegewrk.com/customerjobhistory'">View All</button>
                         </div> -->
                      <!-- </div> -->
                        <!-- <p>We Don't Have Any History Yet</p> -->
                       
                <!--</div>-->
                  </div>

                  <div class="col-md-4">
                    <div class="your-account">
                      <h2>Your Account</h2>

                      <ul>
                        <li><strong>Member Since:</strong>
                        <?php
                          $createdate = explode(' ', $usercustomer->created_at);
                           ?>
                          {{date('d M Y',strtotime($createdate[0]))}}
                        </li>
                        <!-- <li><strong>Balance:</strong> $0.00 </li> -->
                        <li class="invite_code_field"><strong>Jobs Posted:</strong>
                          @if ($clientjobpost->count()){{$clientjobpost->count()}}
                          @else {{'No job posted yet'}}
                          @endif
                        </li>
                        <li class="invite_code_field"><strong>Invite Code:</strong> {{$usercustomer->invitecode}} <!-- mannatm16204 --></li>
                      </ul>
                    </div>
                    <a href="{{url('/customeradminchat')}}" class="support-chat">Message us <i class="fa fa-envelope" aria-hidden="true"></i></a>
                  </div>
                  
                 </div>


<!--<div class="row">
 <div class="col-md-9"></div> 
 <div class="col-md-3 nopadding"></div>  
</div>-->


     </div>
         
    </div>
</div>
</div>
</section>

<script>
sendEvent = function(sel, step) {
    $(sel).trigger('next.m.' + step);
}
</script>
<style type="text/css">
.sweep-image img {
    width: 60px;
    border-radius: 65px;
    height: 60px;
}
.denied_btn {
    background: #ff0000;
    color: #fff;
    padding: 10px 20px 10px 20px;
    font-size: 16px;
    font-weight: bold;
    border-radius: 5px;
}
.btn_section {
    margin-top: 20px;
    padding: 0px 20px 20px 0px;
}
.hire_btn {
    background: #3399cc;
    padding: 10px 30px 10px 30px;
    font-size: 16px;
    color: #fff;
    font-weight: bold;
    border-radius: 5px;
}
.swe-btn {
    background: #3399cc;
    padding: 6px 12px;
    font-size: 14px;
    color: #fff;
    font-weight: bold;
    border-radius: 5px;
    display: inline-block;
}
.swe-btn:hover {
  color: #fff;
  text-decoration: none;
}
.swe-btn:active {
  color: #fff;
  text-decoration: none;
}
.sweep-btn {margin-top: 25px;}
.share-online {text-align: center;}
.invite_code_field {margin-top: 5px !important;}
/*.col-md-3.col-sm-6.col1:nth-child(even) {
    padding: 0px;
}*/
.homedas {
    background-color: #fff;
    border: 1px solid #e4e4e4;
    -webkit-box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.04);
    box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.04);
    border-radius: 3px;
    margin-bottom: 20px;
    overflow: hidden;
}
.homepagetable .row .col-sm-12 {
    margin-bottom: -20px;
}
@media screen and (max-width:767px) {
        .homedas h2 {
    font-size: 18px;
    padding: 5px 0px;
    text-align: center;
}
.homedas p.welcome-text {
    font-size: 16px;
    padding: 5px 0px;
    text-align: center;
}
}
.recent-job-head h2 {
    font-size: 15px;
    color: #3399cc;
    font-family: "ProximaNova-Semibold";
    padding: 10px 20px;
    background: #eeeeee;
}
</style>
@stop