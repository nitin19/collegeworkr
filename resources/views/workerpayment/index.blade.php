@extends('layouts.workerdefault')

@section('title', 'Worker Jobs')

@section('content')
        <div class="tab-pane active text-style" id="tab1">
        <div class="row">
       <div class="col-md-12 rightcontent">
        <!-- <p>You will be able to compete for and work jobs once you finish your application</p> -->

<div class="tab-section">
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active custom-width"><a href="#upcoming" aria-controls="upcoming" role="tab" data-toggle="tab">Upcoming Payment<span></span></a></li>
    <li role="presentation" class="custom-width"><a href="#receivedpayment" aria-controls="received_payment" role="tab" data-toggle="tab">Received Payment<span></span></a></li>
    <li role="presentation" class="custom-width"><a href="#rejectpayment" aria-controls="reject_payment" role="tab" data-toggle="tab" >Reject Payment<span></span></a></li> 
  </ul>

  <!-- Tab panes -->  
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="upcoming">
      <div class="tab_contentarea">
        <h1>Upcoming Payment</h1>
        <div class="tablewrapper tablediv">
          <table class="table-striped">
        <thead>
          <tr class="headings">
            <th class="column1 col5">Contract Name</th>
            <th class="column2 col5">Client Name</th>
            <th class="column3 col5">Amount</th>
            <th class="column4 col5">Detail</th>
          </tr>
        </thead>
        <tbody>
          @if(count($worker_upcoming_payment) > 0)
          @foreach($worker_upcoming_payment as $worker_upcoming_payments)
          <?php
                /*$job_id = $worker_upcoming_payments->client_jobid;
                $job_data = DB::table('clientjobpost')->where('id',$job_id)->first();
                $cat_id = $job_data->job_heading;
                $cat_data = DB::table('categories')->where('id',$cat_id)->first();
                $cat_name = $cat_data->cat_name;
                $client_id = $worker_upcoming_payments->client_userid;
                if($client_id !=''){
                $client_data = DB::table('users')->where('id',$client_id)->first();
                if($client_data !=''){
                $client_first_name = $client_data->firstname;
                $client_last_name = $client_data->lastname; 
                }
              } 
              */
          ?>

          <tr class="familydata custom-reports">
            <td class="column1 col5" data-label="Contract Name"><span>{{$worker_upcoming_payments->cat_name}}</span></td>
            <td class="column2 col5" data-label="Client Name">{{$worker_upcoming_payments->firstname}} {{$worker_upcoming_payments->lastname}}</td>
            <td class="column3 col5" data-label="Amount">
              <?php echo money_format("$%i", $worker_upcoming_payments->amount_paid ); ?>
            </td>
            <td class="column4 col5" data-label="Detail"><a href="{{url('/workerjobs')}}" class="view-table">View Detail</a></td>
          </tr>
         @endforeach
         @else
      <tr>
         <td>
           No job posted yet.
         </td>
     </tr>
     @endif
        </tbody>
      </table>
        </div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="receivedpayment">
      <div class="tab_contentarea">
        <h1>Received Payment</h1>
        <div class="tablewrapper tablediv">
          <table class="table-striped">
        <thead>
          <tr class="headings">
            <th class="column1 col5">Contract Name</th>
            <th class="column2 col5">Client Name</th>
            <th class="column3 col5">Amount</th>
            <th class="column4 col5">Detail</th>
          </tr>
        </thead>
        <tbody>
          @if(count($worker_received_payment) > 0)
          @foreach($worker_received_payment as $worker_received_payments)
          <?php 
                /*$job_id = $worker_received_payments->client_jobid;
                $job_data = DB::table('clientjobpost')->where('id',$job_id)->first();
                $cat_id = $job_data->job_heading;
                $cat_data = DB::table('categories')->where('id',$cat_id)->first();
                $cat_name = $cat_data->cat_name;
                $client_id = $worker_received_payments->client_userid;
                if($client_id !=''){
                $client_data = DB::table('users')->where('id',$client_id)->first();
                if($client_data !=''){
                $client_first_name = $client_data->firstname;
                $client_last_name = $client_data->lastname; 
                }
              }*/
          ?>
          <tr class="familydata custom-reports">
            <td class="column1" data-label="Contract Name"><span>{{$worker_received_payments->cat_name}}</span></td>
            <td class="column2" data-label="Client Name">{{$worker_received_payments->firstname}}{{$worker_received_payments->lastname}}</td>
            <td class="column3" data-label="Amount">
            <?php echo money_format("$%i", $worker_received_payments->amount_paid ); ?></td>
            <td class="column4" data-label="Detail"><a href="#" class="view-table">View Details</a></td>
          </tr>
        @endforeach
         @else
      <tr>
         <td>
           No job posted yet.
         </td>
     </tr>
     @endif
        </tbody>
      </table>
        </div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="rejectpayment">
      <div class="tab_contentarea">
        <h1>Reject Payment</h1>
        <div class="tablewrapper tablediv">
          <table class="table-striped">
        <thead>
          <tr class="headings">
            <th class="column1 col5">Contract Name</th>
            <th class="column2 col5">Client Name</th>
            <th class="column3 col5">Amount</th>
            <th class="column4 col5">Detail</th>
          </tr>
        </thead>
        <tbody>
          @if(count($worker_reject_payment) > 0)
          @foreach($worker_reject_payment as $worker_reject_payments)
          <?php 
                /*$job_id = $worker_reject_payments->client_jobid;
                $job_data = DB::table('clientjobpost')->where('id',$job_id)->first();
                $cat_id = $job_data->job_heading;
                $cat_data = DB::table('categories')->where('id',$cat_id)->first();
                $cat_name = $cat_data->cat_name;
                $client_id = $worker_reject_payments->client_userid;
                  if($client_id !=''){
                $client_data = DB::table('users')->where('id',$client_id)->first();
                if($client_data !=''){
                $client_first_name = $client_data->firstname;
                $client_last_name = $client_data->lastname;
                }
              } */
          ?>
          <tr class="familydata custom-reports">
            <td class="column1 col5" data-label="Contract Name"><span>{{$worker_reject_payments->cat_name}}</span></td>
            <td class="column2 col5" data-label="Client Name">{{$worker_reject_payments->firstname}}{{$worker_reject_payments->lastname}}</td>
            <td class="column3 col5" data-label="Amount">
              <?php echo money_format("$%i", $worker_reject_payments->amount_paid ); ?>
            </td>
            <td class="column4 col5" data-label="Detail"><a href="#" class="view-table">View Detail</a></td>
          </tr>
        @endforeach
         @else
      <tr>
         <td>
           No job posted yet.
         </td>
     </tr>
     @endif
        </tbody>
      </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row pagination-list">
  <div class="col-md-6 col-sm-6">
      <nav aria-label="Page navigation example">
       <div class="pagination_section">
   
      </div>
  </nav>
  </div>
  <div class="col-md-6 col-sm-6">
    <ul class="list-inline select-pages">
      
      <!-- <li><h4> Worker Total</h4></li> -->
      <li>
          <form name="searchForm" id="searchForm" method="GET" action="" role="form" class="srchfrm">
                   <span>Showing:</span>
                        <select name="show" id="show">
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        </select>
                  </form>
      </li>
    </ul>
  </div>
</div> 
</div>
<!-- <div class="col-md-3">
                <div class="become-cont">
                  <h4>Become a CollegeWRK</h4>
                  <p>Complete your Sweeps Profile to become an active Sweeper and work the jobs you want.</p>
                </div>
              </div> -->
</div>
</div>
</div>
</div>
<style type="text/css">
  .custom-width{
    width: calc(100% / 3);
  }
</style>>

@stop