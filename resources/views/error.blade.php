@extends('layouts.default')

@section('title', 'Error')

@section('content')

   <div class="col-md-12 dashboard-right">

          <div class="cut-dash-inner">

                 <div class="homepagetable">

                   <div class="row">
                     <div class="col-sm-12">
                  <?php 
                    $errors = DB::table('error_logs')->orderBy('id','DESC')->paginate('10');
                    $count  = count($errors);    
                  ?>
                @if($count > '0')

                    <div class="recent_jobs">
                      <table class="table table-striped history-table">
                      <thead>
                      <tr>
                        <th class="col13">Error Message</th>
                        <th class="col1">Line</th>
                        <th class="col4">File Name</th>
                        <th class="col2">Error Date</th>
                      </tr>

                    </thead>
                    <tbody>
                    @foreach($errors as $error)
                      <tr>
                        <td  class="col13" data-label="Job Post Date">{{$error->error_message}}</td>
                        <td  class="col1" data-label="Name of Job">{{$error->line_number}}</td>
                        <td  class="col4" data-label="Description">{{$error->file_name}}</td>
                        <td  class="col2" data-label="Required Worker">{{date('d M Y',strtotime($error->created_at))}}</td>
                      </tr>
                      @endforeach

                    </tbody>
                  </table>
                </div> 
               @else              
                <div class="table-error-mesg">
                    <p>Yepiee!!!!! No Error found</p>
                    
                </div>
              @endif
              </div>
              <div>{{$errors->render()}}</div>
            </div>
          </div>
     </div>
   </div>
</div>
</div>
</section>

@stop