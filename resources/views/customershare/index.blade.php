@extends('layouts.clientdefault')

@section('title', 'Customer Share')

@section('content')
 <div class="col-md-9 dashboard-right">
    <div class="share_workers share-sweep1">
           <!-- <div class="row pro-heading">
              <div class="col-sm-12">
                <div class="pro-head">
                <h3>Share Worker</h3>
              </div>
              </div>
            </div> -->

            <div class="row">
              <div class="col-md-12">
                <div class="invite-box">

                  <div class="customer-price">
                              <div class="row">
                                        <div class="col-md-12">
                                        <h4>Give $20 Credit and Get $20 </h4>
                                        <p class="wrk-p">For each new Customer you refer.</p>
                                      </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-12 paid-text">
                                       
                                        <h6>Your Referral Code</h6>
                                    
                                          <div class="form-group">
                                            <input type="text" name="" id="invite_code" placeholder="" value="{{$usercustomer->invitecode}}" readonly="readonly">
                                            <button onclick="myFunction()">Copy</button>
                                          </div>
                                          <div id="myTooltip"></div>
                                          
                                      </div>
                                      </div>

                                      <div class="row">
                                        <div class="col-md-6 share-online">
                                          <h2>Invite Friends and Get Paid</h2>
                                          <form name="sendmail_form" id="sendmail_form" action="#" method="POST">
                                            <div class="form-group">
                                              <input type="email" name="recipient_email[]" id="recipient_email" placeholder="Enter Your Friend's Email Here" class="form-control" value="">
                                              <input type="hidden" name="refferal_code" id="refferal_code" class="form-control" value="{{$usercustomer->invitecode}}">
                                              <input type="hidden" name="customer_name" id="customer_name" class="form-control" value="{{$usercustomer->name}}">
                                               <input type="hidden" name="customer_id" id="customer_id" class="form-control" value="{{$usercustomer->id}}">
                                              <button type="button" class="send-btn" id="send-btn">SEND</button>
                                            </div>
                                          </form>
                                          <p>Separate email address with commas.</p>
                                           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#preview_email">
                                           Preview Email
                                          </button>
                                          <ul class="list-inline soc-links">
                                            <li><a href="https://www.facebook.com/CollegeWRK" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="https://twitter.com/CollegeWrk_" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="https://www.linkedin.com" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                          </ul>
                                        </div>
                                        <div class="col-md-6 print-file yelp-img">
                                           <h2>Tell others about your experience</h2>
                                           <img src="{{url('/public')}}/images/yelp.png" class="center-block img-responsive">
                                           <img src="{{url('/public')}}/images/nextdoor.png" class="center-block img-responsive">
                                           <img src="{{url('/public')}}/images/google.png" class="center-block img-responsive">
                                        </div>

                                      </div>
                                      <!-- <div class="row referral-program">
                          <div class="col-md-4 col-sm-4">
                            <div class="inner-prog1">
                              <h5 id="referl-share-count">{{$code_share_count}}<br>
                                Total Referrals
                              </h5>
                            </div>
                          </div>
                         
                          <div class="col-md-4 col-sm-4">
                            <div class="inner-prog2">
                              <h5>$0<br>
                                Potential Reward
                              </h5>
                            </div>
                          </div>
                           <div class="col-md-4 col-sm-4">
                            <div class="inner-prog3">
                              <h5>0<br>
                                Paid To You
                              </h5>
                            </div>
                          </div>
                          <div class="col-md-12">
                          <h4>Referral Program Terms of Use</h4>
                        </div>
                        </div> -->
                        </div>
                        
                  
                </div>
                
              </div>
   <!--            <div class="col-md-3">
                
              </div> -->
            </div>


        </div>
</div>

<div class="modal fade" id="preview_email" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Preview Email</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p> Email Preview is here.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>

</div>
</section>

<script>
function myFunction() {
  var copyText = document.getElementById("invite_code");
  copyText.select();
  document.execCommand("copy");
  
  var tooltip = document.getElementById("myTooltip");
  tooltip.innerHTML = "Copied ";

  setTimeout(function() {
    $('#myTooltip').fadeOut('fast');
  }, 2000);
}
</script>
<script>
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
</script>

<script type="text/javascript">
jQuery(document).ready(function() {
  $("#send-btn").click(function(){
    var recipient_email = jQuery('#recipient_email').val();
    var refferal_code = jQuery('#refferal_code').val();
    var customer_name = jQuery('#customer_name').val();
    var customer_id = jQuery('#customer_id').val();
    var checkEmail = isEmail(recipient_email);
    if( checkEmail == true ){
    jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/customersendinvitation') }}",
            data:{'recipient_email':recipient_email,'refferal_code':refferal_code,'customer_name':customer_name,'customer_id':customer_id},
            success :  function(resp) {
             $('#referl-share-count').html(resp.totalcount+'<br>Total Referrals');
              alert(resp.message);
             
             }
       });
    }else{
      alert('Please provide a valid email');
    }
 
  });
});
</script>
<style type="text/css">
  .invite-box {
    margin-top: 0px;
  }
</style>
<!-- <script>
$(document).ready(function() {

jQuery("#sendmail_form").validate({
        rules: {
            'recipient_email[]': {
                    required: true,
                    email:true 
                }
            },
        messages: {
            'recipient_email[]': {
                  required : "Atleast one email address is required.",
                  email : "Please enter valid email address."
                }
              },
        submitHandler: function(form) {
            form.submit();
          }
        });
});

</script>
 -->
@stop