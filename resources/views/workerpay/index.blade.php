@extends('layouts.workerdefault')

@section('title', 'Worker Pay')

@section('content')



       <div class="" id="tab4">
         <div class="bank_account">

           @if($bnkworker!='')
           <input type="hidden" name="" id="bnkdetail" value="{{$bnkworker->id}}">
           @endif
            <div class="up-bnk-account">
              <div class="row pro-heading">
              <div class="col-sm-12">
                <div class="pro-head">
               <h3>CollegeWRK Pay</h3>
              </div>
              </div>
             </div>
              @if (\Session::has('success'))
              <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                  {!! \Session::get('success') !!}
              </div>
              @endif

         @if (\Session::has('error'))
            <div class="alert alert-danger alert-dismissable">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
               {!! \Session::get('error') !!}
            </div>
        @endif
           <div class="row">
          
           <div class="col-md-9" >
           
            @if($bnkworker=='')
            <div id="storedetial">
           <form class="profileFrm" id="profileFrm" action="{{ url('workerpay/store') }}" method="POST" enctype="multipart/form-data">
            {!! \Session::get('error') !!}
                <div class="add-acct">
                <div class="form-group col-md-6">
                   {{ csrf_field() }}
                  <label>Bank Routing Number</label>
                  <input type="text" name="bank_routing_number" placeholder=""  class="form-control">
                </div>
                <div class="form-group col-md-6">
                  <p>*The routing number of your bank, you can often find this by googling for "
                Bank Name Routing Number" or from the bottom of your check.</p>
                </div>
                <div class="form-group col-md-6">
                  <label>Bank Account Number</label>
                  <input type="text" name="bank_account_number" placeholder="" class="form-control">
                </div>
                <div class="form-group col-md-6">
                  <p>*Your bank account number. This is the middle number on the bottom of your bank checks.</p>
                </div>
                <div class="processing-txt col-md-12">
                  <p><input type="checkbox" name="bankck" style="margin-right: 10px;">I agree to the payment processing <a hrf="#">Terms of Service.</a></p> 

                </div>
                <div class="account-btn  col-md-12">
                  <button type="submit" class="bnk-btn">Add Bank Account</button>
                  <!-- <button class="bnk-btn">Cancel</button> -->
                </div>
              </div>
                </form>
            </div>
                @else
                <div id="updatediv">
                {{ Form::model($bnkworker, array('route' => array('workerpay.update', $bnkworker->id), 'id' => 'addproductform', 'class' => 'innerform', 'name' => 'addproductform','method' => 'PUT', 'files' => true)) }}
      
                <div class="add-acct">
                <div class="form-group col-md-6"> 
                  <label>Bank Routing Number</label>
                  <input type="text" name="bank_routing_number" placeholder=""  class="form-control" value="{{ $bnkworker->bank_routing_number}}" required="true">
                </div>
                <div class="form-group col-md-6">
                  <p>*The routing number of your bank, you can often find this by googling for "
                Bank Name Routing Number" or from the bottom of your check.</p>
                </div>
                <div class="form-group col-md-6">
                  <label>Bank Account Number</label>
                  <input type="text" name="bank_account_number" placeholder="" class="form-control" value="{{ $bnkworker->bank_account_number}}" required="true">
                </div>
                <div class="form-group col-md-6">
                  <p>*Your bank account number. This is the middle number on the bottom of your bank checks.</p>
                </div>
                <div class="processing-txt col-md-12">
                  <p><input type="checkbox" name="bankck" style="margin-right: 10px;" checked="true">I agree to the payment processing <a hrf="#">Terms of Service.</a></p>  

                </div>
                <div class="account-btn  col-md-12">
                  <button type="submit" class="bnk-btn update"> Bank Account Update</button><!--<a class="cancel-btn">Cancel</a>-->
                </div> 
              </div>
                  {{ Form::close() }}
                </div>

                  @endif
              </div>
              <div class="col-md-3">
                <div class="become-cont">
                  <h4>Become a CollegeWRKer</h4>
                  <p>Complete your CollegeWRK profile to become an active WRKer and work the jobs you want.</p>
                </div>
              </div>
            </div>
           </div>

           <div class="lower-bnk-account">
              <div class="row pro-heading">
              <div class="col-sm-12">
                <div class="pro-head">
                <h3>CollegeWRK Pay</h3>
              </div>
              </div>
            </div>
            @if (\Session::has('error'))
            <div class="alert alert-danger alert-dismissable">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
               {!! \Session::get('error') !!}
            </div>
        @endif
         
              <div class="row">
                <div class="col-md-9">
                <div class="bank-detail">
                  <div class="row">
                    <div class="col-md-12">
                      <h3>Bank Account</h3>
                      <p>When a job is completed and paid, your payment will be automatically transfered to this bank account. It typically takes 1 business day before the money arrives to your account. Please add one here.</p>
                    </div>
                    <div class="account-btn  col-md-12">
                  <button class="bnk-btn updt-btn" style="margin-left: 0px;margin-top:30px;">Add Bank Account</button>
                </div>
                  </div>
              </div>

              

              </div>
              <div class="col-md-3">
                <div class="become-cont">
                  <h4>Become a WRKer</h4>
                  <p>Complete your CollegeWRK Profile to become an active WRKer</p>
                </div>
              </div>
            </div>
            
          </div>
        </div>
        <div class="col-sm-9">
          <div class="row">
          <div class="bank-detail">
                  <div class="row">
                    <div class="col-md-12">
                      <h3>Transaction History</h3> <br />
                      <?php if( !empty($past_payments) ): ?>
                        <div class="table-responsive">
                        <table class="table table-striped">
                          <tr>
                            <th>Trancation ID</th>
                            <th>Job Title</th>
                          </tr>
                        <?php foreach ($past_payments as $past_payment) { ?>
                         <tr>
                            <td><?php echo $past_payment->worker_payment_transaction_id;?></td>
                            <td><?php echo $past_payment->cat_name;?></td>
                         </tr>
                      <?php  }
                      ?>
                    </table>
                  </div>
                      <?php else: echo'<p>No transaction history. Try working some jobs to get paid!</p>'; endif; ?> 
                    </div>
                  </div>
              </div>
            </div>
            </div>
      
        </div>
        </div>
      </div>
      <script type="text/javascript">
      $(document).ready(function() {
       $("#profileFrm").validate({
        rules: {
                bank_routing_number: {
                    required: true,
                    number: true,
                    minlength: 8,
                    maxlength: 9
                   },
                bank_account_number: {
                    required: true,
                    number: true,
                    minlength: 8,
                    maxlength: 16
                   },
                bankck: {
                    required: true
                   }
                },
        messages: {
                bank_routing_number: {
                  required: "Routing Number is required.", 
                  number: "Routing Number must be an number.",
                  minlength: 'Routing number cannot be less than 8 digits',
                  maxlength: 'Routing number cannot be more than 9 digits'
                   },
                bank_account_number: {
                  required: "Account number is required.", 
                  number: "Account Number must be an number.",
                  minlength: 'Account number cannot be less than 8 digits',
                  maxlength: 'Account number cannot be more than 16 digits'
                   },
               bankck: {
                  required: "field is required."
                   }
                },
        submitHandler: function(form) {
            form.submit();
          }
        });
              var hiddenval= $('#bnkdetail').val();
              if(hiddenval){
                  $('.up-bnk-account').show();
                  $('.lower-bnk-account').hide();
                
              }else{
                $('.up-bnk-account').hide();
                $('.lower-bnk-account').show();
              }

             //$('.up-bnk-account').hide();
             $('.updt-btn').click(function(){
             $('.up-bnk-account').show();
             $('.lower-bnk-account').hide();
                });
             
             $('.cancel-btn').click(function(){
             $('.up-bnk-account').hide();
             $('.lower-bnk-account').show();
                });
              });
      </script>
      <style type="text/css">
      .cancel-btn {
    background: #d8dcde;
    /*height: 50px;*/
    font-size: 14px;
    padding: 8px 12px;
    border: none;
    color: #303030;
}</style>
@stop