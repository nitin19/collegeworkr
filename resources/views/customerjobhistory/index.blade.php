@extends('layouts.clientdefault')

@section('title', 'Customer Job History')

@section('content')
  <div class="col-md-9 dashboard-right">
           <div class="job-history">
            <div class="row pro-heading">
              <div class="col-sm-12">
                <div class="pro-head">
                <h3>Job History <button class="pst-newjb" data-toggle="modal" data-target="#demo-modal-3">Post New Job</button></h3>
              </div>
              </div>
            </div>
            
    <table class="table table-striped history-table">
    <thead>
    <tr class="headings">
      <th class="col2">Job Post Date</th>
      <th class="col3">Type of Job</th>
      <th class="col3">Description</th>
      <th class="col3">Location</th>
      <th class="col4">Number of Required WRkers</th>
      <!-- <th class="col3">Job Status</th> -->
      <th class="col2">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php 
        $count=count($clientjobpost);    
    ?>
    @if($count > '0')
    @foreach($clientjobpost as $clientjob)
    <?php 
     $catid = $clientjob->job_heading;
     $categories   = DB::table('categories')
                            ->where(['id'=>$catid, 'status' => '1','deleted' => '0'])
                            ->first();
      $cat_name = $categories->cat_name; 
       $createdate = explode(' ', $clientjob->created_at);  
      

    ?>
                        
    <tr>
      <td data-label="Job Post Date" class="col2">{{date('d M Y',strtotime($createdate[0]))}}</td>
      <td data-label="Name of Job" class="col3">{{$cat_name}} in <!-- {{$clientjob->job_location}} --></td>
      <td data-label="Description" class="col3">{{$clientjob->job_description}}</td>
      <td data-label="Location" class="col3">{{$clientjob->job_location}}</td>
      <td data-label="Required Worker" class="col4">{{$clientjob->worker_required}}</td>
      <!-- <td data-label="Required Worker" class="col3">{{$clientjob->status}}</td> -->
      <td class="column5 col2" data-label="Detail" class="col2"><a href="{{ route('customerjobhistory.show', $clientjob->id )}}" class="view-table">View Detail </a></td>
    @endforeach
    @else
    <tr>
     <td>
        No job posted yet.
     </td>
  </tr>
  @endif

  </tbody>

</table>
     <div class="pagination_section">

                               {!! $clientjobpost->render() !!} 
                </div>
</div>

</div>
</div>
</section>

<script type="text/javascript">
  $(document).ready(function() {
    $("#preview_email").modal({
        show: false,
        backdrop: 'static'
    });
    
    $("#click-me").click(function() {
       $("#preview_email").modal("show");             
    });
});
</script>

<style type="text/css">
 a.btn.btn-lg {
    background: #3399cc;
    color: #fff;
}#click-me {
    background: #337ab7;
    border: 1px solid #337ab7;
    padding: 8px 17px;
    border-radius: 5px;
    color: #fff;
}

  button#complehide {
    opacity: 0.6;
    cursor: default;
}
</style>
@stop