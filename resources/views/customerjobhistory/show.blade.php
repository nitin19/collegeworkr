@extends('layouts.clientdefault')

@section('title', 'Customer job History')

@section('content')
<div class="col-md-9 dashboard-right">
        <div class="tab-pane active text-style" id="tab1">
        <div class="row">
       <div class="col-md-12 rightcontent">
        
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="openjobs">
      <div class="tab_contentarea">
        <?php

          $catid = $job_listing->job_heading;
          $categories   = DB::table('categories')
                            ->where(['id'=>$catid, 'status' => '1','deleted' => '0'])
                            ->first();
         $cat_name = $categories->cat_name; 
         $posted=$job_listing->created_at;         
         $createdate = explode(' ', $job_listing->created_at);   
         $timedate=$job_listing->created_at;
         $time=date('h:i A', strtotime($timedate));
         ?>
         <div class="customer-head">
             <h1> {{ $cat_name}} in {{  $job_listing->job_location }} </h1>
         </div>
        
        <div class="row">
        <div class="col-md-8 tablewrapper tablediv">
            <div class="customer-head">
                <h1>Details</h1>
            </div>
          <table class="table-striped">
                   
            <tr class="headings"><th class="column1">Category</th><td>{{ $cat_name}}</td> </tr>
            <tr class="headings"><th class="column1">Description</th><td>{{  $job_listing->job_description}}</td> </tr>
            <tr class="headings"><th class="column1">Equipment Needed </th><td>
            <?php
             $equipment = explode(',',$job_listing->equipment_needed); 
            // print_r($equipment );
             $equ_name='';
             if(count(array_filter($equipment)) > 0){
            ?>

            @foreach($equipment as $equ)
            <?php
               $equl_tbl   = DB::table('equipment')
                             ->where(['id'=>$equ, 'status' => '1','deleted' => '0'])
                             ->first();
               $equ_name.=$equl_tbl->equ_name.',';                           
            ?>
           
            @endforeach  
             {{ rtrim($equ_name,',') }}
             <?php } ?>
            </td> </tr>
            <tr class="headings"><th class="column1">Worker Requested </th><td>{{  $job_listing->worker_required}}</td> </tr>
            <!-- <tr class="headings"><th class="column1">Worker Type </th><td>{{  $job_listing->worker_type}}</td> </tr> -->
            <tr class="headings"><th class="column1">Worker Description </th><td>{{  $job_listing->worker_desc}}</td> </tr>
           <!-- <tr class="headings"><th class="column1">When and Where</th> </tr> -->
            <tr class="headings"><th class="column1">Location</th><td>{{  $job_listing->job_location }}</td> </tr>
            <tr class="headings"><th class="column1">Start Time </th><td>{{  $job_listing->start_time}}</td> </tr>
            <tr class="headings"><th class="column1">Time Estimate</th><td>{{  $job_listing->time_estimate}}</td> </tr>
            <tr class="headings"><th class="column1">When Posted </th><td>{{date('d M Y ',strtotime($createdate[0]))}}  , {{ $time }}</td> </tr>
         </table>
        </div>
      
      <div class="col-md-4">
          <!-- <h3><b>Want to compete for and work this job?</b></h3>
          <p>Once you finish your application and are approved by a Worker Admin, you can compete for jobs. -->
          <?php //$type = $job_listing->worker_type;
            /*$job_id     = $job_listing->id; 
            $user_id    = $job_listing->user_id; 
            $id         = Auth::user()->id;
            $applypost  = DB::table('reservedjobs')
                          ->where('client_userid', '=', $user_id)
                          ->where('client_jobid', '=', $job_id)
                          ->where('worker_userid', '=', $id)
                          ->first();*/
          /*$user_data  = DB::table('users')
                                  ->where('id', '=', $user_id)
                                  ->first();
        
          $worker_type = $user_data->workertype;*/
         ?>
            <?php //echo $user_type = Auth::user()->workertype; 
             //@if($type == $worker_type)
            ?>
            
           
        <!--</p>-->

         @if(count($worker_chat_listing) > 0)
          @foreach($worker_chat_listing as $chat_listing)
          <?php
        $worker_id= $chat_listing->worker_id;
        $job_name = DB::table('users')
                      ->where('id', '=', $worker_id)
                      ->where('status', '=', 1)
                      ->where('deleted', '=', 0)
                      ->first();

       $worker_name = $job_name->firstname;
                      ?>
            <a href="{{url('/')}}/chat/?hired_id={{$chat_listing->id}}&&job_id={{$job_id}}&&client_id={{$clientid}}&&worker_id={{$chat_listing->worker_id}}" class="start-chat">Start Chat with {{$worker_name}}</a>
          @endforeach 
        @else
          <span></span>
        @endif

    </div> 
  </div>

<!-- ================= Thank You modal pop up =========================== -->   
      <div class="modal fade" id="preview_email" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header"></div>
                 {{ Form::model($job_listing, array('route' => array('workerjobs.update', $job_listing->id), 'id' => 'addproductform', 'class' => 'innerform', 'name' => 'addproductform','method' => 'PUT', 'files' => true)) }}
              <div class="modal-body">
                  <input type="hidden" name="client_userid" value="{{ $job_listing->user_id}}">
                  <input type="hidden" name="client_jobid" value="{{ $job_listing->id}}">
                  <input type="hidden" name="client_jobdesc" value="{{  $job_listing->job_description}}">
                 <input type="hidden" name="job_status" value="Applied">
        
                   <h3><b>Thankyou for applying this job we will get back soon!</b><h3>
              </div>
              <div class="modal-footer">
                <input type="submit" name="Close" value="Close" class="btn_close_ne" id="click-me">       
              </div>

                {{ Form::close() }}
            </div>
          </div>
        </div>
<!-- ==================================================================== -->
    </div>
   </div>
  </div>
 </div>
</div>
</div>
<!--         </div>
        </div> -->
<script type="text/javascript">
  $(document).ready(function() {
    $("#preview_email").modal({
        show: false,
        backdrop: 'static'
    });
    
    $("#click-me").click(function() {
       $("#preview_email").modal("show");             
    });
});
</script>
  <style type="text/css">
          a.btn.btn-lg {
    background: #3399cc;
    color: #fff;
}#click-me {
    background: #337ab7;
    border: 1px solid #337ab7;
    padding: 8px 17px;
    border-radius: 5px;
    color: #fff;
}
.start-chat {
    background: #3399cc;
    padding: 6px 12px;
    font-size: 14px;
    color: #fff;
    font-weight: bold;
    border-radius: 5px;
    display: inline-block;
    margin-bottom: 10px;
}
  </style>
        
@stop