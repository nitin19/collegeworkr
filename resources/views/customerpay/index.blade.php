@extends('layouts.clientdefault')

@section('title', 'Clientdashboard')

@section('content')

<div class="col-md-9 dashboard-right">
     <form>
           <div class="bank_account">
             <div class="lower-bnk-account">
              <div class="row pro-heading">
              <div class="col-sm-12">
                <div class="pro-head">
                <h3>CollegeWRK Payment</h3>
              </div>
              </div>
            </div>
         
              <div class="row">
                <div class="col-md-12">
                <div class="bank-detail">
                  <div class="row">
                    <div class="col-md-12">
                      <h3>Sweeps Credit: $0.00</h3>
                      <p>Share your <a href=""> CollegeWRK</a> referral code <strong>mannatm16204</strong> to increase your balance. Visit the Share <a href=""> CollegeWRK</a> Page to learn more!</p>
                    </div>
                   
                  </div>
              </div>

              <div class="bank-detail">
                  <div class="row">
                    <div class="col-md-12">
                      <h3>Cards<a href="add-new-card.html" class="adcrd">ADD CARD</a></h3>
                      <p>You can select your preferred card at job checkout. Otherwise the first card in this table will be charged automatically 48 hours after timesheets are submitted. No credit cards present. <a href=""> Add one</a> now!</p>
                    </div>
                  </div>
              </div>
               <div class="bank-detail">
                  <div class="row">
                    <div class="col-md-12">
                      <h3>Transaction</h3>
                      <p>No transactions present. Time to <a href="#"> Post a Job</a> or <a href="#"> Refer Friends!</a></p>
                    </div>
                  </div>
              </div>

              </div>
              
            </div>
            
          </div>
        </div>
           </form>
</div>
</div>
</section> 
@stop