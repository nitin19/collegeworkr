@extends('layouts.defautladmin')

@section('title', 'Support')

@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />
<?php $current_userId = Auth::id(); ?>
<div class="collegechat">
   
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-comment"></span> Chat
                    
                </div>
                <div class="">
                <div class="col-md-3">
                    <ul class="sidepanl" id="loadChatcnt">
                    <?php
                    if($usersCount > 0 ) {
                        foreach($users as $user) {
                    ?>  
                    <script type="text/javascript">
                        $(document).ready(function(){ 
                          fetchmsgcnt(<?php echo $user['id'];?>, <?php echo $current_userId;?>);
                          setInterval(function(){
                              fetchmsgcnt(<?php echo $user['id'];?>, <?php echo $current_userId;?>);
                            }, 9000);
                        });
                    </script>
                        <li class="left clearfix selectedUser <?php if($firstUser->id == $user['id']) { echo 'active'; }?>" id="<?php echo $user['id'];?>"><span class="chat-img pull-left">
                              <img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" />
                           <!--  <?php if($user['image']!='') { ?>
                            <img src="{{url('/public')}}/uploads/profile/{{$user['image']}}" alt="User Avatar" class="profilpic img-circle" />
                            <?php } else { ?>
                            <img src="{{url('/public')}}/uploads/profile/1548139021.download.png" alt="User Avatar" class="profilpic img-circle" />
                          <?php  } ?> -->
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <strong class="primary-font"><?php echo ucfirst($user['firstname']).' '.ucfirst($user['lastname']); ?></strong> <small class="pull-right text-muted">
                              <span class="dyn_Msg_cnt" id="dynMsgcnt_<?php echo $user['id'];?>"></span></small>
                                </div>
                                <p>
                                   <?php echo ucfirst($user['user_role']); ?>
                                </p>
                            </div>
                        </li>
                    <?php             
                 }
               } else {
            echo '<li class="left clearfix"><span class="chat-img pull-left">
                            <p>No user found</p>
                        </li>';
                    }
                ?>    
                    </ul>
                </div>
            <div class="col-md-9">
                <div class="panel-body">
                    <ul class="chat" id="loadChat">
                     
                    </ul>
                </div>
                <div class="panel-footer adnch">
                    <form>
                    <div class="input-group">

                    <input type="hidden" name="sender_id" id="sender_id" value="<?php echo $current_userId;?>">
                    <input type="hidden" name="receiver_id" id="receiver_id" value="<?php echo $firstUser->id;?>">
                    <input type="hidden" name="chat_type" id="chat_type" value="<?php if( isset($_GET['type']) ): echo $_GET['type']; endif;?>">

                        <input id="message" name="message" type="text" class="form-control input-sm" placeholder="Type your message here..." />
                        <span class="input-group-btn">
                            <button class="btn btn-warning btn-sm" id="btn-chat">
                                Send</button>
                        </span>
                    </div>

                    <div class="alert alert-danger" id="alert-danger" style="display: none;">
                        <strong>Alert!</strong> Empty Message can not be send 
                    </div>
                     </form>

                </div>
            </div>
        </div>
                
            </div>
        </div>

<script type="text/javascript">

 $(document).ready(function(){
  
   /* $(window).load(function(){
        $("ul.sidepanl li:first-child").trigger('click');
    });*/

    $('ul li').click(function() {
       $('ul li.selectedUser').removeClass('active');
       $(this).closest('li').addClass('active');
    });

    $(".alert-danger").slideUp(2000);

    $( "#message" ).focus(function() {
         $("#alert-danger").hide();
    });

    $(".selectedUser").click(function(e){
       e.preventDefault();
       //========================================//
       var receiverid = $(this).attr('id');
       var newURL = location.href.split("?")[0];
       window.history.pushState('object', document.title, newURL);
       $('#chat_type').val('');
       //========================================//
        $('#dynMsgcnt_'+receiverid).removeClass('newMsgcnt');
        $('#dynMsgcnt_'+receiverid).text('');

       var receiver_id = $("#receiver_id").val(receiverid); 
       var senderid = $("#sender_id").val();
       fetchRecords(senderid, receiverid);
       clearInterval(myInterval);
       myInterval = setInterval(function(){
        fetchRecords(senderid, receiverid);
       }, 5000);

     });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#btn-chat").click(function(e){
        e.preventDefault();
        var message = $("#message").val();
        var sender_id = $("#sender_id").val();
        var receiver_id = $("#receiver_id").val(); 
        var chat_type = $("#chat_type").val(); 
        if(message!='') {
        $.ajax({
           type:'POST',
           global: false,
           url:'/adminajaxRequest',
           data:{ sender_id:sender_id, receiver_id:receiver_id, message:message , chat_type : chat_type},
           success:function(data){
              data=jQuery.parseJSON(data);
              if(data.statusCode==1) {
                 $("#message").val('');
              } else {
                return false;
              }
           }
        });
       } else {
         $("#alert-danger").show();
       } 
  });

  var senderid = $("#sender_id").val();
  var receiverid = $("#receiver_id").val();
  fetchRecords(senderid, receiverid);

    myInterval = setInterval(function(){
    fetchRecords(senderid, receiverid);
    }, 5000);

});

    function fetchRecords(senderid, receiverid){
      // url: 'getadminChat/'+senderid+'/'+receiverid,
       $.ajax({
         url: 'https://collegewrk.com/chatsystem/getadminchat.php',
         type: 'GET',
         global: false,
         dataType: 'html',
         data:{ senderid:senderid, receiverid:receiverid },
         success: function(response){
            $('#loadChat').html(response);
         }
       });
    }

    function fetchmsgcnt(sndid, rcvid){

       // console.log(sndid+'=='+rcvid);
       /*url: 'getadminChatmsgcnt/'+sndid+'/'+rcvid,*/

       $.ajax({
         url: 'https://collegewrk.com/chatsystem/getmsgcount.php',
         type: 'GET',
         global: false,
         data:{ sndid:sndid, rcvid:rcvid },
         success: function(response){
           // console.log(response);

            var receiver_id = $("#receiver_id").val(); 
            $('#dynMsgcnt_'+receiver_id).removeClass('newMsgcnt');
            $('#dynMsgcnt_'+receiver_id).text('');

           if( response > 0 ) {
             $('#dynMsgcnt_'+sndid).addClass('newMsgcnt');
             $('#dynMsgcnt_'+sndid).text(response);
           } else {
             $('#dynMsgcnt_'+sndid).removeClass('newMsgcnt');
             $('#dynMsgcnt_'+sndid).text('');
           }
           
         }
       });
    }

</script>
<style>
.collegechat {padding: 20px;}
.panel {
    margin-bottom: 20px;
    background-color: #fff;
    border: 1px solid transparent;
    border-radius: 4px;
    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,0.05);
    box-shadow: 0 1px 1px rgba(0,0,0,0.05);
}
.panel-primary {
    border-color: #55c1e7;
    border: 1px solid #55c1e7;
    float: left;
    width: 100%;
}
.panel-primary>.panel-heading {
    color: #fff;
    background-color: #55c1e7;
    border-color: #55c1e7;
    padding: 10px 15px;
    border-bottom: 1px solid transparent;
    border-top-right-radius: 3px;
    border-top-left-radius: 3px;
}
.pull-left {
    float: left!important;
}
.pull-right {
    float: right!important;
}
.img-circle {
    border-radius: 50%;
    margin-right:5px;
}
.panel-body {
    padding: 15px;
    overflow-y: scroll;
    height: 550px !important;
    background-color: #e5f1f6;
}
    .chat
{
    list-style: none;
    margin: 0;
    padding: 0;
}

.chat li
{
    margin-bottom: 10px;
    padding-bottom: 10px;
    border-bottom: 1px dotted #B3A9A9;
    overflow: hidden;
}

.chat li.left .chat-body
{
    margin-left: 40px;
}

.chat li.right .chat-body
{
    margin-right: 40px;
}


.chat li .chat-body p
{
    margin: 0;
    color: #777777;
    font-size:13px;
    line-height: 16px;
    word-break: break-all;
}

.panel .slidedown .glyphicon, .chat .glyphicon
{
    margin-right: 5px;
}

.panel-body
{
    overflow-y: scroll;
    height: 550px;
}

::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}

::-webkit-scrollbar
{
    width: 5px;
    background-color: #ccc;
}

::-webkit-scrollbar-thumb
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
    background-color: #ccc;
}
.panel-footer {
    padding: 10px 15px;
    background-color: #55c1e7;
    border-top: 1px solid #ddd;
    border-bottom-right-radius: 3px;
    border-bottom-left-radius: 3px;
    text-align: right;
}
.panel-footer .input-group {
    position: relative;
    display: table;
    border-collapse: separate;
    width: 100%;
}
.panel-footer input#btn-input {
    padding: 8px;
    border: 1px solid #ccc;
    margin-right: -6px;
}
.input-group-btn {
    position: relative;
    white-space: nowrap;
}
.btn-warning {
    color: #fff;
    background-color: #f0ad4e;
    border-color: #eea236;
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.428571429;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    cursor: pointer;
    border: 1px solid transparent;
    border-radius: 0px 4px 4px 0px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    -o-user-select: none;
    user-select: none;
    margin-left:2px;
}
.btn-warning:hover, .btn-warning:active, .btn-warning:focus {color:#fff;}
.sidepanl {
    max-width: 100%;
    width:100%;
    float: left;
    list-style: none;
    max-height: 601px;
    overflow-y: scroll;
    padding: 0px 0px;
    margin: 0px;
}
.sidepanl .chat-body.clearfix {hidden
    margin-left: 40px;
}

.sidepanl .left.clearfix {
    border-bottom: 2px solid #eee;
    padding: 10px 10px 2px 10px;
    cursor: pointer;
}
.sidepanl p {
    font-size: 12px;
    font-weight: 400;
    line-height: 24px;
    color: #878787;
}
.primary-font {
    font-size: 12px;
}
#loadChat .img-circle {
    width: 30px;
    height: 30px;
    object-fit: cover;
}
.adnch #message {
    width: 92%;
    height: 30px;
    float: left;
    margin-right: 0px;
    border: 1px solid #e5f1f6;
}
.adnch .input-group-btn {
    width: 8%;
    float: left;
    height: 30px;
}
.adnch #btn-chat {
    height: 30px;
    padding: 0px;
    width: 100%;
    margin-left:-2px;
}
.adnch form {margin:0px;}
.clearfix.selectedUser.active {
    background-color: rgba(85, 193, 231, 0.35)
}
.clearfix.selectedUser:hover {
    background-color: rgba(85, 193, 231, 0.35)
}
.clearfix.selectedUser:focus {
    background-color: rgba(85, 193, 231, 0.35)
}
.newMsgcnt {
    color: red;
}
.chat li.right {
    background-color: #ffffff;
    list-style: none;
    width: 50%;
    border-radius: 40px;
    padding: 10px 25px 7px 25px;
    float: right;hidden
    box-shadow: 0px 0px 20px #eee;
    border: none;
}
.chat li.left {
    background-color: #55c1e7;
    list-style: none;
    width: 50%;
    color:#fff;
    border-radius: 40px;
    padding: 7px 25px 5px 25px;
    float: left;
    box-shadow: 0px 0px 20px #eee;
    border: none;
}
.chat li.left .chat-body p {color:#fff;word-break: break-all;}
.chat li.left .text-muted {color:#fff !important;}
.collegechat .col-md-9 {
    padding: 0px;
}
.collegechat .col-md-3 {
    padding: 0px;
}
#adminChat {
    width: 100%;
    display: block;
    float: left;
}
@media only screen and (max-width: 991px) {
    .chat li.left {width: 90%;}
    .chat li.right {width: 90%;}
    .img-circle {width: 30px;height: 30px;}
    .sidepanl p {line-height: 10px;}
    .adnch #message {width: 85%;}
    .adnch .input-group-btn {width: 15%;}
}
@media only screen and (max-width: 910px) {
.sidepanl .chat-body.clearfix {
    float: left;
    width: 60%;
    margin-left: 0px;
    line-height: 13px;
}
}
@media only screen and (max-width: 575px) {
    #adminChat {position:relative;padding:15px 0px 10px 0px;}
    .chat li.left .img-circle {position:absolute;display:block;top:-15px;left:0px;z-index:999;}
    .chat li.right .img-circle {position:absolute;display:block;top:-15px;right:0px;z-index:999;}
    .chat li.left .chat-body {margin-left: 0px;}
    .chat li.right .chat-body {margin-right: 0px;}
    .sidepanl .chat-body.clearfix {width: 80%;}
}
</style>


    @stop