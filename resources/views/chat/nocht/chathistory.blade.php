@extends('layouts.defautladmin')

@section('title', 'Chat history')

@section('content')

<?php 
$current_userId = Auth::id(); 
    function humanTiming ($time) {
        $time = time() - $time; 
        $time = ($time<1)? 1 : $time;
        $tokens = array (
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
        );
    foreach ($tokens as $unit => $text) {
    if ($time < $unit) continue;
    $numberOfUnits = floor($time / $unit);
    return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
      }
    }
?>
<div class="content Contextua_sec customers_detail">
	<div class="col-sm-12">
		<h2>Chat history <span class="badge pull-right"> </h2>
	</div>
	<div class="col-sm-12">

		<div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-comments"></i> Chat
                    <span class="pull-right"><a href="{{url('/adminjobs/')}}/{{$jobid}}" class="project-link">Go to project</a></span>
                </div>
                <div class="panel-body">
                    <ul class="chat">
		 
		<?php
		if($chatCount > 0 ) {
           
			foreach($chatData as $chat) {
                $time = strtotime($chat['created_at']);
				$senderInfo = DB::table('users')->where('id', '=', $chat['sender_id'])->first();
               // $receiverInfo = DB::table('users')->where('id', '=', $chat['receiver_id'])->first();
                ?>
                <li class="right clearfix"><span class="chat-img pull-right">
               <?php if($senderInfo->image!='') { ?>
                    <img src="{{url('/public')}}/uploads/profile/<?php echo $senderInfo->image;?>" alt="User Avatar" class="profilpic img-circle" />
                    <?php } else { ?>
                    <img src="{{url('/public')}}/uploads/profile/1548139021.download.png" alt="User Avatar" class="profilpic img-circle" />
                    <?php } ?>
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <small class=" text-muted"><i class="fa fa-clock-o" aria-hidden="true"></i><?php echo humanTiming($time).' ago';?></small>
                                    <strong class="pull-right primary-font"><?php echo $senderInfo->firstname.' '.$senderInfo->lastname;?></strong>
                                </div>
                                <p>{{$chat['message']}}</p>
                            </div>
                        </li>
             <?php             
		      }
		    } else {
            echo '<li class="left clearfix">
                            <div class="chat-body clearfix">
                                <p>
                                    No caht history.
                                </p>
                            </div>
                        </li>';
        }
		
		?>
		</ul>
                </div>
               
            </div>

	</div>
<div class="content pagi_div">

</div>
@include('layouts.footer_admin') 
  
</div> 

    </div>
<style type="text/css">
	
	/********Chat-box-css**********/
.chat
{
    list-style: none;
    margin: 0;
    padding: 0;
}

.chat li
{
    margin-bottom: 10px;
    padding-bottom: 5px;
    border-bottom: 1px dotted #B3A9A9;
}

.chat li.left .chat-body
{
    margin-left: 60px;
}

.chat li.right .chat-body
{
    margin-right: 60px;
}


.chat li .chat-body p
{
    margin: 0;
    color: #777777;
    text-align: right;
}

.panel .slidedown .glyphicon, .chat .glyphicon
{
    margin-right: 5px;
}

.panel-body {
    overflow-y: scroll;
    height: 450px;
    background-color: #fff;
    padding: 20px 10px;
    border:1px solid #337ab7;
   
}
.panel-heading {
    color: #fff;
    background-color: #337ab7;
    border-color: #337ab7;
    padding: 10px;
    border-radius: 5px 5px 0px 0px;
}
::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}

::-webkit-scrollbar
{
    width: 12px;
    background-color: #F5F5F5;
}

::-webkit-scrollbar-thumb
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
    background-color: #555;
}
.profilpic.img-circle {
    width: 35px;
    height: 35px;
    border-radius: 50%;
}
.primary-font {
    font-size: 12px;
}
.panel-body p {
    font-size: 13px;
}
.project-link {
    color: #fff;
    font-weight: bold;
    font-size: 12px;
    text-decoration: underline;
    padding: 5px;
}
.project-link:hover, .project-link:active, .project-link:focus {
    color: #fff;
    font-weight: bold;
    font-size: 12px;
    text-decoration: underline;
    padding: 5px;
}

</style>

    @stop