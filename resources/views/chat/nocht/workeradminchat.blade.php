@extends('layouts.workerdefault')

@section('title', 'Chat Box')

@section('content')

<?php $current_userId = Auth::id(); ?>

<meta name="csrf-token" content="{{ csrf_token() }}" />

   <div class="col-md-9 dashboard-right">
          <div class="cut-dash-inner">
            <!--chat-box-div-->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-comments"></i> Chat 
                  
                </div>


                <div class="panel-body">

                    <ul class="chat" id="loadChat">

                    </ul>


                </div>



                <div class="panel-footer">
                   <form>
                    <div class="input-group">
                       
                        <input type="hidden" name="sender_id" id="sender_id" value="<?php echo $current_userid;?>">
                        <input type="hidden" name="receiver_id" id="receiver_id" value="<?php echo $admin_id;?>">
                        <input id="message" name="message" type="text" class="form-control input-sm" placeholder="Type your message here..." value="" />
                        <span class="input-group-btn">
                            <button class="btn btn-warning btn-sm" id="btn-chat">
                                Send</button>
                        </span>
                    </div>

                    <div class="alert alert-danger" id="alert-danger" style="display: none;">
                        <strong>Alert!</strong> Empty Message can not be send 
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<!--Chat-box-end-->
                
</div>
</div>

<script type="text/javascript">

 $(document).ready(function(){

   $(".alert-danger").slideUp(2000);

    $( "#message" ).focus(function() {
         $("#alert-danger").hide();
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#btn-chat").click(function(e){
        e.preventDefault();
        var message = $("#message").val();
        var sender_id = $("#sender_id").val();
        var receiver_id = $("#receiver_id").val(); 
        if(message!='') {
        $.ajax({
           type:'POST',
           url:'/workeradminajaxRequest',
           data:{ message:message, sender_id:sender_id, receiver_id:receiver_id },
           success:function(data){
              data=jQuery.parseJSON(data);
              if(data.statusCode==1) {
                 $("#message").val('');
              } else {
                return false;
              }
           }
        });
       } else {
         $("#alert-danger").show();
       } 
  });
    
  var senderid = $("#sender_id").val();
  var receiverid = $("#receiver_id").val();
  /*fetchRecords(senderid, receiverid);
  setInterval(function() {
    fetchRecords(senderid, receiverid);
  }, 1000000000000000000);*/

 });  
 
    function fetchRecords(senderid, receiverid){
       $.ajax({
         url: 'getworkeradminChat/'+senderid+'/'+receiverid,
         type: 'GET',
         dataType: 'html',
         success: function(response){
            $('#loadChat').html(response);
         }
       });
    }

</script>
<style>
.panel {
    margin-bottom: 20px;
    background-color: #fff;
    border: 1px solid transparent;
    border-radius: 4px;
    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,0.05);
    box-shadow: 0 1px 1px rgba(0,0,0,0.05);
}
.panel-primary {
    border-color: #428bca;
}
.panel-primary>.panel-heading {
    color: #fff;
    background-color: #55c1e7;
    border-color: #55c1e7;
    padding: 10px 15px;
    border-bottom: 1px solid transparent;
    border-top-right-radius: 3px;
    border-top-left-radius: 3px;
}
.pull-left {
    float: left!important;
}
.pull-right {
    float: right!important;
}
.img-circle {
    border-radius: 50%;
}
.panel-body {
    padding: 15px;
    overflow-y: scroll;
    height: 450px;
}
    .chat
{
    list-style: none;
    margin: 0;
    padding: 0;
}

.chat li
{
    margin-bottom: 10px;
    padding-bottom: 10px;
    border-bottom: 1px dotted #B3A9A9;
    overflow: hidden;
}

.chat li.left .chat-body
{
    margin-left: 60px;
}

.chat li.right .chat-body
{
    margin-right: 60px;
}


.chat li .chat-body p
{
    margin: 0;
    color: #777777;
}

.panel .slidedown .glyphicon, .chat .glyphicon
{
    margin-right: 5px;
}

.panel-body
{
    overflow-y: scroll;
    height: 450px;
}

::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}

::-webkit-scrollbar
{
    width: 5px;
    background-color: #ccc;
}

::-webkit-scrollbar-thumb
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
    background-color: #ccc;
}
.panel-footer {
    padding: 10px 15px;
    background-color: #f5f5f5;
    border-top: 1px solid #ddd;
    border-bottom-right-radius: 3px;
    border-bottom-left-radius: 3px;
    text-align: right;
}
.panel-footer .input-group {
    position: relative;
    display: table;
    border-collapse: separate;
    width: 100%;
}
.panel-footer input#btn-input {
    padding: 8px;
    border: 1px solid #ccc;
    margin-right: -6px;
}
.input-group-btn {
    position: relative;
    white-space: nowrap;
}
.btn-warning {
    color: #fff;
    background-color: #f0ad4e;
    border-color: #eea236;
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.428571429;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    cursor: pointer;
    border: 1px solid transparent;
    border-radius: 0px 4px 4px 0px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    -o-user-select: none;
    user-select: none;
}
.sidepanl {
    max-width: 100%;
    width:100%;
    float: left;
    list-style: none;
    height: 550px;
    overflow-y: scroll;
    padding: 10px;
    margin: 0px;
}
.sidepanl .chat-body.clearfix {
    margin-left: 60px;
}

.sidepanl .left.clearfix {
    border-bottom: 2px solid #eee;
    padding-top: 10px;
}
.sidepanl p {
    font-size: 12px;
    font-weight: 400;
    line-height: 24px;
    color: #878787;
}
.primary-font {
    font-size: 12px;
}
#loadChat .img-circle {
    width: 50px;
    height: 50px;
    object-fit: cover;
}
.adnch #message {
    width: 92%;
    height: 30px;
    float: left;
}
.adnch .input-group-btn {
    width: 8%;
    float: left;
    height: 30px;
}
.adnch #btn-chat {
    height: 30px;
    padding: 0px;
    width: 100%;
    margin-left:-2px;
}
.adnch form {margin:0px;}
.clearfix.selectedUser.active {
    background-color: #e3e3e3;
}
</style>

@stop