@extends('layouts.clientdefault')

@section('title', 'Chat Box')

@section('content')

<?php $current_userId = Auth::id(); ?>

<meta name="csrf-token" content="{{ csrf_token() }}" />

   <div class="col-md-9 dashboard-right">
          <div class="cut-dash-inner">
            <!--chat-box-div-->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-comments"></i> Chat 
                   <!--  <div class="btn-group pull-right">
                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu slidedown">
                            <li><a href="https://www.jquery2dotnet.com"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</a></li>
                        </ul>
                    </div> -->
                </div>


                <div class="panel-body">

                    <ul class="chat" id="loadChat">

                    </ul>


                </div>



                <div class="panel-footer">
                   <form>
                    <div class="input-group">
                        <input type="hidden" name="hired_id" id="hired_id" value="<?php echo $hired_id;?>">
                        <input type="hidden" name="job_id" id="job_id" value="<?php echo $job_id;?>">
                        <input type="hidden" name="client_id" id="client_id" value="<?php echo $client_id;?>">
                        <input type="hidden" name="worker_id" id="worker_id" value="<?php echo $worker_id;?>">
                        <input id="message" name="message" type="text" class="form-control input-sm" placeholder="Type your message here..." value="" />
                        <span class="input-group-btn">
                            <button class="btn btn-warning btn-sm" id="btn-chat">
                                Send</button>
                        </span>
                    </div>

                    <div class="alert alert-danger" id="alert-danger" style="display: none;">
                        <strong>Alert!</strong> Empty Message can not be send 
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<!--Chat-box-end-->
                
</div>
</div>

<script type="text/javascript">

 $(document).ready(function(){

   $(".alert-danger").slideUp(2000);

    $( "#message" ).focus(function() {
         $("#alert-danger").hide();
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#btn-chat").click(function(e){
        e.preventDefault();
        var hired_id = $("#hired_id").val();
        var job_id = $("#job_id").val();
        var message = $("#message").val();
        var client_id = $("#client_id").val();
        var worker_id = $("#worker_id").val(); 
        if(message!='') {
        $.ajax({
           type:'POST',
           url:'/ajaxRequest',
           data:{ hired_id:hired_id, job_id:job_id, message:message, client_id:client_id, worker_id:worker_id },
           success:function(data){
              data=jQuery.parseJSON(data);
              if(data.statusCode==1) {
                 $("#message").val('');
              } else {
                return false;
              }
           }
        });
       } else {
         $("#alert-danger").show();
       } 
  });


/*    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });*/

/*    $('#but_fetchall').click(function(){
     var hireid = $("#hired_id").val();
     var jobid = $("#job_id").val();
        $.ajax({
         url: 'getChat/'+hireid+'/'+jobid,
         type: 'GET',
         success: function(response){
            alert(response);
         }
       });

    });*/
    
  var hireid = $("#hired_id").val();
  var jobid = $("#job_id").val();
  /*fetchRecords(hireid, jobid);
  setInterval(function() {
    fetchRecords(hireid, jobid);
  }, 1000000000000000000);*/

 });  
 
    function fetchRecords(hireid, jobid){
       $.ajax({
         url: 'getChat/'+hireid+'/'+jobid,
         type: 'GET',
         dataType: 'html',
         success: function(response){
            $('#loadChat').html(response);
         }
       });
    }

    

</script>


@stop