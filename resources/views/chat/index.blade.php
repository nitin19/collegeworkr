
<?php 
    $client_id = $_GET['client_id'];
    $worker_id = $_GET['worker_id'];
    $current_userId = Auth::id(); 
    if( $current_userId == $client_id ){
        $header = 'clientdefault';
    }else{
        $header = 'workerdefault';
    }
?>
@extends('layouts.'.$header)
@section('title', 'Chat Box')
@section('content')


<meta name="csrf-token" content="{{ csrf_token() }}" />

   <div class="">
          <div class="cut-dash-inner">
            <!--chat-box-div-->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-comments"></i> Chat 
                   <!--  <div class="btn-group pull-right">
                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu slidedown">
                            <li><a href="https://www.jquery2dotnet.com"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</a></li>
                        </ul>
                    </div> -->
                </div>


                <div class="panel-body">

                    <ul class="chat" id="loadChat">

                    </ul>


                </div>



                <div class="panel-footer">
                   <form>
                    <div class="input-group">
                        <input type="hidden" name="crnt_uid" id="crnt_uid" value="<?php echo $current_userId;?>">
                        <input type="hidden" name="hired_id" id="hired_id" value="<?php echo $hired_id;?>">
                        <input type="hidden" name="job_id" id="job_id" value="<?php echo $job_id;?>">
                        <input type="hidden" name="client_id" id="client_id" value="<?php echo $client_id;?>">
                        <input type="hidden" name="worker_id" id="worker_id" value="<?php echo $worker_id;?>">
                        <input id="message" name="message" type="text" class="form-control input-sm" placeholder="Type your message here..." value="" />
                        <span class="input-group-btn">
                            <button class="btn btn-warning btn-sm" id="btn-chat">
                                Send</button>
                        </span>
                    </div>

                    <div class="alert alert-danger" id="alert-danger" style="display: none;">
                        <strong>Alert!</strong> Empty Message can not be send 
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<!--Chat-box-end-->
                
</div>
</div>

<script type="text/javascript">

 $(document).ready(function(){

   $(".alert-danger").slideUp(2000);

    $( "#message" ).focus(function() {
         $("#alert-danger").hide();
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#btn-chat").click(function(e){
        e.preventDefault();
        var hired_id = $("#hired_id").val();
        var job_id = $("#job_id").val();
        var message = $("#message").val();
        var client_id = $("#client_id").val();
        var worker_id = $("#worker_id").val(); 
        if(message!='') {
        $.ajax({
           type:'POST',
           url:'/ajaxRequest',
           global: false,
           data:{ hired_id:hired_id, job_id:job_id, message:message, client_id:client_id, worker_id:worker_id },
           success:function(data){
              data=jQuery.parseJSON(data);
              if(data.statusCode==1) {
                 $("#message").val('');
              } else {
                return false;
              }
           }
        });
       } else {
         $("#alert-danger").show();
       } 
  });


/*    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });*/

/*    $('#but_fetchall').click(function(){
     var hireid = $("#hired_id").val();
     var jobid = $("#job_id").val();
        $.ajax({
         url: 'getChat/'+hireid+'/'+jobid,
         type: 'GET',
         success: function(response){
            alert(response);
         }
       });

    });*/
    
  var hireid = $("#hired_id").val();
  var jobid = $("#job_id").val();
  var crntuid = $("#crnt_uid").val();
  fetchRecords(hireid, jobid, crntuid);
  setInterval(function() {
    fetchRecords(hireid, jobid, crntuid);
  }, 5000);

 });  
 
    function fetchRecords(hireid, jobid, crntuid){
      //url: 'getChat/'+hireid+'/'+jobid,
       $.ajax({
         url: 'https://collegewrk.com/chatsystem/getjobchat.php',
         type: 'GET',
         global: false,
         dataType: 'html',
         data:{ hireid:hireid, jobid:jobid, crntuid:crntuid },
         success: function(response){
            $('#loadChat').html(response);
         }
       });
    }

    

</script>
<style>
.collegechat {padding: 20px;}
.panel {
    margin-bottom: 20px;
    background-color: #fff;
    border: 1px solid transparent;
    border-radius: 4px;
    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,0.05);
    box-shadow: 0 1px 1px rgba(0,0,0,0.05);
}
.panel-primary {
    border-color: #55c1e7;
    border: 1px solid #55c1e7;
    float: left;
    width: 100%;
}
.panel-primary>.panel-heading {
    color: #fff;
    background-color: #55c1e7;
    border-color: #55c1e7;
    padding: 10px 15px;
    border-bottom: 1px solid transparent;
    border-top-right-radius: 3px;
    border-top-left-radius: 3px;
}
.pull-left {
    float: left!important;
}
.pull-right {
    float: right!important;
}
.img-circle {
    border-radius: 50%;
    margin-right:5px;
}
.panel-body {
    padding: 15px;
    overflow-y: scroll;
    height: 450px;
    background-color: #e5f1f6;
}
    .chat
{
    list-style: none;
    margin: 0;
    padding: 0;
}

.chat li
{
    margin-bottom: 10px;
    padding-bottom: 10px;
    border-bottom: 1px dotted #B3A9A9;
    overflow: hidden;
}

.chat li.left .chat-body
{
    margin-left: 40px;
}

.chat li.right .chat-body
{
    margin-right: 40px;
}


.chat li .chat-body p
{
    margin: 0;
    color: #777777;
    font-size:16px;
    line-height: 16px;
    word-break: break-all;
}

.panel .slidedown .glyphicon, .chat .glyphicon
{
    margin-right: 5px;
}

.panel-body
{
    overflow-y: scroll;
    height: 450px;
}

::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}

::-webkit-scrollbar
{
    width: 5px;
    background-color: #ccc;
}

::-webkit-scrollbar-thumb
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
    background-color: #ccc;
}
.panel-footer {
    padding: 10px 15px;
    background-color: #55c1e7;
    border-top: 1px solid #ddd;
    border-bottom-right-radius: 3px;
    border-bottom-left-radius: 3px;
    text-align: right;
}
.panel-footer .input-group {
    position: relative;
    display: table;
    border-collapse: separate;
    width: 100%;
}
.panel-footer input#btn-input {
    padding: 8px;
    border: 1px solid #ccc;
    margin-right: -6px;
}
.input-group-btn {
    position: relative;
    white-space: nowrap;
}
.btn-warning {
    color: #fff;
    background-color: #f0ad4e;
    border-color: #eea236;
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.428571429;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    cursor: pointer;
    border: 1px solid transparent;
    border-radius: 0px 4px 4px 0px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    -o-user-select: none;
    user-select: none;
    margin-left:2px;
}
.btn-warning:hover, .btn-warning:active, .btn-warning:focus {color:#fff;}
.sidepanl {
    max-width: 100%;
    width:100%;
    float: left;
    list-style: none;
    max-height: 501px;
    overflow-y: scroll;
    padding: 0px 0px;
    margin: 0px;
}
.sidepanl .chat-body.clearfix {hidden
    margin-left: 40px;
}

.sidepanl .left.clearfix {
    border-bottom: 2px solid #eee;
    padding: 10px 10px 2px 10px;
    cursor: pointer;
}
.sidepanl p {
    font-size: 12px;
    font-weight: 400;
    line-height: 24px;
    color: #878787;
}
.primary-font {
    font-size: 12px;
}
#loadChat .img-circle {
    width: 30px;
    height: 30px;
    object-fit: cover;
}
.adnch #message {
    width: 92%;
    height: 30px;
    float: left;
    margin-right: 0px;
    border: 1px solid #e5f1f6;
}
.adnch .input-group-btn {
    width: 8%;
    float: left;
    height: 30px;
}
.adnch #btn-chat {
    height: 30px;
    padding: 0px;
    width: 100%;
    margin-left:-2px;
}
.adnch form {margin:0px;}
.clearfix.selectedUser.active {
    background-color: rgba(85, 193, 231, 0.35)
}
.clearfix.selectedUser:hover {
    background-color: rgba(85, 193, 231, 0.35)
}
.clearfix.selectedUser:focus {
    background-color: rgba(85, 193, 231, 0.35)
}
.newMsgcnt {
    color: red;
}
.chat li.right {
    background-color: #ffffff;
    list-style: none;
    width: 50%;
    border-radius: 40px;
    padding: 10px 25px 7px 25px;
    float: right;hidden
    box-shadow: 0px 0px 20px #eee;
    border: none;
}
.chat li.left {
    background-color: #55c1e7;
    list-style: none;
    width: 50%;
    color:#fff;
    border-radius: 40px;
    padding: 7px 25px 5px 25px;
    float: left;
    box-shadow: 0px 0px 20px #eee;
    border: none;
}
.chat li.left .chat-body p {color:#fff;word-break: break-all;}
.chat li.left .text-muted {color:#fff !important;}
.text-muted {font-size:9px;}
.collegechat .col-md-9 {
    padding: 0px;
}
.collegechat .col-md-3 {
    padding: 0px;
}
#adminChat {
    width: 100%;
    display: block;
    float: left;
}
@media only screen and (max-width: 991px) {
    .chat li.left {width: 90%;}
    .chat li.right {width: 90%;}
    .img-circle {width: 30px;height: 30px;}
    .sidepanl p {line-height: 10px;}
     .adnch #message {width: 85%;}
    .adnch .input-group-btn {width: 15%;}
}
@media only screen and (max-width: 910px) {
.sidepanl .chat-body.clearfix {
    float: left;
    width: 60%;
    margin-left: 0px;
    line-height: 13px;
}
}
@media only screen and (max-width: 575px) {
    #adminChat {position:relative;padding:15px 0px 10px 0px;}
    .chat li.left .img-circle {position:absolute;display:block;top:-15px;left:0px;z-index:999;}
    .chat li.right .img-circle {position:absolute;display:block;top:-15px;right:0px;z-index:999;}
    .chat li.left .chat-body {margin-left: 0px;}
    .chat li.right .chat-body {margin-right: 0px;}
}
</style>

@stop
