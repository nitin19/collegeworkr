<section class="dashboard_header">
  <div class="container-fluid">
    <div class="row">
    <div class="col-sm-3 logodiv">
      <a href="{{url('/')}}">
      <img src="{{ url('/public') }}/images/newlogo.png" class="img-responsive">
      </a>
    </div>
    <div class="col-sm-9 dashboard_rgtheader">
      <div class="col-sm-4 col-xs-12 header_left">
        <h3>Worker Dashboard</h3>
      </div>
      <div class="mobile_navdiv">
       <div class="col-sm-8 col-xs-9 header_right text-right">
        
        
        <div class="col-sm-12 ma-hu">
         <?php  $id = Auth::user()->id;
          $count_total = DB::table('notification')
                      ->where('worker_userid', '=', $id)
                      ->where('job_status', '=', 'hired')
                      ->where('status_read', '=', 0)
                      ->get();
          $social_login = DB::table('users')
                      ->where('id', '=', $id)
                      ->where('status', '=', 1)
                      ->where('deleted', '=', 0)
                      ->first(); 
         $social_login_token = $social_login->google_id;
          ?>  
    <ul class="dashboardright">
                    <?php 
  if ($social_login_token=='') {
?> 
      <li class="custdshbrd"><a href="{{url('customerdashboard')}}" class="btn btn-lg"> Customer Dashboard</a></li>
      <?php } else { ?>
      <li class="custdshbrd"><a class="btn btn-lg"> Customer Dashboard</a></li>
      <?php } ?>
     <li class="notification">
     <div class="dropdown">
          <!-- <button class="dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-globe" ></i></span></button> -->
          <button class="dropdown-toggle" type="button" data-toggle="dropdown"><img src="{{url('/public')}}/images/nitification-icon.png"></span></button>
          <ul class="dropdown-menu">
            @if(count($count_total) > 0)
            @foreach($count_total as $notify)
            <li><a href="#" id="notity" class="title" notify="{{$notify->id}}">{{$notify->message}}</a></li>
            @endforeach
            @endif
          </ul>
     </div>
     <span class="counter">{{count($count_total)}}</span></li>
       <li class="dropdown prflimgdiv">
      @if(Auth::user()->image !='')
         <img src="{{url('/public')}}/uploads/profile/{{Auth::user()->image}}" class="profilpic">
      @else
         <img src="{{url('/public')}}/images/avatar.jpg" class="profilpic">
      @endif
     </li>
            
             @if(Auth::check())

                 <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->firstname }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{url('/')}}/workerprofile" class="logount_hed_sec">
                                            Profile
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="logount_hed_sec">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown prflimgdiv">
                                <a href="" class="cutmer-dash-tog"><i class="fa fa-bars" aria-hidden="true"></i></a>
                            </li>
        @endif
     </ul>
    </div>
      </div>
    <div class="col-xs-3 nav-toggle">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
    <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
  </button>
</div>
  
  </div>
  </div>
</div>
</div>
</section>  
<section class="dashboard-main">
  <div class="container-fluid">
    <div class="row dashboard-rw">
      <div class="col-md-3 sidebar-left nopadding" id="nav-div1">
          <nav class="nav-sidebar">
              <?php 
  if ($social_login_token=='') {
?> 
          <ul class="nav tabs">
                <li class=""><a href="{{url('workerjobs')}}"><i class="fa fa-briefcase" aria-hidden="true"></i><span>Jobs</span></a></li>
                <li class=""><a href="{{url('workerprofile')}}"><i class="fa fa-user" aria-hidden="true"></i>Profile</a></li>
                <li class=""><a href="{{ url('workershare')}}"><i class="fa fa-share-alt" aria-hidden="true"></i>Share Workers </a></li>
                <li class=""><a href="{{ url('workerpay')}}"><i class="fa fa-usd" aria-hidden="true"></i>CollegeWRK Pay</a></li>
                <li class=""><a href="{{ url('workerpayment')}}"><i class="fa fa-usd" aria-hidden="true"></i>Payments</a></li>  
                <li class=""><a href="{{ url('workerhelp')}}"><i class="fa fa-question-circle" aria-hidden="true"></i>Help </a></li>
          </ul>
          <?php } else {?>
          <ul class="nav tabs">
                <li class=""><a><i class="fa fa-briefcase" aria-hidden="true"></i><span>Jobs</span></a></li>
                <li class=""><a><i class="fa fa-user" aria-hidden="true"></i>Profile</a></li>
                <li class=""><a><i class="fa fa-share-alt" aria-hidden="true"></i>Share Workers </a></li>
                <li class=""><a><i class="fa fa-usd" aria-hidden="true"></i>CollegeWRK Pay</a></li>
                <li class=""><a><i class="fa fa-usd" aria-hidden="true"></i>Payments</a></li>  
                <li class=""><a><i class="fa fa-question-circle" aria-hidden="true"></i>Help </a></li>
          </ul>
             <?php }?>
        </nav>
    </div>
<div class="col-md-9 dashboard-right width80">
  <div class="tab-content ">

<script>
$(document).ready(function(){
    $(".cutmer-dash-tog").click(function(){
        $("#nav-div1").fadeToggle("slow");
    });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
$("#notity").click(function(){
    var noti = $("#notity").attr("notify");
    jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/notitysec') }}",
            data :{'noti': noti},
            success :  function(resp) {
                if(resp=='true'){
                  window.location.href="{{url('/workerjobs')}}";

                }
             }
       });
});
});
</script>
<style type="text/css">
  .dashboardright li {
    display: inline-block;
}
  .dashboardright button.dropdown-toggle {
    background: transparent;
    border: none;
}
.dropdown-menu {
    left: -206px !important;
}
</style>