@extends('layouts.workerdefault')

@section('title', 'Workerdashboard')

@section('content')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<section class="dashboard_header">
  <div class="container-fluid">
    <div class="row">
    <div class="col-sm-3 logodiv">
      <img src="{{ url('/public') }}/images/newlogo.png" class="img-responsive">
    </div>
    <div class="col-sm-9 dashboard_rgtheader">
      <div class="col-sm-5 col-xs-12 header_left">
        <h3>Worker Dashboard</h3>
      </div>
      <div class="mobile_navdiv">
      <div class="col-sm-7 col-xs-9 header_right text-right">

        <div class="col-md-4">
          <a href="{{ url('customerdashboard') }}" class="updt-btn">Customer Dashboard</a>
        </div>
 

    <ul class="dashboardright">

     <li class="notification"><i class="fa fa-globe"></i>
     <span class="counter">5</span></li>
     <li class="dropdown prflimgdiv">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="{{ url('/public') }}/images/profileimg.jpg" class="profilpic"> 
            
             @if(Auth::check())

                 <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->firstname }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{url('/')}}/workerdashboard" class="logount_hed_sec">
                                            Dashboard
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="logount_hed_sec">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
        @endif
    </a>
        </li>
     </ul>
    </div>
    <div class="col-xs-3 nav-toggle">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
    <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
  </button>
</div>
</div>
    </div>
  </div>
  </div>
</section>
  
<section class="dashboard-main">
  <div class="container-fluid">
    <div class="row dashboard-rw">
      <div class="col-md-3 sidebar-left nopadding">
          <nav class="nav-sidebar">
          <ul class="nav tabs">
                <li class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-briefcase" aria-hidden="true"></i><span>Jobs</span></a></li>
                <li class=""><a href="#tab2" data-toggle="tab"><i class="fa fa-user" aria-hidden="true"></i>Profile</a></li>
                <li class=""><a href="#tab3" data-toggle="tab"><i class="fa fa-share-alt" aria-hidden="true"></i>Share Workers </a></li>
                <li class=""><a href="#tab4" data-toggle="tab"><i class="fa fa-usd" aria-hidden="true"></i>CollegeWRK Pay</a></li> 
                <li class=""><a href="#tab5" data-toggle="tab"><i class="fa fa-question-circle" aria-hidden="true"></i>Help </a></li>
          </ul>
        </nav>
    </div>
<div class="col-md-9 dashboard-right">
  <div class="tab-content ">
        <div class="tab-pane active text-style" id="tab1">
        <div class="row">
       <div class="col-md-9 rightcontent">
        <p>You will be able to compete for and work jobs once you finish your application</p>

        <div class="tab-section">
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#openjobs" aria-controls="openjobs" role="tab" data-toggle="tab">Open Jobs <span>(8)</span></a></li>
    <li role="presentation"><a href="#competing" aria-controls="competing" role="tab" data-toggle="tab">Competing For<span>(0)</span></a></li>
    <li role="presentation"><a href="#awarded" aria-controls="awarded" role="tab" data-toggle="tab">Awarded<span>(0)</span></a></li>
    <li role="presentation"><a href="#confirmed" aria-controls="confirmed" role="tab" data-toggle="tab">Confirmed <span>(0)</span></a></li>
    <li role="presentation"><a href="#completed" aria-controls="completed" role="tab" data-toggle="tab">Completed<span>(0)</span></a></li>
     <li role="presentation"><a href="#paid" aria-controls="paid" role="tab" data-toggle="tab">Paid<span>(0)</span></a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="openjobs">
      <div class="tab_contentarea">
        <h1>Open Jobs</h1>
        <div class="tablewrapper tablediv">
          <table class="table-striped">
        <thead>
          <tr class="headings">
            <th class="column1">Job Type</th>
            <th class="column2">Description</th>
            <th class="column3">Time Estimation</th>
            <th class="column4">Detail</th>
          </tr>
        </thead>
        <tbody>
          <tr class="familydata custom-reports">
            <td class="column1" data-label="Job Type"><img src="{{ url('/public') }}/images/2.png"><span>Cleaning in Durham, 
                NC 27707</span></td>
            <td class="column2" data-label="Description">Shop vac basement. Wash down basement 
                shelves. Wash cement patio. Wash a few windows.
                  I have all equipment.</td>
            <td class="column3" data-label="Time Estimation">Thu. Jul 19 at 10:00am <br/>3 hours</td>
            <td class="column4" data-label="Detail"><a href="#" class="view-table">View Detail</a></td>
          </tr>
          <tr class="familydata custom-reports">
            <td class="column1" data-label="Job Type"><img src="{{ url('/public') }}/images/8.png"><span>Odd Jobs in Chapel Hill</span></td>
            <td class="column2" data-label="Description">Shop vac basement. Wash down basement 
              shelves. Wash cement patio. Wash a few windows.
              I have all equipment.</td>
            <td class="column3" data-label="Time Estimation">Thu. Jul 19 at 10:00am <br/>3 hours</td>
            <td class="column4" data-label="Detail"><a href="#" class="view-table">View Detail</a></td>
          </tr>
          <tr class="familydata custom-reports">
            <td class="column1" data-label="Job Type"><img src="{{ url('/public') }}/images/6.png"><span>Painting in Cary</span></td>
            <td class="column2" data-label="Description">Shop vac basement. Wash down basement 
              shelves. Wash cement patio. Wash a few windows.
              I have all equipment.</td>
            <td class="column3" data-label="Time Estimation">Thu. Jul 19 at 10:00am <br/>3 hours</td>
            <td class="column4" data-label="Detail"><a href="#" class="view-table">View Detail</a></td>
          </tr>
          <tr class="familydata custom-reports">
            <td class="column1" data-label="Job Type"><img src="{{ url('/public') }}/images/1.png"><span>Moving in Raleigh</span></td>
            <td class="column2" data-label="Description">Shop vac basement. Wash down basement 
                shelves. Wash cement patio. Wash a few windows.
                I have all equipment.</td>
            <td class="column3" data-label="Time Estimation">Thu. Jul 19 at 10:00am <br/>3 hours</td>
            <td class="column4" data-label="Detail"><a href="#" class="view-table">View Detail</a></td>
          </tr>
          <tr class="familydata custom-reports">
            <td class="column1" data-label="Job Type"><img src="{{ url('/public') }}/images/8.png"><span>Odd Jobs in Chapel Hill</span></td>
            <td class="column2" data-label="Description">Shop vac basement. Wash down basement 
              shelves. Wash cement patio. Wash a few windows.
              I have all equipment.</td>
            <td class="column3" data-label="Time Estimation">Thu. Jul 19 at 10:00am <br/>3 hours</td>
            <td class="column4" data-label="Detail"><a href="#" class="view-table">View Detail</a></td>
          </tr>
          <tr class="familydata custom-reports">
            <td class="column1" data-label="Job Type"><img src="{{ url('/public') }}/images/2.png"><span>Cleaning in Durham, 
                NC 27707</span></td>
            <td class="column2" data-label="Description">Shop vac basement. Wash down basement 
                shelves. Wash cement patio. Wash a few windows.
                  I have all equipment.</td>
            <td class="column3" data-label="Time Estimation">Thu. Jul 19 at 10:00am <br/>3 hours</td>
            <td class="column4" data-label="Detail"><a href="#" class="view-table">View Detail</a></td>
          </tr>
          <tr class="familydata custom-reports">
            <td class="column1" data-label="Job Type"><img src="{{ url('/public') }}/images/8.png"><span>Odd Jobs in Chapel Hill</span></td>
            <td class="column2" data-label="Description">Shop vac basement. Wash down basement 
              shelves. Wash cement patio. Wash a few windows.
              I have all equipment.</td>
            <td class="column3" data-label="Time Estimation">Thu. Jul 19 at 10:00am <br/>3 hours</td>
            <td class="column4" data-label="Detail"><a href="#" class="view-table">View Detail</a></td>
          </tr>
        </tbody>
      </table>

        </div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="competing">2</div>
    <div role="tabpanel" class="tab-pane" id="awarded">3</div>
    <div role="tabpanel" class="tab-pane" id="confirmed">4</div>
    <div role="tabpanel" class="tab-pane" id="completed">5</div>
    <div role="tabpanel" class="tab-pane" id="paid">6</div>
  </div>

</div>
<div class="row pagination-list">
  <div class="col-md-6 col-sm-6">
      <nav aria-label="Page navigation example">
        <ul class="pagination">
          <li class="page-item"><a class="page-link" href="#"><i class="fa fa-caret-left" aria-hidden="true"></i></a></li>
          <li class="page-item active"><a class="page-link" href="#">1</a></li>
          <li class="page-item"><a class="page-link" href="#">2</a></li>
          <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item"><a class="page-link" href="#"><i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
       </ul>
  </nav>
  </div>
  <div class="col-md-6 col-sm-6">
    <ul class="list-inline select-pages">
      <li><h4>855 Students Total</h4></li>
      <li><label>Showing:</label>
        <select>
          <option>10</option>
          <option>20</option>
          <option>30</option>
        </select>
      </li>
    </ul>
  </div>
</div>
</div>
<div class="col-md-3">
                <div class="become-cont">
                  <h4>Become a CollegeWRK</h4>
                  <p>Complete your Sweeps Profile to become an active Sweeper and work the jobs you want.</p>
                </div>
              </div>
</div>
</div>
  <div class="tab-pane text-style" id="tab2">
    <div  class="main-profile">
    <div class="upper-profile">
      <div class="row pro-heading">
        <div class="col-sm-12">
          <div class="pro-head">
          <h3>PROFILE</h3>
        </div>
        </div>
      </div>
      <div class="row user-info">
        <div class="col-md-2 nopadding">
             @if(count($userworker) > 0)
            <img src="{{ url('/public') }}/uploads/profile/{{ $userworker->image }}" style="margin: 0 10px; width:150px;height:150px;" class="img-responsive">
             @else
             <img src="{{ url('/public') }}/images/user-pic.png" class="img-responsive">
            @endif
         </div>
        <div class="col-md-10">
          <div class="user-info">
          <h2> 
          <?php echo ucfirst($userworker->firstname); ?> </h2>
          <p>{{ $userworker->worker_attend }}</p>
          <h6><span>SLOGAN:</span> {{ $userworker->worker_slogan }}</h6>
        </div>
        </div>
      </div>
      <div class="row user-about">
        <div class="col-sm-12">
          <h4>ABOUT ME:</h4>
          <p>{{ $userworker->worker_about }} </p>
        </div>
      </div>
      <div class="row ">
        <div class="col-md-12">
          <a href="#info_profile" class="updt-btn">Update Account Information</a>
        </div>
      </div>
      
    </div>
    <div class="lower-profile" id="info_profile">
      <div class="row pro-heading">
        <div class="col-sm-12">
          <div class="pro-head">
          <h3>Update Profile Information</h3>
        </div>
        </div>
      </div>
      <div class="row jb-prefrence" >
        <div class="col-md-9">
              <div class="panel with-nav-tabs panel-default panel-preference">
          <div class="panel-heading">
                  <ul class="nav nav-tabs">
                      <li class="active"><a href="#tab1default" data-toggle="tab">Account Information</a></li>
                      <li><a href="#tab2default" data-toggle="tab">Job preference</a></li>
                      <li><a href="#tab3default" data-toggle="tab">Onboarding</a></li>
                      
                  </ul>
          </div>
<div class="panel-body">
  <div class="tab-content">
    <div class="tab-pane fade in active" id="tab1default">
     <form class="profileFrm" id="profileFrm" action="{{ url('workerdashboard/store') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
     <div class="about-tb">
      <h4>About You</h4>
      <div class="row tp-rw-1">
        <div class="col-sm-3">
        <!--   <img src="{{ url('/public') }}/images/user-pic.png" class="img-responsive"> -->
         @if(count($userworker) > 0)
            <div class="col-sm-12 col-xs-3 browseimage nopadding" id="browse_img">
               <img src="{{ url('/public') }}/uploads/profile/{{ $userworker->image }}" style="margin: 0 10px; width:150px;height:150px;" class="img-responsive">
                 </div>
                @else
                <img src="{{ url('/public') }}/images/user-pic.png" class="img-responsive">
          @endif
            <div class="col-sm-12 col-xs-9 new_up_sec">
            <input type="file" id="image" name="image">
            <div class="browse_btn" id="browsebtn">
            </div>
          </div>
      </div>
        <div class="col-sm-9">
          <h5> <?php echo ucfirst($userworker->firstname); ?></h5>
          <h6>Add a Profile Photo</h6>
          <p>In your profile photo, we recommend a high-resolution, well-lit photo of your smiling face (without sunglasses). Recommended dimensions are 300x300 pixels.</p>
        </div>
        <div class="col-md-12 tp-rw-col1">
          <h6>Slogan</h6>
          <p class="dr-clr">In a few words, describe yourself and your ambitions. This will be visible and prominent on your profile. (20 - 100 characters)</p>
          <input type="text" name="worker_slogan" placeholder="I am Painter & intersted in doing paintings. And want work." class="form-control" value="{{ $userworker->worker_slogan }}">
        </div>
         <div class="col-md-12 tp-rw-col1">
          <h6>About</h6>
          <p class="dr-clr">In a few words, describe yourself and your ambitions. This will be visible and prominent on your profile. (20 - 100 characters)</p>
        <textarea placeholder="" class="form-control" rows="5" name="worker_about">{{ $userworker->worker_about }}</textarea>
        </div>
      </div>
    </div>

  <div class="university-info">
    <h4>College/University Info</h4>
    <div class="row">
      <div class="form-group  col-sm-12">
          <label>What school do you attend? </label>
        <select class="form-control" id="worker_attend" name="worker_attend"> 
           <option>Select a School</option>
            @foreach ($worker_attend as $worker_attends)
            <option value="{{ $worker_attends->name }}" {{ ($userworker->worker_attend ==  $worker_attends->name ) ? 'selected' : '' }}>{{ $worker_attends->name }}</option>
          @endforeach
          </select>
        </div>
        <div class="form-group col-md-6">
          <label>Your school email address (.edu) </label>
          <input type="email" name="worker_school" placeholder="youremail@university.edu" class="form-control" value="{{ $userworker->worker_school }}">
        </div>
        <div class="form-group col-md-6">
          <label>What are you studying? </label>
        <select class="form-control" id="worker_qualification" name="worker_qualification"> 
          <option>Select Your Primary Major</option>
            @foreach ($worker_qualification as $worker_qualifications)
            <option value="{{ $worker_qualifications->id }}" {{ ($userworker->worker_qualification ==  $worker_qualifications->id ) ? 'selected' : '' }}>{{ $worker_qualifications->name }}</option>
          @endforeach
          </select>
        </div>
      
    </div>
  </div>
<div class="pers-info">
  <h4>Personal Info</h4>
  <div class="row">
    <div class="form-group col-md-6">
      <label>Birthday</label>
      <div class="date">
        <div class="input-group input-append date" id="datePicker">
             <input type="text" class="form-control" name="dob"  value="{{ $userworker->dob }}"/>
           <span class="input-group-addon add-on"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></span> 
        </div>
    </div>
    </div>
  
  <div class="form-group col-md-6">
    <label>Phone Number</label>
    <input type="tel" name="phone" placeholder="Phone Number" class="form-control" value="{{ $userworker->phone }}">
  </div>
   <div class="form-group col-md-12 gender-input">
    <label class="head-gen">Gender</label>
    <label class="radio-inline">
    <input type="radio" name="gender" checked="checked" value="Male" <?php if($userworker->gender=="Male"){ echo "checked";}?>> Male
    </label>
     <label class="radio-inline">
    <input type="radio" name="gender" value="Female" <?php if($userworker->gender=="Female"){ echo "checked";}?>> Female
    </label>
  </div>
   <div class="form-group col-md-12 gender-input">
    <label class="head-gen">Have you ever been convicted of a crime?</label>
    <label class="radio-inline">
      <input type="radio" name="worker_crime"  checked="checked" value="Yes" <?php if ($userworker->worker_crime=="Yes"){echo "checked";} ?>>Yes
    </label>
     <label class="radio-inline">
     <input type="radio" name="worker_crime" value="No" <?php if ($userworker->worker_crime=="No"){echo "checked";}?>> No
     </label>
  </div>
  <div class="form-group col-md-6">
    <label>Address 1</label>
    <input type="text" name="address" placeholder="Address" class="form-control" value="{{ $userworker->address }}">
  </div>
  <div class="form-group col-md-6">
    <label>Address 2</label>
    <input type="text" name="address1" placeholder="Address" class="form-control" value="{{ $userworker->address1 }}">
  </div>
  <div class="form-group col-md-5">
    <label>City</label>
    <input type="text" name="city" placeholder="City" class="form-control"  value="{{ $userworker->city }}"> 
  </div>
  <div class="form-group col-md-5">
    <label>State</label>
    <select class="form-control" id="state" name="state"> 
      <option>Select Your State</option>
          @foreach ($state as $states)
          <option value="{{ $states->id }}" {{ ($userworker->state ==  $states->id ) ? 'selected' : '' }}>{{ $states->name }}</option>
        @endforeach
        </select>
  </div>
  <div class="form-group col-md-2">
    <label>Zip code</label>
    <input type="text" name="zip_code" placeholder="Zip code" class="form-control" value="{{ $userworker->zip_code }}">
  </div>
   <div class="form-group col-md-6">
    <label>How'd you hear about CollegeWRK?</label>
    <select class="form-control" id="worker_college" name="worker_college"> 
         <option>Select One</option>>
          @foreach ($worker_college as $worker_colleges)
          <option value="{{ $worker_colleges->id }}" {{ ($userworker->worker_college ==  $worker_colleges->id ) ? 'selected' : '' }}>{{ $worker_colleges->name }}</option>
        @endforeach
        </select>
  </div>
  <div class="form-group col-md-6 refer-code">
    <label>Do you have a Referral Code?</label>
     <input type="text" name="" placeholder="" class="form-control"><button class="apl-btn">Apply</button>
  </div>
</div>
</div>
<div class="col-md-12">
 <button type="submit" class="updt-btn" id="save_info_work">Save Information</button>
</div>
</form>
</div>
<div class="tab-pane fade" id="tab2default">
  
  <div class="panel with-nav-tabs perf-content">
    <div class="panel-heading">
            <ul class="nav nav-tabs" id="mytabs" role="tablist">
                <li class="active">
                <a href="#tab1primary" data-toggle="tab" role="tab">Campuses</a></li>
                <li><a href="#tab2primary" data-toggle="tab" role="tab">Categories</a></li>
                <li><a href="#tab3primary" data-toggle="tab" role="tab">Equipment</a></li>
                <li><a href="#tab4primary" data-toggle="tab" role="tab">Notifications</a></li>
             </ul>
    </div>
  <div class="panel-body">
      <div class="tab-content">
          <div class="tab-pane fade in active" id="tab1primary">
            <div class="travel-bonus">
              <div class="row">
                <div class="col-md-12">
                  <h3>Become a CollegeWrk</h3>
                  <p>Complete your Sweeps Profile to become an active Sweeper and work the jobs you want.</p>
                  <h3>Travel Bonuses</h3>
                  <p>You are responsible for your own transportation to and from jobs.</p>

                  <p>Travel bonuses are only paid for jobs > 10 miles from the nearest campus and are clearly displayed on jobs. Note, that travel bonuses are not based on where you are coming from, rather where the job address is.</p>

                  <p>If you work a job that has unexpected or a lot of travel involved, please communicate with the customer proactively about adding travel cost as an expense in your timesheet.</p>
                  <h4>Where do you want to work?</h4>
                  <p>Select all campuses you may want to work. You will receive notifications based on these preferences and can edit them at any point.</p>

               <form class="jobpre" id="jobpre" action=""  enctype="multipart/form-data">
                 {{ csrf_field() }}
                  <div class="form-group">
                  <select id="location" class="form-control selectpicker" multiple data-done-button="true" name="location[]" required="required">
                    <option>Asheville</option>
                    <option>Boone</option>
                    <option>Chapel Hill</option>
                    <option>Charlotte</option>
                    <option>Greensboro</option>
                    <option>Greenville</option>
                    <option>Raleigh</option>
                    <option>Wilmington</option>
                    <option>Durham</option>
                    <option>Virtual</option>
                    </select>
                  <button class="sve-btn" type="button" id="save_new">Save</button>
                  <button class="sve-btn save_new1" type="button"  id="changetabbutton">Continue</button>
                   </div>
                </form>
                 </div>
              </div>
            </div>
         
          </div>
          <div class="tab-pane fade" id="tab2primary">
            <div class="travel-bonus catg-pro">
              <div class="row">
                <div class="col-md-12">
                  <h4>What kind of jobs do you want to work?</h4>
                  <p>Select all the categories you may want to work and include a brief summary of your experience. You will receive notifications based on these preferences and can edit them at any point. Jobs are awarded based on these responses and they are visible on your public profile, so be concise, candid, and compelling.</p>

  @foreach ($categories as $category)
  
  <?php
    $user_id = Auth::user()->id;
    $cat_id  = $category->id;
    $workerexperience = DB::table('worker_categories_exprience')
                        ->where(['user_id' => $user_id,'cat_id' => $cat_id,'status' => '1','deleted'=>'0'])
                        ->first();
  ?>

  <?php if($workerexperience !=''){ ?>
Do you have any equipment?
  <div class="event-box">
    <h2 class="btnleft-head">{{$category->cat_name}}</h2>
      <div class="checkbox checkbox-toggle">
        <label>
          <input data-toggle="toggle" type="checkbox" class="chk_toogle_{{$category->id}}" onchange="valueChanged_{{$category->id}}()" checked="checked">
        </label>
      </div>
    <h6>{{$category->cat_rate}}</h6>
    <p>* {{$category->cat_description}}</p>
    <div id="event_form_{{$category->id}}">
    <form class="cat_form" action="#" method="" id="exp_form_{{$category->id}}">

          <input type="hidden" name="categoryid" value="{{$category->id}}" id="categoryid_{{$category->id}}">
          <input type="hidden" name="user_id" value="{{Auth::user()->id}}" id="user_id_{{$category->id}}" >

    <div class="row">
      <div class="form-group col-md-12">
        <label class="price-radio">What level of experience do you have?</label>
        <label class="radio-inline"><input type="radio" name="experience" class="experience" <?php if($workerexperience->experience =="none") {echo "checked";}?> value="none">None</label>
         <label class="radio-inline"><input type="radio" name="experience" class="experience" <?php if($workerexperience->experience =="some") {echo "checked";}?> value="some">Some</label>
         <label class="radio-inline"><input type="radio" name="experience" class="experience" <?php if($workerexperience->experience =="professional") {echo "checked";}?> value="professional">Professional</label>
      </div>
      <div class="form-group col-md-12">
        <label>Summarize your skills, experience, and interest in {{$category->cat_name}} jobs (10 - 110 characters).</label>
        <textarea class="form-control" name="additionalinfo" id="additionalinfo_{{$category->id}}" rows="5">{{$workerexperience->additional_info}}</textarea>

      </div>
      <div class="col-md-12">
       <button type="button" class="updt-btn" id="save_info_work_{{$category->id}}">Save Information</button>
      </div>
    </div>
    </form>
  </div>
  </div>

    <script type="text/javascript">
      function valueChanged_{{$category->id}}(){
        
        if($('.chk_toogle_{{$category->id}}').is(":unchecked")){   
            
          $("#event_form_{{$category->id}}").hide();
            var user_id            = jQuery('#user_id_{{$category->id}}').val();
            var catid              = jQuery('#categoryid_{{$category->id}}').val();
            var status             = '0';
            var deleted            = '1';          
            jQuery.ajax({
            method : 'GET',
            url  : "{{url('/updtworkexp')}}",
            data:{ 'user_id': user_id, 'catid': catid,'status': status ,'deleted': deleted },
            success :  function(resp) {
             /* alert(resp);*/
                          }
                    });
         }else{
            
            $("#event_form_{{$category->id}}").show();
            var user_id            = jQuery('#user_id_{{$category->id}}').val();
            var catid              = jQuery('#categoryid_{{$category->id}}').val();
            var status             = '1';
            var deleted            = '0';          
            jQuery.ajax({
            method : 'GET',
            url  : "{{url('/updtworkexp')}}",
            data:{ 'user_id': user_id, 'catid': catid,'status': status ,'deleted': deleted },
            success :  function(resp) {
              /*alert(resp);*/
                          }
                    });

         }
      }
    </script>

<?php }else{ ?>
    <div class="event-box">
    <h2 class="btnleft-head">{{$category->cat_name}}</h2>
      <div class="checkbox checkbox-toggle">
        <label>
          <input data-toggle="toggle" type="checkbox" class="chk_toogle_{{$category->id}}" onchange="valueChanged_{{$category->id}}()">
        </label>
      </div>
    <h6>{{$category->cat_rate}}</h6>
    <p>* {{$category->cat_description}}</p>
    <div id="event_form_{{$category->id}}" style="display: none;">
    <form class="cat_form" action="#" method="" id="exp_form_{{$category->id}}">

          <input type="hidden" name="categoryid" value="{{$category->id}}" id="categoryid_{{$category->id}}">
          <input type="hidden" name="user_id" value="{{Auth::user()->id}}" id="user_id_{{$category->id}}" >

    <div class="row">
      <div class="form-group col-md-12">
        <label class="price-radio">What level of experience do you have?</label>
        <label class="radio-inline"><input type="radio" name="experience" class="experience" value="none">None</label>
         <label class="radio-inline"><input type="radio" name="experience" class="experience" value="some">Some</label>
         <label class="radio-inline"><input type="radio" name="experience" class="experience" value="professional">Professional</label>

      </div>
      <div class="form-group col-md-12">
        <label>Summarize your skills, experience, and interest in {{$category->cat_name}} jobs (10 - 110 characters).</label>
        <textarea class="form-control" name="additionalinfo" id="additionalinfo_{{$category->id}}" rows="5"></textarea>

      </div>
      <div class="col-md-12">
       <button type="button" class="updt-btn" id="save_info_work_{{$category->id}}">Create Endoresement</button>
      </div>
    </div>
    </form>
  </div>
  </div>
    <script type="text/javascript">
      function valueChanged_{{$category->id}}(){
        if($('.chk_toogle_{{$category->id}}').is(":checked"))   
            $("#event_form_{{$category->id}}").show();
        else
            $("#event_form_{{$category->id}}").hide();
          }
    </script>

   <?php } ?>

    <script >
     jQuery(document).ready(function(){

       var exp_form = jQuery("#exp_form_{{$category->id}}").validate({
        rules: {
                experience: {
                    required: true,
                  },
                additionalinfo: {
                    required: true,
                }
               },
        messages: {
              experience: {
                  required: "Please select one."
                },
              additionalinfo: {
                 required: "Please enter about your experience."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });
      jQuery("#save_info_work_{{$category->id}}").click(function(){ 
         if (exp_form.form()) {

         var user_id            = jQuery('#user_id_{{$category->id}}').val();
         var catid              = jQuery('#categoryid_{{$category->id}}').val();
         var experience         = jQuery('.experience:checked').val();
         var additionalinfo     = jQuery('#additionalinfo_{{$category->id}}').val();
         jQuery.ajax({
          method : 'GET',
          url  : "{{url('/workerexperience')}}",
          data:{ 'user_id': user_id, 'catid': catid,'experience': experience ,'additionalinfo': additionalinfo },
          success :  function(resp) {
                        }
                    });
                  }
              });
    });
    </script>

    @endforeach
         <div class="cont-btn"><button class="sve-btn" id="changetabbutton_sec">Continue</button></div>
       </div>
    </div>

  </div>
</div>
          <div class="tab-pane fade" id="tab3primary"> 
            <div class="travel-bonus equipment_category">
              <div class="row">
                <div class="col-md-12">
                  <h4>Equipment Bonuses</h4>
                  <p>Equipment bonuses are only paid when they are specifically requested on jobs and you are awarded the equipment.
                  When competing on a job with an equipment bonus you can indicate whether you can provide the equipment, and if you are willing to work without the bonus.</p>
                  <p>Competing for equipment does not guarantee you will be awarded it. You must be awarded the job and the equipment to receive the bonus. Equipment bonuses can add up, from $7-$40+ per item, and having equipment will make you more likely to be awarded jobs. Thus, some Sweepers invest in equipment just to receive the bonuses!</p>
                  <h3>Do you have any equipment?</h3>
                  <p>Equipment bonuses are only paid when they are specifically requested on jobs and you are awarded the equipment.
                    When competing on a job with an equipment bonus you can indicate whether you can provide the equipment, and if you are willing to work without the bonus.</p>
                    <a href="#"><h3 style="text-align: center;">Skip this step >></h3></a>
                    @foreach ($equipment as $equipments)
                        <?php
                          $user_id = Auth::user()->id;
                          $equ_id  = $equipments->id;
                          $workerequipments = DB::table('worker_equipment')
                                              ->where(['user_id' => $user_id,'equ_id' => $equ_id,'status' => '1','deleted'=>'0'])
                                              ->first();
                          if($workerequipments !=''){ ?>

                      <div class="event-box">
                          <h2 class="btnleft-head">{{$equipments->equ_name}}</h2>
                           <div class="checkbox checkbox-toggle">
                                <label>
                                  <input data-toggle="toggle" type="checkbox" class="chk_toogle_equ_{{$equipments->id}}" onchange="valueChanged_equ_{{$equipments->id}}()" checked="checked">
                                </label>
                              </div>
                          <h6>{{$equipments->equ_price}}</h6>
                          <p>{{$equipments->equ_description}}</p>
                      <div id="event_form_equ_{{$equipments->id}}">
                      <form class="equ_form" action="#" method="" id="equ_form_{{$equipments->id}}">
                         <input type="hidden" name="equ_id" value="{{$equipments->id}}" id="equ_id_{{$equipments->id}}">
                         <input type="hidden" name="user_id" value="{{Auth::user()->id}}" id="user_id_{{$equipments->id}}" >
                      <div class="row">
                        <div class="form-group col-md-12">
                          <label>Please describe your {{$equipments->equ_name}} (10 - 30 characters).</label>
                          <textarea class="form-control" name="equlinfo" id="equlinfo_{{$equipments->id}}" rows="5">{{$workerequipments->description}}</textarea>
                        </div>
                        <div class="col-md-12">
                         <button type="button" class="updt-btn" id="save_info_equ_{{$equipments->id}}">Create Equipment</button>
                        </div>
                      </div>
                      </form>
                    </div>
                       </div>

          <script type="text/javascript">
          function valueChanged_equ_{{$equipments->id}}(){
        
         if($('.chk_toogle_equ_{{$equipments->id}}').is(":unchecked")){   
            
          $("#event_form_equ_{{$equipments->id}}").hide();
            var user_id            = jQuery('#user_id_{{$equipments->id}}').val();
            var equ_id              = jQuery('#equ_id_{{$equipments->id}}').val();
            var status             = '0';
            var deleted            = '1';          
            jQuery.ajax({
            method : 'GET',
            url  : "{{url('/updtworkequ')}}",
            data:{ 'user_id': user_id, 'equ_id': equ_id,'status': status ,'deleted': deleted },
            success :  function(resp){
             /*alert(resp);*/
                          }
                    });
             }else{
            $("#event_form_equ_{{$equipments->id}}").show();
            var user_id            = jQuery('#user_id_{{$equipments->id}}').val();
            var equ_id              = jQuery('#equ_id_{{$equipments->id}}').val();
            var status             = '1';
            var deleted            = '0';          
            jQuery.ajax({
            method : 'GET',
             url  : "{{url('/updtworkequ')}}",
             data:{ 'user_id': user_id, 'equ_id': equ_id,'status': status ,'deleted': deleted },
             success :  function(resp) {
              /*alert(resp);*/
                          }
                    });

                   }
                }
              </script>
            <?php } else { ?>
             <div class="event-box">
                     <h2 class="btnleft-head">{{$equipments->equ_name}}</h2>
                     <div class="checkbox checkbox-toggle">
                          <label>
                            <input data-toggle="toggle" type="checkbox" class="chk_toogle_equ_{{$equipments->id}}" onchange="valueChanged_equ_{{$equipments->id}}()">
                          </label>
                        </div>
                    <h6>{{$equipments->equ_price}}</h6>
                    <p>{{$equipments->equ_description}}</p>
                      <div id="event_form_equ_{{$equipments->id}}" style="display: none;">
                      <form class="equ_form" action="#" method="" id="equ_form_{{$equipments->id}}">

                        <input type="hidden" name="equ_id" value="{{$equipments->id}}" id="equ_id_{{$equipments->id}}">
                         <input type="hidden" name="user_id" value="{{Auth::user()->id}}" id="user_id_{{$equipments->id}}" >
                      <div class="row">
                        <div class="form-group col-md-12">
                          <label>Please describe your {{$equipments->equ_name}} (10 - 30 characters).</label>
                          <textarea class="form-control" name="equlinfo" id="equlinfo_{{$equipments->id}}" rows="5"></textarea>
                        </div>
                        <div class="col-md-12">
                         <button type="button" class="updt-btn" id="save_info_equ_{{$equipments->id}}">Create Equipment</button>
                        </div>
                      </div>
                      </form>
                    </div>
                       </div>
                     <script type="text/javascript">
                      function valueChanged_equ_{{$equipments->id}}(){
                        if($('.chk_toogle_equ_{{$equipments->id}}').is(":checked"))   
                            $("#event_form_equ_{{$equipments->id}}").show();
                        else
                            $("#event_form_equ_{{$equipments->id}}").hide();
                          }
                    </script>

                   <?php } ?>
     <script>
      jQuery(document).ready(function(){
       var equ_form = jQuery("#equ_form_{{$equipments->id}}").validate({
        rules: {
                equlinfo: {
                    required: true
                }
               },
        messages: {
                equlinfo: {
                 required: "Please describe."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });
      jQuery("#save_info_equ_{{$equipments->id}}").click(function(){ 
        if (equ_form.form()) {
         var user_id            = jQuery('#user_id_{{$equipments->id}}').val();
         var equ_id             = jQuery('#equ_id_{{$equipments->id}}').val();
         var equlinfo           = jQuery('#equlinfo_{{$equipments->id}}').val();

         jQuery.ajax({
          method : 'GET',
          url  : "{{url('/workerequipment')}}",
          data:{ 'user_id': user_id, 'equ_id': equ_id,'equlinfo': equlinfo },
          success :  function(resp) {
             
                        }
                    });
                  }
              });
    });
    </script>
                  @endforeach

                 <div class="cont-btn"><button class="sve-btn" id="changetabbutton_new">Continue</button></div>
                </div>
              </div>

            </div>
          </div>
          <div class="tab-pane fade" id="tab4primary">
            <div class="travel-bonus equipment_category">
              <div class="row">
                <div class="col-md-12">
                  <h4>Become a Sweeper</h4>
                  <p>Complete your Sweeps Profile to become an active Sweeper and work the jobs you want.</p>
                    <h3>Manage your Messages</h3>
                  <p>Tune Sweeps' notifications to work for you. Whether you want text alerts for each job or to mute messages while on vacation, you can manage that here.</p>
                   

                  <div class="event-box">
                    <h2 class="btnleft-head">Important Notifications</h2>
                     
                    <p style="margin-top: 15px;">Job reminders and notifications cannot be turned off, and we may message you with questions, updates, and about potential jobs.</p>
                    <h2 class="btnleft-head">New Job Alert</h2>
                     
                    <p style="margin-top: 15px;"><strong>Recommended :</strong> Get notified immediately when jobs are posted that match your campus and category preferences.</p>
                    <div class="row">
                    <div class="col-md-12">
                    <div class="btn-group fl-width" id="status" data-toggle="buttons">
                      <label class="btn btn-default btn-on btn-lg active">
                      <input type="radio" value="1" name="multifeatured_module[module_id][status]" checked="checked">Yes</label>
                      <label class="btn btn-default btn-off btn-lg ">
                      <input type="radio" value="0" name="multifeatured_module[module_id][status]">No</label>
                    </div>
                    <span class="tgl-btn">text</span>
                  </div>
                  
                  <div class="col-md-12">
                    <div class="btn-group fl-width" id="status" data-toggle="buttons">
                      <label class="btn btn-default btn-on btn-lg active">
                      <input type="radio" value="1" name="multifeatured_module[module_id][status]" checked="checked">Yes</label>
                      <label class="btn btn-default btn-off btn-lg ">
                      <input type="radio" value="0" name="multifeatured_module[module_id][status]">No</label>
                    </div>
                     <span class="tgl-btn">Email</span>
                   </div>
                 </div>
                     <h2 class="btnleft-head">Daily Digests</h2>
                     <p>Receive a (nearly) daily email summary of open jobs and updates.</p>
                     <div class="row">
                      <div class="col-md-12">
                     <div class="btn-group fl-width" id="status" data-toggle="buttons">
                      <label class="btn btn-default btn-on btn-lg active">
                      <input type="radio" value="1" name="multifeatured_module[module_id][status]" checked="checked">Yes</label>
                      <label class="btn btn-default btn-off btn-lg ">
                      <input type="radio" value="0" name="multifeatured_module[module_id][status]">No</label>
                     </div>
                     <span class="tgl-btn">Email</span>
                   </div>
                    </div>
                    </div>
                  <div class="cont-btn"><a href="#"><button>Continue</button></a></div>
              </div>
              </div>

            </div>
          </div>
      </div>
  </div>
</div>
</div>
                  <div class="tab-pane fade" id="tab3default"></div>
              </div>
          </div>
      </div>
        </div>
      </div>
    </div>
  </div>
    
   
  </div>
  <div class="tab-pane text-style" id="tab3">
  
  </div>

  <div class="tab-pane text-style" id="tab4">
    <form>
     <div class="bank_account">
      <div class="up-bnk-account">
        <div class="row pro-heading">
        <div class="col-sm-12">
          <div class="pro-head">
          <h3>Add a new bank account</h3>
        </div>
        </div>
      </div>
   
        <div class="row">
          <div class="col-md-9">
          <div class="add-acct">
          <div class="form-group col-md-6">
            <label>Bank Routing Number</label>
            <input type="text" name="" placeholder=""  class="form-control">
          </div>
          <div class="form-group col-md-6">
            <p>*The routing number of your bank, you can often find this by googling for "
Bank Name Routing Number" or from the bottom of your check.</p>
          </div>
          <div class="form-group col-md-6">
            <label>Bank Account Number</label>
            <input type="text" name="" placeholder="" class="form-control">
          </div>
          <div class="form-group col-md-6">
            <p>*Your bank account number. This is the middle number on the bottom of your 
bank checks.</p>
          </div>
          <div class="processing-txt col-md-12">
            <p><input type="checkbox" name="" style="margin-right: 10px;">I agree to the payment processing <a hrf="#">Terms of Service.</a></p> 

          </div>
          <div class="account-btn  col-md-12">
            <button class="bnk-btn">Add Bank Account</button><button class="cancel-btn">Cancel</button>
          </div>
        </div>
        </div>
        <div class="col-md-3">
          <div class="become-cont">
            <h4>Become a CollegeWRK</h4>
            <p>Complete your Sweeps Profile to become an active Sweeper and work the jobs you want.</p>
          </div>
        </div>
      </div>
      
    </div>


     <div class="lower-bnk-account">
        <div class="row pro-heading">
        <div class="col-sm-12">
          <div class="pro-head">
          <h3>CollegeWRK Pay</h3>
        </div>
        </div>
      </div>
   
        <div class="row">
          <div class="col-md-9">
          <div class="bank-detail">
            <div class="row">
              <div class="col-md-12">
                <h3>Bank Account</h3>
                <p>When a job is completed and paid, your payment will be automatically transfered to this bank account. It typically takes 2 business days before the money arrives to your account. Please add one here.</p>
              </div>
              <div class="account-btn  col-md-12">
            <button class="bnk-btn" style="margin-left: 15px;margin-top:30px;">Add Bank Account</button>
          </div>
            </div>
        </div>

        <div class="bank-detail">
            <div class="row">
              <div class="col-md-12">
                <h3>Transaction</h3>
                <p>No transaction history. Try working some jobs to get paid!</p>
              </div>
            </div>
        </div>

        </div>
        <div class="col-md-3">
          <div class="become-cont">
            <h4>Become a CollegeWRK</h4>
            <p>Complete your Sweeps Profile to become an active Sweeper and work the jobs you want.</p>
          </div>
        </div>
      </div>
      
    </div>
  </div>
     </form>
  </div>

  <div class="tab-pane text-style" id="tab5">
     <h2>lkjfhji</h2>
     <p>Complete your Sweeps Profile to become an active Sweeper and work the jobs you want.</p>
  </div>

</div>
</div>
</div>
</div>
</section>
<script>
  $(document).ready(function () {

    var mySelect = $('#first-disabled2');

    $('#special').on('click', function () {
      mySelect.find('option:selected').prop('disabled', true);
      mySelect.selectpicker('refresh');
    });

    $('#special2').on('click', function () {
      mySelect.find('option:disabled').prop('disabled', false);
      mySelect.selectpicker('refresh');
    });

    $('#basic2').selectpicker({
      liveSearch: true,
      maxOptions: 1
    });
  });
</script>

<script>
 $(document).ready( function() {
    $("#profileFrm").validate({
        rules: {
                worker_slogan: {
                    required: true,
                    minlength: 2,
                    maxlength: 100
                   },
                worker_about: {
                    required: true,
                    minlength: 2,
                    maxlength: 500
                   },
                 worker_attend: {
                    required: true
                   },
                 worker_school: {
                    required: true,
                    email: true
                   },
                  worker_qualification: {
                    required: true
                   },
                   phone: {
                    required: true,
                    number: true,
                    minlength: 10,
                    maxlength: 12
                   },
                   gender: {
                    required: true
                   },
                   address: {
                    required: true
                   },
                   city: {
                    required: true
                   },
                   state : {
                    required: true
                   },
                   zip_code : {
                    required: true
                   }
                },
        messages: {
                worker_slogan: {
                  required: "This is required field.", 
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 100 characters allowed."
                   },
                  worker_about: {
                  required: "This is required field.", 
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 500 characters allowed."
                   },
                  worker_attend: {
                  required: "This is required field."
                   },
                  worker_school: { 
                  required: "This field is required.",
                  email: "Enter valid edu email."
                  },
                  worker_qualification: {
                  required: "This is required field."
                   },
                  phone: {
                  required: "This field is required.",
                  number: "Enter only number",
                  maxlength: "Maximum 14 numbers are allowed."
                   },
                  gender: {
                  required: "This is required field."
                   },
                  address: {
                  required: "This is required field."
                   },
                  city: {
                  required: "This is required field."
                   },
                  state: {
                  required: "This is required field."
                   },
                   zip_code: {
                  required: "This is required field."
                   }
                },
        submitHandler: function(form) {
            form.submit();
          }
        });

     // $("#jobpre").validate({
     //    rules: {
     //            location: {
     //                required: true
     //               }
     //            },
     //    messages: {
     //            location: {
     //              required: "Please let us know where you would like to work.."
     //               }
     //            },
     //       submitHandler: function(form) {
     //        form.submit();
     //      }
     //    });

  $('.save_new1').hide();
  $('#save_new').click(function(){
    
    var location       = jQuery('#location').val();
     $.ajax({
              url: "{{url('/jobpreference')}}",
              type: "get",
              data: {'location': location},
               success: function(response){
               alert(response); 
               $('#save_new').hide();
               $('.save_new1').show();
               
        }
    });
  });
 });
</script>
<script>
$(document).ready(function() {
  $('.lower-profile').hide();
  $('.updt-btn').click(function(){
    $('.lower-profile').show();
  });
    $('#datePicker')
        .datepicker({
            format: 'yyyy-mm-dd'
        })
        .on('changeDate', function(e) {
            // Revalidate the date field
            $('#eventForm').formValidation('revalidateField', 'date');
        });

    $('#eventForm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'The name is required'
                    }
                }
            },
            date: {
                validators: {
                    notEmpty: {
                        message: 'The date is required'
                    },
                    date: {
                        format: 'MM/DD/YYYY',
                        message: 'The date is not a valid'
                    }
                }
            }
        }
    });
});
</script>
<script>
    $(document).ready(function() {

     $('#browsebtn').on('click', function() {
             $('#image').click();
       });

    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img style="margin:0 10px;width:150px;height:150px;">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };
    $('#image').on('change', function() {
         jQuery('#nofile').empty(); 
          jQuery('#browse_img').empty(); 
        imagesPreview(this, '#browse_img');
      });
  
  });
</script>
<script type="text/javascript">
  $('#changetabbutton').click(function(e) {
  e.preventDefault();
  var link = $('#mytabs .active').next().children('a').attr('href');

  $('#mytabs a[href="' + link + '"]').tab('show');
});
$('#changetabbutton_sec').click(function(e) {
  e.preventDefault();
  var link = $('#mytabs .active').next().children('a').attr('href');

  $('#mytabs a[href="' + link + '"]').tab('show');
});
$('#changetabbutton_new').click(function(e) {
  e.preventDefault();
  var link = $('#mytabs .active').next().children('a').attr('href');

  $('#mytabs a[href="' + link + '"]').tab('show');
});
</script>
@stop