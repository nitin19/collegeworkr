@extends('layouts.workerdefault')

@section('title', 'Worker Share')

@section('content')

           <div class="" id="tab3">
          <div class="share_workers ">
<!--            <div class="row pro-heading">
              <div class="col-sm-12">
                <div class="pro-head">
                <h3>Share Worker</h3>
              </div>
              </div>
            </div> -->

            <div class="row">
              <div class="col-md-12">
                <div class="invite-box">
                <div class="row">
                    <div class="col-md-12 paid-text">
                                       
                                        <h6>Your Referral Code</h6>
                                    
                                          <div class="form-group">
                                            <input type="text" name="" id="invite_code" placeholder="" value="{{$usercustomer->invitecode}}" readonly="readonly">
                                            <button onclick="myFunction()">Copy</button>
                                          </div>
                                          <div id="myTooltip"></div>
                                          
                    </div>
                </div>
                  <div class="customer-price">
                        <div class="panel with-nav-tabs panel-default">
                            <div class="panel-heading">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#Invite1" data-toggle="tab">Recruit College Students</a></li>
                                        <li><a href="#Invite2" data-toggle="tab">Get New Customers</a></li>
                                    </ul>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="Invite1">
                                      <div class="row">
                                        <div class="col-md-12">
                                        <h4>Give $10 and Get $10</h4>
                                        <p class="wrk-p">For each new WRKer you refer that completes a job</p>
                                      </div>
                                      </div>

                                      <div class="row">
                                        <div class="col-md-12 share-online">
                                          <h2>Share Online</h2>
                                              <form name="sendmail_form" id="sendmail_form" action="" method="GET">
                                               {{ csrf_field() }} 
                                            <div class="form-group">
                                              <input type="email" name="recipient_email[]" id="recipient_email" placeholder="Enter Your Email id" class="form-control" value="" multiple="multiple">
                                              <input type="hidden" name="refferal_code" id="refferal_code" class="form-control" value="{{$usercustomer->invitecode}}">
                                              <input type="hidden" name="worker_name" id="worker_name" class="form-control" value="{{$usercustomer->name}}">
                                               <input type="hidden" name="worker_id" id="worker_id" class="form-control" value="{{$usercustomer->id}}">
                                              <button type="button" class="send-btn" id="send-btn">SEND</button>
                                            </div>
                                          </form>
                                          <p>Separate email addresses with commas.</p>
                                          <!-- <h6>Preview Email</h6> -->
                                          <ul class="list-inline soc-links">
                                            <li><a href="https://www.facebook.com/CollegeWRK/?modal=admin_todo_tour" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="https://twitter.com/CollegeWrk_/" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="https://www.linkedin.com/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                          </ul>
                                        </div>
   <!--                                      <div class="col-md-6 print-file">
                                           <h2>Share in Person</h2>
                                           <button class="card_btn">Get Free Referal Card</button>
                                         
                                          <p>Preview Card</p>
                                          <button class="print-btn">Download Files to Print</button>
                                        </div>
 -->
                                      </div>
                                     
                                    </div>
                                    <div class="tab-pane fade" id="Invite2">
                                     <div class="row">
                                        <div class="col-md-12">
                                        <h4>Give $20 Credit and Get $15 Cash</h4>
                                        <p class="wrk-p">For every new paying customer you refer</p>
                                      </div>
                                      </div>

                                      <div class="row">
                                        <div class="col-md-12 share-online">
                                          <h2>Share Online</h2>
                                             <form name="sendmail_form" action="" id="sendmail_form_new" method="POST">
                                               <!-- {{ csrf_token() }} --> 
                                               {{ csrf_field() }} 
                                            <div class="form-group">
                                              <input type="email" name="recipient_email_new[]" id="recipient_email_new" class="form-control" value="" placeholder="Enter Your Email id">
                                              <input type="hidden" name="refferal_code_new" id="refferal_code_new" class="form-control" value="">
                                              <button type="button" class="send-btn_new" id="send-btn_new">SEND</button>
                                            </div>
                                          </form>
                                          <p>Separate email addresses with commas.</p>
                                          <h6>Preview Email</h6>
                                          <ul class="list-inline soc-links">
                                            <li><a href="https://www.facebook.com/CollegeWRK/?modal=admin_todo_tour" target="_blank"><i class="fa fa-facebook" aria-hidden="true" ></i></a></li>
                                            <li><a href="https://twitter.com/CollegeWrk_/" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="https://www.linkedin.com/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                          </ul>
                                        </div>
                                       <!--  <div class="col-md-6 print-file">
                                           <h2>Share in Person</h2>
                                           <button class="card_btn">Get Free Referal Card</button>
                                         
                                          <p>Preview Card</p>
                                          <button class="print-btn">Download Files to Print</button>
                                        </div> -->

                                      </div>
                                      <!-- <div class="row referral-program">
                          <div class="col-md-4 col-sm-4">
                            <div class="inner-prog1">
                              <h5>0<br>
                                Total Referrals
                              </h5>
                            </div>
                          </div>
                         
                          <div class="col-md-4 col-sm-4">
                            <div class="inner-prog2">
                              <h5>$0<br>
                                Potential Reward
                              </h5>
                            </div>
                          </div>
                           <div class="col-md-4 col-sm-4">
                            <div class="inner-prog3">
                              <h5>0<br>
                                Paid To You
                              </h5>
                            </div>
                          </div>
                          <div class="col-md-12">
                          <h4>Referral Program Terms of Use</h4>
                        </div>
                        </div> -->
                        </div>
                        </div>
                        </div>
                        </div>
                        </div>
                  
                </div>
                
              </div>
              
            </div>


        </div>
      </div>
<script>
function myFunction() {
  var copyText = document.getElementById("invite_code");
  copyText.select();
  document.execCommand("copy");
  
  var tooltip = document.getElementById("myTooltip");
  tooltip.innerHTML = "Copied ";
  setTimeout(function() {
    $('#myTooltip').fadeOut('fast');
}, 2000);
}
</script>
<script>
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
</script>

<script type="text/javascript">
jQuery(document).ready(function() {
  $("#send-btn").click(function(){
    var recipient_email = jQuery('#recipient_email').val();
    var refferal_code = jQuery('#refferal_code').val();
    var worker_name = jQuery('#worker_name').val();
    var worker_id = jQuery('#worker_id').val();
    var checkEmail = isEmail(recipient_email);
    if( checkEmail == true ){
         jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/workersendinvitation') }}",
            data:{'recipient_email':recipient_email,'refferal_code':refferal_code,'worker_name':worker_name,'worker_id':worker_id},
            success :  function(resp) {
                //alert(resp);
             if(resp == 'true' )
             {
              alert('Refferal code is successfully send');
             } 
             else
              {
              alert('Refferal code is not successfully send');
             }
             }
       });
    }else{
      alert('Please provide a valid email');
    }
   
 
  });
});
</script>
<style type="text/css">
  .invite-box {
    margin-top: 0px;
  }
</style>
<!-- <script>
$(document).ready(function() {

jQuery("#sendmail_form").validate({
        rules: {
            'recipient_email[]': {
                    required: true,
                    email:true 
                }
            },
        messages: {
            'recipient_email[]': {
                  required : "Atleast one email address is required.",
                  email : "Please enter valid email address."
                }
              },
        submitHandler: function(form) {
            form.submit();
          }
        });
});

</script> -->
@stop
