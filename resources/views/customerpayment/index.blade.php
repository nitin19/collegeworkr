@extends('layouts.clientdefault')

@section('title', 'Customer Payments')

@section('content')
      <div class="col-md-9 dashboard-right">
        <div class="tab-pane active text-style" id="tab1">
        <div class="row">
       <div class="col-md-12 rightcontent">
       <!--  <p>You will be able to compete for and work jobs once you finish your application</p> -->

        <div class="tab-section">
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active custom-width"><a href="#upcoming" aria-controls="upcoming" role="tab" data-toggle="tab">Payment Due<span></span></a></li>
    <li role="presentation" class="custom-width"><a href="#receivedpayment" aria-controls="received_payment" role="tab" data-toggle="tab">Completed Payment<span></span></a></li>
    <!-- <li role="presentation" class="custom-width"><a href="#rejectpayment" aria-controls="reject_payment" role="tab" data-toggle="tab">Reject Payment<span></span></a></li> -->
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="upcoming">
      <div class="tab_contentarea">
        <h1>Payment Due</h1>
        <div class="tablewrapper tablediv">
          <table class="table-striped">
        <thead>
          <tr class="headings">
            <th class="column1 col7">Contract Name</th>
            <th class="column2 col7">Worker Name</th>
            <th class="column3 col6">Amount</th>
            <!-- <th class="column4">Detail</th> -->
          </tr>
        </thead>
        <tbody>
          @if(count($customer_upcoming_payment) > 0)
          @foreach($customer_upcoming_payment as $customer_upcoming_payments)
          <?php //$job_id = $customer_upcoming_payments->client_jobid;
                //$job_data = DB::table('clientjobpost')->where('id',$job_id)->first();
                //$cat_id = $job_data->job_heading;
                //$cat_data = DB::table('categories')->where('id',$cat_id)->first();
                //$cat_name = $cat_data->cat_name;
                //$worker_id = $customer_upcoming_payments->worker_userid;
                //$worker_data = DB::table('users')->where('id',$worker_id)->first();
                //$worker_first_name = $worker_data->firstname;
                //$worker_last_name = $worker_data->lastname;
                    $hours_to_complete = $customer_upcoming_payments->hours_to_complete;
                    $mints_to_complete = $customer_upcoming_payments->mints_to_complete;
                    $updated_hours = $customer_upcoming_payments->updated_hours;
                    $updated_mints = $customer_upcoming_payments->updated_mints;
                    $price_per_hour = 35;
                    $price_per_mint = 35 / 60;
                    if($updated_hours !='' && $updated_mints !=''){
                        $total_time_mints = intval($updated_hours) * 60 + intval($updated_mints);
                        $approved_time_formated = $updated_hours .' : '. $updated_mints .'hr';
                    }else{
                        $total_time_mints = intval($hours_to_complete) * 60 + intval($mints_to_complete); 
                        $approved_time_formated = $hours_to_complete .' : '. $mints_to_complete .'hr';
                    }


          ?>
          <tr class="familydata custom-reports">
            <td class="column1 col7" data-label="Contract Name"><span>{{$customer_upcoming_payments->cat_name}}</span></td>
            <td class="column2 col7" data-label="Worker Name">{{$customer_upcoming_payments->firstname}} {{$customer_upcoming_payments->lastname}}</td>
            <td class="column3 col6" data-label="Amount"><?php $total_amount = $total_time_mints *  $price_per_mint;  
                                //setlocale(LC_MONETARY,"en_US");
                                echo $amountToPay = money_format("$%i", round($total_amount));  
                                ?></td>
            <!-- <td class="column4" data-label="Detail"><a href="{{url('/workerjobs')}}" class="view-table">View Detail</a></td> -->
          </tr>
         @endforeach
         @else
      <tr>
         <td>
           No Data found.
         </td>
     </tr>
     @endif
        </tbody>
      </table>
        </div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="receivedpayment">
      <div class="tab_contentarea">
        <h1>Completed Payment</h1>
        <div class="tablewrapper tablediv">
          <table class="table-striped">
        <thead>
          <tr class="headings">
            <th class="column1 col7">Contract Name</th>
            <th class="column2 col7">Worker Name</th>
            <th class="column3 col6">Amount</th>
            <!-- <th class="column4">Detail</th> -->
          </tr>
        </thead>
        <tbody>
          @if(count($customer_received_payment) > 0)
          @foreach($customer_received_payment as $customer_received_payments)
          <?php 
                //$job_id = $customer_received_payments->client_jobid;
                //$job_data = DB::table('clientjobpost')->where('id',$job_id)->first();
                //$cat_id = $job_data->job_heading;
                //$cat_data = DB::table('categories')->where('id',$cat_id)->first();
                //$cat_name = $cat_data->cat_name;
                //$worker_id = $customer_received_payments->worker_userid;
                //$worker_data = DB::table('users')->where('id',$worker_id)->first();
                //$worker_first_name = $worker_data->firstname;
                //$worker_last_name = $worker_data->lastname;
          ?>
        <tr class="familydata custom-reports">
            <td class="column1 col7" data-label="Contract Name"><span>{{$customer_received_payments->cat_name}}</span></td>
            <td class="column2 col7" data-label="Worker Name">{{$customer_received_payments->firstname}} {{$customer_received_payments->lastname}}</td>
            <td class="column3 col6" data-label="Amount">
              <?php echo $amountToPay = money_format("$%i", $customer_received_payments->amount_paid ); ?>
            </td>
            <!-- <td class="column4" data-label="Detail"><a href="#" class="view-table">Applied Job</a></td> -->
          </tr>
        @endforeach
         @else
      <tr>
         <td>
           No date found.
         </td>
     </tr>
     @endif
        </tbody>
      </table>
        </div>
      </div>
    </div>
<!--     <div role="tabpanel" class="tab-pane" id="rejectpayment">
      <div class="tab_contentarea">
        <h1>Reject Payment</h1>
        <div class="tablewrapper tablediv">
          <table class="table-striped">
        <thead>
          <tr class="headings">
            <th class="column1">Contract Name</th>
            <th class="column2">Worker Name</th>
            <th class="column3">Amount</th> -->
            <!-- <th class="column4">Detail</th> -->
          <!-- </tr>
        </thead>
        <tbody>
          @if(count($customer_reject_payment) > 0)
          @foreach($customer_reject_payment as $customer_reject_payments)
          <?php //$job_id = $customer_reject_payments->client_jobid;
                //$job_data = DB::table('clientjobpost')->where('id',$job_id)->first();
                //$cat_id = $job_data->job_heading;
                //$cat_data = DB::table('categories')->where('id',$cat_id)->first();
                //$cat_name = $cat_data->cat_name;
                //$worker_id = $customer_reject_payments->worker_userid;
                //$worker_data = DB::table('users')->where('id',$worker_id)->first();
                //$worker_first_name = $worker_data->firstname;
                //$worker_last_name = $worker_data->lastname;
          ?>
        <tr class="familydata custom-reports">
            <td class="column1" data-label="Contract Name"><span>{{$cat_name}}</span></td>
            <td class="column2" data-label="Worker Name">{{$worker_first_name}}{{$worker_last_name}}</td>
            <td class="column3" data-label="Amount">{{$customer_reject_payments->paid_amount}}</td> -->
            <!-- <td class="column4" data-label="Detail"><a href="#" class="view-table">Applied Job</a></td> -->
          <!-- </tr>
        @endforeach
         @else
      <tr>
         <td>
           No job posted yet.
         </td>
     </tr>
     @endif
        </tbody>
      </table>
        </div>
      </div>
    </div>
  </div>
</div> -->

<div class="row pagination-list">
  <div class="col-md-6 col-sm-6">
      <nav aria-label="Page navigation example">
       <div class="pagination_section">
   
      </div>
  </nav>
  </div>
  <div class="col-md-6 col-sm-6">
    <ul class="list-inline select-pages">
      
      <!-- <li><h4> Worker Total</h4></li> -->
      <li>
          <form name="searchForm" id="searchForm" method="GET" action="" role="form" class="srchfrm">
                   <span>Showing:</span>
                        <select name="show" id="show">
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        </select>
                  </form>
      </li>
    </ul>
  </div>
</div> 
</div>
<!-- <div class="col-md-3">
                <div class="become-cont">
                  <h4>Become a CollegeWRK</h4>
                  <p>Complete your Sweeps Profile to become an active Sweeper and work the jobs you want.</p>
                </div>
              </div> -->
</div>
</div>
</div>
</div>
</div>
@stop