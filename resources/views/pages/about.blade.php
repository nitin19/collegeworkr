@extends('layouts.default')

@section('title', 'About Us')

@section('content')

<div class="Travel_exp_head">
  <div class="header_overlay">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="header_text">
           <h1>About us</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="about_box">
    <div class="jumbotron about-color">
        <div class="container">
          <h2>Who We are</h2>
          <p>CollegeWRK was started by two college students who wanted a way to foster their own financial independence while being active in the community. What started as simple chores for family and friends soon blossomed into an opportunity for growth when more and more students and community members began asking how to get involved. After months of planning, outreach, and strategizing, CollegeWRK launched in early 2019 to move students from the classroom to the community. Our WRKers are more than just their profile pictures, they care about the work they are doing and the people they are helping. Because of this, each WRKer becomes a familiar face in the community bringing people together to make life a little easier for everyone. </p>
  
</div>
</div>
</div>
<div class="container">
  <div class="row about-cta about-chairman">
    <div class="col-sm-3 thumbnail">
          <img class="aboutimg" src="{{url('/public')}}/images/kyle.jpg" height="auto"; width="100%">
    </div>
    <div class="col-sm-1">
    </div>
    <div class="col-sm-8 message-clr">
        <h2>Kyle Courtemanche, Co-Founder of CollegeWRK.</h2>
        <p>“Working with people I meet in the community showed me that they understood the pressure that we as students are under to pay back student debt.The relationships that WRKers develop within the community are part of the reason why our system works so well. We want to help, we put effort into our jobs, and we do it all with a smile.”</p>
        <!-- <h6 class="about-sign"> -Grant T. Hoke, CEO and Founder of CollegeWRK.</h6> -->
    </div>
  </div>
<div class="row about-cta about-chairman">
    <div class="col-sm-3 thumbnail">
          <img class="aboutimg grant_img" src="{{url('/public')}}/images/Grant.jpg" height="auto" width="100%">
        	<!-- <img class="aboutimg" src="{{url('/public')}}/images/agent.jpg" height="auto"; width="100%"> -->
    </div>
    <div class="col-sm-1">
    </div>
    <div class="col-sm-8 message-clr">
        <h2>Grant Hoke, Co-Founder Of CollegeWRK.</h2>
        <p>“When we started this company, we were the only two WRKers. But after seeing the types of jobs we were doing on a flexible schedule, other students expressed a lot of interest in working with us. But it wasn’t just the students who were interested--members of the community saw that we were working hard and wanted to find a way to help support us. They understood the pressure we have to pay back student debt and other financial obligations. This made us that much more committed to providing great service to our customers.”</p>
        <!-- <h6 class="about-sign"> -Grant T. Hoke, CEO and Founder of CollegeWRK.</h6> -->
    </div>
  </div>
</div>
<div class="mission">
    <div class="container">
        <div class="row">
            
            <div class="col-sm-6">
            	<h3>Our vision</h3>
                 <div class="row about-cta">
            		<div class="col-sm-4">
                    	<a class="thumbnail pull-left" href="#">
                    		<img class="media-object" src="{{url('/public')}}/images/Zurich.jpg"height="150">
                    	</a>
                    </div>	
                    <div class="col-sm-8">
                       <p>Our vision at CollegeWRK is to have a strong network of students committed to doing what they can for members of the community requiring help with simple tasks. What began as a way for students to start combating student loan debt with flexible scheduling soon resulted in even greater benefits to the community at large. Allowing students the opportunity to integrate into their communities while also allowing them to earn fair wages for their hard work is what entices our WRKers to give quality work. And because of that, the impact is beyond transactional. We help create mutually beneficial relationships between students and the members of their local communities who need help with day to day activities.</p> 
                    </div>
                </div>
            </div>
          
            <div class="col-sm-6 about-cta-mar">
            	<h3>Our mission</h3>
                <div class="row about-cta">
                	<div class="col-sm-4">
                    	<a class="thumbnail pull-left" href="#">
                    		<img class="media-object" src="{{url('/public')}}/images/Zurich.jpg"height="150">
                    	</a>
                    </div>	
                    <div class="col-sm-8">
                       <p>From Classroom, to the Community . . . 
From Classroom to Community means that when a WRKer is doing work, they are providing a service to the people that make up their home. Even if it is a temporary home, as college campuses often are for many students, doing work for CollegeWRK can help students get acquainted with their local area, meet people in the community, and take control of their economic independence.</p> 
                    </div>
            	</div>
            </div>
        </div>
    </div>
 </div>  
<style type="text/css">
    .grant_img {
        height: 292px !important;
    }
</style>
@stop