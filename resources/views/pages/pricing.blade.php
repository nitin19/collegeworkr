@extends('layouts.default')

@section('title', 'Pricing')

@section('content')
<link href="http://www.iebasketball.com/collegeworker/public/css/animate.css" rel="stylesheet">
 <script type="text/javascript">
  document.addEventListener('DOMContentLoaded',function(event){
  // array with texts to type in typewriter
  var dataText = [ "From the Classroom, to the Community"];
  
  // type one text in the typwriter
  // keeps calling itself until the text is finished
  function typeWriter(text, i, fnCallback) {
    // chekc if text isn't finished yet
    if (i < (text.length)) {
      // add next character to h1
     document.querySelector(".header-title").innerHTML = text.substring(0, i+1) +'<two aria-hidden="true"></two>';

      // wait for a while and call this function again for next character
      setTimeout(function() {
        typeWriter(text, i + 1, fnCallback)
      }, 100);
    }
    // text finished, call callback if there is a callback function
    else if (typeof fnCallback == 'function') {
      // call callback after timeout
      setTimeout(fnCallback, 700);
    }
  }
  // start a typewriter animation for a text in the dataText array
   function StartTextAnimation(i) {
     if (typeof dataText[i] == 'undefined'){
        setTimeout(function() {
          StartTextAnimation(0);
        }, 20000);
     }
     // check if dataText[i] exists
    if (i < dataText[i].length) {
      // text exists! start typewriter animation
     typeWriter(dataText[i], 0, function(){
       // after callback (and whole text has been animated), start next text
       StartTextAnimation(i + 1);
     });
    }
  }
  // start the text animation
  StartTextAnimation(0);
});
</script>

<div class="">
  <div class="banner_img">
    <div class="container">
    <div class="row">
      <div class="col-sm-12">
          <div class="text-prise">
         <h1 class="header-title">Delightful College <br/> <span class=".Student_brk" style="color:erd;"> From the Classroom, to the Community</span></h1>
        <p>CollegeWRK provides simple and affordable help with moving,<br> events, odd jobs, and more jobs help.</p>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>
<div class="pricing_sec">
  <div class="container">
    <div class="row wow fadeInLeft">
      <div class="col-sm-12">
        <h2>WRKer Pricing</h2>
        <p>Get an estimate for your listing right away by selecting the number of WRKers you need and the estimated number of hours required to complete the job, below.</p>
      </div>
    </div>
    <div class="row">
      <!-- <div class="col-sm-4 wow fadeInLeftBig">
        <form>
          <div class="form-group">
            <label for="sel1">I need <span class="label_sec">(Delightful College Students)</span></label>
            <select class="form-control" id="sel1">
              <option>Brounce</option>
              <option>Silver</option>
              <option>Gold</option>
            </select>
          </div>
        </form>
      </div> -->
      <div class="col-sm-6 wow fadeInLeft">
        <form>
          <div class="form-group">
            <label for="sel1">How many <span class="label_sec">WRKers do you need?</span></label>
            <select class="form-control" id="sel1">
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
              <option>6</option>
              <option>7</option>
              <option>8</option>
              <option>9</option>
              <option>10</option>
            </select>
          </div>
        </form>
      </div>
      <div class="col-sm-6 wow fadeInRight">
        <form>
          <div class="form-group">
            <label for="sel1">Estimate <span class="label_sec">of Total Hours?</span></label>
            <select class="form-control" id="sel1">
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
              <option>6</option>
              <option>7</option>
              <option>8</option>
              <option>9</option>
              <option>10</option>
            </select>
          </div>
        </form>
      </div>
    </div>
    <div class="row"> 
      <div class="col-sm-12">
        <h3 class=" wow fadeInLeft">We can help, <span style="color:#1aa4df;font-weight:700">starting at $35</span></h3>
        @if(Auth::check())
        <div class="cen_btn wow fadeInLeft"><a href="#" class="blue_btn" data-toggle="modal" data-target="#demo-modal-4">BEGIN BOOKING FOR SPECIFIC ESTIMATES</a></div>
        @else
        <div class="cen_btn wow fadeInLeft"><a href="#" class="blue_btn" data-toggle="modal" data-target="#demo-modal-3">BEGIN BOOKING FOR SPECIFIC ESTIMATES</a></div>
        @endif
      </div>
    </div>
  </div>
</div>
<div class="Pricing_Cmpnt">
  <div class="container">
  <div class="row wow fadeInRight">
        <div class="col-sm-12">
          <h2>Pricing Component</h2>
         <p class="p-text"></p> 
        </div>
      </div>
      <div class="row same_flex">
        <div class="gray_box col-sm-3 wow fadeInLeft">
          <div class="">
            <img class="img_sec" src="{{url('public')}}/images/hourly_rate.png">
              <h6>Hourly Rate</h6>
              <p>While most jobs begin at $35/hour, this rate may vary by category of job. Browse Our Services to see minimum pricing in selected categories, or begin booking to get a specific price estimate. Don’t worry, you won’t be charged until you confirm your job listing.</p>
          </div>
        </div>
        <div class="gray_box col-sm-3 wow fadeInLeft">
          <div class="">
            <img class="img_sec" src="{{url('public')}}/images/travel_charge.png">
              <h6>Travel Charge</h6>
              <p>You may incur travel charges if your WRKer is required to travel more than  miles from their campus. See if a fee will apply to your listing when you begin booking. 
</p>
          </div>
        </div>
        <div class="gray_box col-sm-3 wow fadeInLeft">
          <div class="">
            <img class="img_sec" src="{{url('public')}}/images/equipment.png">
              <h6>Equipment</h6>
              <p>Your WRKer will not bring equipment unless you specify it in the listing as a requirement for accepting the job. Uniforms, tools, or safetywear will not be provided by your WRKer. If you need specialized equipment or machinery, we suggest renting through outside companies such as The Home Depot or Lowes Hardware.</p>
          </div>
        </div>
        <div class="gray_box col-sm-3 wow fadeInRight">
          <div class="">
            <img class="img_sec" src="{{url('public')}}/images/expenditures.png">
              <h6>Expenditures</h6>
              <p>You must provide WRKers with money for any on-the-job expenses they incur at your direction or with your approval. Such expenses could include purchasing additional supplies or tools as needed. WRKers will not be expected to pay out-of-pocket for such expenses.</p>
          </div>
        </div>
      </div>
  </div>
</div>
<div class="paymt_faq">
  <div class="container">
    <div class="row wow fadeInLeft">
      <div class="col-sm-12">
        <h2>Payment FAQs</h2>
        <p class="p_text">Have questions? See the drop-down boxes below for answers to your most common concerns. If you can’t find what you’re looking for, ask us by phone, email, or chat by visiting our home page for contact info.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default wow fadeInLeft">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="accordion_text_icon_section">
                  <i class="fa fa-plus" aria-hidden="true"></i>
                  One Hour Minimum
                </a>
              </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
               The minimum hours billed for any job posting is 1 (one) hour. After the first hour, the rate is charged to the nearest 15 minutes. You will have a chance to review time sheets and your invoice once the job is complete to request changes or adjustments if necessary.
              </div>
            </div>
          </div>
          <div class="panel panel-default wow fadeInLeft">
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title">
                <a class="collapsed accordion_text_icon_section" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  <i class="fa fa-plus" aria-hidden="true"></i>
                  Cancellation Policy
                </a>
              </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body">
                Cancellation of an accepted job posting within 24 hours of the start time will prevent any charges from incurring for WRKer performance. Cancellations within 24 hours of an accepted job’s start time will incur a cancellation fee equal to the rate of one hour for the selected job category. This policy applies even where a job listing is for only one hour. Additional expenses such as for travel will not be charged for canceled listings at any time. 
              </div>
            </div>
          </div>
          <div class="panel panel-default wow fadeInLeft">
            <div class="panel-heading" role="tab" id="headingThree">
              <h4 class="panel-title">
                <a class="collapsed accordion_text_icon_section" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  <i class="fa fa-plus" aria-hidden="true"></i>
                  Sweeps Gives
                </a>
              </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
              <div class="panel-body">
                Sweeps has a one hour minimum charge per Sweeper. Beyond that the rate is charged to the nearest 15 minutes. You will have a chance to review time sheets and your invoice once the job is completed
              </div>
            </div>
          </div>
    </div>
</div>

<!--       <div class="col-sm-6">
        
        <div class="accodin_sec wow fadeInRightBig">
          <div class="row" style="display: flex;">
            <div class="col-sm-1">
              <p class="pluse_sin">+</p>
            </div>
            <div class="col-sm-11">
              <h6 class="acc_title">One Hour Minimum</h6>
              <p id="div1">Sweeps has a one hour minimum charge per Sweeper. Beyond that the rate is charged to the nearest 15 minutes. You will have a chance to review time sheets and your invoice once the job is complete</p>
            </div>
          </div>
        </div>
        
        <div class="accodin_sec wow fadeInleftBig">
          <div class="row" style="display: flex;">
            <div class="col-sm-1">
                <p class="pluse_sin">+</p>
            </div>
            <div class="col-sm-11">
              <h6 class="acc_title1">Cancellation Policy</h6>
              <p id="div2">Sweeps has a one hour minimum charge per Sweeper. Beyond that the rate is charged to the nearest 15 minutes. You will have a chance to review time sheets and your invoice once the job is completed</p>
            </div>
          </div>  
        </div>  

        <div class="accodin_sec wow fadeInleftBig">
          <div class="row" style="display: flex;">
            <div class="col-sm-1">
                <p class="pluse_sin">+</p>
            </div>
            <div class="col-sm-11">
              <h6 class="acc_title2">Sweeps Gives</h6>
              <p id="div3">Sweeps has a one hour minimum charge per Sweeper. Beyond that the rate is charged to the nearest 15 minutes. You will have a chance to review time sheets and your invoice once the job is completed</p>
            </div>
        </div>
      </div>
</div> -->
<div class="col-md-6">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default wow fadeInRight">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseOne" class="accordion_text_icon_section">
                  <i class="fa fa-plus" aria-hidden="true"></i>
                  Credit Card Requirement
                </a>
              </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                CollegeWRK requires you to put in credit card information to post a job. This is both for safety reasons as well as to ensure payment for completed jobs and for any cancellation fees incurred within 24 hours of the cancelled job’s scheduled start time. We do not store any credit card information, and all transactions are managed by a secure third-party. 
              </div>
            </div>
          </div>
          <div class="panel panel-default wow fadeInRight">
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title">
                <a class="collapsed accordion_text_icon_section" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseTwo">
                  <i class="fa fa-plus" aria-hidden="true"></i>
                  Easy, Cashless and Secure Payment
                </a>
              </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body">
               Payment is not to be given directly to WRKers. Please make all payments through your easy, secure, cashless payment portal on your CollegeWRK dashboard. CollegeWRK accepts payment from most major credit card providers. All transactions are managed by a secure third party.
              </div>
            </div>
          </div>
          <div class="panel panel-default wow fadeInRight">
            <div class="panel-heading" role="tab" id="headingThree">
              <h4 class="panel-title">
                <a class="collapsed accordion_text_icon_section" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseThree">
                  <i class="fa fa-plus" aria-hidden="true"></i>
                  Gratuity
                </a>
              </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
              <div class="panel-body">
                Gratuities for your WRKers’ exceptional service and hard work are accepted, but optional. While we at CollegeWRK strive to give college students livable wages and fair pay, gratuity is always greatly appreciated and helps keep our WRKers happy and motivated. Please consider tipping your CollegeWRKer if you feel they have given you outstanding service, but know that such payments are not required
              </div>
            </div>
          </div>
    </div>
</div>
<!--       <div class="col-sm-6">
        <div class="accodin_sec wow fadeInRightBig">
          <div class="row" style="display: flex;">
            <div class="col-sm-1">
                <p class="pluse_sin">+</p>
            </div>
            <div class="col-sm-11">
              <h6 class="acc_title3">Credit Card Requirement</h6>
              <p id="div4">Sweeps requires a credit card to post a job. We do this for safety reasons and to ensure payment. Credit card information is not stored on our servers and is managed by a secure third-part</p>
            </div>
        </div>
      </div>

        <div class="accodin_sec wow fadeInleftBig">
          <div class="row" style="display: flex;">
            <div class="col-sm-1">
                <p class="pluse_sin">+</p>
            </div>
            <div class="col-sm-11">
              <h6 class="acc_title4">Easy, Cashless and Secure Payment</h6>
              <p id="div5">Sweeps requires a credit card to post a job. We do this for safety reasons and to ensure payment. Credit card information is not stored on our servers and is managed by a secure third-part</p>
            </div>
          </div>
        </div>

        <div class="accodin_sec wow fadeInRightBig">
          <div class="row" style="display: flex;">
            <div class="col-sm-1">
                <p class="pluse_sin">+</p>
            </div>
            <div class="col-sm-11">
              <h6 class="acc_title5">Gratuity</h6>
              <p id="div6">Sweeps has a one hour minimum charge per Sweeper. Beyond that the rate is charged to the nearest 15 minutes. You will have a chance to review time sheets and your invoice once the job is complete</p>
            </div>
          </div>
        </div>   
      </div> -->
    </div>
  </div>
</div>
</div>
<div class="address_sec">
  <div class="container">
    <div class="row">
      <?php
            $bussiness_info = DB::table('bussinesssetting')
                                ->get();

          ?>
      @foreach($bussiness_info as $bussiness_infos)
      <?php
           $bussiness_phone_number = $bussiness_infos->phone;
           $bussiness_email = $bussiness_infos->emails;
           $bussiness_address = $bussiness_infos->address;
      ?>
      <div class="col-sm-6 wow fadeInLeft">
        <div class="pay_mathed1">
          <div class="media pay_pal">
              <div class="media-left">
                <i class="fa fa-phone" aria-hidden="true"></i>
              </div>
              <div class="media-body">
                <h4 class="media-heading">Make A APPOINMENT</h4>
                <p><a href="tel:{{$bussiness_phone_number}}">{{$bussiness_phone_number}}</a></p>
              </div>
            </div>
            
          </div>
        </div>
      
      <div class="col-sm-6">
        <div class="pay_mathed1 wow fadeInRight">
          <div class="media pay_pal">
              <div class="media-left">
                <i class="fa fa-envelope" aria-hidden="true"></i>
              </div>
              <div class="media-body">
                <h4 class="media-heading">SEND USE MESAGE</h4>
                <p><a href="mailto:{{$bussiness_email}}">{{$bussiness_email}}</a></p>
              </div>
            </div>
            
          </div>
        </div>
     
      <!-- <div class="col-sm-4 wow fadeInRightBig">
        <div class="pay_mathed1">
          <div class="media pay_pal">
              <div class="media-left">
                <i class="fa fa-home"></i>
              </div>
              <div class="media-body">
                <h4 class="media-heading">VISIT US AT ADDRESS</h4>
                <p>{{$bussiness_address}}</p>
              </div>
          </div>
            
        </div>
      </div> -->
      @endforeach
    </div>
  </div>
</div>
<script>
    wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100,
        callback:     function(box) { 
          console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
      }
    );
    wow.init();
    document.getElementById('moar').onclick = function() {
      var section = document.createElement('section');
      section.className = 'section--purple wow fadeInDown';
      this.parentNode.insertBefore(section, this);
    };
 </script>
 
<style>
      one {
              border-right: .05em solid;
              animation: caret 1s steps(1) infinite;
             margin-left: 15px;
            }

            @keyframes caret {
              50% {
                border-color: transparent;
              }
            }
.banner_img h2.Student_brk {
    font-size: 70px;
    font-weight: 700;
    font-family: "proxima-bold";
    display: block;
    color: #ff8638;
}
.accordion_text_icon_section {
  font-size: 16px !important;
  font-weight: bold;
  font-family: "proxima-semibold";
}
</style>
@stop